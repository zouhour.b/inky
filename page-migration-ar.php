<?php if (isset($_GET['reset'])){
/*
	$posts = get_posts(array('numberposts'=>-1));

	foreach ($posts as $p){
		wp_set_object_terms( $p->ID, array('Not Ready'), 'migration-status', false );
		echo 'updating '.$p->ID.'  migration status...<br/>';
		if (!is_arabic($p->post_title)){
			pll_set_post_language($p->ID, 'fr');
		}

	}*/



	
include get_template_directory().'/inc/migration.class.php';


	$migrator = new InkyfadaMigration();
	
$query = $wpdb->get_results("SELECT * FROM `wp_terms` as t left join wp_term_taxonomy as tt on tt.term_id=t.term_id where tt.taxonomy='genre'");


	foreach ($query as $term) {
		$migrator->migrateTerm($term);
	}
	die();
}
global $post;

$posts = get_posts(array('numberposts'=>-1, 'offset'=>20, 'migration-status'=>'not-ready')); 
$readyposts = get_posts(array('numberposts'=>-1, 'offset'=>20, 'migration-status'=>'ready')); 




get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main row" role="main">
		<div class="col-md-6 " style="margin-top: 100px">
		<h2>Not Ready <span class="badge"><?php echo count($posts) ?></span></h2>
			<ul>
				<?php foreach ($posts as $post):setup_postdata($post); ?>

					<li><a  target="_blank" class="" href="<?php the_permalink(  ); ?>"><?php the_title() ?></a></li>
				<?php endforeach; ?> 
			</ul>
		</div>

		<div class="col-md-6" style="margin-top: 100px">
		<h2 class="pt-0">Ready <span class="badge"><?php echo count($readyposts) ?></span></h2>
			<ul>
				<?php foreach ($readyposts as $post):setup_postdata($post); ?>

					<li><a class="" href="<?php the_permalink(  ); ?>"><?php the_title() ?></a></li>
				<?php endforeach; ?> 
			</ul>
		</div>
	</main>
</div>
<?php get_footer() ?>