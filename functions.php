<?php

//require_once 'migration/fixes.php';
//require_once 'migration/backward-compat.php';

if ( ! function_exists( 'inkyfada_setup' ) ) :

	/* Unregister wordpress default widgets */
	function unregister_default_widgets() {
		unregister_widget('WP_Widget_Pages');
		unregister_widget('WP_Widget_Calendar');
		unregister_widget('PLL_Widget_Calendar');
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_Links');
		unregister_widget('WP_Widget_Meta');
		unregister_widget('WP_Widget_Search');
	//unregister_widget('WP_Widget_Text');
		unregister_widget('WP_Widget_Categories');
		unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_Recent_Comments');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Tag_Cloud');
	//unregister_widget('WP_Nav_Menu_Widget');
	//unregister_widget('WP_Widget')
	}
	add_action('widgets_init', 'unregister_default_widgets', 0);

	function inkyfada_setup() {
		load_theme_textdomain( 'inkyfada', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support('post-formats', array('gallery', 'status', 'aside', 'image'));

		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'inkyfada' ),
			'secondary' => esc_html__( 'Secondary', 'inkyfada' ),
			'footer' => esc_html__( 'Footer Menu', 'inkyfada' ),
			'podcast' => esc_html__( 'podcast Menu', 'inkyfada' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'gallery',
			'caption',
			'custom-fields'
		) );

		add_theme_support( 'customize-selective-refresh-widgets' );
	}

endif;
add_action( 'after_setup_theme', 'inkyfada_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function inkyfada_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'inkyfada_content_width', 640 );
}
add_action( 'after_setup_theme', 'inkyfada_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

/**
 * Enqueue scripts and styles.
 */
function inkyfada_scripts() {
	wp_register_script('skrollr', get_template_directory_uri() . '/assets/js/skrollr.min.js');
	wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js');
	wp_register_script('scroll-reveal', get_template_directory_uri() . '/assets/js/scroll-reveal.js');
	wp_enqueue_script('scroll-reveal');

	wp_register_script( 'scroll-listener', get_template_directory_uri().'/assets/js/scroll-listener.js');

	wp_enqueue_style('fontawesome');

	wp_enqueue_style('droid-arabic-naskh', WP_CONTENT_URL.'/uploads/fonts/DroidArabicNaskh/DroidArabicNaskh.css');
	wp_enqueue_style('tanseek', WP_CONTENT_URL.'/uploads/fonts/Tanseek/Tanseek.css');

	if (is_rtl()){
		wp_enqueue_style( 'inkyfada-main-rtl', get_template_directory_uri() . '/assets/css/main.rtl.css') ;
	} else {
		wp_enqueue_style( 'inkyfada-main', get_template_directory_uri() . '/assets/css/main.css' );
	}
    wp_enqueue_style( 'houssem-fixes', get_template_directory_uri() . '/assets/css/fixes.css' );

	wp_enqueue_script( 'inkyfada-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'inkyfada-main', get_template_directory_uri() . '/assets/js/main.js', array('inkube-front',  'skrollr', 'wp-api', 'scroll-reveal'));
}
add_action( 'wp_enqueue_scripts', 'inkyfada_scripts' );

/**
 * Custom functions that act independently of the theme templates.
*/
require get_template_directory() . '/app/app.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/images.php';

 /**
  * Les Modification de Houssem
  */
require get_template_directory() . '/inc/fixes.php';


add_shortcode('parsedBlocks', function($a,$c){
	$r = new WP_Block_Parser();
	$c = '<!-- wp:div {"className":"row"} --><div class="row">'.$c.'xxxx<a class="ok"></a></div><!-- /wp:div -->';
	return $c.json_encode($r->parse($c), JSON_PRETTY_PRINT);
});



add_filter('inkube.haythem', function($var, $a=""){
	return $var.' minutes';
});