<?php
$p = isset($_GET['cid'])?(int)$_GET['cid']:0;
$post = get_post($p);
if (!$p || get_post_type($p) != "component"){
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<title>Inkyfada | <?php echo $post->post_title ?></title>
	<link rel='dns-prefetch' href='//s.w.org' />
	<link rel="alternate" type="application/rss+xml" title="Inkyfada &raquo; Flux" href="http://inkyfada.dev/fr/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Inkyfada &raquo; Flux des commentaires" href="http://inkyfada.dev/fr/comments/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Inkyfada &raquo; Home officielle Flux des commentaires" href="http://inkyfada.dev/fr/home-7-july/feed/" />
	<link rel='stylesheet' id='fa-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.8' type='text/css' media='all' />
	<link rel='stylesheet' id='inkyfada-main-css'  href='http://inkyfada.dev/wp-content/themes/inkyfada/assets/css/main.css?ver=4.8' type='text/css' media='all' />
	<script type='text/javascript' src='http://inkyfada.dev/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
	<script type='text/javascript' src='http://inkyfada.dev/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
	<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js?ver=4.8'></script>
	<script type='text/javascript' src='http://inkyfada.dev/wp-content/themes/inkyfada/assets/js/bootstrap.min.js?ver=4.8'></script>
	<script type='text/javascript' src='http://inkyfada.dev/wp-content/themes/inkyfada/assets/js/main.js?ver=4.8'></script>
	<link rel='https://api.w.org/' href='http://inkyfada.dev/wp-json/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://inkyfada.dev/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://inkyfada.dev/wp-includes/wlwmanifest.xml" /> 
	<meta name="generator" content="WordPress 4.8" />
	<link rel="canonical" href="http://inkyfada.dev/fr/" />
	<style>
		.component{
			padding: 0;
		
		}
	</style>
</head>
<body>
	<?php echo do_shortcode('[components id="'.$post->ID.'"]' , false ); ?>
</body>
</html>