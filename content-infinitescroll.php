<?php
 global $wp_query;
?>
<div class="col-12 mt-4 mb-4 text-center">
  <a id="loadMore" href="javascript:void(0);">
      <h4 class="col-md-6 p-2 offset-md-3 bg-gray-blue uppercase"><?php _e('Afficher plus de contenu', 'inkyfada'); ?></h4> 
  </a>
</div>
<script type="text/javascript">
  var count = 2;
  var total = <?php echo $wp_query->max_num_pages; ?>;
  function loadPosts(pageNumber) {
      jQuery.ajax({
          url: "/wp-admin/admin-ajax.php",
          type:'POST',
          data: "action=infinite_scroll&page_no="+ pageNumber + '&loop_file=content-loop&<?php echo http_build_query(array('query' => $wp_query->query_vars)) ?>', 
          success: function(html){
              jQuery(".infinite-scroll-content").append(html);    // This will be the div where our content will be loaded
              jQuery( document ).trigger( "infiniteScrollLoaded");
              count++;
              if (count > total){
                jQuery('#loadMore').hide();
              } 
          }
      });
      return false;
  }
  jQuery( document ).ready(function() {
    jQuery('#loadMore').on('click', function() {
      loadPosts(count);
    })
  });
</script>