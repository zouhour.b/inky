<?php 
function get_image_id_by_url($attachment_path){
	global $wpdb;
	$query = $wpdb->prepare(
		"SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND guid LIKE %s",
		'%' . basename($attachment_path).'%'
		);

	$attachment = $wpdb->get_col( $query );

	if ( is_array( $attachment ) && ! empty( $attachment ) ) {
		return (int)array_shift( $attachment );
	}
	else  return 'notFound';
}
function get_images_id_by_url( $url ){
	

	$ap = explode(',',$url);
	$ids = array();
	foreach ($ap as $path){
		$id = get_image_id_by_url($path);
		$ids[] = $id;
		echo "<br/>$id =>".wp_get_attachment_image_url( $id, 'full' )."<br/>";
	}

	return implode(',', $ids);
}

$imgurl = isset($_POST['imgurl']) ? $_POST['imgurl']:"" ?>

get_header(); ?>
<link rel="stylesheet" href="<?php get_template_directory_uri() ?>/assets/css/main.css" /> 
<div id="primary" class="content-area">
	<main id="main" class="site-main row mt-5"  style="margin-top: 100px" role="main">
		<form class="mt-5" action="" method="POST">
			<label>Image URL</label>
			<div class="input-group">
				<input type="text" name="imgurl" class="form-control" style="width:700px" value="<?php echo $imgurl ?>" />
			</div>
			<button type="submit" class="btn btn-primary">Send</button>
		</form>

		<div class="response" >
			<?php if ($imgurl): ?>
				<?php $id = get_images_id_by_url($imgurl) ?>
				<input type="form-control" value="<?php echo $id; ?>" />
			<?php endif; ?>
		</div>
	</main>
</div>
<?php get_footer() ?>