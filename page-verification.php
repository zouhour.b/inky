<?php 
get_header();
$posts = get_posts(array('numberposts'=>-1));
$n = count($posts);
$users = array( 'Kais', 'malek','monia', 'Chayma', 'Aymen', 'Haifa','Hortense','Walid' , 'marouen' );

$m = count($users);
$i=$j=$k=0;
?>

<div class="col-md-8 offset-md-2 " style="margin-top: 200px;">
	<ul class="list-inline">
		<?php foreach ($posts as $p): ?>
			<?php if ($k >= ($n/$m)): ?>
				<?php $k = 0;$j++; ?>
			<?php endif; ?>
			<?php if ($k==0): ?>
				<li class="mt-5"><h3><?php echo $users[$j]; ?></h3></li>
			<?php endif; ?>
			<?php $k++; ?>
			<li ><?php echo $k ?>. <a href="<?php echo get_post_permalink($p->ID); ?>"><?php echo $p->post_title ?></a></li>
			<?php $i++; ?>
		<?php endforeach; ?>
	</ul>
</div>

<?php get_footer() ?>