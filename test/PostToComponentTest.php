<?php 
global $InkubeTests;
global $postToComponentData;
PostComponentRelation::$data['post_to_component']=[];
PostComponentRelation::$data['component_to_post']=[];
class PostComponentRelation{
	public static $data =[
		'post_to_component'=>[],
		'component_to_post'=>[],
		"current_post"=>null
	];

}
$InkubeTests->register('post-component-relation', 'Lien entre composants et articles', function(){

	$posts = get_posts(['numberposts'=>-1, 'lang'=>'ar,fr']);
	$i =0;
	add_filter('shortcode_html', function($html, $shortcode, $atts, $content){
		
		
		if ($shortcode->tag !='components'){
			return $html;
		}
		$id = $atts['id']??null;
		if (!$id){
			return $html;
		}
		$p = PostComponentRelation::$data['current_post'];
		$value = PostComponentRelation::$data['post_to_component'][$p->ID]??[];
		$value[] = $id;
		PostComponentRelation::$data['post_to_component'][$p->ID] = $value; 
		$value = PostComponentRelation::$data['component_to_post'][$id]??[];
		$value[] = $p->ID;
		PostComponentRelation::$data['component_to_post'][$id] = $value;

	},4,4);
	foreach ($posts as $p){
		global $postToComponentData;
		PostComponentRelation::$data['current_post'] = $p;
		try{
			$c = do_shortcode($p->post_content);
		} catch(Exception $e){
			echo sprintf('<div class="alert alert-danger"><h4><a href="%s" target="_blank">%s</a></h4>%s</div>',get_post_permalink($p->ID), $p->post_title, $e->getMessage());
		}
	}
	function renderPostLinks($ids){
		$out = '';
		foreach ($ids as $id){
			$p = get_post($id);
			$out .= sprintf('<div class="row mx-0 mb-2">
				<span class="fw-700 fs-16px h4 col-2">%s</span>
				<a class="col-10" href="%s" target="_blank">%s</a></span></div">
				</div>', $p->ID, get_post_permalink($p->ID), $p->post_title);
		}
		return $out;
	}
	echo sprintf('<div class="row ff-akrobat p-0 lh-150 mx-0 fs-20px fw-400 uppercase mb-1">
		<div class="col-1 text-right p-2 ">#</div>
		<div class="col-5 p-2">%s</div>
		<div class="col-2 p-2">%s</div>
		<div class="col-4 p-2">%s</div>

		</div>', 'component', 'Type/status', 'Attached to');
	
	$components = get_posts(['lang'=>'', 'numberposts'=>-1, 'post_type'=>'component', 'post_status'=>'any']);
	$t = $i = $e = $w = $s = 0;
	foreach ($components as $index=>$c){
		$attached = PostComponentRelation::$data['component_to_post'][$c->ID]??[];
		update_post_meta($c->ID, 'parent_posts', $attached);
		$status = 'bg-success';
		$type = wp_get_post_terms($c->ID, 'component-type');
		$type = $type[0]->name??'';
		$t++;
		if (count($attached)==0){
			$status = 'bg-red color-white';
			wp_update_post(['ID'=>$c->ID, 'post_status'=>'trash']);
			$e++;
		}
		if ($attached && $c->post_status!='publish') {
			$status = 'bg-warning color-dark';
			wp_update_post(['ID'=>$c->ID, 'post_status'=>'publish']);

			$w++;
		}
		if (count($attached)>1){
			$status = 'bg-info color-white';
			$i ++;
		}
		echo sprintf('<div class="row %s ff-akrobat p-0 lh-100 mx-0 mb-1">
			<div class="col-1 bg-white text-right p-2  m-0 uppercase fw-400 color-black">%s.</div>
			<div class="col-5 p-2 uppercase fw-400 fs-18px">%s</div>
			<div class="col-2 p-2"><h4 class="m-0 p-0 fs-16px">%s</h4><div>%s</div></div>
			<div class="col-4 p-2">%s</div>
			</div>', $status,$index+1, renderPostLinks([$c->ID]), $type, $c->post_status, renderPostLinks($attached));
		
	}
	echo sprintf('<div class="col-10 mx-auto fs-22px lh-150 my-5 ff-akrobat fw-400">
		<div><b class="label px-3  bg-red color-white">%s</b> unused component</div>
		<div><b class="label px-3  bg-warning color-black">%s</b> component are used but their status is not "publish" </div>
		<div><b class="label px-3  bg-info color-white">%s</b> components are attached to more than 1 post</div>
		<br/><b>%s</b> Component total</div>', $e, $w, $i, $t);
	$index =1;
	echo sprintf('<div class="row ff-akrobat p-0 lh-150 mx-0 fs-20px fw-400 uppercase mb-1">
		<div class="col-1 text-right p-2 ">#</div>
		<div class="col-5 p-2">%s</div>
		<div class="col-5 p-2">%s</div>

		</div>', 'Article', 'Attached Components');
	
	foreach (PostComponentRelation::$data['post_to_component'] as $pid=>$cids){
		echo sprintf('<div class="row mb-1 bg-black color-white p-0 m-0">
			<div class="col-1 bg-white p-2 h5 m-0 color-black">%s.</div>
			<div class="col-5 p-2 h5 fs-20px">%s</div>
			<div class="col-4 fs-16px p-2 h5 fw-400 lowercase">%s</div>
			</div>',$index, renderPostLinks([$pid]), renderPostLinks($cids) );
		$index++;
	}

});