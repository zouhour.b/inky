<?php 
global $InkubeTests;
$InkubeTests->register('hasScripts', 'Test de migration des scripts', function(){
	$posts = get_posts(['numberposts'=>-1]);
	$ps = [];
	foreach ($posts as $p){
		$id = $p->ID;
				$ps[$id] = false;


		foreach (['wpcf-scripts', 'wpcf-styles', 'wpcf-footer'] as $key){

			$scripts = get_post_meta($id, $key,true);
			$scripts = trim($scripts);
			if ($scripts){
				$ps[$id] = true;
				echo sprintf('<article id="post-%s" class="bg-%s mb-2 h5 p-2">%s : %s<br/><textarea class="w-100">%s</textarea></article>',$id, 
					$key=="wpcf-scripts"?'danger color-white':'warning',
					$p->post_title,  $key, $scripts);

			}
		}
		$i=0;

	}
	foreach ($ps as $k=>$v){
			if ($v) $i++;
		}
		echo sprintf('<div class="h4">%s posts</div>', $i);

});