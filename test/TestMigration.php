<?php 
global $InkubeTests;

$InkubeTests->register('hasBrokenLinks', 'Test des liens internes cassés', function(){
	$posts = get_posts(['numberposts'=>-1, 'lang'=>'ar,fr']);
	$i =0;
	foreach ($posts as $p){
		$post_errors = [];
		$scripts = get_post_meta($p->ID, 'wpcf-scripts', true);
		$styles = get_post_meta($p->ID, 'wpcf-styles', true);
		$footer = get_post_meta($p->ID, 'wpcf-footer', true);
		

		if (strpos($p->post_content, 'dev.inkyfada.com')!==false){
			$post_errors[] = 'le contenu contient un lien de vers dev.inkyfada.com';
		}
		if (strpos($styles, 'dev.inkyfada.com')!==false){
			$post_errors[] = 'le custom style contient un lien de vers dev.inkyfada.com';
		}
		if (strpos($scripts, 'dev.inkyfada.com')!==false){
			$post_errors[] = 'le custom script contient un lien de vers dev.inkyfada.com';
		}
		
		if (strpos($footer, 'dev.inkyfada.com')!==false){
			$post_errors[] = 'le custom footer contient un lien de vers dev.inkyfada.com';
		}

		if (strpos($styles, 'themes/inky')!==false){
			$post_errors[] = 'le custom style contient un lien de vers le theme ancien';
		}
		if (strpos($footer, 'themes/inky')!==false){
			$post_errors[] = 'le custom footer contient un lien de vers le theme ancien';
		}


		if (strpos($scripts, 'themes/inky')!==false){
			$post_errors[] = 'le custom script contient un lien de vers le theme ancien';
		}

		if (strpos($p->post_content, '<style')!==false){
			$post_errors[] = 'le contenu contient une balise < style >';
		}
		if (strpos($p->post_content, 'themes/inky')!==false){
			$post_errors[] = 'le contenu contient le theme ancien';
		}
		if (strpos($p->post_content, '<script')!==false){
			$post_errors[] = 'le contenu contient une balise < script >';
		}
		if (strpos($p->post_content, '[caption')!==false){
			$post_errors[] = 'le contenu contient encore une [ caption ]';
		}

		if ($post_errors){
			$i++;
			$s = '';
			foreach ($post_errors as $e){
				$s .=  sprintf('<li>%s</li>',$e);
			}
			echo sprintf('<div class="alert alert-danger"><h5>%s. <a href="%s" target="_blank">%s</a></h5><ul class="mb-5">%s</ul></div>', $i,get_post_permalink($p->ID), $p->post_title, $s);

			
		}
	}

});