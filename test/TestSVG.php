<?php 
global $InkubeTests;

$InkubeTests->register('hasBrokenSVG', 'Test de migration des SVG', function(){
	$posts = get_posts(['numberposts'=>-1, 'lang'=>'ar,fr']);
	$i =0;
	add_filter('shortcode_html', function($html, $shortcode, $atts, $content) {
		
		if ($shortcode->tag !='components'){
			return $html;
		}
		$id = $atts['id']??null;
		if (!$id) return $html;
		$type = wp_get_object_terms( $id, 'component-type' );
		$is_svg = false;
		foreach ($type as $t){
			if ($t->slug =='svg') $is_svg=true;
		}
		if (!$is_svg){
			return $html;
		}
		$p = $_SESSION['current_post_id'];
		$_SESSION['broken_svg'][$p->ID] = 	$p; 


	},4,4);
	echo '<div class="h1" id="progress-status"></div>';
	foreach ($posts as $p){
		$post_errors = [];
		$_SESSION['current_post_id'] = $p;
		try{
			echo '<script>jQuery("#progress-status").html("ID: '.$p->ID.'");</script>';
			$c = do_shortcode($p->post_content);
		} catch(Exception $e){
			echo sprintf('<div class="alert alert-danger"><h4><a href="%s" target="_blank">%s</a></h4>%s</div>',get_post_permalink($p->ID), $p->post_title, $e->getMessage());
		}
	}
	$s = '';

	foreach ($_SESSION['broken_svg'] as $p){
		$i++;
		$s .=  sprintf('<li><a href="%s" target="_blank">%s</a></li>',get_post_permalink($p->ID), $p->post_title);


	}
	echo sprintf('<div class="alert alert-danger"><h5>%s SVG are in old component format</h5><ul class="mb-5">%s</ul></div>', $i,$s);

});