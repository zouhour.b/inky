<?php 
global $InkubeTests;

$InkubeTests->register('fixGenreMeta', 'Vérification des genres', function(){
	$genres = get_terms(['taxonomy'=>'genre', 'lang'=>'ar,fr', 'hide_empty'=>false]);
	$i =0;
	$row = sprintf('<div class="row mx-0 bg-gray-900 color-white mb-1">
		<div class="col-5 fs-20px bg-%s fw-700 p-2"><a href="%s"target="_blank">%s</a></div>
		<div class="col-1 align-self-center">%s</div>
		<div class="col-1 align-self-center">%s</div>
		<div class="col-2 align-self-center">%s</div>
		</div>
		', '', '#','name','Article number', 'Image', 'color' );
	echo $row;
	foreach ($genres  as $genre){
		$color = get_term_meta($genre->term_id, 'color', true);
		$featuredImage = get_term_meta($genre->term_id, '_thumbnail_id', true);
		$status = 'success';
		$lang = pll_get_term_language($genre->term_id);
		$trlang = $lang=='fr'?'ar':'fr';
		$translationStatus = 'Not translated';
		$translated = pll_get_term($genre->term_id, $trlang );
		if ($translated){
			$tr = get_term_by('id', $translated, 'genre');
			$translatedStatus = sprintf('translation %s : %s', $trlang, $tr->name);
		}
		
		if ($featuredImage){
			$featuredImage = do_shortcode('[image id="'.$featuredImage.'" container_class="w-100"]');
		}
		if (!$color || !$featuredImage || !$translated){
			$status = 'warning';
		}
		if ($lang=='fr' && $translated !== $genre->term_id){
			update_term_meta($translated, '_thumbnail_id', $featuredImage);
			update_term_meta($translated, 'color', $color);
			update_term_meta($translated, 'view_options', [
				'view'=>'@theme/genre/single/default',
				'meta_class'=>'fw-400 fs-24px mb-4 ff-sans-serif',
			]);
			update_term_meta($genre->term_id, 'view_options', [
				'view'=>'@theme/genre/single/default',
				'meta_class'=>'fw-400 fs-18px mb-4 ff-sans-serif'
			]);
		}

		$row = sprintf('<div class="row mx-0 bg-gray-400  mb-1">
			<div class="col-5 fs-20px bg-%s fw-700 p-2">
			<a href="%s"target="_blank">%s</a>
			</div>
			<div class="col-1 align-self-center h4 text-center p-0">%s</div>
			<div class="col-1 p-0">%s</div>
			<div class="col-2" style="background:%s"></div>
			<div class="col-3 align-self-center h5 fs-20px">%s</div>
			</div>
			', $status, get_term_link( $genre ),$genre->name, $genre->count, $featuredImage, $color, $translatedStatus );
		echo $row;
	}

});