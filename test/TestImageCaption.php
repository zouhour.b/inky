<?php 
global $InkubeTests;

$InkubeTests->register('imageCaptions', 'Vérification des captions', function(){
	$posts = get_posts(['numberposts'=>-1, 'post_type'=>'attachment']);
	$hascaption = 0;
	$html = '';
	$hasnocaption = 0;
	foreach ($posts as $p){
		if ($p->post_excerpt){
			$hascaption++;
			$ar  = InkyfadaDBMigration::is_arabic($p->post_excerpt);
			if ($ar) {
				update_post_meta($p->ID, 'caption_ar', $p->post_excerpt);
				} 
				else {
				update_post_meta($p->ID, 'caption_fr', $p->post_excerpt);
			
			}
			$html .= do_shortcode( '[image background="1" image_class="d-block w-100 h-3" container_class="col-md-3 mb-5 " id="'.$p->ID.'"]'.$p->post_excerpt.'[/image]'  );
		}
		else{
			$hasnocaption++;
		}
	}
	echo sprintf('<div class="row">%s</div>', $html);
	echo sprintf('<div class="h2">Caption: %s</div><div class="h2">No Caption: %s</div>', $hascaption, $hasnocaption);

});