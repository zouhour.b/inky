<?php

global $InkubeTests;


$InkubeTests->register('Translations', 'Translations', function(){
	global $Inkube;
	$translator = $Inkube->getContainer()->get('Inkube\Translations\TranslationsModule');
	$translations_fr = [
		'all articles'=>'Tous les articles',
		'newsletter_btn_text_1'=>'Recevez le meilleur de inkyfada',
		'newsletter_btn_text_2'=>'Abonnez vous à notre newsletter'
	];
	$translations_ar = [
		'all articles'=>'كل المقالات',
		'newsletter_btn_text_1'=>'تحصلوا على أفضل اصداراتنا',
		'newsletter_btn_text_2'=>'اشتركوا في رسالتنا الاخبارية',
		
	];
	$translator->saveTranslation('fr', $translations_fr);
	$translator->saveTranslation('ar', $translations_ar);


	$fr = $translator->getTranslations('fr');
	$ar = $translator->getTranslations('ar');
	$data = [];
	foreach ($fr as $k=>$v){
		$data[$k] = ['fr'=>$v];
	}
	foreach ($ar as $k=>$v){
		$d = $data[$k]??[];
		$d['ar']=$v;
		$data[$k]=$d;
	}
	foreach ($data as $k=>$d){
		$ar = $d['ar']??null;
		$fr = $d['fr']??null;
		$status = (is_null($ar) || is_null($fr))?'bg-warning':'bg-success color-white';
		echo sprintf('<div class="row %s fs-20px mb-1"><div class="rtl text-right p-2 col-sm">%s</div><div class="p-2 col-sm">%s</div></div>',$status,  $ar, $fr);
	}

});