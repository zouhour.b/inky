<?php



global $InkubeTests;
$InkubeTests->register('hasBrokenSVGFonts', 'Vérification des fonts dans les SVG', function(){

	global $svgtbl, $currentid;
	$svgtbl = [];
	$currentid  =0;
	

	add_filter('shortcode_html', function($html, $shortcode, $atts, $content) {
		global $svgtbl, $currentid;
		if ($shortcode->tag !='svg'){
			return $html;
		}
		$md = $atts['md']??'';
		$sm = $atts['sm']??'';
		if ($md){
			$v = $svgtbl[$md]??[];
			$v[] = $currentid;
			$svgtbl[$md]=$v;
		}
		if ($sm){
			$v = $svgtbl[$sm]??[];
			$v[] = $currentid;
			$svgtbl[$sm]=$v;
		}
		return $html;
	},4,4);
	
	$posts = get_posts(['s'=>'[svg', 'numberposts'=>-1]);
	foreach ($posts as $p){
		$currentid = $p;
		$c = do_shortcode($p->post_content);
	}

	function findSvgsfromdb(){
		$list = [];
		$svgs = get_posts(['post_type'=>'attachment', 'post_mime_type'=>'image/svg', 'numberposts'=>-1]);

		foreach ($svgs as $svg){
			$path = get_attached_file( $svg->ID);
			$dir = dirname($path);
			$dirname = str_replace(WP_CONTENT_DIR.'/uploads', '', $dir );

			$url = wp_get_attachment_image_url( $svg->ID);
			$filename =basename($path);
			$list[]=[
				'file' =>$filename,
				'dir'=>$dir,
				'dirname'=>$dirname,
				'url'=>$url
			];
		}
		return $list;
	}


	function FindSvgs($dir, $list=[]){
		$files = scandir($dir);
		foreach ($files as $file){
			$path = $dir.'/'.$file;
			if ($file=="." || $file==".." || $file=="fonts" || $file=="font"){
				continue;
			}
			if (is_dir($path) ){
				$list = FindSvgs($path, $list);
			}

			$dirname = str_replace(WP_CONTENT_DIR.'/uploads', '', $dir );

			$ext = pathinfo($path, PATHINFO_EXTENSION);
			if ($ext=='svg'){
				$list[]= [
					'dir'=>$dir,
					'dirname'=>$dirname,
					'file'=>$file,
					'url'=>str_replace(WP_CONTENT_DIR, '/wp-content', $path)
				];				
			}
		}
		return $list;
	}




	$s = '';
	$svgs = findSvgsfromdb();
	echo '<div class="my-4 h5" id="svg-log"></div><input type="checkbox" class="hide-success"/> Show problems only';
	foreach ($svgs as$i=> $svg){
		$att = 'Not Attached';
		$file = $svg['file'];
		if (isset($svgtbl[$file])){
			$att = '';
			foreach ($svgtbl[$file] as $p){
				$att.=sprintf('<a class="d-block" href="%s">%s</a>', '/?p='.$p->ID, substr($p->post_title, 0, 400));
			}
		}
		$s .=  sprintf('
			<div class="w-100 mb-1 pos-relative">
			<div class="w-50 lh-50 p-2" data-svg-url="%s" data-svg-file="%s" data-svg-dir="%s">
			<h5 class="fs-14px lh-100" ><a target="_blank">%s. %s</a>
			</h5>
			<div class="fw-700 h6 fs-14px"><span class="fa fa-link"></span> Attached to<<br/> %s</div>

			<div class="fw-700 h6 fs-14px"><span class="fa fa-folder"></span> <a href="%s" target="_blank"> %s</a></div>
			<hr/><div class="fw-700 h6 fs-14px"><span class="fa fa-font"></span> Fonts</div>
			<div class="font-list">Aucune utilisation de font détectée</div>

			</div>
			<div class="pos-absolute opposite-0 top-0 px-2 w-50 h-auto bg-white" style="z-index:100;"><div class="preview"></div></div>
			</div>',
			$svg['url']??'###',
			$svg['file']??'###',
			$svg['dirname']??'###',
			$i+1,
			$svg['file']??'###',

			$att,
			$svg['url']??'###',
			$svg['dirname']??'###'
		);
	}
	echo $s;
	echo '<script src="'.get_template_directory_uri().'/migration/js/svg.fonts.js"></script>';
	echo '<h1>SVG ATTACHMENTS ERRORS</h1>';
	foreach ($svgtbl as $file=>$posts){
		$found = false;
		foreach ($svgs as $svg){
			if ($svg['file']==$file){
				$found =true;
				continue;
			}

		}

		if (!$found){
			echo sprintf('<div class="alert alert-danger"><a class="d-block h6" href="%s">%s</a>calls the file %s but the file is not found</div>', '/?p='.$posts[0]->ID, $posts[0]->post_title, $file);
		}
	}
});