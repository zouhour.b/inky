<?php 
global $InkubeTests;

$InkubeTests->register('fs-integrity', 'Vérification des uploads', function(){
	function renderActionForm($id){
		return sprintf('<form method="POST"><input type="hidden" name="del" value="%s"/><input type="hidden" name="testname" value="fs-integrity"/><button type="submit">Supprimer</button>', $id);
	}
	function renderAdminLink($id, $str){
		return sprintf('<a target="_blank" href="/wp-admin/post.php?post=%s&action=edit">%s</a>',$id, $str);
	}
	$del = $_POST['del']??null;
	if ($del){
		wp_delete_attachment( $del);
	}
	$posts = get_posts(['numberposts'=>-1, 'post_type'=>'attachment']);
	foreach ($posts as $p){
		$id = $p->ID;
		$url = $p->guid;
		$path = explode('uploads',$url)[1]??null;

		if (!$path){

			echo sprintf('<div class="bg-red color-white"><a href="/wp-admin/post.php?post=%s&action=edit">%s</a> URL invalide : %s</div>',$id, $id,$url);
			continue;
		}
		$filepath = WP_CONTENT_DIR.'/uploads'.$path;
		if (!file_exists(($filepath))){
			if (file_exists('/home/production/www/wp-content/uploads'.$path)||
				file_exists('/home/dev/inkyfada/www/wp-content/uploads'.$path)
			){
				echo sprintf('<div class="p-2 bg-yellow">FILE %s not found but exists</div>', $filepath);

		}else{
			echo sprintf('<div class="mb-1 p-2 bg-red color-white">FILE %s not found<br/>%s</div>', renderAdminLink($id,$filepath), renderActionForm($p->ID));
		}
	}

}

});