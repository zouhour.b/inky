<?php 
get_header();

$icons = [
	'icon-play',
	'icon-pause',
	'icon-download',
	'icon-volume-on',
	'icon-volume-off',
	'icon-subs-off',
	'icon-subs-on',
	'icon-rewind',
	'icon-ff',
	'icon-full-screen',
	'icon-settings-off',
	'icon-settings-on',
	'icon-close',
	'icon-slide-gallery',
	'icon-post-menu',
	'icon-dots',
	'icon-link',
	'icon-share',
	'icon-search',
	'icon-pdf',
	'icon-link-video',
	'icon-menu',
	'icon-ok',
	'icon-play-video',
	'icon-photo',
	'icon-cc',
	'icon-facebook',
	'icon-twitter',
	'icon-insta',
	'icon-pinterest',
	'icon-snapchat',
	'icon-mail',
	'icon-youtube',
	'icon-soundcloud',
	'icon-rss',
	'icon-facebook-2',
	'icon-twitter-2',
	'icon-instagram-2',
	'icon-pinterest-2',
	'icon-snapchat-2',
	'icon-mail-2',
	'icon-youtube-2',
	'icon-soundcloud-2',
	'icon-rss-2',
	'icon-facebook-3',
	'icon-twitter-3',
	'icon-instagram-3',
	'icon-pinterest-3',
	'icon-snapchat-3',
	'icon-mail-3',
	'icon-youtube-3',
	'icon-soundcloud-3',
	'icon-rss-3',
	'icon-information',
	'icon-inkube',
	'icon-inkyfada-fr',
	'icon-inkyfada-ar',
	'icon-facebook-4',
	'icon-twitter-4',
	'icon-law-text',
	'icon-headphones',
	'icon-stouchi',
	'icon-dot',
	'icon-arabe',
	'icon-rss-full',
	'icon-download-full',
	'icon-menu-hamburger',
	'icon-chapitre',
	'icon-rewind-chapitre',
	'icon-ff-chapitre',
	'icon-francais',
	'icon-newsletter',
	'icon-lang-fr',
	'icon-lang-ar',
];
echo '<div class="row">';
foreach ($icons as $icon){
	echo sprintf('<div class="text-center box">
		<span class="fs-70px d-block %s"></span>%s
		</div>', $icon, '');
}
echo '</div>';

?>
<style>
.box{
	height: 200px;
	width:200px;

	line-height: 200px;
}
[class*="icon-"]:before{
	transform: translateY(14%) !important;
}
.box{
	position: relative;
	border: 1px solid #333;
}
.box:before, .box:after{
	content: '';
	border-bottom: 1px solid #333;
	position: absolute;
	top:50%;
	width: 100%;

}
.box:after{
	left: 50%;
	top:0;
	height:100%;
	width: 0;
}
</style>