<?php
		do_action('before_content');
		$object = get_post();
		$view = new PostView($object);
		$view->load_data();
		echo $view->render();	
