<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/foldgutter.css" />
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/lib/codemirror.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/foldcode.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/foldgutter.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/brace-fold.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/xml-fold.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/indent-fold.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/markdown-fold.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/addon/fold/comment-fold.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/mode/javascript/javascript.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/mode/xml/xml.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/mode/css/css.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/lib/util/formatting.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/codemirror/mode/markdown/markdown.js"></script>

<script src="<?php echo get_template_directory_uri() ?>/migration/js/single.js"></script>

<script>
	jQuery(document).ready(function(){
		ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
		migrator = new SingleMigrator(<?php echo get_the_ID() ?>);
	});
</script>
<div class="migration-single">
	<div role="notification">

	</div>
	<div role="node-editor" class="inkube-panel" id="node-editor">
		<a class="button button-head" role="close"><span class="fa fa-close"></span></a>
		<a class="button button-head" role="expand" onclick="jQuery('#node-editor').toggleClass('expanded')"><span class="fa fa-expand"></span></a>
		<a class="button button-head" role="format"><span class="fa fa-cog"></span></a>
		<a class="button update">Change</a>
		<a class="button make-component">Make Component</a>
		<div class="clearfix">
		</div>
		<textarea  class="node">
		</textarea>
	</div>
	<div role="main-panel" class="inkube-main-panel inkube-panel" >
		<a role="close"><span class="fa fa-close"></span></a>
		<div class="actions"></div>
	</div>
	<div class="actions-bar-container inkube-panel" role="post-actions">
		<div class="actions-bar right">
			<a class="button" role="auto_correct">AutoFIX</a>

			<a class="button" role="edit_html">HTML</a>
			<a class="button" role="edit_styles">CSS</a>
			<a class="button" role="edit_scripts">JS</a>
			<select role="migration-status">
				<option class="not-ready" value="not-ready">Not ready</option>
				<option class="ready" value="ready">Ready</option>
				<option class="valid" value="valid">Valid</option>
			</select>
			<a class="button update" id="post-<?php echo get_the_ID(); ?>-save" role="update_post">Save</a>
			<a class="button" href="?backup=revert">Reset</a>
		</div>
		<div class="actions-bar left">
			<div  class="inkube-title" role="post-title"></div>
		</div>

	</div>
</div>