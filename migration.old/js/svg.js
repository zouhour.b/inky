jQuery(document).ready(function(){
    matcher = new SVGMatcher();
    jQuery('.svg-container').each(function(){
        var sel = '#'+jQuery(this).attr('id');
        new SVGMigration({sel:sel});
    });
});
SVGMigration = function(args){
    this.args = args;
    this.sel = this.args.sel;
    this.init();
}

SVGMigration.prototype = {
    init: function(){
        this.$ = jQuery(this.args.sel+ " svg");
        this.findFonts();
        
    },

    findFonts: function(){
        var fonts = [];
        this.$.find('[font-family]').each(function(){
            var _font = jQuery(this).attr('font-family');
            if (_font){
                fonts[fonts.length] = _font;
            }
        });
        function onlyUnique(value, index, self) { 
            return self.indexOf(value) === index;
        }

        this.fonts = fonts.filter( onlyUnique );
        var html = "<h4>Fonts</h4><ul>";
        for (i= 0; i<this.fonts.length; i++ ){
            html += '<li><a>'+this.fonts[i]+'</a></li>';
        }
        html += "</ul>";
        jQuery(this.sel + ' .svg-info .fonts').html(html);

    }
}

SVGMatcher = function(){
    this.isMatching = false;
    this.init();
};
SVGMatcher.prototype = {
    init: function(){
        var M = this;
        jQuery('.svg-matches .btn-save' ).click(function(){
            M.saveMatches();
        });
        jQuery('.svg-matches .list').on('click', '[filename]', function(){
            var filename = jQuery(this).attr('filename');
            if (M.isMatching) {
                M.match(M.isMatching, filename);
                return;
            } 
            jQuery(this).toggleClass('prematch');
            M.isMatching = filename;

        });

    },
    match: function(f1,f2){
        jQuery('.list [filename="'+f1+'"]').slideUp().remove();
        jQuery('.list [filename="'+f2+'"]').slideUp().remove();
        jQuery('.svg-matches .matches').append('<div class="match" one="'+f1+'" two="'+f2+'">'+f1+'<br/>'+f2+'</div>');
        this.isMatching = false;
    },

    saveMatches: function(){
        M = this;
        M.matches = [];
        jQuery('.svg-matches .match').each(function(){
            M.matches[M.matches.length] = {
                f1: jQuery(this).attr('one'), 
                f2: jQuery(this).attr('two') 
            };
        });
        jQuery.post(ajaxurl, {action: 'migrateSVG', data: M.matches}, function(r, s){
            alert(r.data.message);
        });
    }
}
