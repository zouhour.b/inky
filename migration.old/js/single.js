jQuery(document).ready(function(){
	setTimeout(function(){
		jQuery('.inkube-toggle').trigger('click');
	},1000);
});

SingleMigrator = function(postid){
	this.nodeEditor = new NodeEditor();
	var f = this;
	this.active = false;
	this.postid=postid;

	this.panel = {
		$:jQuery('[role="main-panel"]').hide(),
		$actions: jQuery('[role="main-panel"] .actions'),
	}
	this.buttons = {
		enable: function(args){
			this[args].addClass('enabled');
		},
		disable: function(args){
			this[args].removeClass('enable');
		},
		update_post: jQuery('[role="update_post"]'),
		edit_scripts: jQuery('[role="edit_scripts"]'),
		edit_styles: jQuery('[role="edit_styles"]'),
		edit_html:jQuery('[role="edit_html"]'),
		autoCorrect: jQuery('[role="auto_correct"]'),
	}
	this.notification = {
		set: function(status, message){
			var notif =  jQuery('[role="notification"]');
			var cls = status=='loading'?'info':status;
			notif.attr('class', 'alert bg-'+cls).html('<strong>'+message+'</strong>').fadeIn(500);
			if (status !='loading'){
				setTimeout(function(){
					notif.fadeOut(500);
				}, 2500);
			}

		}
	}
	this.init();
}

SingleMigrator.prototype = {
	init: function(){
		var f = this;
		setTimeout(f.detectSVG, 2000);
		jQuery('body').addClass('inkube-on');
		this.mode = 'row-fixer';
		this.loadPost(jQuery('.entry-content'));
		jQuery('#post-'+this.id).addClass('fix-content');
		jQuery('.entry-content').on('click', '> *,blockquote, aside, [shortcode]', function(e){
			var isInteractiveBtn = jQuery(e.originalEvent.target).parents('[data-slide]').length >0;
			isInteractiveBtn = isInteractiveBtn ||  jQuery(e.originalEvent.target).parents('.carousel-indicators').length > 0; 
			isInteractiveBtn = isInteractiveBtn || jQuery(e.originalEvent.target).parents('[target="_blank"]').length>0;
			isInteractiveBtn = isInteractiveBtn || jQuery(e.originalEvent.target).parents('[role="download"]').length>0;
			if (isInteractiveBtn){
				return;
			}
			e.preventDefault();
			e.stopPropagation();
			f.panel.$.css('left', e.clientX).css('top',e.clientY);
			if (jQuery(this).hasClass('row'))
				f.loadRowFixAction(jQuery(this));
			else 
				f.loadRegularAction(jQuery(this));
		});
		jQuery('.entry-content').on('click', 'img, .highlighted, .section-title-ar, .section-text, .section-title, .section-text-ar, > h3, > p, > h2, > h1, > h5, > h4, > blockquote', function(e){
			if (f.mode !="inkube") return;
			if (!f.active) return;
			e.stopPropagation();
			f.panel.$.css('left', e.clientX).css('top',e.clientY);
			f.loadNodeAction(jQuery(this));
			
		});
		jQuery('.inkube-toggle').click(function(){
			
			f.active = !f.active;
			if (f.active){
				jQuery(this).addClass('active');
				jQuery('[role="post-actions"]').show();
			}
			else {
				jQuery(this).removeClass('active');
				jQuery('.inkube-panel').hide();
			}
			jQuery('.inkube-current').removeClass('inkube-current');
		});
		jQuery('.fa-close').click(function(){
			jQuery(this).parent().parent().hide();
		});
		this.buttons.update_post.click(function(){
			if (f.active){
				f.save();
			}
		});
		this.buttons.edit_scripts.click(function(){
			f.nodeEditor.load(jQuery('.entry-scripts'), 'inner');
		});
		this.buttons.edit_html.click(function(){
			f.nodeEditor.load(jQuery('.entry-content'), 'inner');
		});
		this.buttons.edit_styles.click(function(){
			f.nodeEditor.load(jQuery('.entry-styles'), 'inner');
		});
		this.buttons.autoCorrect.click(function(){
			f.autoCorrect();
		})
		this.panel.$.hide();

		this.nodeEditor.$.hide();


	},
	detectSVG: function(){
		jQuery('svg').parent().addClass('svg-parent-detected');
		_scripts = jQuery('.entry-scripts').html();
		re =  /[a-z0-9\-_]+\.svg/g;
		files = [];
		do {
			m = re.exec(_scripts);
			if (m){
				files[files.length] = m[0];
			}
		} while (m);
		

	},

	loadPost($SelectedNode){
		var id = this.postid;
		this.id = parseInt(id);
		jQuery('.inkube-current').removeClass('inkube-current');
		jQuery('#post-'+this.id).addClass('inkube-current');
		jQuery('[role="post-title"]').html('<a target="_blank" href="https://inkyfada.com/?p='+this.postid+'">Original Post</a>');
		this.$content = jQuery('#post-'+this.id+' .entry-content');
		this.$scripts = jQuery('#post-'+this.id+' .entry-scripts');
		this.$styles = jQuery('#post-'+this.id+' .entry-styles');
		if(jQuery('#post-'+this.id).hasClass('migration-status-ready')){
			jQuery('[role="migration-status"]').val('ready');
		}
		this.checkPost();
		if(jQuery('#post-'+this.id).hasClass('migration-status-valid')){
			jQuery('[role="migration-status"]').val('valid');
		}
		return true; 
	},

	checkPost: function(){
		var c =null;
		if (this.$content.hasClass('checked')) return;
		this.$content.addClass('checked');
		if (c = this.$content.find('p[style] img').length){
			alert(c+ 'invisible image');
			this.$content.find('p[style] img').each(function(){
				jQuery(this).parent().css('display', 'block');
				jQuery(window).scrollTop(jQuery(this).parent().offset().top);
				if (confirm('Laisser visible')){
					jQuery(this).parent().attr('id', 'invisible-img');
				}
				else {
					jQuery(this).parent().css('display', 'none');

				}


			});
		}

	},


	isEmptyNode: function($node){
		var nodeHTML = $node.text();
		return (nodeHTML.length < 10);
	},



	autoCorrect: function(){
		var f = this;
		jQuery('.row').each(function(){
			var $node = jQuery(this);
			var structure =  f.parseRow($node);
			if (structure.type=="mixed" || structure.type=="title" ){

				f.run(
				{
					node: $node, 
					action: "convert_"+structure.type, 
					params: structure,
				});
			}

		});
	},

	save: function(){
		var f = this;
		if (!f.id){
			alert('ID = null');
			return;
		}
		this.$content.find('[shortcode]').each(function(){
			var sh = jQuery(this).attr('shortcode');
			jQuery(this).replaceWith(base64_decode(sh));
		});
		this.$content.find('[data-sr-tmp]').each(function(){
			jQuery(this).attr('data-sr', jQuery(this).attr('data-sr-tmp'));
			jQuery(this).removeAttr('data-sr-tmp');
			var el = jQuery(this);
			srAttrs = [ "visibility", "transform", "opacity", "-webkit-transform", "opacity", "-webkit-transition", "transition", "-webkit-perspective", "-webkit-backface-visibility"];
			srAttrs.forEach(function(prop){
				el[0].style[prop]= '';
			});
			if (!el.attr('style')) el.removeAttr('style');
		});
		this.$content.find('[old-reference]').each(function(){
			var oldref = jQuery(this).attr('old-reference');
			jQuery(this).attr('reference', oldref).removeAttr('old-reference');

		});
		this.$content.find('p br').replaceWith(' ');
		var html = this.$content.html().replace(/(?:\r\n|\r|\n)/g, '');
		var content = btoa(unescape(encodeURIComponent(html)));
		var scripts = btoa(unescape(encodeURIComponent(this.$scripts.html())));
		var styles = btoa(unescape(encodeURIComponent(this.$styles.html())));
		var status = jQuery('[role="migration-status"]').val();
		f.notification.set('loading', 'Please Wait...');
		jQuery.post(ajaxurl, {action: "fix_content", content:content, scripts:scripts, styles:styles, status:status, post_id:f.id}, function(response, status){

			if (response.success){
				f.notification.set('success', 'Success: '+response.data.message);
			}
			else {
				f.notification.set('danger', 'Error: '+response.data.message);
			}

		});
	},

	makeComponent: function(args){
		var cls = prompt('Container Class');
		var filename = args.node.find('svg').attr('filename');
		jQuery.post(ajaxurl, {action: "convertToShortcode", filename:filename, class:cls}, function(response, status){
			if (response.success && response.data.shortcode){
				args.node.replaceWith(response.data.shortcode);
			}
			else alert(response.data.message);
		});

	},

	parseRow: function($node){
		var structure = {valid:true, data:[], type:'mixed'};

		if (jQuery('img', $node).length==1 	&& jQuery('[class^="section-capt"]', $node).length == 1 && jQuery('.section-text', $node).length < 2){

			structure = {type:'caption', valid:true, data:[{type:'image+caption'}]};
			return structure;
		}
		if (jQuery('img', $node).length==1 && jQuery($node).text().replace(/\r?\n|\r/g, '').replace(/ /g, '').length < 10){
			structure = {type:'caption', valid:true, data:[{type:'image+caption'}]};
			return structure;
		}

		if (jQuery('svg', $node).length == 1){
			structure = {type:'svg', valid:true, data:[{type:'svg'}]};
			return structure;
		}

		if ($node.find('.section-title, .section-title-ar').length == 1){
			$title = $node.find('h1, h2, h3, h4, h5, h6');
			if ($title.length == 1){
				var tag = $title[0].tagName;
				structure = {type:"title",  valid:true, tag:tag, data:[{type:"title"}]};
				return structure; 
			}
		}
		$node.children().each(function(){
			var hasChildren = false;
			structure.hasSubChildren = true;
			if (jQuery('div', this)){
				hasChildren = true;
			}
			if (jQuery('img', this).length > 0 || jQuery(this)[0].tagName.toUpperCase() == 'IMG') {
				if (jQuery(this).next().length && jQuery(this).next()[0].className.search('caption')>-1){
					structure.data[structure.data.length] = {type:'image+caption'};
					flag = 1;
					return;
				}
				structure.data[structure.data.length] = {type:'image', children:hasChildren};
				return;
			}
			if (jQuery(this).hasClass('post-intro')){
				structure.data[structure.data.length] = {type:'intro', children:hasChildren};
				return;
			}


			if (jQuery(this).hasClass('highlighted') || (jQuery(this).children().length == 1 && jQuery('> .highlighted ', this).length ==1)){
				structure.data[structure.data.length] = {type:'highlighted'};
				return;
			} 
			if ( ( jQuery(this).hasClass('section-text')|| jQuery(this).hasClass('section-text-ar')) && jQuery(this).children('[class]').length == 0){
				structure.data[structure.data.length] = {type:'text'};
				return;
			}
			structure.data[structure.data.length] = {type:"unknown", children:hasChildren};
			structure.valid = false;
			return;
		});
		return structure;
	},

	convert_title: function(args){
		var html='';
		var $title = args.node.find('h1, h2, h3, h4, h5');
		var tag = $title[0].tagName.toLowerCase();
		args.node.replaceWith('<'+tag+'><span class="uppercase">'+$title.html()+'</span></'+tag+">");

	},

	convert_svg: function(args){
		var cls = prompt('Container Class');
		var id = args.node.find('svg').attr('componentid');
		jQuery.post(ajaxurl, {action: "convertToShortcode", id:id, class:cls}, function(response, status){
			if (response.success && response.data.shortcode){
				A = args.node.replaceWith(response.data.rendered);


			}
			else alert(response.data.message);
		});
	},

	convert_mixed: function(args){
		var html = '';
		var errors = false;
		args.node.children().each(function(){


			if (jQuery(this).hasClass('post-intro')){
				html +='<p class="post-intro">'+jQuery(this).html()+'</p>';
				return;
			}

			if(jQuery('> .note', this).length==1){
				var text = jQuery(this).text().replace(/(?:\r\n|\r|\n)/g, '');
				jQuery('> .note a', this).html(text);
				var link = jQuery('> .note a', this)[0].outerHTML;
				html += '<div class="col-12"></div><aside>'+link+'</aside>';
				return;
			}

			if (jQuery(this).hasClass('highlighted') || (jQuery(this).children().length == 1 && jQuery('> .highlighted ', this).length ==1)){
				if (jQuery('> .highlighted', this).length == 1){
					text = jQuery('> .highlighted', this).html();
				}
				else {
					text = jQuery(this).html();
				}
				html +='<blockquote>'+text+'</blockquote>';
				return;
			} 
			if ( ( jQuery(this).hasClass('section-text')|| jQuery(this).hasClass('section-text-ar')) && jQuery(this).children('[class]').length == 0){
				html +="<p>"+jQuery(this).html()+'</p>';
				return;
			}
			errors = true;
			return;
		});
		if (errors){
			alert('error!');
			return;
		}
		args.node.replaceWith(html);


	},

	convert_caption: function(args){
		var url = args.node.find('img').attr('src');
		var $caption = args.node.find('[class^="section-capt"]');
		var text= "";
		if ($caption.length){
			text = $caption.html().replace(/(?:\r\n|\r|\n)/g, '');
		}
		var f = this;
		if(text=="") {
			var cls = prompt('Enter className for container');
			if (!cls) cls = 'col-12';
			var replace = '<div class="'+cls+'">'+args.node.find('img')[0].outerHTML+'</div>';
			args.node.replaceWith(replace);
			return;
		}
		var M = this;


		jQuery.post(ajaxurl, {action: "get_attachment_id", url:url}, function(response, status){
			var cls = prompt('Enter className for container');
			var  _cls = cls?' class="'+cls+'" ':'';

			var replace = '[caption align="alignnone" ';
			if (response.success){
				var id = response.data.id;
				var url = response.data.url;
				var width = response.data.width;
				var height = response.data.height;
				replace +='id="attachment_'+id+'" width="'+width+'" '+_cls+' ]';
				replace +='<img class="wp-image-'+id+' size-full" src="'+url+'" width="'+width+'" height="'+height+'" /> '+text+'[/caption]';

			}
			else{
				replace +=_cls+' width="800"] '+args.node.find('img')[0].outerHTML+ ' '+text+'[/caption]';
			}

			M.nodeEditor.helpers.doShortcode(replace, args.node);






		});

	},
	loadRegularAction: function($node){
		if (!this.loadPost($node)) return;

		this.currentNode = $node;
		if (this.currentNode.attr('shortcode') ){
			actions = [
			{action: 'editShortcode', icon: 'cog', params: '', desc: "Edit Shortcode"},
			];
			var cid = this.currentNode.attr('component-id');
			if (cid ){
				actions[actions.length] = {action: "editComponent", icon: "link", params: cid, desc:"Edit Component"};
			}

		}
		else {
			actions = [
			{action: 'editClasses', icon: 'flag', params: '', desc: "Edit Classes"},
			{action: 'editNode', icon: 'edit', params: '', desc: "Edit HTML"},
			];

		}
		var f = this;
		this.panel.$actions.html('');
		for (var i in actions){
			var text = actions[i].icon?'<span class="fa fa-'+actions[i].icon+'"></span>':actions[i].text;
			this.panel.$actions.append('<a class="button" action="'
				+actions[i].action
				+'" title="'+actions[i].desc+'" params="'+actions[i].params+'">'+text+'</a>');
		}

		this.panel.$actions.find('a[action]').click(function(){
			f.panel.$.hide();
			f.run(
			{
				node:$node, 
				action: jQuery(this).attr('action'), 
				params:jQuery(this).attr('params')
			});
		});
		this.panel.$.show();

	},
	loadRowFixAction: function($node){
		if (!this.loadPost($node)) return;
		this.currentNode = $node;
		var f = this;
		$node.find('p').each(function() {
			var $this = jQuery(this);
			if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
			$this.remove();
		});
		var structure = f.parseRow($node);
		actions = [
		{action: 'editNode', icon: 'edit', params: '', desc: "Edit HTML"},
		{action: 'makeQuote', icon:'quote-left', desc:"make a quote"},
		{action: 'extractImageUrl', icon:"image", desc:"Extract images..."},
		{action: 'explodeRow', icon:"bomb", desc:"explode row"}
		];
		if (structure.valid){
			actions[actions.length] = {action:"convert_"+structure.type, params:structure, desc:"Convert Row to "+structure.type, icon:"wrench"  }
		}
		this.panel.$actions.html('');
		for (var i in actions){
			var text = actions[i].icon?'<span class="fa fa-'+actions[i].icon+'"></span>':actions[i].text;
			this.panel.$actions.append('<a class="button" action="'
				+actions[i].action
				+'" title="'+actions[i].desc+'" params="'+actions[i].params+'">'+text+'</a>');
		}

		this.panel.$actions.find('a[action]').click(function(){
			f.panel.$.hide();
			f.run(
			{
				node:$node, 
				action: jQuery(this).attr('action'), 
				params:jQuery(this).attr('params')
			});
		});
		this.panel.$.show();
	},

	loadNodeAction: function($node){
		if (!this.loadPost($node)) return;

		this.$content = jQuery('#post-'+this.id+' .entry-content');
		this.currentNode = $node;
		var f = this;

		var actions = [
		{action: 'replaceNodeWith', icon: 'paragraph', params:'p', desc: "Replace Node with 'p' "},
		{action: 'replaceNodeWith', icon: "quote-left", params:'blockquote.aside', desc: "Replace Node with 'blockquote'"},
		{action: 'replaceNodeWith', text: "h3", params:'h3', desc: "Replace Node with 'h3'"},
		{action: 'replaceNodeWith', text: "h4", params:'h4', desc: "Replace Node with 'h4'"},
		{action: 'replaceNodeWith', text: "h5", params:'h5', desc: "Replace Node with 'h5'"},
		{action: 'replaceNodeWith', icon: 'header', params: 'blockquote', desc: "Replace Node with highlight"},
		{action: 'replaceNodeWith', icon: 'font', params: 'p.font-sans-serif', desc: "Replace Node with intro paragraph"},
		{action: 'editNode', icon: 'edit', params: '', desc: "Edit HTML"}

		];
		var nodeHTML = $node.html();
		var nodeHTML = nodeHTML.replace(' ', '');
		var isEmpty = (nodeHTML == '');
		if (isEmpty || nodeHTML.length < 10){
			actions=[
			{action: 'deleteNode', icon: "trash", params:"", desc:'Delete Empty node'}
			];
		}



		this.panel.$actions.html('');
		for (var i in actions){
			var text = actions[i].icon?'<span class="fa fa-'+actions[i].icon+'"></span>':actions[i].text;
			this.panel.$actions.append('<a class="btn btn-primary btn-small" action="'
				+actions[i].action
				+'" title="'+actions[i].desc+'" params="'+actions[i].params+'">'+text+'</a>');
		}

		this.panel.$actions.find('a[action]').click(function(){
			f.panel.$.hide();
			f.run(
			{
				node:$node, 
				action: jQuery(this).attr('action'), 
				params:jQuery(this).attr('params')
			});
		});
		this.panel.$.show();

	},

	run: function(args){
		if (this[args.action]){
			this[args.action](args);
		}

	},
	deleteNode: function(args){
		args.node.remove();

	},
	replaceNodeWith: function(args){
		var tag = args.params;
		_class="";
		var _p = args.params.split('.');
		if (_p.length>1){
			tag = _p[0];
			_class = ' class="'+_p[1]+'"';
		}
		var style = args.node.attr('style')?' style="'+args.node.attr('style')+'"':'' ;
		args.node.replaceWith('<'+tag+_class+style+'>'+args.node.html()+'</'+tag+'>');
	},
	editClasses:  function(args){
		var cls = prompt('Tag Class', args.node[0].className);
		if (cls!== null){
			args.node.attr('class', cls);
		}

	},
	editComponent: function(args){
		var id = args.node.attr('component-id');
		alert(id);
		window.open('/wp-admin/post.php?post='+id+'&action=edit', '_blank');
	},
	editShortcode: function(args){
		var shortcode = args.node.attr('shortcode');
		var sh = base64_decode(shortcode);
		this.nodeEditor.load(args.node, 'shortcode');
		this.nodeEditor.editor.setValue(sh);
	},
	explodeRow: function(args){
		str = [];
		args.node.children().each(function(){
			str = str+ '<div class="row">'+jQuery(this)[0].outerHTML+'</div>';

		});
		args.node.replaceWith(str);
	},
	extractImageUrl: function(args){
		str = [];
		args.node.find('img').each(function(){
			str[str.length]= this.src;
		});
		alert(str.join(','));
	},
	makeQuote: function(args){
		qb = '';
		var $qb = args.node.find('.quote-by');
		if ($qb.length){

			var qb = '<span class="blockquote-footer">'+$qb.text().trim()+'</span>';
			$qb.remove();
		}
		var t = '<blockquote>'+args.node.text() + qb+'</blockquote>';
		args.node.replaceWith(t);
	},
	editNode: function(args){
		this.nodeEditor.load(args.node);
	},

	convertSection: function(args){
		if (args.node[0].tagName.toUpperCase()=="IMG"){
			var url = args.node.attr('src');
			var par = this.helpers.getParentRow(args.node);
			if (!par){
				alert('parent Row is not found');
				return;
			}

			var caption = par.find('[class^="section-capt"]');
			if (caption.length <1){
				alert('no caption');
				return;
			}
			var text = caption.text().replace(/(?:\r\n|\r|\n)/g, '');
			var f = this;

			jQuery.post(ajaxurl, {action: "get_attachment_id", url:url}, function(response, status){
				var replace = '[caption align="alignnone" ';
				if (response.success){
					var id = response.data.id;
					var url = response.data.url;
					var width = response.data.width;
					var height = response.data.height;
					var cls = prompt('Enter className for container');
					var  _cls = cls?' class="'+cls+'" ':'';
					replace +='id="attachment_'+id+'" width="'+width+'" '+_cls+' ]';
					replace +='<img class="wp-image-'+id+' size-full" src="'+url+'" width="'+width+'" height="'+height+'" /> '+text+'[/caption]';

				}
				else{
					replace +='] '+args.node[0].outerHTML+ ' '+text+'[/caption]';
				}
				par.replaceWith(replace);






			});
		}

		if (args.node.hasClass('highlighted')){
			var text = args.node.text();
			par = this.helpers.getParentRow(args.node);
			if (!par){
				alert('parent Row is not found');
				return;
			}
			par.replaceWith('<blockquote>'+text+'</blockquote>');
			return;
		}

		if (args.node.hasClass('section-title')){
			var text= args.node.text().replace(/(?:\r\n|\r|\n)/g, '');
			par = this.helpers.getParentRow(args.node);
			if(!par){
				alert('parent Row is not found');
				return;
			}
			par.replaceWith('<h2>'+text+'</h2>');
			return;
		}
		if (args.node.hasClass('post-intro')){
			var text = args.node.text();
			par = this.helpers.getParentRow(args.node);
			if (!par){
				alert('parent Row is not found');
				return;
			}
			par.replaceWith('<p class="post-intro">'+text+'</p>');
			return;

		}
		if (args.node.hasClass('section-text-ar') || args.node.hasClass('section-text') ){
			var text = args.node.text();

			var par = args.node.parent();
			if (!par.hasClass('row')){
				alert('section parent is not row');
				return;
			}
			if (par.children().length == 1){
				par.replaceWith('<p>'+text+'</p>');
				return;
			}
			//fix multiparagraph
			if (this.helpers.isMultiParagraph(par)){
				text = '';
				par.children().each(function(){
					text += '<p>'+jQuery(this).text()+'</p>';
				});
				par.replaceWith(text);
			}


			return;
		}
		

	},


	helpers:{
		
		getParentRow: function($node){
			var par = $node.parent();
			if (par.hasClass('entry-content')){
				return false;
			}
			if (par.hasClass('row')){
				return par; 
			}
			return this.getParentRow(par);
		},
		isMultiParagraph: function(node){
			var isMultiP = true;
			node.children().each(function(){
				if (!jQuery(this).hasClass('section-text') && !jQuery(this).hasClass('section-text-ar')){
					isMultiP = false;
				}
			});
			return isMultiP;
		}
	}
}

NodeEditor = function(){
	this.node = null;
	this.init();
}

NodeEditor.prototype= {
	init: function(){
		var n = this;
		jQuery('[role="node-editor"]').find('.button.update').click(function(){
			n.save();
		});
		jQuery('[role="node-editor"]').find('.button.make-component').click(function(){
			n.makeComponent();
		});
		jQuery('[role="node-editor"]').find('[role="format"]').click(function(){
			n.editor.autoFormatRange({line:0, ch:0}, {line:n.editor.lineCount()});
		});
		this.$ = jQuery('[role="node-editor"]');
		var ta = this.$.find('textarea')[0];
		this.editor = CodeMirror.fromTextArea(ta, {
			mode: "text/html",
			lineNumbers: true,
			lineWrapping: true,
			extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }},
			foldGutter: true,
			gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
		});
	},
	save: function(){
		if (this.node){
			switch (this.mode){
				case 'shortcode':
				this.helpers.doShortcode(this.editor.getValue(), this.node);
				break;
				case 'inner':
				this.node.html(this.editor.getValue());
				this.node = null;
				break;
				default:
				this.helpers.doShortcode(this.editor.getValue(), this.node);
				
				break;
			}

		}
		jQuery('[role="node-editor"]').hide();
	},
	makeComponent: function(){
		if (!this.node){
			alert("no node selected");
			return;
		}
		var content = this.editor.getValue();
		jQuery('body').append('<div class="tmp"></div>');
		jQuery('.tmp').html(content);
		var cls = jQuery('.tmp').children()[0].className;
		content = jQuery('.tmp>*').html();
		var post_title = prompt("Titre du composant :");
		if(!post_title){
			alert("Tu veux creer un composant sans nom ?");
			return;
		}
		var n = this.node;
		jQuery.post(ajaxurl, {action:"make_component", content:content, class:cls, post_title:post_title}, function(response){
			if(response.success){
				n.replaceWith(response.data.content);
			}else{
				alert(response.data.message);
			}
		});
	},
	load: function($node, mode){
		this.node = $node;
		this.mode = mode;
		jQuery('[role="node-editor"]').show();
		if (this.mode=="inner"){
			this.editor.setValue($node.html());
		}
		else{
			this.editor.setValue($node[0].outerHTML);
		}
		var $this=this;
		setTimeout(function(){
			$this.editor.refresh();
		}, 50);

	},
	helpers: {
		doShortcode: function(shortcode, node){
			jQuery.post(ajaxurl, {action: 'doShortcode', shortcode:shortcode}, function(response, status){
				node.replaceWith(response.data.html);
				if (typeof Inkube.Players!="undefined") Inkube.Players.refresh();
			});
		},
	}
}


function base64_decode (encodedData) { // eslint-disable-line camelcase
	var decodeUTF8string = function (str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(str.split('').map(function (c) {
    	return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
}
if (typeof window !== 'undefined') {
	if (typeof window.atob !== 'undefined') {
		return decodeUTF8string(window.atob(encodedData))
	}
} else {
	return new Buffer(encodedData, 'base64').toString('utf-8')
}
var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
var o1
var o2
var o3
var h1
var h2
var h3
var h4
var bits
var i = 0
var ac = 0
var dec = ''
var tmpArr = []
if (!encodedData) {
	return encodedData
}
encodedData += ''
do {
    // unpack four hexets into three octets using index points in b64
    h1 = b64.indexOf(encodedData.charAt(i++))
    h2 = b64.indexOf(encodedData.charAt(i++))
    h3 = b64.indexOf(encodedData.charAt(i++))
    h4 = b64.indexOf(encodedData.charAt(i++))
    bits = h1 << 18 | h2 << 12 | h3 << 6 | h4
    o1 = bits >> 16 & 0xff
    o2 = bits >> 8 & 0xff
    o3 = bits & 0xff
    if (h3 === 64) {
    	tmpArr[ac++] = String.fromCharCode(o1)
    } else if (h4 === 64) {
    	tmpArr[ac++] = String.fromCharCode(o1, o2)
    } else {
    	tmpArr[ac++] = String.fromCharCode(o1, o2, o3)
    }
} while (i < encodedData.length)
dec = tmpArr.join('')
return decodeUTF8string(dec.replace(/\0+$/, ''))
}