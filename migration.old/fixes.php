<?php 
fix_dispatch();
function fix_dispatch(){
	$fix = isset($_GET['fixer'])?$_GET['fixer']:'';
	switch ($fix){
		case "covers":
			$res = fix_post_covers();
			break;
			case '':
			return;
			break;
			case 'reading-duration':
			$res = fix_post_reading_duration();
			break;
			default: 
			die('Commandes disponibles: ?fixer=covers ');
			break;



	}
	die($res. ' posts fixed');
}


function fix_post_covers(){
	$posts = get_posts(array('numberposts'=>-1));
	foreach ($posts as $p){
		update_post_meta($p->ID, 'wpcf-cover-layout', 'default');
		update_post_meta($p->ID, 'wpcf-cover-panel-class', 'default');
		update_post_meta($p->ID, 'wpcf-cover-class', 'default');
		update_post_meta($p->ID, 'wpcf-content-class', 'default');
	}

	return count($posts);
}


function fix_post_reading_duration(){
	$posts = get_posts(array('numberposts'=>-1));
	foreach ($posts as $p){
		$storyduration = get_post_meta($p->ID, 'inky_story_time', true);
		update_post_meta($p->ID, 'wpcf-reading-duration', $storyduration);
	}

	return count($posts);

}