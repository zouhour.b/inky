<?php 
add_filter('composer_shortcode_atts', function($atts, $tag){
	if (!isset($atts['container_class']) && isset($atts['class']) ){
		$atts['container_class'] = $atts['class'];
		unset($atts['class']);
	}

	return $atts;
}, 2, 2);

add_action( 'admin_bar_menu', function(){
	if (!is_single()){
		return;
	}
	$p = get_queried_object();
	$p = $p->ID;
	global $wp_admin_bar;
	$wp_admin_bar->add_menu(array('id'=>'migration', 'title'=>'Article Original', 'href'=>'http://inkyfada.com/?p='.$p, 'meta'=>array('target'=>'_blank')));


}, 40);
