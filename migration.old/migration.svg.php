<?php 

add_action('wp_ajax_migrateSVG', function(){
	$data = $_POST['data'];
	$svg = new MigrationSVG();
	$r = $svg->migrate($data);
	wp_send_json_success(array('message'=>$r));
});

Class MigrationSVG extends Migration{
	static $old_dir; 
	static $new_dir; 
	static $font_dir; 

	public function __construct(){
		self::$old_dir = WP_CONTENT_DIR.'/themes/inky/assets/svg';
		self::$new_dir = WP_CONTENT_DIR.'/uploads/components/svg/';
		self::$font_dir = WP_CONTENT_DIR.'/themes/inky/assets/fonts';

		add_action('wp_ajax_convertToShortcode', array($this, 'convertToShortcode'));

	}

	public function getInfos(){
		$this->svgs = array();
		$posts = $this->get_posts();
		$files =  $this->listFolderFiles(MigrationSVG::$old_dir);
		$calls = array();
		foreach ($posts as $row){
			$script = get_post_meta($row->ID, 'wpcf-scripts', true);
			if (strstr($script, '.svg')){
				$calls[$row->ID]= $script;
			}
		}


		foreach ($files as $file){
			$needle = basename($file);
			$this->svgs[$file]=array('post_id'=>array());
			foreach ($calls as $key=>$call){

				if (strstr($call, $needle)!=false){
					$this->svgs[$file]['post_id'][] = $key;

				}
			}


		}
		return $this->svgs;
	}
/*
	public function getSVGBy($key, $val){
		switch ($jey) {
			case 'filename':
				global $wpdb;
				$q = $wpdb->get_results("
					SELECT P.ID from ".$wpdb->posts." AS P
					LEFT JOIN ".$wpdb->postmeta. " AS M on P.ID = M.post_id
					WHERE P.post_type='component' AND M.meta_key = 'wpcf-filename' AND M.meta_value='".$filename."'" );
				return $q;
				break;
			
			default:
				return false;.
				break;
		}
	}


*/	
	public function convertToShortcode(){
		$filename = filter_input(INPUT_POST, 'filename', FILTER_SANITIZE_STRING);
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
		$class = filter_input(INPUT_POST, 'class', FILTER_SANITIZE_STRING);
		$cls = $class?' class="'.$class.'" ':'';
		if ($id){
			$shcode = '[components id="'.$id.'"'. $cls .']';
			wp_send_json_success(array('shortcode'=>$shcode, 'id'=>$id, 'rendered'=>do_shortcode( $shcode)));
			die();
		}
		else {
			wp_send_json_error(array('message'=>$id.' not found'));
			die();
		}
		
	}

	public function insertComponentID($file, $id){
		$content = file_get_contents(WP_CONTENT_DIR.$file);
		$content = preg_replace('/(<[^>]+) componentid=".*?"/i', '$1', $content);
		$content = preg_replace('/(<[^>]+) filename=".*?"/i', '$1', $content);

		$content = str_replace('<svg', '<svg componentid="'.$id.'" ',$content);
		file_put_contents(WP_CONTENT_DIR.$file, $content);

	}

	public function fileMatchingInterface(){
		?>
		<div class="svg-matches">
			<div class="list">
				<?php $i=0; ?>
				<?php foreach ($this->getInfos() as $file=>$info):  ?>

					<div filename="<?php echo $file ?>">
						<?php echo $i++; ?>.  
						<?php echo str_replace('/themes/inky/assets/svg/', '',  $file); ?>
					</div>


				<?php endforeach; ?>
			</div>
			<h4>Matches</h4>
			<div class="matches">

			</div>
			<a class="btn btn-save btn-primary">Save</a>
		</div>
		<script>
			ajaxurl = '<?php echo admin_url('admin-ajax.php');?>';
		</script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/migration/js/svg.js"></script>
		<style>
			.prematch{
				border: 1px solid #333;
				background: rgba(0,0,0,.1);
			}
			[filename]{
				cursor: pointer;
			}
			.matches > div.match{
				padding: 5px;
				margin:5px;
				border: 1px solid #333;
			}
		</style>
		<?php
	}

	public function migrate($datas){
		$m = 0;
		foreach ($datas as $data){
			if (strstr( strtolower($data['f1']), 'mobile')){
				$mobile = $data['f1'];
				$desktop = $data['f2'];

			}
			else {
				$mobile = $data['f2'];
				$desktop = $data['f1'];
			}
			$new_desktop = WP_CONTENT_DIR.'/uploads/components/svg/'.basename($desktop);
			$new_mobile = str_replace('.svg','-mobile.svg', $new_desktop);

			copy(WP_CONTENT_DIR.$mobile,  $new_mobile);
			copy(WP_CONTENT_DIR.$desktop,  $new_desktop);


			$maybe = get_page_by_title(str_replace('.svg', '', basename($new_desktop)), OBJECT, 'component' );
			if ($maybe){
				$id = $maybe->ID;
			}
			else {
				$p = array('post_type'=>'component','post_status'=>'publish', 'post_title'=>str_replace('.svg', '', basename($new_desktop)), "post_content"=>'');

				$id = wp_insert_post($p);
			}
			$this->insertComponentID($mobile, $id);
			$this->insertComponentID($desktop, $id);

			wp_set_object_terms( $id, 'svg', 'component-type', false );
			$mobile_str = str_replace(WP_CONTENT_DIR.'/uploads/components/svg/', '', $new_mobile);
			$desktop_str = str_replace(WP_CONTENT_DIR.'/uploads/components/svg/', '', $new_desktop);

			update_post_meta($id, 'wpcf-filename-mobile', $mobile_str );
			update_post_meta($id, 'wpcf-filename', $desktop_str );
			$m ++;
		}
		return $m;
	}	

	public function showAll(){
		?>

		<div class="row svg-migration" style="margin-top:100px;">
			<?php foreach ($this->getInfos() as $file=>$info): ?>
				<div class="col-6">
					<div class="svg-container" id="svg-<?php echo rand(1000,9999) ?>">
						<?php  echo file_get_contents(WP_CONTENT_DIR.$file); ?>
						<div class="svg-info">
							<h5><strong>Filename: </strong><?php  echo $file ?></h5>
							<div class="fonts">

							</div>
							<div>
								Attached to 
								<?php 
								if ($info['post_id']){
									$str = "";
									foreach ($info['post_id'] as $id){
										$article = get_post($id);
										$str .= '<br/><a href="'.get_post_permalink($article->ID).'">'.$id. ' '.$article->post_title.'</a>';
									}
								}
								else $str =  "Not Attached";
								?>
								<span class="post_id"><?php echo $str ?></span>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>


		<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/migration/js/svg.js">

		</script>
		<?php
	}

	public function listFolderFiles($dir, $ext = "svg"){
		$ffs = scandir($dir);
		$list = array();

		unset($ffs[array_search('.', $ffs, true)]);
		unset($ffs[array_search('..', $ffs, true)]);
		$_dir = str_replace(WP_CONTENT_DIR, '', $dir);

	// prevent empty ordered elements
		if (count($ffs) < 1)
			return array();

		foreach($ffs as $ff){
			if(is_dir($dir.'/'.$ff)) {
				if ($ff[0]!="_")
					$list = array_merge($list, $this->listFolderFiles($dir.'/'.$ff));
			}
			else {
				if (!$ext || substr($ff, -3)==$ext)
					$list[] = $_dir.'/'.$ff;
			}

		}
		return $list;

	}


}
new MigrationSVG();