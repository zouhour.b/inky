<?php 
global $Migrator;
$Migrator = new Migration();
Class Migration {
	static $base_url;
	public function __construct(){
		$this->load();
		require_once 'migration.svg.php';
    require_once 'migration.tinymce.php';
	}

	public function load(){
		self::$base_url = get_template_directory_uri().'/migration/';
		if (!get_page_by_title( 'migration svg', OBJECT, 'page' )){
			wp_insert_post( array('post_type'=>'page', 'post_status'=>"publish", 'post_title'=>'migration svg', 'post_content'=>'' ));
		}
		add_action('wp_head', array($this, 'load_interface'));


		
	}

	public function load_interface(){
		if (is_single()){
			require_once "migration.single.php";
			$this->interface = new MigrationSingleInterface();
			if(isset($_GET['backup'])){
				if($_GET['backup']=='backup') $this->interface->backup();
				if($_GET['backup']=='revert') $this->interface->revert();
			}
		}
		?><script type="text/javascript" src="<?php echo Migration::$base_url ?>js/fix-load-events.js"></script><?php
	}

	static function url($target, $file){
		$b = basename($file);
		$_file = str_replace($b, '', $file);
		$_file = str_replace(__DIR__, '', $_file);
		if ($_file == $file){
			return '';
		}
		$url =  self::$base_url.$_file.$target;
		return $url; 

	}

	
	public function get_posts(){
		global $wpdb;
		$q = $wpdb->get_results('SELECT ID, post_title from '.$wpdb->posts.' where post_type="post" and post_status="publish"');
		return $q;
	}
}