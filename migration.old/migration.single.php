<?php 

require_once 'backward-compat.php';

Class MigrationSingleInterface {
	public function __construct(){
		if (current_user_can('edit_posts')){
			add_action( 'admin_bar_menu', array(&$this, 'toolbar_link'), 999 );
			
			add_action('wp_footer', array(&$this, 'panel'));
		}
	}
	public function toolbar_link( $wp_admin_bar ) {
		$args = array(
			'id'    => 'inkube',
			'title' => 'Inkube',
			'href'  => '',
			'meta'  => array( 'class' => 'inkube-toggle', 'role'=>"inkube-toggle" )
			);
		$wp_admin_bar->add_node( $args );
	}
	public function panel(){
		//		include "migration.view.php";
	}	
	public function revert(){
		$post = get_post();
		if (!current_user_can( 'edit_posts' )){
			return;
		}
		$old_content = get_post_meta($post->ID, 'content_backup', true);
		$old_script = get_post_meta($post->ID, 'script_backup', true);
		$old_style = get_post_meta($post->ID, 'style_backup', true);

		wp_update_post(array('ID'=>$post->ID, 'post_content'=>$old_content));
		update_post_meta($post->ID, 'wpcf-scripts', $old_script);
		update_post_meta($post->ID, 'wpcf-styles', $old_style);
		?>
		<script type="text/javascript">
			window.location.href="<?php echo get_post_permalink();?>";
		</script>
		<?php
	}
	public function backup(){
		global $Migrator;
		$post = get_post();
		if (!current_user_can( 'edit_posts' )){
			return;
		}
		$posts = $Migrator->get_posts();
		foreach ($posts as $row) {
			$p = get_post($row->ID);
			update_post_meta($row->ID, 'content_backup', $p->post_content);
			
			$script =  get_post_meta($row->ID, 'wpcf-scripts',true );
			update_post_meta($row->ID, 'script_backup', $script);
			$style =  get_post_meta($row->ID, 'wpcf-styles',true );
			update_post_meta($row->ID, 'style_backup', $style);
		}
	}
}