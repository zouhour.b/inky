<?php 

Class MigrationImages extends Migration{
	static $dir; 

	public function __construct(){
		self::$dir = WP_CONTENT_DIR.'/uploads';

	}


	public function get_post_images(){
		$list = $alllist = array();
		$posts = $this->get_posts();
		foreach ($posts as $row){
			$post = get_post($row->ID);
			$m_content = $m_script = $m_style = array();
			$content = $post->post_content;
			$script = get_post_meta( $row->ID, 'wpcf-scripts', true );
			$style = get_post_meta( $row->ID, 'wpcf-styles', true );

			preg_match_all('!wp\-content\/uploads[a-z0-9\-\.\/]+\.(?:jpe?g|png|gif)!Ui' , $content , $m_content);
			preg_match_all('!wp\-content[a-z0-9\-\.\/]+\.(?:jpe?g|png|gif)!Ui' , $style , $m_style);
			preg_match_all('!wp\-content[a-z0-9\-\.\/]+\.(?:jpe?g|png|gif)!Ui' , $script , $m_script);
			$urls = array_unique(array_merge($m_content[0], $m_style[0], $m_script[0]));
			$urls = array_unique($m_content[0]);
			$filenames = array();
			foreach ($urls as $url){
				$filenames[] = basename($url);
			}
			foreach ($filenames as $f){
			}
			$list[] = array('ID'=>$post->ID, 'title'=>$post->post_title, 'images'=>$urls);

			$alllist = array_unique(array_merge($alllist, $urls));
		}
		var_dump($alllist);
		/*foreach ($alllist as $file){
			echo '<img src="/'.$file.'" />';
			echo '<br/>';
		}*/
		//var_dump($alllist);
	}

	public function getInfos(){
		ini_set('max_execution_time', 3600);
		$files = $this->listFolderFiles(self::$dir, array('png', 'jpg', 'jpeg', 'gif'));

		echo count($files); 
		$i =$j= 1;

		$prefix = WP_CONTENT_URL;
		foreach ($files as $a){
			$j++;
			if ($j > 10000) die();
			foreach ($files as $b){
				if ($a!=$b && basename($a) == basename($b)){
					?>
					<div class="row">
						<div class="col-12">
							<?php echo $i++ ?>
						</div>
						<div class="col-6">
							<div>
								<?php echo $a ?>
							</div>
							<img src="<?php echo $prefix.$a ?>" />
						</div>
						<div class="col-6">
							<div><?php echo $b ?></div>

							<img src="<?php echo $prefix.$b ?>" />
						</div>
					</div>
					<?php 
				}

			}
		}
		echo "NO COLLISION";
	}


	public function migrate($datas){
		$m = "";
		return $m;
	}	

	public function showAll(){
		?>

		<div class="row svg-migration" style="margin-top:100px;">
			<?php foreach ($this->getInfos() as $file=>$info): ?>
				<div class="col-6">
					<div class="svg-container" id="svg-<?php echo rand(1000,9999) ?>">
						<?php  echo file_get_contents(WP_CONTENT_DIR.$file); ?>
						<div class="svg-info">
							<h5><strong>Filename: </strong><?php  echo $file ?></h5>
							<div class="fonts">

							</div>
							<div>
								Attached to 
								<?php 
								if ($info['post_id']){
									$str = "";
									foreach ($info['post_id'] as $id){
										$article = get_post($id);
										$str .= '<br/><a href="'.get_post_permalink($article->ID).'">'.$id. ' '.$article->post_title.'</a>';
									}
								}
								else $str =  "Not Attached";
								?>
								<span class="post_id"><?php echo $str ?></span>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>


		<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/migration/js/image.js">

		</script>
		<?php
	}

	public function listFolderFiles($dir, $ext){
		$ffs = scandir($dir);
		$list = array();

		unset($ffs[array_search('.', $ffs, true)]);
		unset($ffs[array_search('..', $ffs, true)]);
		$_dir = str_replace(WP_CONTENT_DIR, '', $dir);

	// prevent empty ordered elements
		if (count($ffs) < 1)
			return array();

		foreach($ffs as $ff){
			if(is_dir($dir.'/'.$ff)) {
				if ($ff[0]!="_")
					$list = array_merge($list, $this->listFolderFiles($dir.'/'.$ff, $ext));
			}
			else {
				if( !strstr($ff, '300x') && !strstr($ff, '150x')){


					if (in_array(substr($ff,-3), $ext)){
						$list[] = $_dir.'/'.$ff;
					}
				}

			}

		}
		return $list;

	}


}
new MigrationSVG();