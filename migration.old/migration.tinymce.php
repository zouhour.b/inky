<?php

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wpse33318_tiny_mce_before_init( $mce_init ) {

	$mce_init['cache_suffix'] = 'v=162';

	return $mce_init;    
}
add_filter( 'tiny_mce_before_init', 'wpse33318_tiny_mce_before_init' );