<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package assafir
 */

function get_attachment_id_by_url( $url ) {
		// Split the $url into two parts with the wp-content directory as the separator.
	$parsed_url = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );

		// Get the host of the current site and the host of the $url, ignoring www.
	$this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
	$file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );

		// Return nothing if there aren't any $url parts or if the current host and $url host do not match.
	$attachment_path = $parsed_url[1]??null;
	if ( ! isset( $attachment_path ) || empty( $attachment_path ) ) {
		return null;
	}

		// Now we're going to quickly search the DB for any attachment GUID with a partial path match.
		// Example: /uploads/2013/05/test-image.jpg
	global $wpdb;

	$query = $wpdb->prepare(
		"SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND guid LIKE %s",
		'%' . basename($attachment_path)
		);

	$attachment = $wpdb->get_col( $query );

	if ( is_array( $attachment ) && ! empty( $attachment ) ) {
		return (int)array_shift( $attachment );
	}

	return null;
}

function uniord($u) {
    // i just copied this function fron the php.net comments, but it should work fine!
	$k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
	$k1 = ord(substr($k, 0, 1));
	$k2 = ord(substr($k, 1, 1));
	return $k2 * 256 + $k1;
}

function is_arabic($str) {
	if(mb_detect_encoding($str) !== 'UTF-8') {
		$str = mb_convert_encoding($str,mb_detect_encoding($str),'UTF-8');
	}

    /*
    $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
    $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
    */
    preg_match_all('/.|\n/u', $str, $matches);
    $chars = $matches[0];
    $arabic_count = 0;
    $latin_count = 0;
    $total_count = 0;
    foreach($chars as $char) {
        //$pos = ord($char); we cant use that, its not binary safe 
    	$pos = uniord($char);
    	
    	if($pos >= 1536 && $pos <= 1791) {
    		$arabic_count++;
    	} else if($pos > 123 && $pos < 123) {
    		$latin_count++;
    	}
    	$total_count++;
    }
    if(($arabic_count/$total_count) > 0.6) {
        // 60% arabic chars, its probably arabic
    	return true;
    }
    return false;
}

add_shortcode("tpl_url",function($a){
	return get_template_directory_uri();
});
add_filter("widget_text_content",function($a){
	return apply_filters("the_content",$a);
});

function inkyfada_excerpt($length, $_echo = true, $post_id=null){
	if ($length == 0) return;
	if ($_echo){
		echo wp_trim_words( get_the_excerpt(), $length, '...' );
		return;
	}

	return wp_trim_words( get_the_excerpt($post_id), $length, '...' );
}