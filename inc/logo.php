<?php 

add_theme_support( 'custom-logo', array(
	'height'      => 60,
	'width'       => 100,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );