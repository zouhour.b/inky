
<a id="<?php echo $this->get_field_id('button') ?>" class="button button-toggle-widget-cards-form" role="display-form">Settings</a>
<div class="widget-cards-form active">
    <header>
        <a class="close-form"><span class="dashicons dashicons-no" role="close"></span></a>
    </header>
    <section class="widget-cards-form-body">

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('title_class'); ?>"><?php _e('Title class', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title_class'); ?>" name="<?php echo $this->get_field_name('title_class'); ?>" type="text" value="<?php echo esc_attr($title_class); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('readmore_title'); ?>"><?php _e('Readmore Title', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('readmore_title'); ?>" name="<?php echo $this->get_field_name('readmore_title'); ?>" type="text" value="<?php echo esc_attr($readmore_title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('readmore_link'); ?>"><?php _e('Readmore Link', 'readmore_link'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('readmore_link'); ?>" name="<?php echo $this->get_field_name('readmore_link'); ?>" type="text" value="<?php echo esc_attr($readmore_link); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('numberposts'); ?>"><?php _e('Number', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('numberposts'); ?>" name="<?php echo $this->get_field_name('numberposts'); ?>" type="text" value="<?php echo esc_attr($numberposts); ?>" />
        </p>
        <div class="tax-input" id="<?php echo $this->get_field_id('taxonomy-form') ?>">
            <h5>Classification</h5>
            <input role="json" type="hidden" id="<?php echo $this->get_field_id('tax_input') ?>" name="<?php echo $this->get_field_name('tax_input') ?>" value="<?php echo esc_attr($tax_input) ?>"/>
            <?php 
            $tax_query = (array)json_decode($instance['tax_input']);
            
            $taxonomies = get_taxonomies(array('public'=>true));
            printf( '<select role="taxonomy" >');
            
            foreach ( $taxonomies as $tax ) {
                $selected = (isset($tax_query['taxonomy']) && $tax == $tax_query['taxonomy'])?' selected="selected"':'';
                printf( '<option value="%s" %s>%s</option>', esc_attr( $tax ), $selected, esc_html( $tax ) );
            }
            print('</select><div role="terms"></div>');
            ?>
            <p role="group-container-class">
                <label for="<?php echo $this->get_field_id('group_container'); ?>"><?php _e('Group container class', 'inkube'); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id('group_container'); ?>" name="<?php echo $this->get_field_name('group_container'); ?>" type="text" value="<?php echo esc_attr($group_container); ?>" />
            </p>
        </div>

        <p>
            <label for="<?php echo $this->get_field_id('exclude'); ?>">
                <input <?php echo $exclude?'checked="checked"':'' ?> class="checkbox" id="<?php echo $this->get_field_id('exclude'); ?>" name="<?php echo $this->get_field_name('exclude'); ?>" type="checkbox" value="yes" /> 
                <?php _e('Exclude already displayed article', 'inkube'); ?>
            </label> 
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('featured'); ?>">
                <input <?php echo $featured ?> class="checkbox" id="<?php echo $this->get_field_id('featured'); ?>" name="<?php echo $this->get_field_name('featured'); ?>" type="checkbox" value="yes" />
                <?php _e('Featured', 'inkube'); ?>
            </label> 



        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('template'); ?>"><?php _e('Template', 'inkube'); ?></label> 
            <select class="widefat" id="<?php echo $this->get_field_id('template'); ?>" name="<?php echo $this->get_field_name('template'); ?>" type="text" ?>" >
                <?php echo self::displayTemplateOptions($template) ?>
            </select>
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('container'); ?>"><?php _e('Container class', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('container'); ?>" name="<?php echo $this->get_field_name('container'); ?>" type="text" value="<?php echo esc_attr($container); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post_class'); ?>"><?php _e('Post Class', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('post_class'); ?>" name="<?php echo $this->get_field_name('post_class'); ?>" type="text" value="<?php echo esc_attr($post_class); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('caption_class'); ?>"><?php _e('Caption Class', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('caption_class'); ?>" name="<?php echo $this->get_field_name('caption_class'); ?>" type="text" value="<?php echo esc_attr($caption_class); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('title_tag'); ?>"><?php _e('Taille du titre:', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title_tag'); ?>" name="<?php echo $this->get_field_name('title_tag'); ?>" type="text" value="<?php echo esc_attr($title_tag); ?>" />
        </p>        
        
        <p>
            <label for="<?php echo $this->get_field_id('excerpt_length'); ?>"><?php _e('Display words:', 'inkube'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('excerpt_length'); ?>" name="<?php echo $this->get_field_name('excerpt_length'); ?>" type="text" value="<?php echo esc_attr($excerpt_length); ?>" />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('img_size'); ?>"><?php _e('Image size', 'inkube'); ?></label> 
        </p>
        <select id="<?php echo $this->get_field_id('img_size'); ?>" name="<?php echo $this->get_field_name('img_size'); ?>" >
            <?php foreach (static::get_image_sizes() as $size => $attr): ?>
                <?php $selected = ($img_size == $size) ? 'selected="selected"' : ''; ?>
                <option <?php echo $selected ?>  value="<?php echo $size ?>"><?php echo $size . " - (" . $attr['width'] . " x " . $attr['height'] . " ) " . ($attr['crop'] ? " [cropped ".(floor((int)$attr['width']*100/(int)$attr['height'])/100)."] " : ""); ?></option1>

            <?php endforeach; ?>
        </select>
        

    </section>
    <footer>
        <a class="button button-primary close-form">OK</a>
    </footer>
</div>

<script>
    jQuery(document).ready(function(){
        jQuery('#<?php echo $this->get_field_id('button') ?>').click(function(){
            jQuery(this).next().addClass('active');
        });
        jQuery('.widget-cards-form .close-form').click(function(){
            jQuery(this).parents('.widget-cards-form').removeClass('active');
        });
        jQuery(document).ready(function(){
            new widgetCardsForm("<?php echo $this->get_field_id('taxonomy-form') ?>");
        }
        );
    });
</script>