<?php if ($widgettitle): ?>

    <div class="widgettitle">
        <?php if ($widgeticon): ?>
            <span class="fa fa-<?php echo $widgeticon ?>"></span>
        <?php endif; ?>
        <h3 class="widgettitle-text"><?php echo $widgettitle ?></h3>
    </div>

    
<?php endif; ?>
<div class="playlist">
    
        <div id="c-<?php echo $this->id ?>" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php global $post ?>
                <?php $i = 0; ?>
                <?php foreach ($posts as $post): setup_postdata($post); ?>
                    <?php $active  = ($i==0)?" carousel-item item active":' carousel-item item'; ?>
                    <div <?php post_class($post_class.$active) ?>>
                        <?php $i++ ?>

                        <div class="image-holder">
                            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" >
                                <?php the_post_thumbnail() ?>
                                <span class="card-icon fa fa-play"></span>
                            </a>
                            

                            <?php the_category() ?>

                        </div>
                        <div class="card-block">                
                            <h4 class="card-title">
                                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" >
                                    <?php the_title() ?>
                                </a>
                            </h4>                    
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php remove_filter('post_thumbnail_size', array($this, 'set_thumbnail_size')); ?>
    <ol class="hidden-xs carousel-indicators">
        <?php $i = 0; ?>
        <?php foreach ($posts as $post): setup_postdata($post); ?>
            <?php $active  = ($i==0)?" active":''; ?>
            <li data-target="#c-<?php echo $this->id ?>" data-slide-to="<?php echo $i ?>" class="<?php echo $active ?>">
                <?php the_post_thumbnail('square') ?>
                <span><?php the_title() ?></span>
            </li>
            <?php $i++ ?>
        <?php endforeach; ?>
    </ol>


        </div>
        
    
    
</div>