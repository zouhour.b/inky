<?php if ($widgettitle): ?>
    <div class="collection-title">
        <div class="widgettitle">
            <?php if ($widgeticon): ?>
                <span class="fa fa-<?php echo $widgeticon ?>"></span>
            <?php endif; ?>
            <h3 class="widgettitle-text"><?php echo $widgettitle ?></h3>
        </div>
    </div>
<?php endif; ?>    <?php $jsid = "carousel_".$this->id ?>

<?php global $post ?>
<div id="<?php echo $jsid ?>" class="carousel slide carousel-fade"  data-ride="carousel" data-interval="0">
    <div class="carousel-inner">
        <?php global $post; ?>
        <?php for ($i = 0; $i < sizeof($posts); $i++): ?>
            <?php setup_postdata($post) ?>
            <div class="carousel-item thumbnail <?php echo ($i == 0) ? 'active' : ''; ?>">
                <?php $post = $posts[$i]; ?>
                <a class="post-cover" href="<?php the_permalink() ?>"><?php the_post_thumbnail('flexslider') ?></a>
                <div class="post-caption">

                    <h3><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
                    <div class="hidden-sm-down"><?php the_excerpt() ?></div>
                    <div class="social-share">
                            <?php naked_social_share_buttons() ?>
                    </div>

                </div>

            </div>

        <?php endfor; ?>

    </div>
    <a class="icon-prev left carousel-control" href="#<?php echo $jsid ?>" role="button" data-slide="prev">
        <span class="fa fa-caret-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class=" right carousel-control" href="#<?php echo $jsid ?>" role="button" data-slide="next">
        <span class="   fa fa-caret-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


