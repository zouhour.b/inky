<?php if ($widgettitle): ?>
    <div class="collection-title">
        <div class="widgettitle">
            <?php if ($widgeticon): ?>
                <span class="fa fa-<?php echo $widgeticon ?>"></span>
            <?php endif; ?>
            <h3 class="widgettitle-text"><?php echo $widgettitle ?></h3>
        </div>
    </div>
    
<?php endif; ?>
<div class="collection">

    <?php global $post ?>
    <?php $i = 0; ?>
    <?php foreach ($posts as $post): setup_postdata($post); ?>
        <div <?php post_class($post_class) ?>>
            <div class="post-meta row">

                <span class="post-date"><?php the_time('h:i') ?></span>
                <?php the_category() ?>
            </div>
            <h6 class="row">
                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" >
                    <?php the_title() ?>
                </a>
            </h6>
        </div>
    <?php endforeach; ?>
</div>