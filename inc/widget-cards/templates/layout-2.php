<?php if ($widgettitle): ?>
    <div class="collection-title">
        <div class="widgettitle">
            <?php if ($widgeticon): ?>
                <span class="fa fa-<?php echo $widgeticon ?>"></span>
            <?php endif; ?>
            <h3 class="widgettitle-text"><?php echo $widgettitle ?></h3>
        </div>
    </div>
    
<?php endif; ?>
<div class="collection">

    <?php global $post ?>
    <?php $i = 0; ?>
    <?php foreach ($posts as $post): setup_postdata($post); ?>
        <div <?php post_class($post_class) ?>>
            <?php $i++ ?>
            <div class="image-holder">
                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" >
                    <?php the_post_thumbnail() ?>
                </a>
                <span class="card-icon fa fa-play" style="display: none"></span>
                
                <?php the_category() ?>
                
                <div class="social-links">
                    
                    <?php echo naked_social_share_buttons(); ?>
                </div>
            </div>
            
            <div class="card-block">                
                <h4 class="card-title">
                    <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" >
                        <?php the_title() ?>
                    </a>
                </h4>
                <div class="card-text"><?php the_excerpt() ?></div>
                <footer>
                    
                    <div class="post-meta">
                        <span class="post-author"><?php the_author() ?></span>
                        <span class="post-date"><?php the_time('j F Y') ?></span>
                        <span class="post-date"><?php the_time('h:i') ?></span>
                    </div>
                </footer>
            </div>
        </div>
        <div class="separator"></div>
    <?php endforeach; ?>
</div>