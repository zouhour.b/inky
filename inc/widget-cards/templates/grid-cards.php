<div class="row">
    <?php if ($widgettitle): ?>
        <h3 class="widgettitle">
            <?php if ($widgeticon): ?>
                <span class="fa fa-<?php echo $widgeticon ?>"></span>
            <?php endif; ?>
            <?php echo $widgettitle ?>
        </h3>
    <?php endif; ?>
    

    <?php global $post ?>
    <?php $i = 0; ?>
    <?php foreach ($posts as $post): setup_postdata($post); ?>
        <div <?php post_class($post_class) ?>>
            <?php $i++ ?>

            <?php the_post_thumbnail() ?>
            <span class="card-icon fa fa-play"></span>
            <div class="card-block">
                <date class="card-meta"><?php echo the_time('d / m') ?></date>
                <h4 class="card-title"><?php the_title() ?></h4>
                <div class="card-text"><?php the_excerpt() ?></div>
                <a href="#" class="btn btn-primary">Lire plus</a>
            </div>

        </div>
    <?php endforeach; ?>
</div>