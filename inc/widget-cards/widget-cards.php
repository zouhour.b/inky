<?php 

/**
 * Enqueue script for custom customize control.
 */
add_action( 'admin_enqueue_scripts', function () {
    if (is_customize_preview()){
        wp_register_script('selectize', get_template_directory_uri().'/inc/widget-cards/assets/js/selectize.min.js');
        wp_enqueue_script('widget-cards-admin-js', get_template_directory_uri().'/inc/widget-cards/assets/js/widget-cards-admin.js', array('jquery', 'selectize'));
        wp_enqueue_style('selectize', get_template_directory_uri().'/inc/widget-cards/assets/css/selectize.css');
        wp_enqueue_style( 'widget-cards-admin', get_template_directory_uri().'/inc/widget-cards/assets/css/widget-card-admin.css');

    }
    
});

add_action( 'wp_ajax_widgetCardsGetTerms', array('Cards', 'getTerms') );

class Cards extends WP_Widget{
    static $exclude = array();
    static $defaults = array(
        'numberposts' => 4,
        'widgeticon'  => '',
        'title'=>'',
        'title_class'=> 'col-12',
        'readmore_title'=> '',
        'readmore_link'=> '',
        'post_type' => 'post',
        'post_class' => 'col-md-6',
        'caption_class'=>'col-md-6',
        'title_tag' => 'h3',
        'group_container'=>'col-12',
        'img_size' => 'full',
        'excerpt_length' =>20,
        'taxonomy'=>'category',
        'tax_input'=>'{}',
        "featured" => "",
        "exclude" => true,
        "container" => "col-12",
        'template' => 'card.php'
        );
    public function __construct(){
        $widget_ops = array(
            'classname' => 'post-cards'
            );
        parent::__construct('cards', 'Cards', $widget_ops);
    }

    public function form($instance){
        $instance = wp_parse_args($instance, static::$defaults);

        extract($instance);
        include __DIR__.'/widget-cards-form.php';
    }

    public function update($new_instance, $old_instance)
    {
        $new_instance['exclude'] = isset($new_instance['exclude'])?true:false;
        return $new_instance;
    }


    static function displayTemplateOptions($tpl){
        $templates = scandir(get_template_directory().'/template-parts');
        $str = "";
        foreach ($templates as $t){
            if (substr($t, -3)=="php"){
                $selected = ($t == $tpl)?'selected="selected"':'';
                $str .= '<option value="'.$t.'" '.$selected.' >'.substr($t, 0,-4).'</option>';
            }
        }
        return $str;
    }
    
    public function getQueryResult($instance){
        

        if ($instance['exclude'])
        {
            $instance['exclude'] = self::$exclude;
        }

        if ($instance['featured'])
        {
            $instance['meta_key'] = '_is_featured';
            $instance['meta_value'] = 'yes';
        }
        unset($instance['title']);
        $tax_query = (array)json_decode($instance['tax_input']);
        if ($tax_query && isset($tax_query['terms']) && sizeof($tax_query['terms']) > 1){
            $_instance = array_filter($instance);
            
            $this->grouped = true;
            $posts = array();
            foreach ($tax_query['terms'] as $term_id){
                $_instance['tax_query'] = array(array(
                    'field'=>'term_id',
                    'terms'=>array($term_id),
                    'taxonomy'=>$tax_query['taxonomy']
                    ));
                $posts[$term_id]= get_posts($_instance);
                
            }
            

        }
        else {
            if ($tax_query && isset($tax_query['terms']) && sizeof($tax_query['terms']) == 1){
                $instance['tax_query'] = array($tax_query);
            }
            $instance = apply_filters('cards_pre_get_posts', $instance );
            $posts = get_posts(array_filter($instance));
            self::addExcludedPosts($posts);
        }
        
        return $posts;
    }

    public function widget($args, $instance)
    {

        $instance = wp_parse_args($instance, static::$defaults);
        $this->widgetttile = $instance['title'];
        $posts = $this->getQueryResult($instance);
        if (!$posts)
            return;
        $this->render($args, $instance, $posts);

    }
    public function set_thumbnail_size($size)
    {
        return $this->img_size;
    }

    public function getExcerptLength(){
        return $this->excerpt_length;
    }

    public function render($args, $instance, $posts){
        extract($instance);
        global $post;
        $tpl = substr($template, 0, -4);
        $container .= " template-".$tpl;
        $args['before_widget'] = str_replace('class="', 'class="'.$container.' ' , $args['before_widget']);
        echo $args['before_widget'];
        echo '<div class="post-collection row">'; ?>
        <?php  if ($this->widgetttile): ?>
            <div class="widget-header <?php echo $title_class; ?>">
                <?php echo $args['before_title'].'<span>'.$this->widgetttile.'</span>'.$args['after_title']; ?>
            </div>
            <?php  if ($readmore_title): ?>
              <div class="card_readmore d-none d-md-block col-6 text-right lh-150">
                  <a href="<?php echo $readmore_link ?>" target="_blank"><?php echo $readmore_title ?></a>
              </div>
            <?php endif; ?>
        <?php endif;
        if (file_exists(get_template_directory().'/template-parts/'.$template) && substr($template, -3)=="php")
        {
            $this->excerpt_length = $excerpt_length;
            $post_class = "card ".$post_class;
            add_filter( 'excerpt_length', array($this, 'getExcerptLength'), 999 );
            $tax_query = (array)json_decode($instance['tax_input']);
            if ($tax_query && isset($tax_query['terms']) && sizeof($tax_query['terms']) > 1){
                $grouped_posts = $posts;
                foreach ($grouped_posts as $term=>$posts){
                    $term = get_term( $term, $tax_query['taxonomy']); ?>
                    <div class="post-group-container <?php echo $group_container ?>">
                        <div class="post-group post-collection row">
                            <div class="group-header col-12">
                                <h5 class="widget-title">
                                    <span><?php echo $term->name; ?></span>
                                </h5>
                            </div>
                            <?php $loop_index = 1 ?>
                            <?php foreach ($posts as $post){
                                setup_postdata( $post );
                                require get_template_directory().'/template-parts/'.$template;
                                $loop_index++;
                            }
                            ?>
                        </div>
                    </div>
                    <?php 
                }
            }
            else {
                global $post;
                $loop_index = 1 ;
                foreach ($posts as $post){
                    setup_postdata( $post );
                    require get_template_directory().'/template-parts/'.$template; 
                    $loop_index++;
                }
            }
        }
        else{
            echo "template $template not found";
        }
        if ($readmore_title){ 
          ?>
            <div class="card_readmore d-block d-md-none col-12 lh-150 text-center">
                <a target="_blank" href="<?php echo $readmore_link ?>"><?php echo $readmore_title ?></a>
            </div>
          <?php
        }
        echo '</div>';
        
        echo $args['after_widget'];
        wp_reset_postdata();
    }

    static function get_image_sizes()
    {
        global $_wp_additional_image_sizes;

        $sizes = array();

        foreach (get_intermediate_image_sizes() as $_size)
        {
            if (in_array($_size, array('thumbnail', 'medium', 'medium_large', 'large')))
            {
                $sizes[$_size]['width'] = get_option("{$_size}_size_w");
                $sizes[$_size]['height'] = get_option("{$_size}_size_h");
                $sizes[$_size]['crop'] = (bool) get_option("{$_size}_crop");
            } elseif (isset($_wp_additional_image_sizes[$_size]))
            {
                $sizes[$_size] = array(
                    'width' => $_wp_additional_image_sizes[$_size]['width'],
                    'height' => $_wp_additional_image_sizes[$_size]['height'],
                    'crop' => $_wp_additional_image_sizes[$_size]['crop'],
                    );
            }
        }

        return $sizes;
    }

    static function addExcludedPosts($posts){
        if (!$posts) return;
        foreach ($posts as $p){
            static::$exclude[] = $p->ID;
        }
    }

    static function getTerms(){
        $tax = isset($_POST['wctax'])?$_POST['wctax']:"";
        if (!$tax) {
            wp_send_json_error('Empty Taxonomy');
        }
        $r = array();

        $terms = get_terms($tax);
        if ($terms && !is_wp_error($terms)){
            foreach ($terms as $term){
                $r[$term->term_id] = $term->name;
            }
            wp_send_json_success($r);
        }
        wp_send_json_error(array('Invalid Taxonomy or Empty'));
        die();   
    }
}

add_action('widgets_init', function()
{
    register_widget('Cards');
});


// This is required to be sure Walker_Category_Checklist class is available
require_once ABSPATH . 'wp-admin/includes/template.php';
/**
 * Custom walker to print category checkboxes for widget forms
 */
class Walker_Category_Checklist_Widget extends Walker_Category_Checklist {

    private $name;
    private $id;

    function __construct( $name = '', $id = '' ) {
        $this->name = $name;
        $this->id = $id;
    }

    function start_el( &$output, $cat, $depth = 0, $args = array(), $id = 0 ) {
        extract( $args );
        if ( empty( $taxonomy ) ) $taxonomy = 'category';
        $class = in_array( $cat->term_id, $popular_cats ) ? ' class="popular-category"' : '';
        $id = $this->id . '-' . $cat->term_id;
        $checked = checked( in_array( $cat->term_id, $selected_cats ), true, false );
        $output .= "\n<li id='{$taxonomy}-{$cat->term_id}'$class>" 
        . '<label class="selectit"><input value="' 
        . $cat->term_id . '" type="checkbox" name="' . $this->name 
        . '[]" id="in-'. $id . '"' . $checked 
        . disabled( empty( $args['disabled'] ), false, false ) . ' /> ' 
        . esc_html( apply_filters( 'the_category', $cat->name ) ) 
        . '</label>';
    }
}

