
widgetCardsForm = function(sel){
		
	this.sel = sel;
	this.selected = {terms:[]};
	this.data = {terms:[]};
	this.input = {
		taxonomy: jQuery('#'+this.sel).find('[role="taxonomy"]'),
		terms: jQuery('#'+this.sel).find('[role="terms"]'),
		json: jQuery('#'+this.sel).find('[role="json"]')

	}
	this.tax = this.input.taxonomy.val();
	this.init();
}

widgetCardsForm.prototype = {
	init: function(){
		var W = this;
		
		W.val();
		
		this.input.taxonomy.change(function(){
			var tax = jQuery(this).val();
			W.getTerms(tax);
		});
	},
	val: function(){
		var v= JSON.parse(this.input.json.val());
		this.selected.terms = (v.terms)?v.terms:[];
		if (this.tax){
			this.getTerms(this.tax);
		}
	},

	getTerms: function(taxonomy){
		var W = this;
		jQuery.post({url:ajaxurl, data:{action: "widgetCardsGetTerms", wctax:taxonomy}}, function(r, s){
			if (r.success){
				W.tax = W.input.taxonomy.val();
				W.data.terms = r.data;
				W.renderForm();
			}
			else {
				alert(r.data);
				W.input.taxonomy.val(W.tax);
			}
		});
	},
	set: function(terms){
		var data = {};
		console.log(terms);
		if (terms){
			data['terms']=terms.map(function(v){return parseInt(v)});
			data['taxonomy']=this.tax;
			data['field'] = "term_id";
		

		this.input.json.val(json);
		}
		var json = JSON.stringify(data);
		this.input.json.val(json);
		
	},

	renderForm: function(){
		var W = this;
		this.input.terms.html('');
		if (!this.data.terms) {
			
			alert('no data');
			return;
		}
		var str = '<select id="'+this.sel+'-terms"" multiple role="terms">';
		for (k in this.data.terms){
			var selected =  (this.selected.terms.indexOf(parseInt(k)) > -1)?' selected="selected" ':'';
			if (selected){
			console.log(k);

			}
			str += '<option value="'+k+'" '+selected+'>'+this.data.terms[k]+'</option>';
		}
		str += '</select>';
		this.input.terms.html(str);
		jQuery('#'+this.sel+'-terms').selectize({
			maxItems: null,
			onChange: function(value) {
      			W.set(value);
      			if (value.length > 1){
      				jQuery('#'+W.sel).find('[role="group-container-class"]').show();
      			}
      			else {
      				jQuery('#'+W.sel).find('[role="group-container-class"]').hide();	
      			}
    		}
		})
		
	}
}