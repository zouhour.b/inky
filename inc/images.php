<?php 

add_image_size('HD-16by9', 1920, 1080);
add_image_size('lg-16by9', 1366, 768, true);
add_image_size('md-16by9', 960, 540, true);
add_image_size('md-1by1', 960, 900, true);
add_image_size('xs-1by2', 640, 1280, true);
add_image_size('xs-2by3', 640, 960, true);

add_filter( 'image_size_names_choose', 'inkyfada_image_sizes' );
function inkyfada_image_sizes( $sizes ) {
	global $_wp_additional_image_sizes;

	$sizes = array('full'=>'Original');

	foreach ( get_intermediate_image_sizes() as $_size ) {
		$sizes[$_size] = $_size;
	}
    return $sizes;
}