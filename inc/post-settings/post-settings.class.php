<?php

/**
 * Register a meta box using a class.
 */

add_action( 'admin_enqueue_scripts', function () {
    global $pagenow;
    
    
    if ($pagenow == 'post.php' || $pagenow == 'post-new.php'){

        wp_register_script('selectize', get_template_directory_uri().'/inc/post-settings/assets/selectize.min.js');
        wp_enqueue_script('post-settings-js', get_template_directory_uri().'/inc/post-settings/assets/post-settings.js', array('jquery', 'selectize'));
        wp_enqueue_style('selectize', get_template_directory_uri().'/inc/post-settings/assets/selectize.css');

    }
    
});
if (is_admin()){

    add_filter('manage_post_posts_columns', array('PostSettings', 'columns_head'));
    add_action('manage_post_posts_custom_column', array('PostSettings', 'columns_content'), 10, 2);
}

class PostSettings {

    /**
     * Constructor.
     */
    public function __construct() {
    	if ( is_admin() ) {
    		add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
    		add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
        add_action('media_buttons', array('PostSettings', 'media_button'));
    	 
      }

    }
    static public function columns_head($defaults) {
        $defaults['classification'] = 'Classification';
        unset($defaults['categories']);
        return $defaults;
    }

    static  public function columns_content($column_name, $post_ID) {
        if ($column_name == 'classification') {
            global $Featured_Post;
            $Featured_Post->manage_posts_custom_column('featured', $post_ID);

            foreach (array('folder'=>"Dossier", 'post-type'=>"Type",  'genre'=>'Genre', 'folder'=>'Dossier', 'post_tag'=>'Mots clés') as $taxonomy=>$slug){
                echo get_the_term_list( $post_ID, $taxonomy, "<div><strong>$slug</strong>: ", ' | ', '</div>' );
            }
        }

    }

    /**
     * Meta box initialization.
     */
    public function init_metabox() {
      add_action('admin_footer', array($this, 'view'));
       add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
       add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
   }

   public function view(){
      require_once __DIR__.'/post-settings.view.php';
   }

   public static function media_button() {
    echo '<button type="button" data-toggle="modal" data-target="#inkube-modal-post-settings"  class="button" data-post-id="'.get_the_ID().'" >'
      . '<i class="fa fa-wheel"></i> Paramètres'
      . '</button>';
  }

   /**
     * Adds the meta box.
     */
   public function add_metabox() {
       add_meta_box(
       'post-settings',
       __( 'Settings', 'assafir' ),
       array( $this, 'render_metabox' ),
       'post',
       'advanced',
       'default'
       );

   }

   /**
     * Renders the meta box.
     */
   public function render_metabox( $post ) {
        // Add nonce for security and authentication.
    return;
     wp_nonce_field( 'custom_nonce_action', 'custom_nonce' );
     ?>
    <script src="<?php echo inkube::assets_url('assets/js/postmetaController.js') ?>"></script>
    <?php echo inkube::assets_url('assets/js/postmetaController.js') ?>
     <table class="widefat">
      <tr>
        <th style="width:100px;"><strong><?php  _e('Authors', 'assafir') ?></strong></th>
        <td>
            <?php $users = get_users(array('role__in'=>array('administrator', 'contributor', 'author', 'editor')));
            $selected = (array)get_post_meta($post->ID, '_authors', true);
            if (!in_array($post->post_author, $selected)){
                $selected[] = $post->post_author;
            }
            ?>
            <select multiple name="_authors[]" role="post-settings"> 
                <?php foreach ($users as $u): ?>
                <?php $s = in_array($u->ID, $selected)?' selected="selected" ':''; ?>

                <option <?php echo $s ?> value="<?php echo $u->ID ?>"><?php echo $u->display_name ?></option>
            <?php endforeach; ?>
        </select>

    </td>      
</tr>

<?php 
$taxonomies = apply_filters('post_settings_taxonomies', get_taxonomies(array('public'=>true)));
foreach ($taxonomies as $taxonomy): ?>
    <?php 
$allterms =  $terms = array();
$_terms = wp_get_object_terms( $post->ID, $taxonomy );
$_allterms = get_terms($taxonomy, array('hide_empty'=>false)); 
foreach ($_terms as $t) $terms[] = $t->term_id; 
foreach ($_allterms as $t) $allterms[$t->term_id] = $t->name;
$allterms = apply_filters('post_settings_'.$taxonomy.'_terms', $allterms, $taxonomy);
?> 
<tr><th><strong><?php echo $taxonomy; ?></strong></th>
  <td><select multiple role="post-settings" name="taxonomy_<?php echo $taxonomy?>[]" taxonomy="<?php echo $taxonomy ?>">
      <?php foreach ($allterms as $id => $label): ?>
      <?php $selected = in_array($id, $terms)?' selected="selected" ':''; ?>
      <option <?php echo $selected ?> value="<?php echo  $id ?>"><?php echo $label ?></option>
  <?php endforeach; ?>


</select><?php //var_dump($terms, $allterms) ?></td>
<script>

</script>
</tr>
<?php endforeach; ?>
</table>

<?php 
}

/**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
public function save_metabox( $post_id, $_post ) {
        // Add nonce for security and authentication.
 $nonce_name   = isset( $_POST['custom_nonce'] ) ? $_POST['custom_nonce'] : '';
 $nonce_action = 'custom_nonce_action';

        // Check if nonce is set.
 if ( ! isset( $nonce_name ) ) {
  return;
}

        // Check if nonce is valid.
if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
  return;
}

        // Check if user has permissions to save data.
if ( ! current_user_can( 'edit_post', $post_id ) ) {
  return;
}

        // Check if not an autosave.
if ( wp_is_post_autosave( $post_id ) ) {
  return;
}

        // Check if not a revision.
if ( wp_is_post_revision( $post_id ) ) {
  return;
}
$taxonomies = get_taxonomies(array('public'=>true));
foreach ($taxonomies as $taxonomy){
    if (isset($_POST['taxonomy_'.$taxonomy ])){
        $terms = $_POST['taxonomy_'.$taxonomy ];
        foreach ($terms as $i=>$v){
            $terms[$i] = (int)$v;
        }
        wp_set_object_terms( $post_id, $terms, $taxonomy, false );
    }

}
if (isset($_POST['_authors'])){


    $authors = (array)$_POST['_authors'];
    if (!$authors || count($authors) < 1 ){
        delete_post_meta($post_id, '_authors');
    }
    else {
        $_post->post_author = (int)$authors[0];
        foreach ($authors as $i => $authorID){
            $authors[$i] = (int)$authorID;
        }
        update_post_meta($post_id, '_authors', $authors);

    }



}
return $post_id;



}
}
die();
new PostSettings();