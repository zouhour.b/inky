PostSettings = function(sel){

	this.sel = sel;
	this.container = jQuery(this.sel);
	this.input = {};
	this.init();
}

PostSettings.prototype = {
	init: function(){
		var W = this;
		this.container.find('[role="post-settings"]').each(function(){
			var tax = jQuery(this).attr('taxonomy');
			if (tax){
				
				W.input[tax] = new PostTerm(this);
			} else {
				W.input[this.name] = jQuery(this).selectize({
			maxItems: null,
			
		});
			}
			
		});


		
	},
	

	

}


PostTerm = function(sel){
	this.sel = sel;
	this.input = jQuery(this.sel);
	this.terms = null;
	this.taxonomy = this.input.attr('taxonomy');
	this.selected = this.input.val();

	this.init();
}

PostTerm.prototype = {
	init: function(){
		
		var T = this;
		//this.getTerms(this.taxonomy);
		this.input.selectize({
			maxItems: null,
			
		});
	},
	getTerms: function(taxonomy){
		var W = this;
		jQuery.post({url:ajaxurl, data:{action: "widgetCardsGetTerms", wctax:taxonomy}}, function(r, s){
			if (r.success){
				W.terms = r.data;
				W.render();
			}
			else {
				alert(r.data);
			}
		});
	},
	render: function(){
		var W = this;
		if (!this.terms) {
			
			alert('no data');
			return;
		}
		console.log(this.selected);
		for (k in this.terms){

			var selected =  (this.selected && this.selected.indexOf(parseInt(k)) > -1)?' selected="selected" ':'';
			this.input.append('<option value="'+k+'" '+selected+'>'+this.terms[k]+'</option>');
		}
	}

}
jQuery(document).ready(function(){
	PS = new PostSettings("#post-settings");
});