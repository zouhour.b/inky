<div tpl="post-settings" data-post-id="<?php echo get_the_ID() ?>" class="modal fade" id="inkube-modal-post-settings">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">Post Settings</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div id="inkube-post-settings-tabs">
					<ul class="nav nav-tabs">
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-1">Cover</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-2">Extraits et titres</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-3">Footer</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-4">Classification</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-5">Contributeurs</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-6">Références</a></li>
					</ul>
					<div id="inkube-post-settings-tabs-1 row">
						<div class="col-md-6">
							<div class="form-group">
								<select class="form-control" data-meta-key="wpcf-cover-layou">
									<option value="default">Par défaut</option>
									<option value="classic">Classique</option>
									<option value="en-clair">En Clair</option>
									<option value="video-background">Video</option>
								</select>
							</div>

							<div class="form-group">
								<input class="form-control" data-meta-key="wpcf-cover-video">
							</div>

						</div>
						<div id="inkube-post-settings-tabs-2 row">
						</div>
						<div id="inkube-post-settings-tabs-3 row">
						</div>

						<div id="inkube-post-settings-tabs-4 row">

						</div>
						<div id="inkube-post-settings-tabs-5 row">

						</div>
						<div id="inkube-post-settings-tabs-6 row">

						</div>
					</div>
					<p>Modal body text goes here.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		inkubeNonce ='<?php echo wp_create_nonce('inkube') ?>';
	</script>
	<script src="<?php echo inkube::assets_url('assets/js/postmetaController.js') ?>"></script>
	<script src="<?php echo inkube::assets_url('assets/js/postSettings.js') ?>"></script><div tpl="post-settings" data-post-id="<?php echo get_the_ID() ?>" class="modal fade" id="inkube-modal-post-settings">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">Post Settings</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div id="inkube-post-settings-tabs">
					<ul class="nav nav-tabs">
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-1">Cover</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-2">Extraits et titres</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-3">Footer</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-4">Classification</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-5">Contributeurs</a></li>
						<li class="nav-item"><a class="nav-link" href="#inkube-post-settings-tabs-6">Références</a></li>
					</ul>
					<div id="inkube-post-settings-tabs-1 row">
						<div class="col-md-6">
							Layout:
							<div class="form-group">
								<select class="form-control" data-meta-key="wpcf-cover-layout">
									<option value="default">Par défaut</option>
									<option value="classic">Classique</option>
									<option value="en-clair">En Clair</option>
									<option value="video-background">Video</option>
								</select>
							</div>

							<div class="form-group">
								<input class="form-control" data-meta-key="wpcf-cover-class">
							</div>
							<div class="form-group">
								<input class="form-control" data-meta-key="wpcf-cover-panel-class">
							</div>

						</div>
						<div id="inkube-post-settings-tabs-2 row">
						</div>
						<div id="inkube-post-settings-tabs-3 row">
						</div>

						<div id="inkube-post-settings-tabs-4 row">

						</div>
						<div id="inkube-post-settings-tabs-5 row">

						</div>
						<div id="inkube-post-settings-tabs-6 row">

						</div>
					</div>
					<p>Modal body text goes here.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		inkubeNonce ='<?php echo wp_create_nonce('inkube') ?>';
	</script>
	<script src="<?php echo inkube::assets_url('assets/js/postmetaController.js') ?>"></script>
	<script src="<?php echo inkube::assets_url('assets/js/postSettings.js') ?>"></script>