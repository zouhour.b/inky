<?php 
add_action('wp_ajax_fix_content', 'fix_content');
function fix_content(){
	if (current_user_can('edit_posts')){
		$data = base64_decode($_POST['content']);
		$scripts = base64_decode($_POST['scripts']);
		$styles = base64_decode($_POST['styles']);
		$status = $_POST['status'];
		$post_id = (int)$_POST['post_id'];
		
		if ($data && $post_id){
			$r = wp_update_post(array('ID'=>$post_id, 'post_content'=>$data));
			update_post_meta($post_id, 'wpcf-scripts', $scripts);
			update_post_meta($post_id, 'wpcf-styles', $styles);
			wp_set_object_terms($post_id, array($status), 'migration-status', false);
			if ($r){
				wp_send_json_success(array('message'=>'Updated'));
				die();
			}
			ob_start();
			echo $data;
			$ajaxerr = ob_get_clean();
			file_put_contents(ABSPATH.'/ajax-error.php', $ajaxerr);
			wp_send_json_error(array('message'=>"Ajax error has been thrown"));	
			die();
		}
		wp_send_json_error(array('message' => 'id is null or data is empty' ));
		die();
	}
	wp_send_json_error(array('message'=>'user is not allowed'));
}