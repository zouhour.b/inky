<?php 

Class InkyfadaMigration{
	public function __construct(){

	}

	public function run(){
		$ids = $this->get_posts();
		foreach ($ids as $row){
			$this->migrateScripts($row['ID']);

		}
	}

	public function migratePost($post_id){
		$post = get_post($post_id);
		$this->migrateScripts($post_id);



	}


	public function get_posts(){
		global $wpdb;
		$q = $wpdb->get_results('SELECT ID from '.$wpdb->posts.' where post_type="post" and post_status="publish"');
		return $q;
	}

	static function getSVGFiles($dir){
		$files = scandir($dir);
		var_dump($files);
	}

	public function migrateSVG(){
		$dir
		$svgdir = WP_CONTENT_DIR.'/uploads/svg/';
		//copy($svgfile, $svgdir.'/'.$svgfile);
	}

	public function migrateTerm($term){
		
		if (is_arabic($term->name)){
			pll_set_term_language($term->term_id, 'ar');
		}else{
			pll_set_term_language($term->term_id, 'fr');	
		}
	}

	public function migrateScripts($post_id){
		$post = get_post($post_id);
		$content = $post->post_content;
		$scripts = '';
		$styles = '';
		//$content = $content.'<style>.bod{display: none}</style>';
		if ($l = preg_match_all('#(<style[^>]*>.*?<\/style>)#s', $content, $matches)){
			//var_dump($matches);
			$styles = implode('',$matches[0]);
			$content = preg_replace('#<style(.*?)<\/style>#is', '', $content);
			
		}
		if ($l = preg_match_all('#(<script[^>]*>.*?<\/script>)#is', $content, $matches)){
			$scripts = implode('', $matches[0]);
			$content=preg_replace('#(<script[^>]*>.*?<\/script>)#is', '', $content);
		}	
		if ($scripts){
			update_post_meta($post_id, 'wpcf-scripts', $scripts);
			wp_set_object_terms($post_id, 'has script', 'migration-status', true);
		}

		if ($styles){
			wp_set_object_terms($post_id, 'has style', 'migration-status', true);
			update_post_meta($post_id, 'wpcf-styles', $styles);
		}
		if ($styles || $scripts){
			wp_update_post(array('ID'=>$post_id, 'post_content'=>$content));
		}
		
			//var_dump($styles);
			//var_dump($scripts);	
	}

	public function migratePostAuthors($post_id){
		$authors = get_post_meta($post_id, 'inky_authors', false);
		
		if ($authors){
			update_post_meta($post_id, '_authors', $authors);
		}
	}
}