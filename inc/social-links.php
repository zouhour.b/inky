<?php 


class SocialLinks extends WP_Widget{
    public static $default = array(
        'title' => '',
        'facebook'=>"https://facebook.com",
        'twitter'=>'https://twitter.com',
        'instagram' =>'https://instagram.com',
        'youtube'=>'https://youtube.com'
        );
    public function __construct(){
        $widget_ops = array(
            'classname' => 'social-links-widget'
            );
        parent::__construct('social-media-links', 'Social Media', $widget_ops);
        self::$default['title'] = __('Follow us', 'assafir');
    }

    public function form($instance){
        $instance = wp_parse_args($instance, static::$default);
        foreach (self::$default as $k=>$v){
        	?>
			<p>
        <label for="<?php echo $this->get_field_id($k); ?>"><?php _e($k, 'assafir'); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id($k); ?>" name="<?php echo $this->get_field_name($k); ?>" type="text" value="<?php echo esc_attr($instance[$k]); ?>" />
    </p>

        	<?php 
        }
    }

    public function update($new_instance, $old_instance)
    {
        return $new_instance;
    }


   
    public function widget($args, $instance)
    {
        $instance = wp_parse_args($instance, self::$default);
        echo $args['before_widget'];
        if (isset($instance['title'])){
            echo $args['before_title'].$instance['title'].$args['after_title'];
        }
        unset ($instance['title']);
        echo '<ul class="social-media-links">';
        foreach ($instance as $k =>$v){
            if ($v): ?>
                <li class="social-media-link">
                    <a href="<?php echo $v ?>" target="_blank">
                        <i class="fa fa-<?php echo $k ?>"></i>
                    </a>
                </li>
                <?php do_action('append_to_social_links') ?>
            <?php endif; 

        }
        echo '</ul>';
    }
}

add_action('widgets_init', function()
{
    register_widget('SocialLinks');
});