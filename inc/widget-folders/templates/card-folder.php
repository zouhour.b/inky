<?php $img = get_term_meta($term_id,'wpcf-cover-image',true); ?>
<?php $items = get_posts(array('numberposts'=>$numberposts, 'folder'=>$term->slug));?>
<div class="card-cover" style="background-image: url(<?php echo $img; ?>);">
	<img src="<?php echo $img; ?>" />
</div>
<div class="card-caption">
	
	<div class="folder-details">
		<div class="folder-meta">
			<div class="folder-type">
				<a href="<?php echo $term->taxonomy ?>" class="badge post-badge folder">
            <?php _e('Dossier', 'inkyfada') ?>
        </a>
			</div>
			<div class="folder-title">
        <a href="<?php echo get_term_link($term->term_id) ?>">
          <?php echo $term->name;?>
        </a>
      </div>
			<div class="folder-desc"><?php echo $term->description;?></div>
		</div>
		<div class="folder-items">
			<?php foreach ($items as $item): ?>
				<div class="folder-item">
					<div class="post-title">
						<a href="<?php echo get_the_permalink($item->ID);?>" target="_blank">
							<?php echo apply_filters('the_title', $item->post_title);?>
						</a>
					</div>
					<div class="post-meta">
						<!--<span class="post-author">Par <?php the_author();?></span> -->
						<span class="post-date"><?php echo 'Le ' . get_the_date('j F Y');?></span>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

	</div>
</div>
<a href="<?php echo get_term_link($term_id);?>" target="_blank" class="more">
	<span class="icon icon-dots"></span>
</a>
