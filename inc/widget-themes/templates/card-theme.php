<?php $img = get_term_meta($term_id,'wpcf-cover-image',true); ?>
<?php $items = get_posts(array('numberposts'=>$numberposts, 'theme'=>$term->slug));?>

<div class="card-cover" style="background-image: url(<?php echo $img; ?>);">
	<img src="<?php echo $img; ?>" />
</div>

<div class="card-caption">
	
	<div class="theme-details">
		<div class="theme-meta">
			<div class="theme-type">
				<a href="<?php echo get_term_link($term) ?>" class="badge post-badge theme">
          <?php echo get_term_meta($term_id,'wpcf-title-secondary',true) ?>
        </a>
			</div>
			<div class="theme-title"><?php echo $term->name;?></div>
			<div class="theme-desc"><?php echo $term->description;?></div>
		</div>
		<div class="theme-items">
			<?php foreach ($items as $item): ?>
				<div class="theme-item">
					<div class="post-title">
						<a href="<?php echo get_the_permalink($item->ID);?>" target="_blank">
							<?php echo apply_filters('the_title', $item->post_title);?>
						</a>
					</div>
					<div class="post-meta">
						<!--<span class="post-author">Par <?php the_author();?></span> -->
						<span class="post-date"><?php echo get_the_date('l, j F Y');?></span>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

	</div>
</div>
<a href="<?php echo get_term_link($term_id);?>" target="_blank" class="more">
	<span class="icon icon-dots"></span>
</a>
