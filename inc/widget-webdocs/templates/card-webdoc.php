<?php $img = get_term_meta($term_id,'wpcf-cover-image',true); ?>

<div class="card-cover" style="background-image: url(<?php echo $img; ?>);">
	<img src="<?php echo $img; ?>" />
</div>

<div class="card-caption px-25">
	<div class="webdoc-details">
		<div class="webdoc-meta px-0 col-12 col-md-6">
			<div class="webdoc-type">
				<a href="<?php echo $term->taxonomy ?>" class="badge px-0 webdoc"><?php _e('Webdoc', 'inkyfada') ?></a>
			</div>
      <div class="webdoc-title"><a href="<?php echo get_term_link($term_id, 'webdoc') ?>"><?php echo $term->name;?></a></div>
			<div class="webdoc-desc"><?php echo $term->description;?></div>
		</div>
	</div>
</div>