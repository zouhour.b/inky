<?php

class WebdocWidget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'widget-webdoc'
            );
        parent::__construct('WebdocWidget', __('Webdoc', 'inkyfada'), $widget_ops);
    }


    public function widget($args, $instance) {
        $instance = wp_parse_args( $instance, self::$defaults );
        extract($instance);
        $term_id=(int)$term_id;
        
        $container .= ' card card-webdoc ';
        $term =  get_term_by('id', $term_id, 'webdoc');
        if (!$term) return;

        $args['before_widget'] = str_replace('class="', 'class="'.$container.' ' , $args['before_widget']);
        echo $args['before_widget'];
        
        include __DIR__.'/templates/card-webdoc.php';
        
        echo $args['after_widget'];
    }

    static $defaults = array(
        'container'=>'col-12',
        'term_id'=>0,
        'numberposts'=>3
        );

    public function form($instance)
    {

        $instance = wp_parse_args($instance, static::$defaults);

        extract($instance);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('container'); ?>"><?php _e('Container', 'inkyfada'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('container'); ?>" name="<?php echo $this->get_field_name('container'); ?>" type="text" value="<?php echo esc_attr($container); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('term_id'); ?>"><?php _e('Webdoc', 'inkyfada'); ?></label> 
            <?php  
                wp_dropdown_categories(array('taxonomy'=>'webdoc','name'=>$this->get_field_name('term_id'),'class'=>'widefat','selected'=>$term_id));
            ?>
        </p>


        <?php
    }

    public function update($new_instance, $old_instance) {
        return $new_instance;
    }

}

add_action('widgets_init', function() {
    register_widget('WebdocWidget');
});