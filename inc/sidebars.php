<?php 
function inkyfada_widgets_init() {
	$sidebars = array(
		"tools" => __('Tools', 'inkyfada'),
		"author"=>__('Author', 'inkyfada'),
		'header'=>__('Header', 'inkyfada'),
		"archive"=>__('Archive', 'inkyfada'),		
		"content"=>__('Content', 'inkyfada'),
		"footer"=>__('Footer', 'inkyfada')
		);
	
	foreach ($sidebars as $position=>$title){
		register_sidebar( array(
		'name'          => $title,
		'id'            => $position,
		'description'   => esc_html__( 'Add widgets here.', 'inkyfada' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s position-'.$position.'">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
	}
}
add_action( 'widgets_init', 'inkyfada_widgets_init' );
