<?php 
new LayoutsDefaultClasses();
Class LayoutsDefaultClasses{
	public function __construct(){
		add_filter('cover_class', array($this, 'cover_class_default'), 2, 2);
		add_filter('cover_panel_class', array($this, 'cover_panel_class_default'),2,2);
		add_filter('cover_layout', array($this, 'cover_layout'), 2,2);
		add_filter('content_layout', array($this, 'content_layout_default'), 2,2);
		add_action('after_content', array($this, 'after_content_default'));

		add_filter('card_options', array($this, 'card_options'), 3,3);

	}

	public function isFeatured($postid){
		$is_featured = get_post_meta($postid, '_is_featured', true);
		if ($is_featured){
			return true;
		}
		return false;
	}

	public function card_options($options, $id, $args=array()){
		$args = wp_parse_args($args, array('layout'=>'', 'index' => 0));

		switch ($args['layout']){

			case 'classic':
			$img_size_bottom = "md-1by1";
			return array(
				'img_size' => $img_size_bottom,
				'container_class' => 'col-12 badges-style-1 mb-5',
				'clscaption' => '',
				'post_class' => 'badges-style-1 card col-6 px-0 mt-3 card-full',
				'caption_class' => 'bottom bg-white mb-0 col-12 card-caption bg-white p-4',
				'title_tag' => 'h1',
				'excerpt_length' => 999,
				'newsletter_order' => 4
			);

			case 'skrollr':
			$img_size_default = "md-1by1";
			$img_size_bottom = "HD-16by9";
			$img_size = ($args['index']%3 == 2) ? $img_size_bottom : $img_size = $img_size_default;

			return array(
				'img_size' => $img_size,
				'container_class' => 'col-12 badges-style-1 mb-5',
				'clscaption' => '',
				'post_class' => 'badges-style-1 card col-12 px-0 mt-3 card-full',
				'caption_class' => 'bottom bg-white mb-0 col-md-8 offset-md-2 card-caption  bg-white p-4',
				'title_tag' => 'h1',
				'excerpt_length' => 999,
				'newsletter_order' => 2
			);
			case 'masonry':
			$img_size = "HD-16by9";
			$col_class = "col-12";
			$title_tag = 'h2';
			if($args['index']%5 == 0) {
				$col_class = "col-md-7 mt-2 pr-md-2";
				$caption_class = "col-md-10 bottom mb-5 pb-5";
				$title_tag = 'h2';
				$desc_class = "";
			} else if($args['index']%5 == 1) {
				$col_class = "col-md-5 mt-2";
				$caption_class = "col-md-10 bottom mb-5 pb-5";
				$title_tag = 'h2';
				$desc_class = "";
			} else if($args['index']%5 == 2)  {
				$col_class = "mt-2";
				$caption_class = "col-md-6 center vcenter";
				$title_tag = 'h2';
				$desc_class = "";
			} else if($args['index']%5 == 3)  {
				$col_class = "col-md-6 mt-2 pr-md-2";
				$caption_class = "col-md-10 bottom mb-5 pb-5";
				$title_tag = 'h3';
				$desc_class = "d-none";
			} else if($args['index']%5 == 4)  {
				$col_class = "col-md-6 mt-2";
				$caption_class = "col-md-10 bottom mb-5 pb-5";
				$title_tag = 'h3';
				$desc_class = "d-none";
			}
			return array(
				'img_size' => $img_size,
				'col_class' => $col_class,
				'caption_class' => $caption_class,
				'title_tag' => $title_tag,
				'desc_class' => $desc_class,
				'excerpt_length' => 999,
				'template' => 'card-theme',
				'newsletter_order' => 2
			);
			case 'author':
			$contribs = get_post_meta(get_the_ID(), '_contributors', false);
			$contributions = [];
			foreach ($contribs as $contrib){
				foreach ($contrib as $c){
					if ($c['user_id'] == $args['author']){
						if($c['role']=='author')
							$contributions[] = __('Auteur', 'inkyfada');
						else
							$contributions[] = $c['role'];
					}
				}
			}

			$html = [];
			$cnt = count($contributions);
			foreach ($contributions as $k=>$contribution){
				if ($k>0 && $k<$cnt-1) $html[]= ', ';
				else if ($cnt>1 && $k==$cnt-1) $html[]= ' et ';
				$html[] = $contribution;
			}
			$contributions = implode('',$html);
			default:
        // Archive & Author
			$genres = wp_get_object_terms( $id, 'genre', array('fields' => 'slugs') );
			if ($this->isFeatured($id)){
				$post_class = 'card-archive-style-full badges-style-1 card h-100vh col-12 p-0 mask-dark mb-5';
				$caption_class = 'card-caption vcenter col-md-6 offset-md-2';
				$img_size = 'large';
				$title_tag = 'h1';
				$excerpt_length = '9999';
			} else if(array_intersect(array('explication', 'stouchi'), $genres)){
				$post_class = 'badges-style-1 card col-md-8 offset-md-2 style-regular mb-5';
				$caption_class = 'card-caption col-md-6';
				$img_size = 'HD-16by9';
				$title_tag = 'h3';
				$excerpt_length = 20;
			} else {
				$post_class = 'badges-style-1 card-archive-style-medium card col-md-8 offset-md-2 style-medium mb-5';
				$caption_class = 'card-caption col-md-12 py-0';
				$img_size = 'HD-16by9';
				$title_tag = 'h1';
				$excerpt_length = 999;
			}
			return array(
				'post_class' => $post_class,
				'caption_class' => $caption_class,
				'img_size' => $img_size,
				'title_tag' => $title_tag,
				'excerpt_length' => $excerpt_length,
				'contributions' => (isset($contributions))?$contributions:''
			);
			break;
		}    
	}

	public function after_content_default(){
		if (has_term('stouchi', 'genre', get_the_ID())){ ?>
		<div class="row py-4">
			<div class="col-md-4 offset-md-4 text-center">
				<?php the_social_share(2); ?>
			</div>
		</div>
		<?php 
		$stouchis = get_posts(array('genre'=>'stouchi', 'numberposts'=>3, 'exclude'=>array(get_the_ID())));
		$stouchis_ids = [];
		foreach ($stouchis as $stouchi){
			$stouchis_ids[] = $stouchi->ID; 
		}
		?>
		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="col-md-12 relatedPosts-stouchi-header">
					Plus de <span class="post-badge badge stouchi">stouchi</span>
				</div>
				<?php echo do_shortcode( '[relatedPosts ids="'.implode($stouchis_ids, ', ').'" class="row style-2"]' 
				);?>
			</div>	
		</div>
		<?php
	}
	if (has_term('infographie', 'genre', get_the_ID())){?>
	<div class="row py-4">
		<div class="col-md-4 text-left">
			<?php the_social_share(2); ?>
		</div>
	</div>
	<?php if(get_post_meta(get_the_ID(), 'wpcf-related', true)){ ?>
	<div class="row">
		<div class="mx-4 my-2 fullwidth border-bottom-dotted-dark"></div>
		<h4 class="fw-300 uppercase px-4"><?php _e('RETROUVEZ CES INFOGRAPHIES DANS LEUR CONTEXTE', 'inkyfada'); ?></h4>
		<?php echo do_shortcode('[relatedPosts ids="'.get_post_meta(get_the_ID(), 'wpcf-related', true).'" class="h-100vh" layout="footer"]'); ?>
	</div>
	<?php } ?>
	<?php if(get_post_meta(get_the_ID(), 'wpcf-suggestions', true)){ ?>
	<div class="row">
		<div class="mx-4 my-2 mt-5 fullwidth border-bottom-dotted-dark"></div>
		<h4 class="fw-300 uppercase px-4"><?php _e('SUR LE MEME SUJET', 'inkyfada'); ?></h4>
		<?php 
		$suggestions = explode(",", get_post_meta(get_the_ID(), 'wpcf-suggestions', true)); 
		?>
	</div>
	<div class="row">
		<div class="card col-md-6 classic p-1 h-md-70vh">
			<?php echo do_shortcode('[relatedPosts ids="'.$suggestions[0].'" class="h-75vh" layout="infographie"]'); ?>
		</div>
		<div class="card col-md-6 classic p-1 h-md-70vh">
			<?php echo do_shortcode('[relatedPosts ids="'.$suggestions[1].'" class="h-75vh" layout="infographie"]'); ?>
		</div>
	</div>

	<?php } ?>
	<section class="col-12 p-0 mt-2 mb-5">
		<?php echo do_shortcode( '[inky_mailchimp]' ); ?>
	</section>
	<?php }else { ?>
	<div class="row py-4 share-container">
		<div class="col-md-4 offset-md-4 text-center">
			<?php the_social_share(2); ?>
		</div>
	</div>
	<div class="row px-1 footer-content outro-content">
		<?php if(get_post_meta(get_the_ID(), 'wpcf-author-note', true)): ?>
			<div class="col-md-4">
				<h4 class="uppercase fw-300 py-2 border-bottom-dotted-dark"><?php _e("Note de l'auteur", "inkyfada"); ?></h4>
				<p><?php echo get_post_meta(get_the_ID(), 'wpcf-author-note', true); ?></p>
			</div>
			<div class="col-md-4">
				<?php $this->after_content_authors() ?>
			</div>
			<div class="col-md-4">
				<?php $this->after_content_contributers() ?>
			</div>
		<?php else: ?>
			<div class="col-md-4 offset-md-2">
				<?php $this->after_content_authors() ?>
			</div>
			<div class="col-md-4">
				<?php $this->after_content_contributers() ?>
			</div>
		<?php endif ?>
	</div>
	<?php if(get_post_meta(get_the_ID(), 'wpcf-related', true)): ?>
		<div class="row">
			<div class="mx-4 my-2 fullwidth border-bottom-dotted-dark"></div>
			<h4 class="fw-300 uppercase px-4"><?php _e('ça peut vous intéresser', 'inkyfada'); ?></h4>
			<?php echo do_shortcode('[relatedPosts ids="'.get_post_meta(get_the_ID(), 'wpcf-related', true).'" class="h-100vh" layout="footer"]'); ?>
		</div>
	<?php endif ?>
	<?php
}	
}

public function after_content_authors(){
	$contribs = get_post_meta(get_the_ID(), '_contributors', false);
	$authors = [];
	foreach ($contribs as $contrib){
		foreach ($contrib as $c){
			if($c['role']=='author')
				$authors[] = array(
					'id' => $c['user_id'],
					'avatar' => get_avatar( $c['user_id'], 128 ),
					'name' => $c['name'],
					'description' => get_user_meta($c['user_id'], 'description', true),
					'twitter' => get_user_meta($c['user_id'], 'user_twitter', true),
					'mail' => get_user_meta($c['user_id'], 'user_mail', true)
				);
		}
	}
	?>
	<h4 class="uppercase fw-300 py-2 border-bottom-dotted-dark"><?php _e("Écrit par", "inkyfada"); ?></h4>
	<?php foreach ($authors as $author): ?>
		<div class="row my-3 author">
			<div class="author-avatar col-3 text-center"><?php echo $author['avatar']; ?></div>
			<div class="author-infos col-9">
				<h4 class="uppercase"><?php echo $author['name']; ?></h4>
				<p class="fs-7"><?php echo $author['description']; ?></p>
				<ul class="list-inline pl-0 fs-7">
					<li class="list-inline-item"><span class="icon-twitter-2 mr-2"></span>@<?php echo $author['twitter']; ?></li>
					<li class="list-inline-item"><span class="icon-mail-2 mr-2"></span><?php echo $author['mail']; ?></li>
				</ul>
			</div>
		</div>
	<?php endforeach; ?>
	<?php
}

public function after_content_contributers(){
	$contribs = get_post_meta(get_the_ID(), '_contributors', false);
	$contributers = [];
	foreach ($contribs as $contrib){
		foreach ($contrib as $c){
			if($c['role']!=='author'){


				if(isset($contributers[$c['user_id']]))
					$contributers[$c['user_id']]['role'] .= '<br>' . $c['role'];
				else
					$contributers[$c['user_id']] = array(
						'id' => $c['user_id'],
						'role' => $c['role'],
						'avatar' => get_avatar($c['user_id'], 64 ),
						'name' => $c['name'],
					);
			}
		}
	}
	?>
	<h4 class="uppercase fw-300 py-2 border-bottom-dotted-dark"><?php _e("Avec la collaboration", "inkyfada"); ?></h4>
	<?php foreach ($contributers as $contributer): ?>
		<div class="row contributer my-3">
			<div class="contributer-avatar col-3 text-center"><?php echo $contributer['avatar']; ?></div>
			<div class="contributer-infos col-9">
				<h5 class="uppercase"><?php echo $contributer['name']; ?><span class="role capitalize gray-light fw-300"><?php echo $contributer['role']; ?></span></h5>
			</div>
		</div>
	<?php endforeach; ?>
	<?php
}

public function content_layout_default($class, $id=null){
	if (!$id) $id = get_the_ID();
	if ($class && $class!='default'){
		return $class;
	}

	if (has_term('explication', 'genre', $id)){
		return 'content-layout-en-clair';
	}
	if (has_term('stouchi', 'genre', $id)){

		return 'content-layout-stouchi';
	}
	return 'content-layout-default';

}


public function cover_class_default($class, $layout){
	if ($class && $class!='default'){
		return $class;
	}


	switch ($layout){
		case 'en-clair':
		case 'stouchi':
		return 'fullwidth h-75vh mask-dark';
		break;

		case 'infographie':
		return 'fullwidth h-50vh';
		break;

		default:
		return "h-100vh mask-dark";
		break;
	}

}
public function cover_panel_class_default($class, $layout=""){
	if ($class && $class!='default'){
		return $class;
	}
	switch ($layout){
		case 'infographie':
		return "col-md-10 offset-md-1 vcenter left";
		break;

		default:
		return "col-md-6 offset-md-2 vcenter left";
		break;

	}
}

public function cover_layout($layout, $id){
	if ($layout && $layout!="default"){
		return $layout;
	}
	if(has_term('infographie', 'genre', $id)){
		return 'infographie';
	}
	if (has_term('explication', 'genre', $id)){
		return 'en-clair';
	}
	if (has_term('stouchi', 'genre', $id)){
		return 'stouchi';
	}
	return $layout;
}

}

