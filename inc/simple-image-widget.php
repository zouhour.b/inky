<?php

class SimpleImageWidget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'SimpleImageWidget'
        );
        parent::__construct('SimpleImageWidget', 'Simple Image', $widget_ops);
    }

    public function widget($args, $instance) {
        $instance = wp_parse_args($instance, static::$defaults);
        extract($instance);
        if (!$image)
            return;
        $args['before_widget'] = str_replace('class="', 'class="'.$container.' ' , $args['before_widget']);
        echo $args['before_widget']; 
        ?>
            <a class="thumbnail" href="<?php echo $link ?>" >
                <img width="100%" height="auto" src="<?php echo $image ?>">
            </a>

        <?php echo $args['after_widget']; 
        }

        static $defaults = array(

        "link" =>"",
        "image" => "",
        "container" => "col-12",
        'title' => '',

        );

        public function form($instance)
        {

        $instance = wp_parse_args($instance, static::$defaults);

        extract($instance);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'assafir'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('container'); ?>"><?php _e('container', 'assafir'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('container'); ?>" name="<?php echo $this->get_field_name('container'); ?>" type="text" value="<?php echo esc_attr($container); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('image'); ?>"><?php _e('image', 'assafir'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>" type="text" value="<?php echo esc_attr($image); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('link', 'assafir'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo esc_attr($link); ?>" />
        </p>


        <?php
    }

    public function update($new_instance, $old_instance) {
        return $new_instance;
    }

}

add_action('widgets_init', function() {
    register_widget('SimpleImageWidget');
});


