<?php
/**
 * Plugin Name: Push Tools
 * Description: 
 * Plugin URI: http://example.org/
 * Author: nofearinc
 * Author URI: http://devwp.eu/
 * Version: 1.6
 * Text Domain: dx-sample-plugin
 * License: GPL2
*/

/**
 * Get some constants ready for paths when your plugin grows 
 * 
 */

Class StyleToRtl{
	public function __construct($input, $out){
		if (strpos(home_url(), '.localhost')>0 ) {
			$file 	= get_template_directory().'/assets/css/'.$input.'.css';
			$output = get_template_directory().'/assets/css/'.$out.'.css';
			if (!file_exists($file)){
				var_dump("FILE  $file not found!");
				die();
			}
			$css = file_get_contents($file);

			$css = str_replace('navbar-left', 'navbar-l', $css);
			$css = str_replace('navbar-right', 'navbar-r', $css);
			$css = str_replace('left', 'wasleft', $css);
			$css = str_replace('right', 'left', $css);
			$css = str_replace('wasleft', 'right', $css);
			$css = str_replace('navbar-l', 'navbar-left', $css);
			$css = str_replace('navbar-r', 'navbar-right', $css);
			$css .= ' body{direction: rtl; unicode-bidi: embed;}';
			file_put_contents($output, $css);
			$this->count = filesize($output)/1000;
			add_action('admin_notices', array($this, 'notice'));

		}
	}

	public function notice(){
		echo '<div class="notice is-dismissible notice-success"><p>RTL converter is running...'.$this->count.'Ko written</p>
		<button type="button" class="notice-dismiss">
			<span class="screen-reader-text">Dismiss this notice.</span>
		</button></div>';
	}
}

new StyleToRTL('main', 'main.rtl');


