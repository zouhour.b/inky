jQuery.fn.mailchimpNewsletter = function(settings){
  console.log(settings);
  if (!settings || !settings.listId) {
    jQuery(this).before('<div class="alert alert-danger"><span class="fa fa-exclamation-circle">Missing Seetings (ListId)</div>')
    return;
  }
  jQuery(this).on('submit', function(e){
    var form = jQuery(this);
    e.preventDefault();
    var email = form.find('[type="email"]').val();

    jQuery.ajax({
      type: 'POST',
      context: this,
      url: settings.apiUrl,
      data: {
        email: email
      },
      success: function(data, textStatus, XMLHttpRequest){
        console.log(data);
        if (data.status==='error'){
          jQuery('.newsletter-message').html(data.error);
        }
        else{
            jQuery('.newsletter-message').html('Merci pour votre inscription');
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown){
        console.log(XMLHttpRequest, errorThrown);
        jQuery('.newsletter-message', form).html();
      }
    });
    return false;


  });
}