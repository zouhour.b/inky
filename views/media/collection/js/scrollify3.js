
$.fn.scrollify = function(options){
	let container = $(this);
	let args = {transitionDuration: 500, lockTimeout:1200, ...options}
	let wrapper = container.find('.screen-wrapper');
	let wh = $(window).height();
	let state = container.data('ink.scrollify')
	if (!state) state = {
		isLocked:false,
		currentSlide: 0,
		position:'before',
		slidesNumber: container.find('.slide').length
	}

	function setState(newState){
		if (state.isLocked) return;
		if (state.currentSlide!==newState.currentSlide || state.currentStep!=newState.currentStep){
			state = {...state, ...newState};
			container.data('ink.scrollify', state);
			update();
		}
	}

	function slideHasCaption(slide){
	return getSlide(slide).find('.caption') && getSlide(slide).find('.caption').text().trim()	
	}

	function handle(e){
		if (e.direction=='down' && state.currentSlide<=state.slidesNumber){
			if (slideHasCaption(state.currentSlide)){
				if (state.currentStep==0){
					setState({currentSlide:state.currentSlide, currentStep:1, direction:e.direction});
					return;
				}

			}
			setState({currentSlide:state.currentSlide+1,  currentStep:0, direction:e.direction});
			return;
		}
		else if (e.direction=='up' && state.currentSlide>=1){
			if (slideHasCaption(state.currentSlide)){
				if (state.currentStep==1){
					setState({currentSlide:state.currentSlide, currentStep:0, direction:e.direction});
					return;
				}
				else{
					if (state.currentSlide===1){
						setState({currentSlide:0,  currentStep:0, direction:e.direction});
						return;
					}

					setState({currentSlide:state.currentSlide-1,  currentStep:1, direction:e.direction});
					return;
				}

			}
			else{
					setState({currentSlide:state.currentSlide-1,  currentStep:0, direction:e.direction});
					return;
			}
		}
		

	}

	function getSlide(j){
		let slide = wrapper.find('.slide:nth-child('+j+')');
		return slide;
	}

	function update(){
		let currentSlide = state.currentSlide;
		for (var j = 1; j<=state.slidesNumber; j++){
			let className = '';
			if (j<state.currentSlide) {
				className = 'slide past';
			}
			else if (j==currentSlide){
				className = 'slide active';
			}
			else{
				className = 'slide future';
			}
			wrapper.find('.slide:nth-child('+j+')').attr('class', className);
		}
		if (state.currentStep==0){
			getSlide(state.currentSlide).find('.step-1').fadeOut('slow');
		}
		else{
			getSlide(state.currentSlide).find('.step-1').fadeIn('slow');

		}
		if (state.currentSlide==0){
			exitUp();
		}
		else if (state.currentSlide > state.slidesNumber){
			exitDown();
		}
		else {
			if (state.currentSlide>=1 && state.currentState<=state.slidesNumber){
				$(window).scrollTop(container.offset().top);
			}
			enter();
		}


	}
	function lock(){
		
		container.data('ink.scrollify', {...state, isLocked:true});
		setTimeout(e=>{
			unlock()
		}, args.lockTimeout);
	}

	function unlock(){
		container.data('ink.scrollify', {...state, isLocked:false});
	}

	function enter(){
		if (state.isLocked) return;
		lock();
		$('body, html').animate({
			'scrollTop':container.offset().top,
		}, {duration:args.transitionDuration});
	}

	function exitDown(){
		if (state.isLocked) return;
		lock();
		const wh = $(window).height();
		$('body, html').animate({
			'scrollTop':container.offset().top+container[0].getBoundingClientRect().height+20,
		}, {duration:args.transitionDuration});
	}
	function exitUp(){
		if (state.isLocked) return;
		lock();
		const wh = $(window).height();
		$('body, html').animate({
			'scrollTop':container.offset().top -wh,
		}, {
			duration:args.transitionDuration
		});

	}
	if (options.event){

		handle(options.event);
	}
}

function initScrollify(){
	let  preventDefault = (e)=> {
		e = e || window.event;
		if (e.preventDefault)
			e.preventDefault();
		e.stopPropagation();
		e.returnValue = false; 

	}

	let getActiveGallery = () =>{
		let isInViewPort = (el)=>{
			const wh = $(window).height();
			const bbox = el.getBoundingClientRect();

			return (bbox.top<=wh && bbox.bottom>0)
		}
		let slideshow = null;
		$('[data-scrollify]').each(function(i,el){
			if (isInViewPort(el)){
				slideshow = el;
			}
		});
		return slideshow;
	}


	let onScroll = (e)=>{

		var gallery  =getActiveGallery();
		if (gallery){
			preventDefault(e);
			if (e.deltaY>0){
				e.direction = 'down';
			}
			if (e.deltaY<0){
				e.direction  = 'up';
			}
			$(gallery).scrollify({event:e});
			return false;
		}	
	}

	let onKeydown = (e)=>{
		const keys = {33:1, 34:1, 37: 1, 38: 1, 39: 1, 40: 1};
		var gallery  = getActiveGallery();
		if (keys[e.keyCode] && gallery) {
			preventDefault(e);
			if (e.keyCode === 40 || e.keyCode === 34){
				e.direction = 'down';
			}
			if (e.keyCode ===38 || e.keyCode===33){
				e.direction = 'up';
			}

			$(gallery).scrollify({event:e});
			return false;
		}
	}

	if (window.addEventListener) 
		window.addEventListener('DOMMouseScroll', onScroll, false);
	document.body.addEventListener('touchstart', onScroll) 
	document.body.addEventListener('touchmove', onScroll) 
	window.onwheel = onScroll; 
	window.onmousewheel = document.onmousewheel = onScroll; 
	window.ontouchmove  = onScroll;
	document.onkeydown  = onKeydown;
	document.onkeyup  = onKeydown;
}

initScrollify();