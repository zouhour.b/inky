window.isScrolling = false;
class ScrollGallery{
	constructor(el, args){
		this.container = el;
		this.args = {transitionDuration: 500, lockTimeout:1000, ...args}
		if (!el || el.length!==1){
			alert('invalid element passed to ScrollGallery in Scrollify.js');
		}
		this.wrapper = $(this.container).find('.screen-wrapper');
		let wh = $(window).height();
		this.state={
			currentSlide: 0,
			position:'before',
			top: this.getCoords().top,
			slidesNumber: el.find('.slide').length
		}

	}

	getCoords(){
		return this.container[0].getBoundingClientRect();
	}

	setState(state){
		if (this.isLocked) return;
		if (this.state.currentSlide!==state.currentSlide || this.state.currentStep!=state.currentStep){
			this.state = {...this.state, ...state};
			this.update();
		}
	}

	handle(e){
		if (e.direction=='down' && this.state.currentSlide<=this.state.slidesNumber){
			if (this.getSlide(this.state.currentSlide).find('.caption')){
				if (this.state.currentStep==0){
					this.setState({currentSlide:this.state.currentSlide, currentStep:1, direction:e.direction});
					return;
				}

			}
			this.setState({currentSlide:this.state.currentSlide+1,  currentStep:0, direction:e.direction});

		}
		else if (e.direction=='up' && this.state.currentSlide>=1){
			if (this.getSlide(this.state.currentSlide).find('.caption')){
				if (this.state.currentStep==1){
					this.setState({currentSlide:this.state.currentSlide, currentStep:0, direction:e.direction});
					return;
				}

			}
			this.setState({currentSlide:this.state.currentSlide-1,  currentStep:1, direction:e.direction});
		}
		else {
			this.enter();
		}

	}

	getSlide(j){
		return this.wrapper.find('.slide:nth-child('+j+')');
	}

	update(){
		let currentSlide = this.state.currentSlide;
		for (var j = 1; j<=this.state.slidesNumber; j++){
			let className = '';
			if (j<this.state.currentSlide) {
				className = 'slide past';
			}
			else if (j==currentSlide){
				className = 'slide active';
			}
			else{
				className = 'slide future';
			}
			this.wrapper.find('.slide:nth-child('+j+')').attr('class', className);
		}
		if (this.state.currentStep==0){
			this.getSlide(this.state.currentSlide).find('.step-1').fadeOut('slow');
		}
		else{
			this.getSlide(this.state.currentSlide).find('.step-1').fadeIn('slow');

		}
		if (this.state.currentSlide==0){
			this.exitUp();
		}
		else if (this.state.currentSlide > this.state.slidesNumber){
			this.exitDown();
		}
		else {
			if (this.state.currentSlide>=1 && this.state.currentState<=this.state.slidesNumber){
				$(window).scrollTop(this.container.offset().top);
			}
			this.enter();
		}

	}
	lock(){
		
		this.isLocked = true;
		setTimeout(e=>{
			this.unlock()
		}, this.args.lockTimeout);
	}

	unlock(){
		this.isLocked = false;
	}

	enter(){
		if (this.isLocked) return;
		this.lock();
		$('body, html').animate({
			'scrollTop':this.container.offset().top,
		}, {duration:this.args.transitionDuration});
	}

	exitDown(){
		if (this.isLocked) return;
		this.lock();
		const wh = $(window).height();
		$('body, html').animate({
			'scrollTop':this.container.offset().top+this.getCoords().height+20,
		}, {duration:this.args.transitionDuration});
	}
	exitUp(){
		if (this.isLocked) return;
		this.lock();
		const wh = $(window).height();
		$('body, html').animate({
			'scrollTop':this.container.offset().top -wh,
		}, {
			duration:this.args.transitionDuration
		});

	}


}

class Scrollifier{
	constructor(args){
		this.galleries = [];
		this.init();
	}

	init(){
		if (window.addEventListener) 
			window.addEventListener('DOMMouseScroll', e=>this.onScroll(e), false);
		window.onwheel = e=>this.onScroll(e); 
		window.onmousewheel = document.onmousewheel = e=>this.onScroll(e); 
		window.ontouchmove  = e=>this.onScroll(e); 
		document.onkeydown  = e=>this.onKeydown(e);
		
		document.onkeyup  = e=>this.onKeydown(e);
	}

	onScroll(e){
		var gallery  =this.getActiveGallery();
		if (gallery){
			this.preventDefault(e);
			if (e.deltaY>0){
				e.direction = 'down';
			}
			if (e.deltaY<0){
				e.direction  = 'up';
			}
			gallery.handle(e);
			return false;
		}	
	}

	preventDefault(e) {
		e = e || window.event;
		if (e.preventDefault)
			e.preventDefault();
		e.stopPropagation();
		e.returnValue = false; 

	}

	onKeydown(e){
		const keys = {33:1, 34:1, 37: 1, 38: 1, 39: 1, 40: 1};
		var gallery  =this.getActiveGallery();
		if (keys[e.keyCode] && gallery) {
			this.preventDefault(e);
			if (e.keyCode === 40 || e.keyCode === 34){
				e.direction = 'down';
			}
			if (e.keyCode ===38 || e.keyCode===33){
				e.direction = 'up';
			}

			gallery.handle(e);
			return false;
		}
	}


	getActiveGallery(){
		const wh = $(window).height();
		let isInActiveZone = false;
		this.galleries.map(gallery=>{
			const bbox = gallery.getCoords();
			if (bbox.top<=wh && bbox.bottom>0){
				isInActiveZone = gallery;
			}
		});
		return isInActiveZone;
	}

	load(el){
		this.galleries.push(new ScrollGallery(el));
	}
}

window.mainScrollifier = new Scrollifier();

