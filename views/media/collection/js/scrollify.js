jQuery.fn.fpGallery = function(){
	if (jQuery(this).attr('loaded')) return;
	var container = $(this);
	var currentSlide = 0;
	var currentStep = 0;
	var isInTransition = false;
	var slidesNumber = container.find('.slide').length;	
	var currentPos = 0;

	function init(){
		currentSlide = getInitialSlide();

		jQuery(window).on('wheel', function(e){
			let direction = e.originalEvent.wheelDelta;
			if (isOnScreen()){
				e.preventDefault();
				e.stopPropagation();
				const lockDelay = Math.min(2000, Math.max(1000,Math.abs(direction)*10));
				handle(direction, lockDelay );	
			}

		});
		setTimeout(function(){

			jQuery('#go-to-top').click(function(){
				currentSlide = 0;
				currentStep = 0;
			});
		}, 1000);
		jQuery(window).on('touchstart', function(e){
			currentPos = e.originalEvent.touches[0].clientY;
		});
		jQuery(window).on('swipe', function(e){
			if (isOnScreen()){
				e.preventDefault();
			}
		});

		jQuery(window).on('touchend', function(e){
			let direction =   e.originalEvent.changedTouches[0].clientY - currentPos  ;
			if (isOnScreen()){
				e.stopPropagation();
				const lockDelay = Math.min(2000, Math.max(1000,Math.abs(direction)*10));
				handle(direction, 0 );	
			}

		});
		jQuery(window).on('keydown keyup', function(e){
			if (!isOnScreen()) return;
			if (e.keyCode == 34 || e.keyCode==40){
				e.preventDefault();
				e.stopPropagation();
				handle(-1, 0);
			}
			if (e.keyCode == 38 || e.keyCode==33){
				e.preventDefault();
				e.stopPropagation();
				handle(1,0);
			}

		});
		

		jQuery(window).resize(function(){
			if (isOnScreen()){
				initCaptions();
				goTo(currentSlide, 0);

			}
		});
		initCaptions();
		if (isOnScreen()){
			goTo(currentSlide, 0);
		}
		container.attr('loaded', 'loaded');
	}

	function hideCaptions(){
		container.find('.slide-caption').css('background', 'rgba(0,0,0,.5)').hide(0);
	}

	function initCaptions(){
		if (isDesktop()){
			hideCaptions();
		}
		else{
			container.find('.slide-caption').css('background', '').fadeIn(500);
			container.find('.slide').each(function(){
				if ($(this).find('figcaption').text().trim().length<2){
					$(this).find('.mobile-image-container').css({
						position:"absolute",
						top:'50%',
						left:0,
						transform:"translateY(-50%)"
					});
				}
			});
		}
	}


	function getInitialSlide(){
		let currentPos = -1000000;
		let currentSlide = 0;
		container.find('.slide').each(function(){
			const sbbox = this.getBoundingClientRect();
			if (sbbox.top > currentPos && sbbox.top <= 0){
				currentPos = sbbox.top;
				currentSlide = $(this).index();
			}
		});

		return currentSlide;
	}


	function goTo(n, lockDelay){
		scrollTop = 0;
		if (n<=0){
			n=0;
			scrollTop = container.offset().top - $(window).height();
		}
		else if (n>slidesNumber){
			n=slidesNumber+1;
			scrollTop = container.find('.slide:nth-child('+slidesNumber+')').offset().top+$(window).height();
		}
		else{
			scrollTop = container.find('.slide:nth-child('+n+')').offset().top;
		}

		isInTransition = true;
		$("html, body").animate({ scrollTop }, 500, 'swing', function(){
			setTimeout(function(){isInTransition=false}, lockDelay);
			currentSlide = n;
		});

	}

	function getSlide(n){
		return container.find('.slide:nth-child('+n+')');
	}

	function isDesktop(){
		return $(window).width()>768;
	}

	function hasCaption(n){
		const slideCaption = getSlide(n).find('figcaption');
		return slideCaption.text().trim().length > 1;
	}

	function showCaption(n, lockDelay){
		isInTransition = true;
		container.find('.slide-caption').hide(0);
		getSlide(n).find('.slide-caption').fadeIn(500, function(){
			currentStep = 1;
			setTimeout(function(){isInTransition=false}, lockDelay);
			
		});
	}

	function hideCaption(n, lockDelay){
		isInTransition = true;
		getSlide(n).find('.slide-caption').fadeOut(500, function(){
			currentStep = 0;
			setTimeout(function(){isInTransition=false}, lockDelay);

		});
	}

	function handle(direction, lockDelay){
		if (isInTransition) return false;
		let n = direction< 0 ? currentSlide+1:currentSlide-1;
		if (isDesktop()) {
			if (currentStep==0 && n<currentSlide ){
				goTo(n, lockDelay);
				if (hasCaption(n)) {
					currentStep = 1;
					showCaption(n, lockDelay);
				}
				return;
			}
			if (currentStep==1 && n<currentSlide){
				hideCaption(currentSlide, lockDelay, 0);
				return;
			}
			if (currentStep==1 && n>currentSlide){
				goTo(n, lockDelay);
				hideCaptions();
				currentStep = 0;
				return;
			}

			

			if (currentStep==0 && n>currentSlide){
				if (hasCaption(currentSlide)){
					showCaption(currentSlide, lockDelay);
				}
				else goTo(n,lockDelay);
				return;
			}
		}
		else{

			currentStep = 0;
			showCaption(n,lockDelay)
			goTo(n, lockDelay)
		}
	}
	

	function isOnScreen(){
		let bbox = container[0].getBoundingClientRect();
		const inScreen = bbox.top < jQuery(window).height() && bbox.top+bbox.height > 1;
		const inSecondScreen = Math.abs( container.offset().top -  jQuery(window).height() )<32;
		return (inScreen ||  inSecondScreen);
	}

	init();
}



jQuery.buildFPGallery = function(){
	jQuery('.inkube-fullpage-gallery').each(function(){
		jQuery(this).fpGallery();
	});
}
jQuery(document).ready(function(){
	jQuery.buildFPGallery();
});

