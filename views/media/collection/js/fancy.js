$('#f').fullpage({
    navigation: false,
    sectionSelector: '.slide',
    recordHistory: true,
    resetSliders: true,
    css3: false,
    scrollBar: $(window).height() <= 320,
    onLeave: function(index, nextIndex, direction){
        
    },
    afterResize: function(){
        $.fn.fullpage.reBuild();
    },
    normalScrollElements: '.entry-content'
});