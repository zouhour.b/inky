jQuery.fn.navigate = function(cmd, params){
	if (jQuery(this).find('.svg-wrapper').length!==1){
		alert('no wrapper found, unable to navigate throw the svg');
		return;
	}
	const svg = jQuery(this);
	const calcCadre =  function(i){
		var viewbox = svg[0].viewBox.baseVal;
		viewbox = [
			viewbox.x,
			viewbox.y,
			viewbox.width,
			viewbox.height
		]
		var margin = (jQuery(window).width() > 800)? (jQuery('.voyage-content').width()+40) * 1920 / jQuery(window).width():0;
		var cadre = {x:0, y:0, width: parseFloat(viewbox[2]) - margin, height: parseFloat(viewbox[3])}                        
		return cadre;
	}
	var  cadre =  calcCadre();
	const navHistory = [];

	const wrapper = jQuery(this).find('.svg-wrapper');
	function init(){
		navHistory = [];
	}

	function getStatus(){
		if (navHistory.length == 0 ) return {selector: null};
		return navHistory[navHistory.length - 1];
	}


	function goTo(sel){
		if (jQuery(sel, svg).length==0){
			alert(sel+' not found');
			return;
		}
		if (sel!= getStatus().selector) {
			navHistory.push({selector: sel, status: ""});
			update();
		}

	}


	function update(){            
		var currentStatus = getStatus();
		var next = true;
		switch (currentStatus.status){
			case "choice": 
			next = false;
			t = calcFitbox(calcBiggestBox($el),cadre);
			break;

			case "popup":
			next = false;
			t = calcFitbox(getbox(currentStatus.selector),cadre);
			break;
			default:
			t = calcFitbox(getbox(currentStatus.selector),cadre);
			break;

		}

		wrapper.css('transform', t).attr('svg-focus', currentStatus.selector);
	}

	const calcFitbox =  function(box, _box){
		var scaleX = _box.width/box.width;
		var scaleY = _box.height/box.height;
		var scale = Math.min(scaleX,scaleY);
		var t = {x:0,y:0,s:Math.floor(scale*100)/100};
		t.y = Math.floor(1000   *(_box.y - box.y*t.s))/1000;
		t.x = Math.floor(1000*(_box.x - box.x*t.s))/1000;
		if (scale == scaleX){
			t.y = t.y + (_box.height - t.s * box.height)/2; 
		}
		else t.x = t.x + (_box.width - t.s * box.width)/2;                 

		return "matrix("+[t.s,0,0,t.s,t.x,t.y].join(',')+')';
	}

	const calcBiggestBox = function($sels){
		var box = {id: [], x: 10000,  y: 10000,  x_max: -10000,  y_max: -10000 }
		var n = this;
		jQuery($sels).each(function(){

			var _box = n.getbox(this);
			if (_box.x < box.x)                         box.x = _box.x;
			if (_box.y < box.y)                         box.y = _box.y;
			if (_box.x +_box.width  > box.x_max)        box.x_max  = _box.x + _box.width;
			if (_box.y +_box.height > box.y_max)        box.y_max  = _box.y + _box.height;
			box.id.push($(this).attr('id'));
		});
		box.width = box.x_max - box.x;
		box.height = box.y_max - box.y;
		box.id = box.id.join(', ');
		return box;
	}

	function getbox(el){  
		if ($(el).attr('box-x')){
			return {
				id: $(el).attr("id"),
				x: parseFloat($(el).attr('box-x')),
				y: parseFloat($(el).attr('box-y')),
				width:  parseFloat($(el).attr('box-width')),
				height:  parseFloat($(el).attr('box-height'))
			}

		}
		return $(el)[0].getBBox();

	}

	if (cmd==='focus'){
		goTo(params);
		update();
	}
	if (cmd==='status'){
		return getStatus();
	}
	
}



