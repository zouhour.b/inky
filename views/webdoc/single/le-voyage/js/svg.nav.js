(function($){
    Infographix = function(options){
        this.options = $.extend(this.optionsDefaults, options );
        this.init();

    }
    Infographix.prototype = {
        status: {
            selector: "",
            choice: false
        },

        optionsDefaults: {
            
            container: '.svg-container',
            $scenes : '[id^="scene"]',
            $interactionBtn: '.svg-more',
            $dataContainer: ".svg-data",
            interactionCb: function(btn,Infx){
                var scene = $(btn).parents(Infx.options.$scenes).addClass('focus');
                if (!scene.length) {
                    alert("La scene relative à ce bouton est introuvable... \nle selecteur "+Infx.options.$scenes+' a échoué');
                    return;
                }
                var ref = scene.attr('id');
                Infx.pipe.push({selector: "#"+ref, status: 'popup'});
                Infx.updateNav();
            }
        },
        
        init: function(){        
            var $this = this;
            this.sel = this.options.container+ " svg";
            this.svg = $(this.sel);
            this.pipe = [];
            this.scenes = $(this.sel).find(this.options.$scenes);
            this.interactionBtn = $(this.sel).find(this.options.$interactionBtn);
            this.dataContainer = $(this.options.$dataContainer);
            $(this.options.container).append(this.dataContainer);
            this.svg.before('<span class="svg-nav fa fa-chevron-right"></span><span class="svg-nav fa fa-chevron-left"></span>');
            this.controls = {
                next: $this.svg.parent().find('.svg-nav.fa-chevron-right'),
                prev: $this.svg.parent().find('.svg-nav.fa-chevron-left')
            };
            this.svg.parent().find('.svg-nav').click(function(){
                if ($(this).hasClass('fa-chevron-left')){
                    $this.prevScene();
                }
                if ($(this).hasClass('fa-chevron-right')) {
                    $this.nextScene();
                }
            });
            

            this.svg.parent().find('[scene-ref]').each(function () {                
                var ref = $(this).attr('scene-ref');
                $this.svg.find('#' + ref).find('interaction').addClass('available');

            });
            this.interactionBtn.click(function () {

                $this.options.interactionCb(this, $this);
            });

            //handle choices;
            this.scenes.click(function(){
                var currentStatus = $this.getStatus();
                if (currentStatus.status=="choice"){
                    $this.goTo('#'+$(this).attr('id'));
                    $this.updateNav();
                }
            });
            $(document).keyup(function (e) {
                if (e.keyCode == 27) { // escape key maps to keycode `27`
                    if ($this.getStatus().status == "popup"){
                        $('.popup.active a[role="close"]').trigger('click');
                    }

                }
            });



            
            this.isNavigable = this.svg.find('[nav-init]').length;
            var firstScene = this.svg.find('[nav-init]');
            if (this.isNavigable){
                this.goTo(this.svg.find('[nav-init]').attr('nav-init'));
                $(document).keyup(function (e) {
                if (e.keyCode == 39) { // escape key maps to keycode `27`
                    $this.nextScene();
                };
                if (e.keyCode == 37) { // escape key maps to keycode `27`
                    $this.prevScene();
                };

            });






                this.svg.trigger('nav-change');
            }
            else{
                this.pipe.push({selector:null});
                this.svg.find('#circuit-0 polygon').each(function(i){
                    $(this).css('animationDelay', (0.7*i)+"s");
                });
                this.updateNav();

            }
            if (this.options.onReady){
                this.options.onReady(this)
            }
            this.svg.trigger('svgReady', this);
            $('.push-loader').fadeOut(1000);
                        

        },

        goTo: function(sel){
            if (sel!= this.getStatus().selector) {
                this.pipe.push({selector: sel, status: ""});
                this.updateNav();
            }

        },

        nextScene: function(){
            var currentStatus = this.pipe[this.pipe.length - 1];
            if (currentStatus.status =="choice" || currentStatus.status == "popup"){
                return;
            }
            var nextSelector = $(currentStatus.selector).attr('next-scene');
            if ($(nextSelector).length == 1){
                this.goTo(nextSelector);
                return;
            }
            if ($(nextSelector).length > 1){
                this.pipe.push({selector:nextSelector, status:"choice"});
                this.updateNav();
                return;
            }
            $('.svg-dialog').removeClass('hidden');  
            /*if (confirm('Proécdure terminée. \nVoulez vous comprendre une autre issue du processus?')){          
                this.reset();
            }*/


        },

        prevScene: function(){
            if (this.pipe.length > 1){
                this.pipe.pop();
                this.updateNav();
            }

        },

        reset: function(){
            this.pipe = this.pipe.slice(0,1);
            this.updateNav();
        },



        getStatus: function(){
            if (this.pipe.length == 0 ) return {selector: null};
            return this.pipe[this.pipe.length - 1];
        },

        updateNav: function(el){            
            var currentStatus = this.getStatus();
            var next = true;
            if (currentStatus.status !="popup"){
                $('.popup.active').removeClass('active');
            }
            this.svg.find('.focus').removeClass('focus');
            this.svg.attr('status', currentStatus.status?currentStatus.status:"initial");

            var t = "";
            if (this.isNavigable){this.svg.addClass('navigable');} else { this.svg.removeClass('navigable');    }
            if (this.pipe.length <= 1) this.controls.prev.hide(500); else this.controls.prev.show(500);
            if (!currentStatus.selector){
                next = false;
            }
            else {
                this.cadre = this.options.cadre(this);
                var $el = $(currentStatus.selector).addClass('focus');

                switch (currentStatus.status){
                    case "choice": 
                    next = false;
                    t = this.helpers.calcFitbox(this.helpers.calcBiggestBox($el),this.cadre);
                    break;
                    
                    case "popup":
                    next = false;
                    t = this.helpers.calcFitbox(this.helpers.getbox(currentStatus.selector),this.cadre);
                    break;
                    default:
                    t = this.helpers.calcFitbox(this.helpers.getbox(currentStatus.selector),this.cadre);
                    break;

                }
            }
            if (next) this.controls.next.show(); else this.controls.next.hide();
            $(this.sel + ' > .svg-wrapper').css('transform', t).attr('svg-focus', currentStatus.selector);
            this.svg.trigger('nav-change');
            var $this = this;
            
        },
        helpers: {
            

            calcFitbox: function(box, _box){
                var scaleX = _box.width/box.width;
                var scaleY = _box.height/box.height;
                var scale = Math.min(scaleX,scaleY);
                var t = {x:0,y:0,s:Math.floor(scale*100)/100};
                t.y = Math.floor(1000   *(_box.y - box.y*t.s))/1000;
                t.x = Math.floor(1000*(_box.x - box.x*t.s))/1000;
                if (scale == scaleX){
                    t.y = t.y + (_box.height - t.s * box.height)/2; 
                }
                else t.x = t.x + (_box.width - t.s * box.width)/2;                 
                
                return "matrix("+[t.s,0,0,t.s,t.x,t.y].join(',')+')';
            },

            calcBiggestBox($sels){
                var box = {id: [], x: 10000,  y: 10000,  x_max: -10000,  y_max: -10000 }
                var n = this;
                jQuery($sels).each(function(){

                    var _box = n.getbox(this);
                    if (_box.x < box.x)                         box.x = _box.x;
                    if (_box.y < box.y)                         box.y = _box.y;
                    if (_box.x +_box.width  > box.x_max)        box.x_max  = _box.x + _box.width;
                    if (_box.y +_box.height > box.y_max)        box.y_max  = _box.y + _box.height;
                    box.id.push($(this).attr('id'));
                });
                box.width = box.x_max - box.x;
                box.height = box.y_max - box.y;
                box.id = box.id.join(', ');
                return box;
            },

            getbox: function(el){  

                if ($(el).attr('box-x')){
                    return {
                        id: $(el).attr("id"),
                        x: parseFloat($(el).attr('box-x')),
                        y: parseFloat($(el).attr('box-y')),
                        width:  parseFloat($(el).attr('box-width')),
                        height:  parseFloat($(el).attr('box-height'))
                    }

                }
                return $(el)[0].getBBox();

            }
        }

    };
})(jQuery);