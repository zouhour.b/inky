function init(){
    return new Infographix({
        sel: '#le-voyage',
        cadre: function(i){
            var viewbox =  d3.select(i.sel).attr('viewBox' ).split(' ');
            var margin = (jQuery(window).width() > 800)? (jQuery('.voyage-content').width()+40) * 1920 / jQuery(window).width():0;
            var cadre = {x:0, y:0, width: parseFloat(viewbox[2]) - margin, height: parseFloat(viewbox[3])}                        
            return cadre;
        },
        onReady: function(i) {
            goChoices = function(_i){
                var _s = [1,2,3,4,5,6,7,8,9,10,11].map(function(d){return "#scene-"+d}).join(',');
                _i.pipe.push({selector:_s, status: 'choice'});
                _i.updateNav();
                jQuery(window).scrollTop(jQuery(window).height());
                jQuery('body').removeClass('popup-open');
                jQuery('.popup [scene-ref]').removeClass('active');
            }
            jQuery('.voyage-container').append('<a class="btn-close"><span class="fa fa-times"></a>')
            jQuery('.voyage-container .btn-close, .voyage-container image').click(function(){goChoices(i)});
            for (_i=1; _i<12; _i++){

                var scene = d3.select('#scene-'+_i);
                var coords = {x: scene.select('circle').attr('cx'), y:scene.select('circle').attr('cy')}
                scene.select('a').style('transform-origin', coords.x+"px "+coords.y+"px")
                for (var j=0; j<3; j++ ){
                    scene.append('circle')
                    .attr('cx', coords.x)
                    .attr('cy', coords.y)
                    .attr('class', 'pin pin-'+j)
                    .attr('r', 20)
                }
                scene.selectAll('circle').style('transform-origin', coords.x+"px "+coords.y+"px");
            }

            var w = jQuery('.svg-container').width();
            var h  = jQuery('.svg-container').height()  * 1920 / w;
            d3.select(i.sel).attr('viewBox' , "0 0 1920 "+h).attr('width', 1920).attr( 'height', h);

            i.updateNav();
            jQuery(window).resize(function(){
                var w = jQuery('.svg-container').width();
                var h  = jQuery('.svg-container').height()  * 1920 / w;
                d3.select(i.sel).attr('viewBox' , "0 0 1920 "+h).attr('width', 1920).attr( 'height', h);
                i.updateNav();
            });
            jQuery('svg .scene').click(function(){
                i.goTo('#'+jQuery(this).attr('id'));
            });

            i.lastPos = 0;
            i.isScrolling = false;

            jQuery(window).scroll(function(e){
                if (i.isScrolling) return;
                sel = null;
                jQuery('.voyage-content [scene-ref]').each(function(){             
                    if (jQuery(this)[0].getBoundingClientRect().top < 100){
                        sel = jQuery(this).attr('scene-ref');
                    }
                });
                if (jQuery(window).scrollTop() < jQuery(window).height() && jQuery(window).width()>=800 ){
                    var pos =  (jQuery(window).scrollTop() > i.lastPos)?jQuery(window).height():0; 
                    i.isScrolling = true;
                    jQuery("html, body").animate({scrollTop:pos}, 500,  function() { 
                        i.isScrolling = false;
                    });
                    (i.lastPos = pos)?"":i.reset();
                    return;
                }
                i.lastPos = jQuery(window).scrollTop();

                if (!sel) {
                    i.reset();
                    return;
                }
                var choice = (sel.split(',').length > 1)?"choice":"";
                i.pipe.push({selector: sel, status: choice });
                if (sel.split(',').length == 1){
                    jQuery('body').addClass('popup-open');
                }
                else {
                    jQuery('body').removeClass('popup-open');
                }
                i.updateNav();              

            });




        }
    });
}

function initClock(){
    if (jQuery('svg#le-voyage').length){
        return init();
    }
    setTimeout(initClock, 100);
}

initClock();