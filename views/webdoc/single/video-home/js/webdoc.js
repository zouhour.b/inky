$(document).ready(function(){	

	function enterFullscreen(element) {
		if (element.requestFullscreen) {
			element.requestFullscreen();
		} else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if (element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		} else if (element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}
	}
	let isHome = window.location.href.includes('/webdoc/')>0;
	let isEpisode = $('.single-video-cover').length > 0;

	let videos = [
	{index:1, ended:false, el: document.querySelector('#cover-video'), showAfterEnded: $('.webdoc-cover-caption') },
	]

	let muted = true;
	
	$('.video-controls [role="toggle-mute"]').click(function(){
		muted = !muted;
		videos.map(video=>video.el.muted=muted)
		if (muted){
			$('.icon-toggle-mute').addClass('muted');
		}
		else{
			$('.icon-toggle-mute').removeClass('muted');
		}
		
	});

	$('[data-slide-to]').click(function(){
		if (isHome){
			target = parseInt($(this).data('slideTo'));
			$('#sncft-home').fullpage.moveTo(target);
			updateNav(target);
		}
		else{
			let link = $(this).attr('href');
			if (link) window.location.href = link;

		}
	});
	$('[role="fullscreen"]').click(function(){
		target = $(this).data('target');
		video = document.querySelector(target);
		enterFullscreen(video);
	});

	//handle skip-replay
	$('.video-controls [role="skip-replay"]').click(function(){
		let target = $(this).data('target');
		let video = videos.find(x=>"#"+x.el.id===target);
		if (video){
			if ($(this).hasClass('skipped')){
				video.el.play();
				$(this).removeClass('skipped');
				if (video.showAfterEnded){
					video.showAfterEnded.fadeOut();
				}
				video.ended = false;
				
			}
			else{
				video.el.currentTime = video.el.duration
			}
		}
	});

	//handle video end
	videos.map((video,i)=>{
		$(video.el).on('ended', function(){
			video.ended=true;
			if (video.showAfterEnded) video.showAfterEnded.fadeIn('slow');
			$('[role="skip-replay"][data-target="#'+video.el.id+'"]').addClass('skipped');
		});
		return video;

	});


	//init fullpage
	$('#sncft-home').fullpage({
		navigation: false,
		sectionSelector: '.section-item',
		recordHistory: true,
		resetSliders: true,
		css3: false,
		scrollBar: $(window).height() <= 320,
		onLeave: function(index, nextIndex, direction){
			videos.map(video=>{
				if (video.index===nextIndex){
					if (!video.ended) {
						video.el.play()
					}
				} else {
					video.el.pause()
				};
			})
			updateNav(nextIndex);

		},
		afterResize: function(){
			$.fn.fullpage.reBuild();
		},
	});

	function updateNav(target){
		$('.webdoc-nav li').removeClass('active');
		var $menuItem = $('.webdoc-nav li:nth-child('+target+')').addClass('active'); 
		$('.webdoc-nav .webdoc-nav-mobile-head').text($menuItem.text());
	}
	if (isHome){
		updateNav(1);
	}
	
	else if (isEpisode){
		let playerId = $('.single-video-cover [data-player-id]').attr('data-player-id');
		$('.webdoc-nav').attr('data-link-to', playerId);
		updateNav(2);
	}
	$('.video-caption-container').click(function(){

		$('.vjs-fullscreen-control').click()
	});

	videos[0].el.play();
	$('#episodes').episodeSlider();
	$('#portraits').portraitsSlider();

})


$.fn.portraitsSlider = function(){
	var container = $(this);
	var episodes = $(this).find('.portrait');
	var navContainer = $(this).find('.portraits-nav');
	
	var currentSlide = 1;
	function slideTo(index){
		var n = container.find('.portraits-nav-block').removeClass('active').removeClass('next').length;
		var next = (index+1)%n;
		var postNext = (index+2)%n;
		var prev = (index-1+n)%n;
		var target = container.find('.portraits-nav-block:eq('+index+')').data('target');
		container.find('.portraits-nav-block').removeClass('active').removeClass('next').removeClass('prev').removeClass('post-next');
		container.find('.portrait-cover').removeClass('active');
		let video = container.find('.portrait-cover:nth-child('+target+')').addClass('active');
		container.find('.portraits-nav-block:eq('+next+')').addClass('next');
		
		container.find('.portraits-nav-block:eq('+prev+')').addClass('prev');
		container.find('.portraits-nav-block:eq('+postNext+')').addClass('post-next');

		container.find('.portraits-nav-block:eq('+index+')').addClass('active');
		

	}

	function init(){
		if (container.find('.portraits-nav-block').length<4){
			container.find('.portraits-nav-block').each(function(){
				navContainer.append(this.outerHTML)
			});

		}
		container.find('.portraits-nav-block').click(function(e){
			var index = $(this).index();
			if ($(this).hasClass('active')){
				return;
			}
			e.preventDefault();

			slideTo(index);
		});
		slideTo(0);
		container.find('video').on('ended', function(){
			this.play();
		});
	}
	init();
}



$.fn.episodeSlider = function(){
	var container = $(this);
	var episodes = $(this).find('.episode');
	var navContainer = $(this).find('.episodes-nav');
	
	var currentSlide = 1;
	function slideTo(index){
		var n = container.find('.episodes-nav-block').removeClass('active').removeClass('next').length;
		var next = (index+1)%n;
		var prev = (index-1);
		container.attr('data-next-index', next);
		var target = container.find('.episodes-nav-block:eq('+index+')').data('target');
		container.find('.episode-cover').removeClass('active');
		let video = container.find('.episode-cover:nth-child('+target+')').addClass('active');
		container.find('.episodes-nav-block:eq('+index+')').addClass('active');
		container.find('.episodes-nav-block:eq('+next+')').addClass('next');
		container.find('.episodes-nav-block:eq('+prev+')').addClass('prev');
		setTimeout(function(){
			container.find('.episodes-nav-block:eq('+prev+')').removeClass('prev');
		}, 500);

	}

	function init(){
		if (container.find('.episodes-nav-block').length<3){
			container.find('.episodes-nav-block').each(function(){
				navContainer.append(this.outerHTML)
			});

		}
		container.find('.episodes-nav-block').click(function(e){
			var index = $(this).index();
			if ($(this).hasClass('active')){
				return;
			}
			e.preventDefault();

			slideTo(index);
		});

		container.find('.slide-next').click(function(){
			let next  = parseInt(container.attr('data-next-index'));
			slideTo(next);
		})
		slideTo(0);
		container.find('video').on('ended', function(){
			this.play();
		});
	}
	init();
}