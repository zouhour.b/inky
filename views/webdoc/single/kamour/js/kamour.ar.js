class KamourWebdoc{
	constructor(){
		let K = this;
		this.disclaimer = $('.disclaimer');
		this.menu = $('#webdoc-kamour-nav');

		this.container = $('.webdoc-kamour-container');
		
		this.refreshMenu();
		this.initDisclaimer();
		this.setupMenu();

		$('body').on('click', '.icon-full-screen', function(){
			K.enterFullscreen(document.documentElement);
		});
		$('.disclaimer').click(function(){
			$('body').removeClass('h-12').removeClass('overflow-hidden');
			$(this).fadeOut('slow');
			K.init();

		});
	}
	initDisclaimer(){
		$('body').addClass('overflow-hidden h-12');
		var init = this.menu.find('.active').attr('data-init');
		this.disclaimer.find('.'+init+'-disclaimer').fadeIn(0);
	}



	init(xhr){
		var init = this.menu.find('.active').attr('data-init');
		switch(init){
			case 'home':
			this.setupHome(xhr);
			break;
			case 'podcast':
			Inkube.players.refresh();
			break;
			case 'context':
			this.initContext();
			break;
			case 'extra':
			this.initExtra();
			break;
			case 'about':
			this.initAbout();
			break;
		}
	}

	initAbout(){
		this.setupHome();
		this.container.fullpage.moveTo(6);
	}

	initExtra(){
		let K = this;
		Inkube.players.refresh();

		let page = {
			accordeons: $('.accordeon-container', this.container),
			playButtons: $('.icon-play[role="play"]', this.container),
			video : $('#webdoc-cover-video', this.container)[0],
			players:$('.extra-players > *', this.container)
		}

		$('.skip-video', this.container).click(function(){
			page.video.currentTime = page.video.duration;
		});
		
		
		$(page.video).on('ended', function () {
			$('.show-after-video-ended').fadeIn('slow');
			$('.hide-after-video-ended').fadeOut('slow');
			page.videoPlayed = true;
		});

		setTimeout(function(){
			page.video.play();

		}, 100)


		page.accordeons.each(function(){
			K.makeAccordeon($(this));
		});
		page.playButtons.on('click', function(){
			let playerid = $(this).attr('data-link-to');
			if (playerid.split && playerid.split){
				const index  = parseInt(playerid.split('-').pop())-1;

				page.players.removeClass('active');
				$(page.players[index]).addClass('active');
				return true;
			}

		});
	}

	makeAccordeon(el){
		let maxHeight = '300px';
		let h = $(el).height();
		el.css({maxHeight: maxHeight, transition:'all .4s ease-in-out'});
		if (!el.find('.toggle-expand').length){
			$('<a>').addClass('fa fa-chevron-down color-white toggle-expand').appendTo(el[0]);
		}

		el.find('.toggle-expand').data('ink.state', {isExpanded:0, originalHeight:h}).click(function(){
			let state = $(this).data('ink.state');
			if (!state.isExpanded){
				$(this).removeClass('fa-chevron-down').addClass('fa-chevron-up')
				$(this).parent().css('maxHeight', '900px');
			}
			else{
				$(this).removeClass('fa-chevron-up').addClass('fa-chevron-down')
				$(this).parent().css('maxHeight', maxHeight);
			}
			state.isExpanded = !state.isExpanded;
			$(this).data('ink.state', state);
		});

		
	}

	initContext(){
		//$("body .videoslider-slides").appendTo($('body #cover-el-kamour'));
		$.videoslider({
			enableRewind: false,
			selector: '#cover-el-kamour'
		});
		/* Auto play */
		let page = {

			video: document.querySelector('#webdoc-context-video')
		}
		$('.skip-video').click(function(){
			page.video.currentTime = page.video.duration;
		});
		


		$(page.video).on('ended', function () {
			$('.show-after-video-ended').fadeIn('slow');
			$('.hide-after-video-ended').fadeOut('slow');
			page.videoPlayed = true;
		});


		var e = $.Event("keydown");
		e.which = 40;
		this.container.trigger(e);

		/* Adapt timeline to mobile*/
		if ($(window).width() <= 768) {
			$('.timeliner-item').each(function(){ $('.timeliner-image', $(this)).insertAfter($('.timeliner-infos', $(this))) });
			$('.timeliner-item:not(.timeliner-center)').removeClass('timeliner-left').removeClass('timeliner-right').addClass('timeliner-center');
		}
	}



	setupMenu(){
		let K = this;
		this.menu.on('click', '.menu-link', function(e){
			
			Inkube.players.pauseAllPlayers({});
			e.preventDefault();
			var url = $(this).data('href');
			console.log(url);
			$.get(url, function(data) {
				Inkube.players.pauseAllPlayers({});
				let div  = $('<div>');
				div.html(data);
				let html = div.find('.webdoc-kamour-container').html();
				if (html){
					if (history.pushState) {
						history.pushState(null, null, url); 
					}
					if ($.fn.fullpage.destroy){
						$.fn.fullpage.destroy('all');
					}
					K.container.html(html);
					K.refreshMenu();
					K.init(true);
				}
				else{
					window.location.href = url;
				}
			});
		});
	}

	refreshMenu(){
		let K = this;
		let l = decodeURI(window.location.href);
		this.menu.find('.active').removeClass('active');
		this.menu.find('.menu-link').each(function(){
			let href= decodeURI($(this).data('href'));

			if (l.indexOf(href)>-1 ){
				if (l.indexOf('#')>-1 && href.indexOf('#')==-1){
					return;
				}
				$(this).parent().addClass('active');
				K.menu.find('.current-page').text($(this).text());
			}
		});
	}

	setupHome(xhr){
		let K = this;
		let page = {
			video: document.querySelector("#webdoc-cover-video"),

		}
		this.page = page;

		$('.skip-video').click(function(){
			page.video.currentTime = page.video.duration;
		});


		$(page.video).on('ended', function () {
			$('.show-after-video-ended').fadeIn('slow');
			$('.hide-after-video-ended').fadeOut('slow');
			page.videoPlayed = true;
		});

		setTimeout(function(){
			page.video.play();

		}, 100)

		if(!($(window).width() <= 412 || $(window).height() <= 412)) {
			$('.about-mobile').remove();
		}

		this.container.fullpage({
			navigation: false,
			sectionSelector: '.section-item',
			recordHistory: true,
			resetSliders: true,
			css3: false,
			scrollBar: $(window).height() <= 320,
			onLeave: function(index, nextIndex, direction){
				if(index == 1 && direction =='down' && !page.videoPlayed){
					page.video.play();
				} else if(index == 2 && direction == 'up' && !page.videoPlayed){
					page.video.play();
				}
			},
			afterResize: function(){
				$.fn.fullpage.reBuild();
			},
		});
	}
	enterFullscreen(element) {
		if (element.requestFullscreen) {
			element.requestFullscreen();
		} else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if (element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		} else if (element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}
	}
	exitFullscreen () {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}

	appendSkipButton(el){
		$('<a>',{
			'id': 'skip-video',
			'class': 'icon icon-ff-chapitre'
		}).appendTo('#cover-el-kamour .section-item:first');
		const video = el.find('video')[0];

	}

	buildMenu(){

	}
}
$(document).ready(function(){
	const kamour = new KamourWebdoc();
	window.kamour = kamour;
	
})