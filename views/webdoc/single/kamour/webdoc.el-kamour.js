alert('loaded');
(function ($) {
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    $(document).ready(function () {
        kamour = new KamourProject();
    });

    KamourProject = function () {
        this.$ = $('#cover-el-kamour');
        this.entryContent = $('.entry-content');
        this.video = this.$.find('video').get(0);
        this.figcaption = this.$.find('figcaption');
        this.overlay = this.$.find('.shadow-overlay');
        this.menuLinks = menuLinks;
        this.videoPlayed = false;
        var K = this;
        
        // Add Fullscreen button and handler
        if (window.mainMenu) {
            window.mainMenu.destroy();
            window.mainMenu.append('<a href="https://www.facebook.com/sharer/sharer.php?u='+document.location.href+'" target="_blank" class="icon icon-facebook-2"></a>');
            window.mainMenu.append('<a href="https://twitter.com/home?status='+ encodeURI(document.title+' '+document.location.href) +'" target="_blank" class="icon icon-twitter-2"></a>');
            window.mainMenu.append('<a class="icon icon-kamour-full-screen kamour-fullscreen"></a>');
            $('#main-nav').show();
            this.Fullscreen = false;
            window.mainMenu.$.find('.kamour-fullscreen').click(function () {
                if (!K.Fullscreen) {
                    K.enterFullscreen(document.documentElement);
                } else {
                    K.exitFullscreen();
                }
                K.Fullscreen = !K.Fullscreen;
            });
        }
        this.helpers.setupMenu(K);
        
        this.displayDisclaimer();
    }

    KamourProject.prototype = {
        displayDisclaimer: function(){
            // Add disclaimer
            var K = this;
            
            // Bypass disclaimer if internal anchor
            if(window.location.hash) {
                $('#disclaimer').hide();
                this.init();
            } else {
                // Check cookie value
                //if ($.cookie("disclaimer") != 'agree') {
                    K.menuLinks.forEach(function(item, index){
                        if(item.disclaimer && $('body').hasClass(item.class)) {
                            $('#disclaimer .' + item.disclaimer + '-disclaimer').show();
                            $('#disclaimer').fadeIn();
                            $('#disclaimer').on('click', function(){
                                $(this).fadeOut();
                                if (item.disclaimer == 'fullscreen') {
                                    K.enterFullscreen(document.documentElement);
                                    setTimeout(function() {K.init();}, 500);
                                } else {
                                    K.init();
                                }
                            });
                        }
                    });
                    // $.cookie("disclaimer", "agree", { expires: 365, path: '/' });
                // } else {
                //    this.init();
                // }
            }
        },
        init: function () {
            var K = this;
            
            K.$.fadeIn();
            K.entryContent.attr('style', 'display: flex !important');
            
            //Add First Screen Init
            if (this.video !== undefined) {
                // if(isMobile.any()){
                //     $('.mobile-video-disclaimer').show();
                //     $('.mobile-video-disclaimer a').on('click', function() {
                //         $('.mobile-video-disclaimer').fadeOut();
                //         K.video.play();
                //     });
                // }
                $(this.video).on('ended', function () {
                    K.figcaption.fadeIn('slow');
                    K.overlay.fadeIn('slow');
                    K.menu.fadeIn('slow');
                    K.mobileMenu.fadeIn('slow');
                    K.videoPlayed = true;
                    $('#skip-video').fadeOut('slow');
                });
            } else {
                K.figcaption.fadeIn('slow');
                K.overlay.fadeIn('slow');
                K.menu.fadeIn('slow');
                K.mobileMenu.fadeIn('slow');
            }
            // Full home page
            if($('body').hasClass('single-webdoc')||$('body').hasClass('term-el-kamour-ar')) {
                if(!($(window).width() <= 412 || $(window).height() <= 412)) {
                    $('.about-mobile').remove();
                }
                this.$.fullpage({
                    // anchors: this.menuLinks.map(function(item) { return item.anchor }),
                    // menu: '.kamour-menu',
                    navigation: false,
                    sectionSelector: '.section-item',
                    recordHistory: true,
                    resetSliders: true,
                    css3: false,
                    parallax: true,
                    responsiveSlides: true,
                    scrollBar: $(window).height() <= 320,
                    onLeave: function(index, nextIndex, direction){
                        if(K.menuLinks[nextIndex-1]) {
                            $('.navbar-toggle', K.mobileMenu).text(K.menuLinks[nextIndex-1].title);
                        }
                        if(index == 1 && direction =='down' && !K.videoPlayed){
                            K.video.play();
                        } else if(index == 2 && direction == 'up' && !K.videoPlayed){
                            K.video.play();
                        }
                    },
                    afterResize: function(){
                        $.fn.fullpage.reBuild();
                    },
                });
            }
            
            // Video auto play
            if (this.video !== undefined) {
                // if(isMobile.any()){
                //     $('.mobile-video-disclaimer').show();
                //     $('.mobile-video-disclaimer a').on('click', function() {
                //         $('.mobile-video-disclaimer').fadeOut();
                //         K.video.play();
                //     });
                // } else {
                    this.video.play();
                // }
            }
            
            // Back to top button
            K.menuLinks.forEach(function(item, index){
                if($('body').hasClass(item.class) && item.backToTop) {
                    $('<a>',{
                        'id': 'go-to-top',
                        'class': 'icon-kamour-back-to-top'
                      }).appendTo('body');
                      $('#go-to-top').each(function(){
                          $(this).click(function(){ 
                              $('html,body').animate({ scrollTop: 0 }, 'slow');
                              return false; 
                          });
                      });
                }
            });
            
            // Skip button
            K.menuLinks.forEach(function(item, index){
                if($('body').hasClass(item.class) && item.skipVideo) {
                    $('<a>',{
                        'id': 'skip-video',
                        'class': 'icon icon-ff-chapitre'
                      }).appendTo('#cover-el-kamour .section-item:first');
                    $('#skip-video').click(function(){
                          K.video.currentTime = K.video.duration;
                      });
                }
            });

            // Replay button
            K.menuLinks.forEach(function(item, index){
                if($('body').hasClass(item.class) && item.replay) {
                    $(K.video).on('ended', function () {
                        $('#replay-video').fadeIn('slow');
                    });
                    $('<a>',{
                        'id': 'replay-video',
                        'class': 'icon icon-replay'
                      }).appendTo('#cover-el-kamour .section-item:first');
                    $('#replay-video').click(function(){
                        K.figcaption.fadeOut('slow');
                        K.overlay.fadeOut('slow');
                        K.menu.fadeOut('slow');
                        K.mobileMenu.fadeOut('slow');
                        K.videoPlayed = true;
                        $('#skip-video').fadeIn('slow');
                        $(this).fadeOut('slow');
                        $.videoslider({
                            enableRewind: false,
                            selector: '#cover-el-kamour'
                          });
                        // Auto play
                        var e = $.Event("keydown");
                        e.which = 40;
                        $('#cover-el-kamour').trigger(e);
                        $('.videoslider-slides').show();
                        $('.videoslider-slide').hide();
                    });
                }
            });

            // About ?
            if(window.location.hash == '#a-propos') {
                setTimeout(function() { $('#skip-video').trigger('click') }, 500);
                $.fn.fullpage.moveTo(6, 0);
            }
            
            $(document).trigger("kamourIsReady");            
        },
        enterFullscreen: function (element) {
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        },
        exitFullscreen: function () {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        },
        helpers: {
            setupMenu: function(K){
                var menu = $('<ul>', {
                    'style': 'display: none'
                });
                
                // if($('body').hasClass('term-el-kamour')) { 
                //     K.menuLinks.forEach(function(item, index){
                //         var menuItem = $('<li>', {
                //             'class': 'nav-item',
                //             'data-menuanchor': item.anchor
                //         }).appendTo(menu);
                //         var link = $('<a>', {
                //             href: '#' + item.anchor,
                //             html: item.title
                //         }).appendTo(menuItem);
                //     });
                // } else {
                    K.menuLinks.forEach(function(item, index){
                        var menuItem = $('<li>', {
                            'class': 'nav-item' + ($('body').hasClass(item.class)? ' active': ''),
                            'id': 'menu-item-'+index
                        }).appendTo(menu);
                        $('<a>', {
                            href: item.link,
                            html: item.title
                        }).appendTo(menuItem);
                    });
                // }
                
                K.mobileMenu = $('<div>', {
                    'id': 'navbarCollapse',
                    'class': 'collapse navbar-collapse text-center kamour-mobile-menu',
                }).insertAfter($('#site-logo'));
                $('<button>', {
                    'id': 'navbarCollapse',
                    'class': 'navbar-toggle icon-menu-hamburger',
                    'html': 'Accueil'
                }).appendTo(K.mobileMenu);
                menu.addClass('text-center kamour-menu');
                menu.appendTo(K.mobileMenu);
                $('#navbarCollapse').on('click', function(){
                    menu.slideToggle('slow');
                })

                K.menu = menu.clone()
                        .addClass('list-inline-with-separators p-3')
                        .appendTo($('#webdoc-nav'));
                
                $('.navbar-toggle', K.mobileMenu).text($('.nav-item.active a', K.mobileMenu).html());
                $('#menu-item-5 a').on('click', function() {
                    $.fn.fullpage.moveTo(6, 0);
                })
            }
        }
    }
}(jQuery))