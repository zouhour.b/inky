﻿1
00:00:00,019 --> 00:00:03,414
Moi depuis 1996, je travaille
comme fakir, cracheur de feu.

2
00:00:05,543 --> 00:00:07,431
Je suis même charmeur de serpents.

3
00:00:09,984 --> 00:00:13,051
L'année où tout a commencé,

4
00:00:14,072 --> 00:00:17,103
je me baladais à Hammamet,

5
00:00:17,106 --> 00:00:18,797
j'y ai rencontré un fakir célèbre,

6
00:00:18,800 --> 00:00:21,830
il m'a emmené voir un de ses spectacles
de charmeurs de serpents.

7
00:00:22,293 --> 00:00:24,528
Je lui ai dit que pour moi
c'était un jeu d'enfant.

8
00:00:25,174 --> 00:00:27,985
On est habitué aux serpents
chez nous.

9
00:00:27,987 --> 00:00:29,877
C'est comme un jouet.

10
00:00:29,879 --> 00:00:31,509
Il m'a dit : "tu peux le faire ?"
Je lui ai dit: "ok."

11
00:00:31,514 --> 00:00:32,492
Il m'a appris le métier.

12
00:00:33,405 --> 00:00:36,354
On a commencé à travailler à Djerba,
on a monté une troupe

13
00:00:36,797 --> 00:00:39,185
avec des gars du quartier
que j'ai formés. 

14
00:00:39,187 --> 00:00:41,689
On travaillait ensemble
dans les hôtels. 

15
00:00:43,258 --> 00:00:45,246
On fait ça depuis 1996.

16
00:00:45,246 --> 00:00:48,091
Maintenant on fait les mariages
et les festivals.

17
00:00:48,091 --> 00:00:50,865
J'ai même travaillé à Carthage.

18
00:00:50,865 --> 00:00:54,187
Lors d'un spectacle, au moment
de l'entrée sur scène,

19
00:00:54,187 --> 00:00:59,365
j'étais un des cracheurs de feu
pour le concert d'un célèbre chanteur.

20
00:01:01,237 --> 00:01:04,631
J'ai fait beaucoup de festivals
dans les régions.

21
00:01:04,899 --> 00:01:06,281
Le festival de Tataouine, le festival de Douz.

22
00:01:06,281 --> 00:01:10,496
L'ancien Président Marzouki y avait assisté,
il y a deux ans.

23
00:01:12,482 --> 00:01:15,070
Tous les étés c'est comme ça.

24
00:01:23,242 --> 00:01:27,313
J'ai appris ça comme mon frère, 
c'est devenu une passion.

25
00:01:28,323 --> 00:01:29,918
On est passé par la même école.

26
00:01:30,341 --> 00:01:32,334
On fait des spectacles encore plus grands.

27
00:01:32,334 --> 00:01:33,394
On ne travaille pas qu'avec le feu,

28
00:01:33,397 --> 00:01:37,332
mais aussi avec les débris de verres,
les serpents, les épées...

29
00:01:37,741 --> 00:01:38,914
C'est tout un programme.

30
00:01:39,196 --> 00:01:42,094
Mais on ne peut pas aller très loin avec ça.

31
00:01:42,980 --> 00:01:47,451
À part travailler lors de mariages ou 
de soirées dans des hôtels,

32
00:01:47,451 --> 00:01:49,683
ça ne mène à rien.
