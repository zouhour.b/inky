﻿1
00:00:00,880 --> 00:00:04,400
Ici Orange ne fonctionne pas du tout.

2
00:00:04,440 --> 00:00:06,880
Il y a Tunisiana (Ooredoo)
et Tunisie Telecom.

3
00:00:06,920 --> 00:00:11,320
Quand on était au carrefour d'El Kamour, 
le réseau de Tunisiana était faible.

4
00:00:11,320 --> 00:00:13,440
C'était avant qu'on aille à la vanne. 

5
00:00:13,440 --> 00:00:15,520
Des employés de Tunisiana sont arrivés,

6
00:00:15,520 --> 00:00:19,000
et nous ont dit : “On soutient le sit-in,

7
00:00:19,000 --> 00:00:20,800
on veut vous distribuer
des puces gratuites 

8
00:00:20,840 --> 00:00:24,680
et on va améliorer le réseau." 

9
00:00:24,680 --> 00:00:33,760
On a vu leurs techniciens travailler
au niveau de l’antenne-relais. 

10
00:00:33,760 --> 00:00:35,480
On a donc pris les puces 

11
00:00:35,480 --> 00:00:37,960
après leur avoir fourni 
nos pièces d’identité. 

12
00:00:37,960 --> 00:00:39,640
Mais le réseau a lâché
après notre arrivée à la vanne, 

13
00:00:39,680 --> 00:00:44,000
le jour où des renforts de
la garde nationale sont arrivés.

14
00:00:44,000 --> 00:00:48,640
Comme on s’était organisés 
en différents groupes

15
00:00:48,640 --> 00:00:53,120
pour éviter de trop consommer
ou de se fatiguer trop vite, 

16
00:00:53,120 --> 00:00:59,280
tous les protestataires
n’étaient pas à El Kamour. 

17
00:00:59,320 --> 00:01:02,520
Quand on a voulu appeler 
les gars à Tataouine 

18
00:01:02,520 --> 00:01:06,000
pour les prévenir qu’on
nous avait envoyé la police,

19
00:01:06,000 --> 00:01:07,400
le réseau a lâché. 

20
00:01:07,400 --> 00:01:09,160
Tunisiana ne fonctionnait plus. 

21
00:01:09,160 --> 00:01:14,200
Le réseau n’était pas totalement coupé
mais il ne marchait quasiment plus.

22
00:01:14,240 --> 00:01:17,520
Même la 4G fonctionnait juste avant,

23
00:01:17,520 --> 00:01:20,200
mais là il n’y avait ni internet, ni rien.

24
00:01:20,240 --> 00:01:23,520
On devait s’éloigner et faire 
plusieurs tentatives 

25
00:01:23,520 --> 00:01:25,280
avant de pouvoir joindre quelqu’un. 

26
00:01:25,280 --> 00:01:27,440
Et eux en ville ne pouvaient pas 
nous joindre non plus. 

27
00:01:27,440 --> 00:01:29,240
Ça ne s’est produit que 
la veille des affrontements.

28
00:01:29,240 --> 00:01:31,840
Le lendemain, le réseau 
fonctionnait très bien

29
00:01:31,840 --> 00:01:35,360
et ça ne s’est plus jamais reproduit.

30
00:01:35,360 --> 00:01:38,520
C’est resté un mystère. 
Moi j’ai des doutes. 

31
00:01:38,560 --> 00:01:39,880
Quand ils sont venus nous voir, 

32
00:01:39,880 --> 00:01:42,720
ils voulaient connaître le nom des protestataires, 

33
00:01:42,720 --> 00:01:47,360
sachant que quand tu prends une puce, 

34
00:01:47,360 --> 00:01:51,240
tu dois donner ta pièce d’identité. 

35
00:01:51,240 --> 00:01:53,360
Pour moi, c’est l’hypothèse la plus probable. 

36
00:01:53,360 --> 00:01:54,680
Mais Dieu seul le sait. 
