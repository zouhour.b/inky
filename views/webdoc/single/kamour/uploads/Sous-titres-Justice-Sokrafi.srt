﻿1
00:00:00,040 --> 00:00:02,000
Qui conduisait la voiture ?

2
00:00:02,000 --> 00:00:03,080
C'est simple.

3
00:00:03,080 --> 00:00:06,320
Personne n'est venu de Libye ou de Daesh
pour tuer Anouar Sokrafi,

4
00:00:06,440 --> 00:00:07,920
ce sont les agents
qui l'ont tué.

5
00:00:08,200 --> 00:00:10,600
Ils peuvent leur demander des comptes,
ils les connaissent.

6
00:00:10,880 --> 00:00:12,480
Ils peuvent trouver qui a tué Anouar.

7
00:00:12,480 --> 00:00:14,240
On vous donne le numéro
de la plaque d'immatriculation

8
00:00:14,240 --> 00:00:15,600
et c'est très simple,

9
00:00:15,600 --> 00:00:16,880
chaque voiture avait son chauffeur.

10
00:00:16,880 --> 00:00:18,280
Mais il n'y a aucune volonté.

11
00:00:18,400 --> 00:00:20,400
S'ils avaient voulu,

12
00:00:20,680 --> 00:00:23,880
on aurait su qui a tué Anouar
dès le premier jour.

13
00:00:24,120 --> 00:00:26,200
Il doit être jugé, 
s'il y a une justice.

14
00:00:26,200 --> 00:00:29,400
C'est la moindre des choses
pour le martyr.

15
00:00:29,464 --> 00:00:34,634
J'étais à 15 mètres quand le drame
s'est produit,

16
00:00:34,957 --> 00:00:37,094
ils avaient l'intention de frapper.

17
00:00:37,516 --> 00:00:39,085
Mais comme toujours,

18
00:00:39,085 --> 00:00:43,561
comme pour la Révolution
ou l'affaire des martyrs,

19
00:00:44,244 --> 00:00:45,973
la justice ne sera pas rendue.

20
00:00:45,973 --> 00:00:48,088
Même s'ils déclarent dans les médias
qu'ils l'ont jugé,

21
00:00:48,097 --> 00:00:49,211
ce sera faux.

22
00:00:49,280 --> 00:00:52,009
Moi en tant que manifestant
et citoyen,

23
00:00:52,009 --> 00:00:53,363
je ne leur fais pas confiance

24
00:00:53,512 --> 00:00:57,988
quand je sors revendiquer mes droits
pacifiquement.

25
00:00:58,088 --> 00:01:01,219
Pendant deux mois, il n'y avait pas
de police à Tataouine

26
00:01:01,225 --> 00:01:03,082
et on était en sécurité.

27
00:01:03,505 --> 00:01:06,967
Tout se passait dans le calme
pendant deux mois

28
00:01:07,290 --> 00:01:10,180
et eux ils décident d'envoyer du renfort.

29
00:01:10,180 --> 00:01:13,696
Ils viennent ici tuer notre frère
et ils rentrent chez eux,

30
00:01:13,696 --> 00:01:15,990
comme si rien ne s'était passé.

31
00:01:16,707 --> 00:01:20,473
L'armée a quand même fait
un bon geste.

32
00:01:20,473 --> 00:01:23,708
Un officier de l'armée affecté à Tataouine
est venu ici.

33
00:01:23,811 --> 00:01:29,468
Il a mené son enquête sur le terrain, 
là où Anouar Sokrafi a été tué.

34
00:01:29,557 --> 00:01:32,792
Il nous a entendus,
il a entendu les militaires

35
00:01:32,792 --> 00:01:36,264
et il a récupéré des photos.
- J'espère qu'on n'en restera pas là.

36
00:01:36,264 --> 00:01:38,549
C'était un chef de l'armée à Tataouine.

37
00:01:38,549 --> 00:01:41,844
Je pense qu'il a été intimidé
lui aussi

38
00:01:41,844 --> 00:01:44,371
par ses supérieurs, au ministère.

39
00:01:44,528 --> 00:01:47,773
Parce qu'en faisant ça,

40
00:01:47,773 --> 00:01:51,256
c'est comme s'il nous soutenait.

41
00:01:51,568 --> 00:01:56,138
Non, il n'y a encore rien.

42
00:01:56,138 --> 00:02:01,167
On en saura peut-être plus
avec le rapport du Parlement

43
00:02:01,176 --> 00:02:05,313
après on verra les conclusions de
la Défense et de la justice militaire.

44
00:02:05,603 --> 00:02:08,671
Jusqu'à maintenant on ne sait rien
de l'affaire.

45
00:02:08,858 --> 00:02:10,900
Ça soulève beaucoup d'interrogations.

46
00:02:11,441 --> 00:02:15,114
On veut que l'État tunisien nous donne
une réponse claire,

47
00:02:15,116 --> 00:02:16,674
le plus tôt possible.
