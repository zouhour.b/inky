﻿1
00:00:00,760 --> 00:00:03,000
Le mouvement n’a pas commencé
à El Kamour 

2
00:00:03,000 --> 00:00:04,840
mais dans le centre-ville
de Tataouine.

3
00:00:05,160 --> 00:00:10,000
Les jeunes sont sortis pour
l’emploi et le développement.

4
00:00:10,040 --> 00:00:13,200
Le mouvement a pris une ampleur
sans précédent,

5
00:00:13,200 --> 00:00:19,600
à tel point que le gouvernement
a dû se pencher sur la question.

6
00:00:19,640 --> 00:00:22,000
Il y avait des manifestations
depuis 2011,

7
00:00:22,000 --> 00:00:24,360
mais les différents gouvernements

8
00:00:24,360 --> 00:00:26,960
n’y avaient pas vraiment
prêté attention,

9
00:00:26,960 --> 00:00:29,280
ou peut-être qu’ils n’avaient pas
les moyens de répondre

10
00:00:29,280 --> 00:00:31,360
au problème du chômage
à Tataouine,

11
00:00:31,360 --> 00:00:35,560
sachant que le taux de chômage
y est très élevé.

12
00:00:35,560 --> 00:00:37,360
En tant qu’agents
des forces de l’ordre,

13
00:00:37,360 --> 00:00:40,080
on a soutenu les mouvements
de protestation,

14
00:00:40,080 --> 00:00:41,600
tant qu’ils restaient pacifiques.

15
00:00:41,600 --> 00:00:45,600
Surtout que ces mouvements
étaient soutenus

16
00:00:45,600 --> 00:00:48,960
par tous les habitants 
de Tataouine.

17
00:00:49,160 --> 00:00:51,720
Même les opérations
de blocage des routes

18
00:00:51,720 --> 00:00:55,600
ont été largement soutenues.

19
00:00:55,600 --> 00:00:57,720
Le mouvement s’est développé

20
00:00:57,720 --> 00:01:00,800
et de grandes manifestations
ont été organisées,

21
00:01:00,800 --> 00:01:02,960
du jamais-vu à Tataouine.

22
00:01:02,960 --> 00:01:04,960
Tout le monde y a participé,

23
00:01:04,960 --> 00:01:08,760
les jeunes, les vieux,
les femmes, les enfants.

24
00:01:08,760 --> 00:01:11,200
Des grèves générales
ont été observées

25
00:01:11,200 --> 00:01:17,840
et soutenues dans tous
les secteurs d’activité.

26
00:01:17,840 --> 00:01:21,200
Ensuite, les visites des délégations
ministérielles ont commencé

27
00:01:21,200 --> 00:01:23,880
et le dialogue s’est fait directement
avec les protestataires.

28
00:01:23,880 --> 00:01:26,040
Ça aussi c'était une première.

29
00:01:26,040 --> 00:01:29,680
Dès le départ, les protestataires
avaient refusé d’être représentés

30
00:01:29,680 --> 00:01:35,080
par les organisations syndicales, 
le patronat ou les partis politiques.

31
00:01:35,080 --> 00:01:38,720
C’est pour ça que le gouvernement
a été contraint

32
00:01:38,720 --> 00:01:41,000
de dialoguer directement
avec les jeunes

33
00:01:41,000 --> 00:01:44,560
et de faire des propositions visant
à réduire le taux de chômage.

34
00:01:44,640 --> 00:01:48,080
Mais le 22 mai n’était pas
un jour normal,

35
00:01:48,080 --> 00:01:50,320
le mouvement a sûrement
été infiltré.

36
00:01:50,320 --> 00:01:54,800
Certaines parties ont voulu
instrumentaliser ce mouvement

37
00:01:54,800 --> 00:01:58,160
au profit de leurs propres intérêts.

38
00:01:58,320 --> 00:02:00,760
C’est pour ça que dès le 21 mai,

39
00:02:00,760 --> 00:02:03,080
il y a eu une grande agitation
sur les réseaux sociaux, 

40
00:02:03,080 --> 00:02:04,720
surtout sur certaines pages.

41
00:02:04,720 --> 00:02:07,320
À tel point qu’on pouvait prévoir
dès le départ

42
00:02:07,320 --> 00:02:09,720
que la situation allait
dégénérer le 22 mai.

43
00:02:10,080 --> 00:02:13,760
Le 21 mai, de nombreuses rumeurs
ont circulé.

44
00:02:13,760 --> 00:02:19,680
Elles ont été largement partagées,
malgré les démentis.

45
00:02:19,680 --> 00:02:22,480
On disait par exemple que les unités
de la garde nationale

46
00:02:22,480 --> 00:02:25,320
étaient venues avec des chiens.

47
00:02:25,320 --> 00:02:26,640
C’est totalement faux.

48
00:02:26,640 --> 00:02:29,040
C’était une accusation intentionnelle

49
00:02:29,040 --> 00:02:30,920
pour diaboliser les forces de sécurité.

50
00:02:30,920 --> 00:02:34,400
Pareil, on a dit que 
la garde nationale avait prévu

51
00:02:34,400 --> 00:02:38,160
de disperser le sit-in
le 21 mai au soir,

52
00:02:38,600 --> 00:02:40,440
en prenant les protestataires
par surprise,

53
00:02:40,440 --> 00:02:42,480
alors qu’il n’en a jamais été question.

54
00:02:42,480 --> 00:02:46,080
Au contraire, le gouvernement
a donné des instructions claires.

55
00:02:46,080 --> 00:02:48,840
Ils ont insisté sur le respect
des protestataires

56
00:02:48,840 --> 00:02:50,280
et le droit de manifester.

57
00:02:50,280 --> 00:02:53,240
On n’avait pas du tout l'intention
de lever le sit-in par la force.

58
00:02:53,240 --> 00:02:55,320
Ces rumeurs ont été véhiculées

59
00:02:55,320 --> 00:02:59,760
dans le but de faire monter
la tension dans la région.

60
00:02:59,760 --> 00:03:01,800
Et effectivement, tous les habitants
de Tataouine

61
00:03:01,800 --> 00:03:06,280
sont sortis le 21 mai au soir
dans une grande manifestation.

62
00:03:06,280 --> 00:03:08,720
Ils se sont dirigés vers
le siège du gouvernorat 

63
00:03:08,720 --> 00:03:11,720
pour exprimer leur refus
de toute intervention.

64
00:03:11,720 --> 00:03:17,400
On leur a assuré qu’on n’avait pas
l’intention d’intervenir.

65
00:03:17,400 --> 00:03:21,640
Malgré ça, le 22 mai
a été un drame.

66
00:03:21,640 --> 00:03:24,560
La veille, des protestataires
d’El Kamour

67
00:03:24,560 --> 00:03:27,960
s'étaient dirigés vers
la station de pompage,

68
00:03:27,960 --> 00:03:29,720
dans le but de stopper
l’activité pétrolière.

69
00:03:29,720 --> 00:03:32,000
Les unités de la garde nationale
étaient sur place, 

70
00:03:32,000 --> 00:03:35,400
en soutien aux militaires.

71
00:03:35,400 --> 00:03:40,400
L'armée n'avait pas été capable
de protéger la station de pompage.

72
00:03:40,400 --> 00:03:43,320
Elle a demandé à la garde nationale
de venir en renfort.

73
00:03:43,320 --> 00:03:46,040
Le commandement revenait à l’armée

74
00:03:46,040 --> 00:03:47,440
et les instructions étaient claires :

75
00:03:47,440 --> 00:03:49,800
N'ouvrir le feu
sous aucun prétexte

76
00:03:49,800 --> 00:03:51,640
et protéger la station de pompage.

77
00:03:51,800 --> 00:03:59,040
Après avoir échoué dans cette tâche,

78
00:03:59,040 --> 00:04:04,280
l’armée a demandé l’aide
de la garde nationale. 

79
00:04:04,280 --> 00:04:08,040
On doit se rappeler que
le président de la République

80
00:04:08,040 --> 00:04:10,400
a officiellement désigné
l’armée nationale

81
00:04:10,400 --> 00:04:13,400
pour protéger les stations
de production pétrolière.

82
00:04:13,440 --> 00:04:14,440
Deux jours plus tard,

83
00:04:14,440 --> 00:04:16,200
la station de pompage
a été prise d’assaut

84
00:04:16,200 --> 00:04:17,560
et la production a été stoppée.

85
00:04:17,560 --> 00:04:19,160
L'alternative pour le gouvernement

86
00:04:19,160 --> 00:04:21,760
a été de recourir à
la garde nationale

87
00:04:21,760 --> 00:04:24,720
parce qu'elle peut
intervenir légalement

88
00:04:24,720 --> 00:04:28,360
et possède les équipements
nécessaires aux interventions

89
00:04:28,360 --> 00:04:29,800
comme le gaz lacrymogène.

90
00:04:29,800 --> 00:04:32,800
Alors que l'armée n'a pas
ce type d'équipements,

91
00:04:32,800 --> 00:04:34,600
elle n'a que des armes à feu.

92
00:04:34,840 --> 00:04:37,080
On savait qu'une
intervention sécuritaire

93
00:04:37,080 --> 00:04:39,280
n'allait pas régler le problème.

94
00:04:39,280 --> 00:04:42,160
Mais tout dépendait du
pacifisme des manifestants.

95
00:04:42,160 --> 00:04:47,160
Nos collègues ignoraient peut-être
les spécificités du terrain.

96
00:04:47,160 --> 00:04:50,080
Cela peut avoir une influence
sur le type d'intervention. 

97
00:04:50,080 --> 00:04:53,360
L’agitation qui a précédé
ces événements

98
00:04:53,360 --> 00:04:55,920
et la campagne de diffamation
contre les agents

99
00:04:55,920 --> 00:04:58,560
ont pu provoquer un certain nombre
de dépassements.

100
00:04:58,560 --> 00:05:02,800
Malgré tout, ils ont suivi
les instructions.

101
00:05:03,000 --> 00:05:06,360
Ils ne devaient pas s'en prendre
aux manifestants pacifiques

102
00:05:06,680 --> 00:05:09,200
mais il fallait aussi protéger
la station de production.

103
00:05:09,200 --> 00:05:11,320
Les unités de la garde nationale
présentes à El Kamour

104
00:05:11,320 --> 00:05:13,680
étaient sous le commandement
de l’armée.

105
00:05:13,840 --> 00:05:16,280
L’intervention a été faite à sa demande.

106
00:05:17,280 --> 00:05:19,280
Il peut y avoir des dépassements, 
je ne le nie pas.

107
00:05:19,680 --> 00:05:21,760
Toute intervention sécuritaire
présente des risques,

108
00:05:21,760 --> 00:05:25,440
il n’y a pas d’intervention parfaite.

109
00:05:26,520 --> 00:05:28,520
On apprend toujours de nos erreurs

110
00:05:28,520 --> 00:05:32,120
et il y a eu de plus grandes bavures
avant El Kamour.

111
00:05:32,360 --> 00:05:34,600
Tout ça est entre les mains
de la justice militaire.

112
00:05:34,800 --> 00:05:36,320
On a demandé une enquête

113
00:05:36,320 --> 00:05:40,640
et on acceptera les conclusions
qui en découleront.

114
00:05:40,640 --> 00:05:42,360
On assumera nos responsabilités.

115
00:05:42,480 --> 00:05:44,080
Un homme a perdu la vie.

116
00:05:44,080 --> 00:05:45,520
Il y a des dégâts matériels,

117
00:05:45,520 --> 00:05:47,800
des blessés des deux côtés,

118
00:05:47,800 --> 00:05:49,280
des équipements brûlés...

119
00:05:49,640 --> 00:05:51,920
Pourquoi après deux mois
d’un sit-in pacifique,

120
00:05:51,920 --> 00:05:55,160
les protestataires ont-ils été attaqués
de cette manière ?

121
00:05:55,160 --> 00:05:57,120
Je veux poser la question
d’un autre point de vue.

122
00:05:57,120 --> 00:06:00,680
Pourquoi après deux mois,

123
00:06:00,880 --> 00:06:04,440
les forces de sécurité voudraient
réprimer du jour au lendemain ?

124
00:06:04,440 --> 00:06:07,400
Ce sont deux points de vue différents

125
00:06:07,400 --> 00:06:08,560
d’un même événement. 

126
00:06:08,560 --> 00:06:11,840
Ils résument ce qui s'est passé
à Tataouine.

127
00:06:12,000 --> 00:06:14,000
Ce qui s’est passé le 21 mai

128
00:06:14,000 --> 00:06:17,000
aurait dû alerter sur ce qui allait 
se passer le lendemain.

129
00:06:17,000 --> 00:06:23,320
Le 21 mai était un prélude
et la suite était prévisible,

130
00:06:23,320 --> 00:06:27,240
mais les parties concernées
n'ont pas réagi comme il fallait.

131
00:06:27,240 --> 00:06:30,280
Au final, on en sort tous perdants,

132
00:06:30,280 --> 00:06:32,680
que ce soit les manifestants
ou les forces de l'ordre.

133
00:06:32,680 --> 00:06:36,920
Au final, la production pétrolière
a été stoppée.

134
00:06:37,160 --> 00:06:38,880
Au final, on a eu des pertes humaines

135
00:06:38,880 --> 00:06:40,400
et des pertes matérielles.

136
00:06:40,400 --> 00:06:42,440
Rien n'a été réalisé après le 22 mai.

137
00:06:42,440 --> 00:06:45,160
Aujourd'hui, les protestataires
attendent encore.

138
00:06:45,160 --> 00:06:48,840
J'espère que ça servira de leçon
pour l'avenir,

139
00:06:48,840 --> 00:06:51,040
pas seulement pour Tataouine
mais pour toutes les régions.

140
00:06:51,040 --> 00:06:54,640
Les demandes des protestataires
doivent aussi être écoutées.

141
00:06:54,640 --> 00:06:58,440
Les moyens du gouvernement
et les demandes des manifestants

142
00:06:58,440 --> 00:07:01,520
ne convergent pas forcément.

143
00:07:01,520 --> 00:07:04,000
Mais il faut être patient, endurant

144
00:07:04,000 --> 00:07:05,840
et toujours privilégier le dialogue.

145
00:07:05,840 --> 00:07:08,560
C'est la seule solution. 
