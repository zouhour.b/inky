jQuery(document).ready(function(){
    jQuery('.post-special').each(function(){

      var hdiv = jQuery('.post-caption', this).height();
      var himg = jQuery('.post-cover img', this).height();
      var post_id = jQuery(this).css('overflowY','hidden').attr('id');
      jQuery('.post-caption', this).attr('data-anchor-target', '#'+post_id+' .post-cover img')
      .attr('data-bottom-top', "transform: translateY("+(himg - hdiv)+'px)')
      .attr('data-top-bottom', "transform: translateY(0px)");

    });


    skrollr.init({
      forceHeight : false
    });
  });