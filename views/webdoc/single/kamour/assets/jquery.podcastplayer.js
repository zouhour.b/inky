/*
 * podcastplayer
 * https://github.com/medmar/jquery-podcastplayer
 *
 * Copyright (c) 2017 Mohamed marrouchi
 * Licensed under the MIT license.
 */


(function($) {
    
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    
    function enterFullscreen(element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }
    
    function toSeconds(t) {
        var s = 0.0
        if(t) {
          var p = t.split(':');
          for(i=0;i<p.length;i++)
            s = s * 60 + parseFloat(p[i].replace(',', '.'))
        }
        return s;
    };
    
    function toMinutes(t) {
        var seconds = parseInt(t);
        var minutes = parseInt(seconds / 60) + '';
        seconds = (seconds - minutes * 60) + '';
        
        if(minutes.length == 1){
            minutes = "0" + minutes;
        }

        if(seconds.length == 1){
            seconds = "0" + seconds;
        }

        return minutes + ':' + seconds;
    };

    function strip(s) {
        return s.replace(/^\s+|\s+$/g,'');
    };
    
    // Build the chapter list
    function injectChapters() {
        var podcastChapters = $('<ul/>', {
            'class': 'podcast-chapters',
            style: 'display:none'
        }).appendTo($($.podcastplayer.options.container));
        $.podcastplayer.chapters = [];
        $.podcastplayer.options.chapters.forEach(function(chapter){
            var podcastChapter = $('<li/>', {
                'class': 'podcast-chapter'
            }).appendTo(podcastChapters);
            $('<a/>', {
                href: 'javascript:void(0)',
                html: chapter.title,
                'data-time': chapter.time
            }).appendTo(podcastChapter);
            // Preload images
            var chapterImg = new Image();
            chapterImg.src = chapter.image;
        });
        // Init display & events
        $('.podcast-chapter:first', podcastChapters).addClass('podcast-chapter-active');
        $('.podcast-chapter a', podcastChapters).on('click', function(){
           var time = $(this).data('time');
           $.podcastplayer.wavesurfer.play(time);
           $(".podcast-player .podcast-pause").show();
           $(".podcast-player .podcast-play").hide();
           $('.podcast-chapter').removeClass('podcast-chapter-active');
           $(this).parent().addClass('podcast-chapter-active');
        });
        // Mobile menu
        var chapitreNavContainer = $('<div>', {
            'class': 'podcast-mobile-chapters',
            style: 'display:none'
        }).appendTo($($.podcastplayer.options.container));
        $('<button>', {
            'id': 'chapitreCollapse',
            'class': 'chapitre-toggle icon-chapitre',
            'html': 'CHAPITRES'
        }).appendTo(chapitreNavContainer);
        var chapitreNav = podcastChapters.clone(true);
        chapitreNav.removeClass('podcast-chapters')
                .addClass('podcast-menu-chapters')
                .appendTo(chapitreNavContainer);
        $('#chapitreCollapse').on('click', function(){
            chapitreNav.slideToggle('slow');
        });
    }
    
    // Build podcast DOM buttons
    function injectButtons() {
        var podcastButtonList = $('<ul/>', {
            'class': 'podcast-buttons'
        });
        if ($(window).width() < 480) { // Small devices
            podcastButtonList.insertAfter($('h1', $($.podcastplayer.options.btnContainer)));
        } else {
            podcastButtonList.prependTo($($.podcastplayer.options.btnContainer));
        }
        ['download','rss','play'].forEach(function(btn){
            var item = $('<li>').appendTo(podcastButtonList);
            $('<a>',{
                'class': 'podcast-button podcast-' + btn,
                'href': 'javascript:void(0)'
            }).appendTo(item);
        });
        
        $(".podcast-play", podcastButtonList)
            .on('click', function(){
                // Hide elements
                $($.podcastplayer.options.hideOnPlay).html('').fadeOut();
                $($.podcastplayer.options.subtitleContainer)
                        .removeClass('hidden-sm-down');
                // Show player
                $(".podcast-player").fadeIn().css('z-index', 100);
                $(".podcast-mobile-chapters").fadeIn();
                $(".podcast-chapters").fadeIn();
                setTimeout(function() {
                    $.podcastplayer.wavesurfer.play();
                }, 1000);                
                playSubtitles();
                $('.podcast-player .podcast-pause').show();
                $('.podcast-player .podcast-play').hide();
                // Set fullscreen on mobile
                if(isMobile.any()){
                    enterFullscreen(document.documentElement);
                }
            });
            
        $(".podcast-download", podcastButtonList).attr('href', $.podcastplayer.options.audioUrl);
        $(".podcast-download", podcastButtonList).attr('download', $.podcastplayer.options.audioUrl.replace(/^.*[\\\/]/, ''));
        $(".podcast-rss", podcastButtonList).attr('href', $.podcastplayer.options.rssUrl);
    };

    // Build the DOM player 
    function injectPlayer() {
        var podcastPlayer = $('<div/>', {
            'class': 'fullwidth podcast-player',
            style: 'z-index:-1'
        }).appendTo($($.podcastplayer.options.container));
        // Play buttons
        var podcastPlayButtons = $('<div/>', {
            'class': 'podcast-player-buttons',
        }).appendTo(podcastPlayer);
        var podcastPlayButtonList = $('<ul/>').appendTo(podcastPlayButtons);
        ['previous','pause','play','next'].forEach(function(btn){
            var item = $('<li>').appendTo(podcastPlayButtonList);
            $('<a>',{
                'class': 'podcast-player-button podcast-' + btn,
                'href': 'javascript:void(0)'
            }).appendTo(item);
        });
        // Current time counter
        var podcastCurrentTime = $('<div/>', {
            'class': 'podcast-counter podcast-current-time',
            'html': '00:00'
        }).appendTo(podcastPlayer);
        // Waveform container
        $('<div/>',{
            'class': 'podcast-waveform'
        }).appendTo(podcastPlayer);
        // Duration counter
        var podcastDuration = $('<div/>', {
            'class': 'podcast-counter podcast-duration vcenter',
            'html': '00:00'
        }).appendTo(podcastPlayer);
        // Sound buttons
        var podcastSoundButtons = $('<div/>', {
            'class': 'podcast-player-buttons',
        }).appendTo(podcastPlayer);        
        var podcastSoundButtonList = $('<ul/>').appendTo(podcastSoundButtons);
        ['volume','bubble','download'].forEach(function(btn){
            var item = $('<li>').appendTo(podcastSoundButtonList);
            $('<a>',{
                'class': 'podcast-player-button podcast-' + btn,
                'href': 'javascript:void(0)'
            }).appendTo(item);
        });
        // Initialize display & events
        
        $('.podcast-volume', podcastPlayer).addClass('podcast-volume-high');
        $(".podcast-pause", podcastPlayer).on('click', function(){
            $.podcastplayer.wavesurfer.pause();
            $(".podcast-pause", podcastPlayer).hide();
            $(".podcast-play", podcastPlayer).show();
        });

        $(".podcast-play", podcastPlayer).on('click', function(){
            $.podcastplayer.wavesurfer.play();
            $(".podcast-pause", podcastPlayer).show();
            $(".podcast-play", podcastPlayer).hide();
        });
        
        $(".podcast-previous", podcastPlayer).on('click', function(){
            var currentTime = $.podcastplayer.wavesurfer.getCurrentTime();
            var chapters = $.podcastplayer.options.chapters;
            for(var i=chapters.length-1; i>=0; i--) {
                var ci = chapters[i].time;
                if(ci < currentTime){
                    $.podcastplayer.wavesurfer.play((i == 0) ? 0 : chapters[i - 1].time);
                    break;
                }
            }
        });
        
        $(".podcast-next", podcastPlayer).on('click', function(){
            var currentTime = $.podcastplayer.wavesurfer.getCurrentTime();
            var chapters = $.podcastplayer.options.chapters;
            for(var i=0; i<chapters.length; i++) {
                var ci = chapters[i].time;
                if(ci > currentTime){
                    $.podcastplayer.wavesurfer.play(ci);
                    break;
                } else if(i === chapters.length - 1) {
                    $.podcastplayer.wavesurfer.play(0);                    
                    $($.podcastplayer.options.subtitleContainer).html('');
                }
            }
        });

        $(".podcast-volume", podcastPlayer).on('click', function(){
            if($(this).hasClass('podcast-volume-high')) {
                $(this).removeClass('podcast-volume-high');
                $(this).addClass('podcast-volume-mute');
                $.podcastplayer.wavesurfer.setVolume(0);
            } else if($(this).hasClass('podcast-volume-mute')) {
                $(this).removeClass('podcast-volume-mute');
                $(this).addClass('podcast-volume-high');
                $.podcastplayer.wavesurfer.setVolume(1);
            }
        });
        
        $(".podcast-bubble", podcastPlayer).on('click', function(){
            $($.podcastplayer.options.subtitleContainer).toggle();
            $(this).toggleClass("podcast-bubble-hidden");
        });
        
        $(".podcast-download", podcastPlayer).attr('href', $.podcastplayer.options.audioUrl);
        $(".podcast-download", podcastPlayer).attr('download', $.podcastplayer.options.audioUrl.replace(/^.*[\\\/]/, ''));
        
        // Play/Pause with spacebar
        $(window).keydown(function(e){
            if(e.keyCode == 32 && e.target == document.body){
                if($.podcastplayer.wavesurfer.isPlaying()) {
                    $(".podcast-pause", podcastPlayer).hide();
                    $(".podcast-play", podcastPlayer).show();
                } else {
                    $(".podcast-pause", podcastPlayer).show();
                    $(".podcast-play", podcastPlayer).hide();
                }
                $.podcastplayer.wavesurfer.playPause();
                e.preventDefault();
            }
        });
    };

    // Build the waveform
    function buildWaveform() {
        $.podcastplayer.wavesurfer = WaveSurfer.create({
            container: '.podcast-waveform',
            waveColor: '#b2b2b2',
            progressColor: 'white',
            hideScrollbar: true, 
            height: 75,
            normalize: true,
            cursorWidth: 0,
            barWidth: 1,
            responsive: true,
            backend: 'MediaElement',
        });
        
        $(window).resize(function() {
            //Resize waveform
            $.podcastplayer.wavesurfer.empty();
            $.podcastplayer.wavesurfer.drawBuffer();
        });
        
        $.get($.podcastplayer.options.waveUrl, function (wave) {
            $.podcastplayer.wavesurfer.load($.podcastplayer.options.audioUrl, wave.data, 'auto');
        });

        $.podcastplayer.wavesurfer.on('ready', function() {
            var duration = toMinutes($.podcastplayer.wavesurfer.getDuration())
            $('.podcast-duration').html(duration);
            $.podcastplayer.wavesurfer.setVolume(1);
        });
        
        $.podcastplayer.wavesurfer.on('finish', function() {
            $(".podcast-player .podcast-pause").hide();
            $(".podcast-player .podcast-play").show();
        });
    };

    // Load SRT file
    function loadSubtitles() {
        $.get($.podcastplayer.options.srtUrl, function (srt) {
            srt = srt.replace(/\r\n|\r|\n/g, '\n');
            srt = strip(srt);
            var srt_ = srt.split('\n\n');
            $.podcastplayer.subtitles = [];
            for(var s=0; s<srt_.length; s++) {
                var st = srt_[s].split('\n');
                if(st[0] === ''){
                    st.shift();
                }
                if(st.length >=2) {
                    var n = st[0],
                        i = strip(st[1].split(' --> ')[0]),
                        o = strip(st[1].split(' --> ')[1]),
                        t = st[2];
                    if(st.length > 2) {
                        for(j=3; j<st.length;j++){
                            if($(window).width() < 480) {
                                t += ' ';
                            } else {
                                t += '<br/>';
                            }
                            t += st[j];
                        }
                    }
                    var is = toSeconds(i);
                    var os = toSeconds(o);
                    $.podcastplayer.subtitles.push({i:i, o: o, t: t, is: is, os: os});
                }
            }
        });
        
    };

    // Display subtitles
    function playSubtitles() {
        var currentSubtitle = -1;
        var currentChapter = 0;
        $($.podcastplayer.options.subtitleContainer).addClass('podcast-subtitle').fadeIn();
        var ival = setInterval(function() {
            // Display appropriate subtitle
            var currentTime = $.podcastplayer.wavesurfer.getCurrentTime();
            
            var subtitle = -1;
            var subtitles = $.podcastplayer.subtitles;
            for(var i=0; i<subtitles.length; i++) {
                if(subtitles[i].is > currentTime)
                  break
                subtitle = i;
            }
            if(subtitle >= 0) {
                if(subtitle != currentSubtitle) {
                    $($.podcastplayer.options.subtitleContainer).html(subtitles[subtitle].t);
                    currentSubtitle=subtitle;
                } else if(subtitles[subtitle].os < currentTime) {
                    $($.podcastplayer.options.subtitleContainer).html('');
                }
            }
            // Highlight current chapter
            var chapters = $.podcastplayer.options.chapters;
            var index = chapters.length-1;
            for(var i=chapters.length-1; i>=0; i--) {
                var ci = parseFloat(chapters[i].time);                
                if(ci <= currentTime){
                    index = i;
                    break;
                }
            }
            if(index != currentChapter){
                currentChapter = index;
                $('.podcast-chapter').removeClass('podcast-chapter-active');
                $($('.podcast-chapter').get(currentChapter)).addClass('podcast-chapter-active');
                $('.background-image', $($.podcastplayer.options.container)).fadeOut('slow', function() {
                    $(this).attr('style', 'background-image: url(' + chapters[currentChapter].image + ')');
                    $(this).attr('data-chapter', currentChapter);
                    $(this).fadeIn('slow');
                });
            }
            // Update current time
            $('.podcast-current-time').html(toMinutes(currentTime));
        }, 250);
    };
    
    // Static method.
    $.podcastplayer = function(options) {
        // Override default options with passed-in options.
        $.podcastplayer.options = $.extend({}, $.podcastplayer.options, options);
        
        // Init
        injectButtons();
        injectPlayer();
        injectChapters();
        loadSubtitles();
        buildWaveform();
    }
    // Static method default options.
    $.podcastplayer.options = {
        selector: '',
        container: '',
        btnContainer: '',
        subtitleContainer: '',
        audioUrl: '',
        waveUrl: '',
        srtUrl: '',
        rssUrl: '',
        hideOnPlay: '',
        chapters: []
    };

}(jQuery));
