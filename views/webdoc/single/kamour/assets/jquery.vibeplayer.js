/*
 * vibeplayer
 * https://github.com/medmar/jquery-vibeplayer
 *
 * Copyright (c) 2017 Mohamed marrouchi
 * Licensed under the MIT license.
 */

(function($) {
    
    var ival;
    
    function toSeconds(t) {
        var s = 0.0
        if(t) {
          var p = t.split(':');
          for(i=0;i<p.length;i++)
            s = s * 60 + parseFloat(p[i].replace(',', '.'))
        }
        return s;
    };
    
    function toMinutes(t) {
        var seconds = parseInt(t);
        var minutes = parseInt(seconds / 60);
        seconds = seconds - minutes * 60;
        
        return (minutes + '').padStart(2, "0") + ':' + (seconds + '').padStart(2, "0");
    };

    function strip(s) {
        return s.replace(/^\s+|\s+$/g,'');
    };
        
    // Build podcast DOM buttons
    function injectButtons(vibe) {
        var detailsContainer = $('<div/>', {
            'class': 'vibe-details'
        }).appendTo($('.vibe-image', vibe));
        var vibeButtonList = $('<ul/>', {
            'class': 'vibe-buttons'
        }).appendTo(detailsContainer);
        ['download','play'].forEach(function(btn){
            var item = $('<li>').appendTo(vibeButtonList);
            $('<a>',{
                'class': 'vibe-button vibe-' + btn,
                'href': 'javascript:void(0)'
            }).appendTo(item);
        });
        var meta = $('<span/>', {
            'class': 'meta',
            'html': ' minutes'
        }).appendTo(detailsContainer);
        $('<span/>', {
            'class': 'extra',
            'html': vibe.data('duration')
        }).prependTo(meta);
        
        $(".vibe-play", vibeButtonList)
            .on('click', function(){
                // Stop any active player
                $(".vibe").trigger("stop");
                // Hide elements
                $(detailsContainer).fadeOut();
                // Show player
                $(".vibe-sticky", vibe).fadeIn();
                buildWaveform(vibe);
            });
        $(".vibe-download", vibeButtonList).attr('href', vibe.data('audio'));
        $(".vibe-download", vibeButtonList).attr('download', vibe.data('audio').replace(/^.*[\\\/]/, ''));
        
        // Social share
        var socialShareButtons = $('<div/>', {
            'class': 'vibe-social-buttons'
        }).appendTo($('.vibe-image', vibe));
        $('<a>', {
            'href': 'https://www.facebook.com/sharer/sharer.php?u='+document.location.href + '#' + $(vibe).attr('id'),
            'target': '_blank',
            'class': 'icon icon-facebook-2'
        }).appendTo(socialShareButtons);
        $('<a>', {
            'href': 'https://twitter.com/home?status='+document.title+' via @inkyfada '+ document.location.href + '%23' + $(vibe).attr('id'),
            'target': '_blank',
            'class': 'icon icon-twitter-2'
        }).appendTo(socialShareButtons);
    };
    
    // Build the DOM player 
    function injectPlayer(vibe, settings) {
        var vibeSticky = $('<div/>', {
            'class': 'fullwidth vibe-sticky',
            style: 'display:none'
        }).appendTo($('.vibe-image', vibe));
        // Player caption
        var vibeCaption = $('<div/>', {
            'class': 'fullwidth vibe-caption',
            style: 'display: none'
        }).appendTo(vibeSticky);
        $('<p/>', {
            'class': 'vibe-caption-content',
        }).appendTo(vibeCaption);
        // Player
        var vibePlayer = $('<div/>', {
            'class': 'row fullwidth vibe-player',
        }).appendTo(vibeSticky);
        // Play buttons
        var vibePlayButtons = $('<div/>', {
            'class': 'vibe-player-buttons',
        }).appendTo(vibePlayer);
        var vibePlayButtonList = $('<ul/>').appendTo(vibePlayButtons);
        ['pause','play'].forEach(function(btn){
            var item = $('<li>').appendTo(vibePlayButtonList);
            $('<a>',{
                'class': 'vibe-player-button vibe-' + btn,
                'href': 'javascript:void(0)'
            }).appendTo(item);
        });
        // Current time counter
        var vibeCurrentTime = $('<div/>', {
            'class': 'vibe-counter vibe-current-time',
            'html': '00:00'
        }).appendTo(vibePlayer);
        // Waveform container
        $('<div/>',{
            'class': 'vibe-waveform'
        }).appendTo(vibePlayer);
        // Duration counter
        var vibeDuration = $('<div/>', {
            'class': 'vibe-counter vibe-duration vcenter',
            'html': '00:00'
        }).appendTo(vibePlayer);
        // Sound buttons
        var vibeSoundButtons = $('<div/>', {
            'class': 'vibe-player-buttons',
        }).appendTo(vibePlayer);
        var podcastSoundButtonList = $('<ul/>').appendTo(vibeSoundButtons);
        ['volume', 'bubble', 'download'].forEach(function(btn){
            var item = $('<li>').appendTo(podcastSoundButtonList);
            $('<a>',{
                'class': 'vibe-player-button vibe-' + btn,
                'href': 'javascript:void(0)'
            }).appendTo(item);
        });
        // Initialize display & events
        $('.vibe-play', vibePlayer).hide();
        $('.vibe-volume', vibePlayer).addClass('vibe-volume-high');
        
        $(".vibe-bubble", vibePlayer).on('click', function(){
            $(".vibe-caption", vibe).slideToggle();
            $(this).toggleClass("vibe-bubble-hidden");
        });
        
        $(".vibe-download", vibePlayer).attr('href', vibe.data('audio'));
        $(".vibe-download", vibePlayer).attr('download', vibe.data('audio').replace(/^.*[\\\/]/, ''));
        
    };
    
    // Build the waveform
    function buildWaveform(vibe) {
        var wavesurfer = WaveSurfer.create({
            container: '#' + vibe.attr('id') + ' .vibe-waveform',
            waveColor: '#b2b2b2',
            progressColor: 'white',
            hideScrollbar: true, 
            height: 75,
            normalize: true,
            cursorWidth: 0,
            barWidth: 1,
            responsive: true,
            backend: 'MediaElement',
        });

        wavesurfer.load(vibe.data('audio'));

        wavesurfer.on('ready', function() {
            var duration = toMinutes(wavesurfer.getDuration());
            $('.vibe-duration', vibe).html(duration);
            wavesurfer.setVolume(1);
            // Play
            loadSubtitles(vibe, wavesurfer);
            wavesurfer.play();
        });
        
        var resetVibe = function() {
            // Hide elements
            $(".vibe-details", vibe).fadeIn();
            // Show player
            $(".vibe-sticky", vibe).fadeOut();
            wavesurfer.destroy();
            clearTimeout(ival);
        };
        
        wavesurfer.on('finish', resetVibe);
        vibe.on('stop', resetVibe);
        
        $(".vibe-pause", vibe).on('click', function(){
            wavesurfer.pause();
            $(".vibe-pause", vibe).hide();
            $(".vibe-play", vibe).show();
        });

        $(".vibe-sticky .vibe-play", vibe).on('click', function(){
            wavesurfer.play();
            $(".vibe-play", vibe).hide();
            $(".vibe-pause", vibe).show();
        });
        
        $(".vibe-volume", vibe).on('click', function(){
            if($(this).hasClass('vibe-volume-high')) {
                $(this).removeClass('vibe-volume-high');
                $(this).addClass('vibe-volume-mute');
                wavesurfer.setVolume(0);
            } else if($(this).hasClass('vibe-volume-mute')) {
                $(this).removeClass('vibe-volume-mute');
                $(this).addClass('vibe-volume-high');
                wavesurfer.setVolume(1);
            }
        });
    };


    // Load SRT file
    function loadSubtitles(vibe, wavesurfer) {
        $.get(vibe.data('srt'), function (srt) {
            srt = srt.replace(/\r\n|\r|\n/g, '\n');
            srt = strip(srt);
            var srt_ = srt.split('\n\n');
            var subtitles = [];
            for(var s=0; s<srt_.length; s++) {
                var st = srt_[s].split('\n');
                if(st[0] === ''){
                    st.shift();
                }
                if(st.length >=2) {
                    var n = st[0],
                        i = strip(st[1].split(' --> ')[0]),
                        o = strip(st[1].split(' --> ')[1]),
                        t = st[2];
                    if(st.length > 2) {
                        for(j=3; j<st.length;j++)
                            t += '<br/>'+st[j];
                    }
                    var is = toSeconds(i);
                    var os = toSeconds(o);
                    subtitles.push({i:i, o: o, t: t, is: is, os: os});
                }
            }
            playSubtitles(vibe, wavesurfer, subtitles);
        });
    };

    // Display subtitles
    function playSubtitles(vibe, wavesurfer, subtitles) {
        var currentSubtitle = -1;
        $('.vibe-caption', vibe).fadeIn();
        ival = setInterval(function() {
            // Display appropriate subtitle
            var currentTime = wavesurfer.getCurrentTime();
            var subtitle = -1;
            for(var i=0; i<subtitles.length; i++) {
                var is = parseFloat(subtitles[i].is);
                if(is > currentTime)
                  break
                subtitle = i;
            }
            if(subtitle >= 0) {
                if(subtitle != currentSubtitle) {
                    $('.vibe-caption-content', vibe).html(subtitles[subtitle].t);
                    currentSubtitle=subtitle;
                } else if(subtitles[subtitle].os < currentTime) {
                    $('.vibe-caption-content', vibe).html('');
                }
            }
            // Update current time
            $('.vibe-current-time', vibe).html(toMinutes(currentTime))
        }, 250);
    };
    
    // Static method.
    $.fn.vibeplayer = function(options) {
        var defaults = {
        
        };
        // Override default options with passed-in options.
        var settings = $.extend({}, defaults, options);
        
        return this.each(function(index, vibe) {
            // Init
            $(vibe).attr('id', 'vibe_' + index);
            injectButtons($(vibe));
            injectPlayer($(vibe), settings);
            
        });
    }

}(jQuery));