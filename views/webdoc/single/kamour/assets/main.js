jQuery(document).ready(function(){
	jQuery('.search-toggler').click(function(){
		jQuery('body').toggleClass('search-toggled');
		if (jQuery('body').hasClass('search-toggled')){
			jQuery('#search-input').focus();
		}
	});
	jQuery('.newsletter-toggler').click(function(){
		jQuery('body').toggleClass('newsletter-toggled');
	});
	jQuery('.newsletter-form>.icon-close').click(function(){
		jQuery('body').removeClass('newsletter-toggled');
	});
	jQuery('.newsletter-container').append(jQuery('.mc4wp-response'));
	if(jQuery('.mc4wp-success').length>0){
		jQuery('#email').val("Abonnement pris en compte. Merci!");
	}else{
		jQuery('#email').val("Entrez votre adresse email");
	}

	jQuery('p').each(function(index, item) {
		if(jQuery.trim(jQuery(item).text()) === "" && jQuery(item).find('img').length==0) {
        jQuery(item).slideUp(); // $(item).remove();
    }
});
});

//SR
jQuery(document).ready(function(){

	jQuery('[data-sr]').each(function(){
		jQuery(this).attr('data-sr-tmp', jQuery(this).data('sr'));
	});
	window.sr = new scrollReveal({reset: false, mobile:   true});
});




Parallaxer = function(args){
	this.args =args;
	this.sel = args.sel;
	this.init();
}

Parallaxer.prototype={
	loadPositions: function(){
		this.$items.each(function(){
			this.pos = jQuery(this).offset();
		});
	},
	init: function(){
		this.$items = jQuery(this.sel);
		this.loadPositions();
		
		var P = this;
		setTimeout(function(){P.loadPositions()}, 500);
		jQuery(window).scroll(function(){
			var s = jQuery(window).scrollTop();
			P.$items.each(function(){
				jQuery(this).css('backgroundPosition', "0 "+((this.pos.top-s)/10)+'px');
				if (jQuery(this).parent('.parallax-fadeOut').length>0 || jQuery(this).parent('.parallax-fadeOut-caption').length>0){
					calcOpacity = function(s,p1,p2){
						if (s<p1){
							return 1;
						}
						if (s>p2){
							return 0;
						}
						return 1-(s-p1)/(p2-p1);
					}


					if (jQuery(this).parent('.parallax-fadeOut').length>0){
						var opacity = calcOpacity(s,this.pos.top - jQuery(this).height(), this.pos.top);
						jQuery(this).css('opacity', (1+ opacity)/2);
						jQuery(this).next('.card-caption').css('opacity', 1-opacity);

					}
					if (jQuery(this).parent('.parallax-fadeOut-caption').length>0){
						var opacity = calcOpacity(s,this.pos.top , this.pos.top+ jQuery(this).height());

						jQuery(this).next('.card-caption').css('opacity', opacity);

					}

				}

			});
		});
	}
}

jQuery(document).ready(function(){
	new Parallaxer({sel:'.parallax .background-image'});
});

jQuery(document).ready(function(){
	contentMenu = new ContentMenu();
});

ContentMenu = function(args){
	this.args = args;
	this.init();
}

ContentMenu.prototype = {
	init: function(){
		this.menu = [];
		this.levelMin = 3;
		var CM = this;
		jQuery('.entry-content').find('.menu-level-1, .menu-level-2, .menu-level-3').each(function(){
			
			var text = jQuery(this).text();
			if (jQuery(this).hasClass('component')){
				var _text = jQuery(this).find('figcaption, .caption, .figcaption');
				if (!_text.length) _text = jQuery(this).next('figcaption, .caption, .figcaption');
				if (_text.length){
					text = _text.text();
				} 
				else text = "#"+jQuery(this).attr('id');
			}
			
			var level = CM.getLevel(jQuery(this));
			if (level > 0){

				CM.levelMin = Math.min(level, CM.levelMin);
				CM.menu[CM.menu.length] = {label:text, level:level, $sel:jQuery(this), children:[]};
			}
		});
		for (var i=0; i<this.menu.length; i++){
			this.menu[i].level = this.menu[i].level - this.levelMin;
		}
		var M = {children:[], level:-1};
		var currentLevel1 = null;
		var currentLevel2 = null;
		for (var i=0; i<this.menu.length; i++){
			var item = this.menu[i];
			if (item.level==0){
				currentLevel1 = item;
				currentLevel2 = null;
				M.children[M.children.length] = item;
			}
			if (item.level == 1 && currentLevel1){
				currentLevel2 = item;
				currentLevel1.children[currentLevel1.children.length] = item;
			}
			if (item.level == 2 && currentLevel2){
				currentLevel2.children[currentLevel2.children.length] = item;
			}

		}
		this.menuComputed = M;
		this.display();
		jQuery('.content-menu').on('click', '[level]', function(){
			var target = CM.getMenuTarget(jQuery(this).attr('level'));
			var $target = target.$sel;
			var top = $target.offset().top;
			if ($target[0].tagName.toLowerCase() != 'figure'){
				top = top -60;
			}
			var  body = jQuery("html, body");
			body.stop().animate({scrollTop:top}, 500, 'swing');
			//jQuery(window).scrollTop(top-60);
		});
		jQuery('.content-menu-toggler').click(function(){
			jQuery('.content-menu').toggleClass('closed');
			var iclose = 'icon-close';
			var iopen = 'icon-post-menu';
			var icon = !jQuery('.content-menu').hasClass('closed')?iclose:iopen;
			jQuery(this).removeClass(iclose).removeClass(iopen).addClass(icon);
		});
		jQuery('[class*="menu-mirror-"]').css('cursor', 'pointer').click(function(){
			var A = this.className.match(/menu-mirror-\d+/);

			var menuID = A[0][A[0].length-1];
			jQuery('.content-menu ul.level--1 > li:nth-child('+menuID+') > a').click();

		});
	},
	display: function($el){
		if (jQuery('.content-menu').length ==0){
			jQuery('.entry-content').after('<div class="content-menu closed menu bg-gray-dark p-2"><span class="content-menu-toggler icon-post-menu"></span><div class="content-menu-menu"></div></div>');
		}
		else {
			jQuery('.content-menu').html('<span class="content-menu-toggler icon-post-menu"></span><div class="content-menu-menu"></div>');
		}
		if (this.menu.length == 0){
			if (!jQuery('body').hasClass('admin-bar')){
				
				jQuery('.content-menu').hide();
			}
			jQuery('.content-menu-menu').html("<h5>Menu vide</h5><small>Ajoutez la classe <strong>menu-level-[1/2/3]</strong> à un élément du contenu pour ajouter des éléments au menu</small>");	
			return;
		}
		var html = this.displayNode(this.menuComputed);
		jQuery('.content-menu-menu').html(html);
	},
	displayNode: function(node,levelprefix){
		var html ='';
		var levelprefix = levelprefix || "";
		if (node.level != -1)
			html += '<a level="'+levelprefix+'" class="level-'+node.level+'">'+levelprefix+" "+node.label+'</a>';
		if (node.children.length){
			html += '<ul class="navbar-nav level-'+node.level+'">';
			for (var i=0; i<node.children.length; i++ ){
				_levelprefix = levelprefix+(i+1)+'.';
				html += '<li>'+this.displayNode(node.children[i], _levelprefix)+'</li>';
			}
			html += '</ul>';
		}
		return html;
	},

	getMenuTarget: function(order){
		var _o = order.substr(0,order.length-1).split('.');

		function returnSub(el,indexArray){
			if (indexArray.length == 0) return null;
			var ind = parseInt(indexArray[0])-1;
			if (indexArray.length == 1) return el.children[ind];
			var _indexArray = indexArray.slice(1);
			return returnSub(el.children[ind], _indexArray)
		};

		return returnSub(this.menuComputed,_o);
	},

	getLevel : function($el){
		var level  = 0;
		if ($el.hasClass('menu-level-1')){
			level = 1;
			return level;
		}
		if ($el.hasClass('menu-level-2')){
			level = 2;
			return level;
		}
		if ($el.hasClass('menu-level-3')){
			level = 3;
			return level;
		}

		return level;
	}
}


jQuery(document).ready(function(){
	new MakeModal();
});

MakeModal = function(args){
	this.args = args;
	this.selector = '.make-modal';
	this.init();
}

MakeModal.prototype = {
	init: function(){
		this.loadItems();
	},
	loadItems: function(){
		var M =this;
		jQuery(this.selector).each(function(){
			M.initItem(jQuery(this));
		})
	},
	initItem: function($item){
		var id= $item.attr('id');
		var content = $item.html();
		var size = $item.attr('data-size');
		size = size?" modal-"+size:'';
		var output = '\
		<div class="modal fade" id="'+id+'-modal">\
		<div class="modal-dialog'+size+'" role="document">\
		<div class="modal-content">\
		<div class="modal-header">\
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">\
		<span aria-hidden="true">&times;</span>\
		</button>\
		</div>\
		<div class="modal-body">'+content+'</div>\
		</div>\
		</div>\
		</div>';
		$item.replaceWith(output);
		var togglers = jQuery('.modal-toggle-'+id);
		if (togglers.length == 0) {
			alert( 'no toggler for '+id);
			return;
		}
		togglers.attr('href', '#'+id+'-modal').attr('data-toggle', 'modal');
		
	}
}




jQuery(document).ready(function(){
	Sticked = new Sticker();
});

Sticker = function(){
	this.init();
}

Sticker.prototype = {
	init: function(){

		var S = this;
		this.items = [];
		jQuery('.make-sticky').each(function(){
			var el = jQuery(this);
			var target = el.attr('data-target');
			if (!target){
				$target = el.parents('.entry-content');
			}
			else {
				$target = jQuery(target);
			}
			if ($target.length != 1){
				alert('humm');
				return;
			}

			S.items[S.items.length] = {$: jQuery(this), $container: $target};

		});
		jQuery(window).scroll(function(){
			S.update();
		});
	},
	update: function(){
		var S = this;
		this.items.forEach(function(item, index){
			var containerTop = item.$container.offset().top;
			var containerBottom = containerTop + item.$container.height();
			var itemBottom = item.$.offset().top+item.$.height();
			var itemMT = S.helpers.getMargin(item.$[0]).top;
			var itemMB = S.helpers.getMargin(item.$[0]).bottom;
			var itemHeight = item.$.height();
			var wh = jQuery(window).height();
			var scrollPos = jQuery(window).scrollTop();
			if (scrollPos<containerTop){
				item.$.css('position', 'absolute').css('top',0);
				return;
			}
			var offset = scrollPos - containerTop;
			if (itemHeight>jQuery(window).height()){
				var	a = (containerBottom - containerTop - itemHeight-itemMB - itemMT)/(containerBottom - containerTop- wh);
				var	b = - containerTop * a;
				offset = a * scrollPos + b; 

			}
			if (scrollPos>containerTop && scrollPos+itemHeight + itemMB  + itemMT< containerBottom ){
				item.$.css('position', 'absolute').css('top', offset);
				return;
			}
			if (scrollPos+ itemHeight + itemMB + itemMT > containerBottom){
				item.$.css('position', 'absolute').css('top', containerBottom - containerTop-itemHeight-itemMT-itemMB);
				return;
			}
		});

	},
	helpers: {
		getMargin: function (element) {
			var style;
			if (window.getComputedStyle) { 
				style = window.getComputedStyle(element, null); 
			}
			else 
				{ style = element.currentStyle; }

			return {
				top: parseFloat(style.marginTop.replace('px','')),
				bottom: parseFloat(style.marginBottom.replace('px', ''))
			}
		}
	}

}



menuHandler = function(args){
	this.args = args;
	this.init();
}

menuHandler.prototype = {
	init:function(){
		var MH = this;
		this.$ = jQuery(this.args.sel);
		this.menuToggler = jQuery('.expanded-nav-toggler');
		this.searchToggler = jQuery('.search-toggler');
		this.expandedNav = jQuery(this.menuToggler.attr('data-target'));
		if (!this.$.length) alert('no menu ' + this.args.sel);

		this.menuToggler.click(function(){
			if (jQuery(this).hasClass('active')){
				MH.closeNav();
			}
			else {
				MH.openNav();
			}
		});
	},
	openNav: function(){
		this.expandedNav.addClass('active');
		this.menuToggler.addClass('active');
		jQuery('body').addClass('main-menu-expanded');
	},
	closeNav: function(){
		this.expandedNav.removeClass('active');
		this.menuToggler.removeClass('active');
		jQuery('body').removeClass('main-menu-expanded');


	},
	append:function(data, index){
		var cls = this.$.children('li:not(.active)').attr('class');
		this.$.append('<li class="'+cls+'">'+data+'</li>')

	},
        destroy: function() {
            this.$.find('li').remove();
        }
}
jQuery(document).ready(function(){
	if (jQuery('#main-nav').length){
		window.mainMenu = new menuHandler({sel:"#main-nav"});
	}
});



ScrollFadeGallery = function(args){
	this.args = args;
	this.init();
}

ScrollFadeGallery.prototype = {
	init: function(){
		this.container = jQuery(this.args.sel);
		var SFG = this;
		items = [];
		this.container.find('.screen').each(function(v,i){
			var ind = jQuery(this).index()+1;
			if (jQuery(this).find('> figcaption').length){
				if (jQuery(this).find('> div.background-image.sfg-caption.mask-dark'));
				jQuery(this).prepend('<div class="mask-dark card-caption top left background-image sfg-caption"></div>');
				jQuery(this).find('> figcaption').addClass('sfg-caption');
				items.push({sel:'.screen:nth-child('+ind+')', endOffset:1});
				items.push({sel:'.screen:nth-child('+ind+') .sfg-caption', endOffset:0});
			}
			else {
				items.push({sel:'.screen:nth-child('+ind+')', endOffset:0});
			}
		});
		this.items = items;
		animations = [{sel:'.screen', fn: 'stick', start:0, end:0}];
		for (var i=1; i<=items.length; i++){
			//Counting progress starts when container top matches window bottom and 
			// ends when container bottom matches window top
			// We need to add 2 extra screen in order to start animation when 
			// the container fills the viewport.
			var start = i==1?0:Math.floor(i*100/(this.items.length+2));
			var endOffset = this.items[i-1].endOffset +1;
			var end = i+endOffset-1==this.items.length?101:Math.floor((i+endOffset)*100/(this.items.length+2));

			animations.push({
				sel: items[i-1].sel,
				start: start,
				end: end,
				fn: 'fadeInOut',
			});
		}
		
		this.scrollListener = new ScrollListener({
			sel: SFG.args.sel,
			slidesHeight: (SFG.items.length * 100)+'vh',
			animations: [
			{
				slide: 1,
				animations: animations
			}
			] 
		});
	}
}

