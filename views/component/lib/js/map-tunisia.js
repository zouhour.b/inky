function buildTunisiaMap(s,d, params){
	var data = d.map(function(e){
		return {hasc: e.key, value: e.value, key:e.key, label: getDepartementName(e.key, params.isRTL?'ar':'fr')}
	});
	var getTooltip = function(params){
		if (params.isRTL){
			return '<span>'+ params.unit +'</span>'
			+'<b>{point.value} </b>'
			+'<span> : </span>'
			+'<span>{point.label}</span>';
			
		}else{
			return '<div> {point.label}</div>'
			+'</div><div style="width:10px">:</div>'
			+'<div style="font-weight:bold;">{point.value}</div>'
			'<div>'+ params.unit +'</div>';

		}
	}
	var options = {
		title : {text : ''},
		subtitle : {text : ''},
		mapHeight: 600,
		mapNavigation: {
			enabled: false,
			buttonOptions: {
				verticalAlign: 'bottom'
			}
		},
		chart:{
			height: jQuery(s).width()*1.5
		},
		legend: {
			enabled:true,
			title: {
				text: params.legend,
				style: {
					color: 'black',
				}
			},

		},
		colorAxis: {
			minColor: params.minColor?params.minColor:'#EEEEEE',
			maxColor: params.maxColor?params.maxColor:'#e74c3c',
		},

		series : [{
			animation: {
				duration: 0 
			},
			data : data,
			borderColor:'#FFFFFF',
			borderWidth: 1,
			mapData: Highcharts.maps['countries/tn/tn-all'],
			joinBy: 'hasc',
			allowPointSelect: true,
			cursor: 'pointer',
			states: {
				hover: {
					color:'#bdc3c7',
				},
				select:{
					color:'#bdc3c7'
				}
			},
			tooltip: {
				headerFormat:'',
				pointFormat: getTooltip(params),
			},
			dataLabels: {
				enabled: false,
				format: '{point.label}'
			}
		}]
	}
	jQuery(s).highcharts('Map', options).highcharts();

}

function getDepartementName(k, lang){
	if (!lang){
		lang = 'fr';
	}
	let gouvs = [{"key":"TN.MN","name_fr":"Ariana","name_ar":"أريانة"},{"key":"TN.SF","name_fr":"Sfax","name_ar":"صفاقس"},{"key":"TN.ME","name_fr":"Medenine","name_ar":"مدنين"},{"key":"TN.TO","name_fr":"Tozeur","name_ar":"توزر"},{"key":"TN.AR","name_fr":"Manouba","name_ar":"منوبة"},{"key":"TN.BJ","name_fr":"Beja","name_ar":"باجة"},{"key":"TN.BA","name_fr":"Ben Arous","name_ar":"بن عروس"},{"key":"TN.BZ","name_fr":"Bizerte","name_ar":"بنزرت"},{"key":"TN.JE","name_fr":"Jendouba","name_ar":"جندوبة"},{"key":"TN.NB","name_fr":"Nabeul","name_ar":"نابل"},{"key":"TN.TU","name_fr":"Tunis","name_ar":"تونس"},{"key":"TN.KF","name_fr":"Le Kef","name_ar":"الكاف"},{"key":"TN.KS","name_fr":"Kasserine","name_ar":"القصرين"},{"key":"TN.GB","name_fr":"Gabes","name_ar":"قابس"},{"key":"TN.GF","name_fr":"Gafsa","name_ar":"قفصة"},{"key":"TN.SZ","name_fr":"Sidi Bouzid","name_ar":"سيدي بوزيد"},{"key":"TN.SL","name_fr":"Siliana","name_ar":"سليانـــة"},{"key":"TN.MH","name_fr":"Mahdia","name_ar":"المهدية"},{"key":"TN.MS","name_fr":"Monastir","name_ar":"المنستير"},{"key":"TN.KR","name_fr":"Kairouan","name_ar":"القيروان"},{"key":"TN.SS","name_fr":"Sousse","name_ar":"سوسة"},{"key":"TN.ZA","name_fr":"Zaghouan","name_ar":"زغوان"},{"key":"TN.KB","name_fr":"Kebili","name_ar":"قبلي"},{"key":"TN.TA","name_fr":"Tataouine","name_ar":"تطاوين"}];
	let g = gouvs.find(e=>e.key==k);
	if (g && g['name_'+lang]){
		var label = g['name_'+lang];
		return label;
	}
	return k;
}

