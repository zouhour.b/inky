jQuery.fn.colorMap = function(data, options){
	
	const $svg = jQuery(this).find('svg');
	const $tooltip = jQuery(this).find('.map-tooltip');
	const $legend = jQuery(this).parent().find('.map-legend');
	let maxValue = parseFloat(data[0].value);
	let minValue = parseFloat(data[0].value);
	data = data.map(r=>{
		r.value = parseFloat(r.value)
		if (r.value < minValue) minValue = r.value;
		if (r.value > maxValue) maxValue = r.value;
		return r;
	});
	options.colors = options.colors?options.colors:[];
	options.colors = options.colors.map(r=>{
		r.min = parseFloat(r.min);
		r.max = parseFloat(r.max);
		return r;
	})
	const getValue = function(k, lang){
		const seek = data.find(e=>e.key ==k);
		if (seek) return seek.value;
		else return null;
	}
	let d3 = window.d3;
	let colorGradient = d3? d3.scale.linear().domain([1,100])
	.interpolate(d3.interpolateHcl)
	.range([d3.rgb(options.minColor), d3.rgb(options.maxColor)]):()=>null;

	let colorPallier = function(v){
		for (var k=0; k<options.colors.length; k++){
			let pallier = options.colors[k];
			if (pallier.min <= v && pallier.max >= v){
				return pallier.color;
			}
		}
		return '';
	}
	const drawLegendGradient = function(){
		let scale = jQuery('<div>');
		scale.css('background', 'linear-gradient(90deg, '+options.minColor+', '+options.maxColor+')');
		scale.css('height', '10px').css('width', '200px').css('minWidth', "150px");
		let text = jQuery('<div>');
		text.addClass('pos-relative w-100').html('<span class="pos-absolute min direct-0 bottom-0"></span><span class="pos-absolute max opposite-0 bottom-0"></span>')
		text.find('.min').html(minValue);
		text.find('.max').html(maxValue);

		$legend.append(text);    	
		$legend.append(scale);
		$(this).addClass('map-legend-scale');
	}

	const drawLegendPallier = function(){
		options.colors.map(pallier=>{
			let $pallier = jQuery('<div>');
			pallier.text = pallier.min+' - '+pallier.max;
			if (pallier.min == pallier.max) pallier.text = pallier.min;
			$pallier.addClass('mb-2 d-inline-block');
			$pallier.html('<div class="mx-1 d-inline-block"><span class="d-block align-self-center rect"></span><span class="d-block">'+pallier.text+'</span></div>');
			$pallier.find('.rect').css('background', pallier.color);
			$legend.append($pallier);
		})
		$legend.find('.rect').css({
			minHeight: '15px',
			minWidth: '50px'
		});
	}

	const getColor = (k)=>{
		const v = getValue(k);
		if (!v && v!==0) {return null};
		if (!options.colors.length){
			let p = 100*(v-minValue)/(maxValue - minValue);
			return colorGradient(Math.floor(p));
		}
		return colorPallier(v);
	}

	$svg.find('.gouvernorat[data-key]').each(function(){
		const key = this.getAttribute('data-key').toUpperCase().replace('-', '.');
		let color  = getColor(key);
		if (color !==null){
			jQuery(this).css('fill', color);
		}
	});
	if (options.colors.length){
		drawLegendPallier();

	}
	else {
		drawLegendGradient();
	}
	$svg.on('mousemove', '.gouvernorat[data-key]', function(e){
		$tooltip.css({
			position: 'fixed',
			display: 'block', 
			top:e.clientY, 
			left:e.clientX, 
			visibility: 'visible',
			transform:'translate(-50%, 20px)'
		});
	});
	$svg.on('mouseleave', '.gouvernorat[data-key]', function(e){
		$tooltip.css({
			display: 'none',
			visibility: 'hidden', 
		});
	});

	$(window).scroll(function(e){
		$tooltip.css({
			display: 'none',
			visibility: 'hidden', 
		});
	});
	$svg.on('mouseenter', '.gouvernorat[data-key]', function(e){
		$tooltip.css({
			visibility: 'visible', 
		});
		const key = this.getAttribute('data-key').toUpperCase().replace('-', '.');

		const label = getDepartementName(key, options.lang)
		$tooltip.find('.key').html(label);
		let v = getValue(key);
		if (!v && v!==0) {
			if (!$('body').hasClass('rtl')){
				v = 'Données indisponibles'
			}
			else{
				v = 'معطيات غير متوفرة'
			}
			$tooltip.find('.unit').hide();
		}
		else{
			$tooltip.find('.unit').show();

		}

		let p = 100*(v-minValue)/(maxValue - minValue)
		$tooltip.find('.value').html(v+ ' ');

	});

	

	const getDepartementName = function(k, lang){
		if (!lang){
			lang = 'fr';
		}
		let gouvs = [{"key":"TN.AR","name_fr":"Ariana","name_ar":"أريانة"},{"key":"TN.SF","name_fr":"Sfax","name_ar":"صفاقس"},{"key":"TN.ME","name_fr":"Medenine","name_ar":"مدنين"},{"key":"TN.TO","name_fr":"Tozeur","name_ar":"توزر"},{"key":"TN.MN","name_fr":"Manouba","name_ar":"منوبة"},{"key":"TN.BJ","name_fr":"Beja","name_ar":"باجة"},{"key":"TN.BA","name_fr":"Ben Arous","name_ar":"بن عروس"},{"key":"TN.BZ","name_fr":"Bizerte","name_ar":"بنزرت"},{"key":"TN.JE","name_fr":"Jendouba","name_ar":"جندوبة"},{"key":"TN.NB","name_fr":"Nabeul","name_ar":"نابل"},{"key":"TN.TU","name_fr":"Tunis","name_ar":"تونس"},{"key":"TN.KF","name_fr":"Le Kef","name_ar":"الكاف"},{"key":"TN.KS","name_fr":"Kasserine","name_ar":"القصرين"},{"key":"TN.GB","name_fr":"Gabes","name_ar":"قابس"},{"key":"TN.GF","name_fr":"Gafsa","name_ar":"قفصة"},{"key":"TN.SZ","name_fr":"Sidi Bouzid","name_ar":"سيدي بوزيد"},{"key":"TN.SL","name_fr":"Siliana","name_ar":"سليانـــة"},{"key":"TN.MH","name_fr":"Mahdia","name_ar":"المهدية"},{"key":"TN.MS","name_fr":"Monastir","name_ar":"المنستير"},{"key":"TN.KR","name_fr":"Kairouan","name_ar":"القيروان"},{"key":"TN.SS","name_fr":"Sousse","name_ar":"سوسة"},{"key":"TN.ZA","name_fr":"Zaghouan","name_ar":"زغوان"},{"key":"TN.KB","name_fr":"Kebili","name_ar":"قبلي"},{"key":"TN.TA","name_fr":"Tataouine","name_ar":"تطاوين"}];
		let g = gouvs.find(e=>e.key==k);
		if (g && g['name_'+lang]){
			var label = g['name_'+lang];
			return label;
		}
		return k;
	}

}