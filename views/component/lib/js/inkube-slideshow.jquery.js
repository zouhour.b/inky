jQuery.fn.InkubeSlideshow1 = function(sliderData, options){
	let container = jQuery(this);
	let getSlides = function(){
		return container.children('.slide');
	}
	let getCurrentActiveSlide = function(s){
		let active = null;
		getSlides().each(function(){
			let r = this.getBoundingClientRect();
			if (r.y < 10) {
				active = this;
			}
		});
		return active;

	}
	let init = function (data){
		container.html('');

		sliderData.map(function(item, index){
			let slide = jQuery('<div>');
			slide.addClass('slide sticky-top slide-'+item.type).css('height', '400vh').css('backgroundColor', options.backgroundColor );
			let screen = jQuery('<div>');
			let src = item.src;
			if (jQuery(window).width()<768 && item.src_mobile){
				src = item.src_mobile;
			}
			screen.addClass('w-100 h-12 pos-relative');
			if (item.type=="image"){

				let image = jQuery('<div>');
				image.css('backgroundImage', 'url('+src+')').addClass('bg-cover h-6 w-100 h-md-12');
				image.addClass(item.image_class);
				
				let mask = jQuery('<div>');
				mask.addClass('mask pos-md-absolute px-3 top-0 left-0 h-6 h-md-12 w-100')
				.css('background', 'rgba(0,0,0,.6)');
				if ($(window).width()<768){
					mask.css('background', 'black');
				}

				let caption = jQuery('<figcaption>');
				caption.html(item.caption).addClass('pos-md-absolute vcenter direct-0 offset-md-6 col-md-6 color-white');
				mask.append(caption);
				
				screen.append(image);
				mask.hide();
				if (item.caption){
					
					screen.append(mask)
				}
				else {
					if (jQuery(window).width()<768){
						screen.css('paddingTop','25vh');
					}
				}
			}
			if (item.type=="svg"){
				let image = jQuery('<div>');
				image.addClass('h-12 w-100 h-md-12');
				jQuery.get(src, function(data){
					let innerHTML = new XMLSerializer().serializeToString(data.documentElement);
					image.html(innerHTML);
				});
				screen.append(image);
			}

			if (item.type=='video'){

				let video = jQuery('<video>');
				video.append('<source src="'+src+'" type="video/mp4" />');
				video.attr('preload','auto').attr('loop', 'loop').attr('autobuffer', 'true');
				video.addClass('w-100 h-md-12 h-6').css('objectFit', 'cover');
				
				screen.append(video);
			}
			if (index==sliderData.length-1){
				screen.addClass('sticky-top').removeClass('pos-relative');
			}
			slide.append(screen);
			container.append(slide);
		});

	}

	let isMobile = function(){
		return jQuery(window).width() < 768;
	}

	jQuery(window).resize(function(){
		let slides = getSlides();
		if (!slides.length) return;
		if (isMobile()){
			slides.css('height', '150vh');
			
		}else{
			slides.css('height', '400vh');
		}
	});


	let data = []
	let previousSlidesHeight = 0;
	getSlides().each(function(){
		previousSlidesHeight += jQuery(this).height();
		let img = jQuery(this).find('.bg-cover');
		let video = jQuery(this).find('video');
		let src = '';
		let caption = '';
		if (video.length){
			src = video.find('source').attr('src');
		}
		if (img.length){
			src = img.css('backgroundImage').replace('url(', '').replace(')', '');
			caption = jQuery(this).find('span').text().trim();
		}
		data.push({src, caption});


	});
	


	let calcSlideProgress = function(slide, s){
		const ct = container.position().top;
		const cb = ct + container.height();
		var slides = getSlides();
		if (s < ct || s > cb){
			return 0;
		}
		const deltaScroll = (s - ct);
		const index = jQuery(slide).index();
		let previousSlidesHeight = 0
		for (var i=0; i<index; i++){
			previousSlidesHeight += jQuery(slides[i]).height();
		}
		let p = deltaScroll - previousSlidesHeight;
		return p/jQuery(slide).height();


	}





	pauseAllPlayerExcept = function(active){
		getSlides().each(function(){
			if (jQuery(this).index() == jQuery(active).index()){
				return;
			}
			let video = jQuery(this).find('video');
			if (video.length){
				video[0].pause();
			}
		})
	}

	playSlidePlayer = function(active){
		let video = jQuery(active).find('video');
		if (video.length && video[0].paused){
			video[0].play();
		}
	}
	container.find('.caption').hide();
	init(sliderData);

	jQuery(window).scroll(function(){
		let s = jQuery(this).scrollTop();
		let active = getCurrentActiveSlide(s);
		let p = 100* calcSlideProgress(active, s);
		getSlides().removeClass('active');
		jQuery(active).addClass('active');
		pauseAllPlayerExcept(active);
		playSlidePlayer(active);
		window.aa = active;
		if (p<25 && !isMobile()){
			jQuery(active).find('.mask').fadeOut(400);
		}
		else {
			jQuery(active).find('.mask').fadeIn(400);
		}

	});
}

