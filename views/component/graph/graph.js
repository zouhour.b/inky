class Dataset{
  constructor(args){
    this.data = args.data;
    this.lines = args.lines;
    this.axis = {
      y:{min:args.yaxis.min, max:args.yaxis.max}

    }
    this.unit = args.unit;
  }

  toAmchartData(){
    let json = [];
    this.data[0].map((x, colIndex)=>{
      if (colIndex==0) return;
      let r = {};
      r.label = this.getAbscisse(colIndex);
      this.data.map((row,rowIndex)=>{
        if (rowIndex===0) return;
        let lab = this.getSerieLabel(rowIndex);
        let val = this.getValue(rowIndex, colIndex )
        r[lab] = val;
      });
      json.push(r);
    });
    return json;


  }
  getGraphs(){
    let graphs = [];
    this.data.map((row, index)=>{
      if (index==0) return;
      let       graphData = {};
      graphData.title = this.getSerieLabel(index);
      graphData.field = graphData.title;

      graphData.color = this.lines[index-1].color;
      let gdata = this.lines[index-1];
      gdata = {color: '#000', thickness:1, dotColor: '#000', ...gdata}
      graphData.thickness = this.lines[index-1].thickness;
      graphData.dotColor = this.lines[index-1].dotColor;

      graphs.push(graphData);
    });
    return graphs;
  }

  getAbscisse(x){
    return this.data[0][x];
  }

  getSerieLabel(y){
    return this.data[y][0];
  }

  getValue(serie, x){
    return this.data[serie][x];
  }


}

makeLineChart = function(elementId, data, args){
  am4core.useTheme(am4themes_animated);

  var chart = am4core.create(elementId, am4charts.XYChart);
  chart.paddingRight = 20;
  chart.data = data.toAmchartData();

  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "label";
  categoryAxis.renderer.line.strokeOpacity = 1;
  categoryAxis.renderer.grid.template.disabled = true;


  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.grid.template.disabled = true;// Pre zoom
  valueAxis.baseValue = 0;
  valueAxis.renderer.line.strokeOpacity = 1;
  if (data.axis.y.min && data.axis.y.max){
    valueAxis.min = parseFloat(data.axis.y.min);
    valueAxis.max = parseFloat(data.axis.y.max);
    valueAxis.strictMinMax = true;
  }

  data.getGraphs().map((g,index)=>{
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = g.field;
    series.name = g.title;
    series.dataFields.categoryX = 'label';
    series.tensionX = 0.77;
    series.stroke = am4core.color(g.color);
    series.strokeWidth = g.thickness;
    let bullets = series.bullets.push(new am4charts.CircleBullet());
    //bullets.circle.stroke = am4core.color("#FFFFFF");
    bullets.circle.strokeWidth = 3;
    bullets.circle.fill = am4core.color(g.color);


    series.tooltipText = "{name} : [bold]{valueY} "+data.unit+'[/bold]';
    series.legendSettings.valueText = "{valueY}";
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color(g.color);

  });

  chart.cursor = new am4charts.XYCursor();
  chart.legend = new am4charts.Legend();


}

makeBarChart = function(elementId, data, args){
  am4core.useTheme(am4themes_animated);

  var chart = am4core.create(elementId, am4charts.XYChart);
  chart.paddingRight = 20;
  chart.data = data.toAmchartData();

  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "label";
  categoryAxis.renderer.line.strokeOpacity = 1;
  categoryAxis.renderer.grid.template.disabled = true;


  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.grid.template.disabled = true;// Pre zoom
  valueAxis.baseValue = 0;
  valueAxis.renderer.line.strokeOpacity = 1;
  if (data.axis.y.min && data.axis.y.max){
    valueAxis.min = parseFloat(data.axis.y.min);
    valueAxis.max = parseFloat(data.axis.y.max);
    valueAxis.strictMinMax = true;
  }

  data.getGraphs().map((g,index)=>{
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = g.field;
    series.name = g.title;
    series.dataFields.categoryX = 'label';
    series.fill = am4core.color(g.color);
    series.sequencedInterpolation = true;
  
  // Make it stacked
    series.stacked = true;
    series.strokeWidth = g.thickness;
    

    series.tooltipText = "{name} : [bold]{valueY} "+data.unit+'[/bold]';
    series.legendSettings.valueText = "{valueY}";
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color(g.color);

  });

//  chart.cursor = new am4charts.XYCursor();
  chart.legend = new am4charts.Legend();


}

$.fn.makeLineChart = function(args){

  const elementId = $(this).attr('id')
  makeLineChart(elementId, new Dataset(args));

}

$.fn.makeBarChart = function(args){
  alert(args.data);
  const elementId = $(this).attr('id');
  makeBarChart(elementId, new Dataset(args));
}


function test(){
  const link = 'https://docs.google.com/spreadsheets/d/12lSdKFCQBVBwYSNgH3aatnEqLujCiU_Di6PcuG6_3JY/edit?usp=sharing';
  const id = link.split('/spreadsheets/d/')[1].split('/')[0];
  const jsonLink = 'https://spreadsheets.google.com/feeds/cells/'+id+'/1/public/values?alt=json-in-script&callback=doData';
  console.log(jsonLink);
}

test();