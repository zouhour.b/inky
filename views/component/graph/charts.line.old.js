class Dataset{
  constructor(args){
    this.data = args.data;
    this.lines = args.lines;
  }

  toAmchartData(){
    let json = [];
    this.data[0].map((x, colIndex)=>{
      if (colIndex==0) return;
      let r = {};
      r.label = this.getAbscisse(colIndex);
      this.data.map((row,rowIndex)=>{
        if (rowIndex===0) return;
        let lab = this.getSerieLabel(rowIndex);
        let val = this.getValue(rowIndex, colIndex )
        r[lab] = val;
      });
      json.push(r);
    });
    return json;


  }
  getGraphs(){
    let graphs = [];
    this.data.map((row, index)=>{
      if (index==0) return;
      let       graphData = {};
      graphData.title = this.getSerieLabel(index);
      graphData.field = graphData.title;

      graphData.color = this.lines[index-1].color;
      let gdata = this.lines[index-1];
      gdata = {color: '#000', thickness:1, dotColor: '#000', ...gdata}
      graphData.thickness = this.lines[index-1].thickness;
      graphData.dotColor = this.lines[index-1].dotColor;

      graphs.push(graphData);
    });
    return graphs;
  }

  getAbscisse(x){
    return this.data[0][x];
  }

  getSerieLabel(y){
    return this.data[y][0];
  }

  getValue(serie, x){
    return this.data[serie][x];
  }


}

makeChart = function(elementId, data, args){
  var chart;

  chart = new AmCharts.AmSerialChart();
  chart.dataProvider = data.toAmchartData();
  chart.categoryField = 'label';
  chart.color = "#000";
  chart.fontSize = 10;
  chart.fontFamily =  'Akrobat';
  chart.panEventsEnabled = false;
  chart.maxZoomLevel = 1;
  chart.startDuration = 1;
  chart.numberFormatter = {
    precision:-1,decimalSeparator:",",thousandsSeparator:""
  };

  chart.responsive = {
    "enabled": true,
  };
  // AXES
  // category
  var categoryAxis = chart.categoryAxis;
  //categoryAxis.minorGridEnabled = true;
  categoryAxis.axisColor = "#666";
  categoryAxis.twoLineMode = true;
  categoryAxis.axisAlpha = 1;
  categoryAxis.labelOffset = 5;
  categoryAxis.gridAlpha = 0;
  categoryAxis.axisThickness = 1;
  categoryAxis.labelRotation = 45;

   // second value axis (on the right)
   var valueAxis2 = new AmCharts.ValueAxis();
   valueAxis2.title = "";
   valueAxis2.titleBold = false;
   valueAxis2.position = "left"; 
   valueAxis2.axisColor = "#666";
   valueAxis2.gridAlpha = 0;
   valueAxis2.axisThickness = 1;
   valueAxis2.labelsEnabled = true;
   chart.addValueAxis(valueAxis2);

  // second graph
  data.getGraphs().map((graphData, index)=>{

    var graph = new AmCharts.AmGraph();
    graph.valueAxis = valueAxis2; 
    graph.title = graphData.title;
    graph.valueField = graphData.field;
    graph.balloonText = "<span style='font-size:14px'>"+graph.title+": <b> [["+graphData.field+"]] </b></span>";
    graph.bullet = "round";
    //graph.type = "smoothedLine";
    graph.lineColor = graphData.color;
    graph.hideBulletsCount = 20;
    graph.bulletBorderThickness = 1;
    graph.lineThickness = graphData.thickness;
    graph.bulletAlpha=1;

    chart.addGraph(graph);
  })

  // CURSOR
  var chartCursor = new AmCharts.ChartCursor();
  chartCursor.cursorAlpha = 0.1;
  chartCursor.fullWidth = true;
  chartCursor.selectWithoutZooming = true;
  chart.addChartCursor(chartCursor);

  // WRITE
  chart.write(elementId);
}

$.fn.makeLineChart = function(args){
  
  const elementId = $(this).attr('id')
  makeChart(elementId, new Dataset(args));

}