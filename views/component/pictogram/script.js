jQuery.fn.pourcentage = function(data, config) {

    $this = jQuery(this)

    var selector = "#" + jQuery(this).attr('id')
    var id = jQuery(this).attr('id');
    var options = {
        cols: 20,
        patternHeight: 70,
        patternScale: 0.11,
        width: 800,
        menuClass: "",
        ...config
    }

    var currentDataSet = config.default || Object.keys(data)[0];


    setOptions(options)
    var viewport = null
    var pattern = {
        width: options.width / options.cols,
        height: options.patternHeight ? options.patternHeight : 100,
        scale: options.patternScale ? options.patternScale : .15
    }


    tooltip = $(this).next();


    function appendData(tooltipData) {
        $('.tooptip-value').html(tooltipData.value)
        $('.tooptip-label').html(tooltipData.label)
        $('.tooptip-unit').html(tooltipData.unit)
        var pourcentage = (tooltipData.value * 100 / tooltipData.total).toFixed(2)
        $('.tooptip-pourcentage').html(pourcentage + "%")
    }

    function initEventListener() {

        $this.find('[serie]').mouseenter(function(e) {

            var bboxSVG = $this[0].getBoundingClientRect()

            var mouse_quadrant = {
                north: e.clientY > bboxSVG.height / 2,
                west: e.clientX > bboxSVG.width / 2
            }

            var tooltipPosition = {
                top: `${mouse_quadrant.north ? e.clientY - tooltip.height() - 20 : e.clientY + 25}px`,
                left: `${mouse_quadrant.west ? e.clientX - tooltip.width() + 23 : e.clientX - 21}px`,
            }

            tooltip.css({
                top: tooltipPosition.top,
                left: tooltipPosition.left
            })


            var s = $(this).attr('serie')
            var tooltipData = data[currentDataSet][s]
            appendData(tooltipData)

            if ($(this).has('serie' + s)) {
                $('use:not(.serie-' + s + ')').addClass('highlight-3')
            }

        }).mouseleave(function() {
            $('use').removeClass('highlight-3')
            tooltip.hide()
        }).mouseenter(function() {
            tooltip.show()
        })
    }


    init()
    build()
    initEventListener()

    function setOptions(options) {
        this.options = {}

        options.cols = options.cols ? options.cols : 10;
        options.width = options.width ? options.width : 640;

        pattern = {
            width: options.width / options.cols,
            height: options.patternHeight ? options.patternHeight : 100,
            scale: options.patternScale ? options.patternScale : .15
        }
    }

    function calcPos(n) {
        var x = n % options.cols * pattern.width;
        var y = Math.floor(n / options.cols) * pattern.height;
        return {
            x: x,
            y: y
        }
    }


    function getD3Data() {
        vd = d3.range(100).map(function(i) {
            return {
                x: calcPos(i).x,
                y: calcPos(i).y,
                to: Math.floor(calcPos(i).x + pattern.width / 2, 3) + "px " + Math.floor(calcPos(i).y + pattern.height / 2, 3) + "px"
            }
        });

        var currentData = data[currentDataSet];

        var sum = d3.sum(currentData, function(d) {
            return d.value
        });
        var currentPos = 0;

        for (var i = 0; i < currentData.length; i++) {

            var percentage = (currentData[i].value * 100 / sum)

            for (var j = currentPos; j < percentage + currentPos; j++) {

                vd[j].color = currentData[i].color
                vd[j].class = "serie-" + i;
                vd[j].serie = i

            }
            currentPos = currentPos + Math.floor(percentage)

        }
        return vd

    }

    function refreshEvents() {

        var $this = this;
        viewport.selectAll('.pattern').on('mouseenter', function() {

            var n = parseInt(d3.select(this).attr('serie'));
            viewport.attr('class', 'selected-' + n + ' selected');

        });

    }

    function build() {

        var d3data = getD3Data();

        currentPos = 0

        d3.select(selector + " #infographic").selectAll('.pattern-origin')
            .attr('transform', 'scale(' + pattern.scale + ')')

        viewport.selectAll('use').data(getD3Data()).enter()
            .append('use').attr('xlink:href', selector + '-def')
            .attr('x', function(d) {
                return d.x
            })
            .attr('y', function(d) {
                return d.y
            })
            .style('transform-origin', function(d) {
                return d.to
            });

        viewport.selectAll('use')
            .attr('class', function(d) {
                return 'pattern ' + d.class
            })
            .attr('serie', function(d) {
                return d.serie
            }).style('fill', function(d) {
                return d.color
            })


        refreshEvents();
    }
    // generate menu
    function generateTabMenu() {

        var count = Object.keys(data).length;

        if (data && count > 1) {

            var ul = document.createElement('ul');
            ul.setAttribute('class', 'svg-control infographic-control ' + options.menuClass);


            var _data = config.lang == 'ar' ? Object.keys(data).reverse() : Object.keys(data)

            Object.keys(data).map(function(key) {
                var li = document.createElement('li');

                var index = config.default || _data[0]

                if (key == index) {
                    li.setAttribute('class', 'active');
                }

                ul.appendChild(li);

                var a = document.createElement('a');
                a.setAttribute('href', '#');
                a.setAttribute('data-target', '#infographic');
                a.setAttribute('data-set', key);
                a.innerText = key;

                li.appendChild(a);
            });
            document.querySelector(config.id).appendChild(ul);
        }
    }
    // init svg
    function init() {

        generateTabMenu()

        $svg = `<svg viewBox="0 0 900 400" dir="ltr"
          class="infographic infographic-2 bg-white" id="infographic">
          <defs>
            <g id="${id}-def"
            ${config.defs}
            </g>
          </defs>
          <g class="viewport"></g>
        </svg>`

        $(config.id).append($svg)

        viewport = d3.select(selector + ' .viewport')
        viewport.attr('transform', 'translate(50,50)')
    }

    jQuery(config.id + ' .infographic-control [data-target][data-set]').click(function(e) {
        e.preventDefault();
        jQuery(config.id + ' .infographic-control li').removeClass('active');
        jQuery(this).parent().addClass('active');
        currentDataSet = jQuery(this).attr("data-set")

        build();
    });
}