jQuery.fn.scrollNav = function(args){
	console.log({args})
	let getSize = function(){
		return (jQuery(window).width()<768)?'sm':'md';
	}
	let container = jQuery(this);
	let size = getSize();
	
	let steps = args.steps;
	
	let getAnimations = function(){
		let animations = args.animations[size]?args.animations[size]:[];
		let steps = args.steps[size]?args.steps[size]:[];
		let path = container.find('.drawpath').each(function(){
			let length = this.getTotalLength();
			jQuery(this).css('strokeDasharray', length+ ' '+length).css('strokeDashoffset', length);

		});

		steps.map((step,i)=>{
			if (i>=steps.length-1) return;
			let  = param = [step, steps[i+1]];
			console.log(param);
			const coords = ['x','y','z'];
			for (var key in coords){
				let k = coords[key];
				param[0][k] = parseFloat(param[0][k]);
				param[1][k] = parseFloat(param[1][k]);
			}
			animations.push({
				sel: '.svg-wrapper',
				fn: 'svgnav',
				start: parseFloat(step.p),
				end: parseFloat(steps[i+1].p),
				param: param
			})
		});
		console.log({animations});
		return animations;

	}
	let setup = ()=>{
		jQuery('.svg-container svg', container).css('height','100%').css('width', '100%');
		new ScrollListener({
			sel: '#'+container.attr('id'),
			slidesHeight: args.height[size],
			animations: [
			{
				slide: 1,
				animations: getAnimations(),
			}
			] 
		});
	}


	let build = function(size){
		let url = args.svg[size];
		if (!url) url = args.svg.md;
		if (!url) url = args.svg.sm;
		if (!url) {
			alert('Select a SVG');
			return;
		}
		jQuery.get(url, function(data) {

			let svg = new XMLSerializer().serializeToString(data.documentElement);
			container.find('.svg-container').html(svg);
			setup();
		});
		
	}
	if (jQuery(window).width()< 768){
		build('sm');
	}
	else build('md');


}

