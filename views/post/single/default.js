jQuery(document).ready(function(){
	var extractHeader = function(articleHeader){
		if (articleHeader.find('.entry-header').length){
			articleHeader.children().each(function(){
				if (!jQuery(this).hasClass('entry-header')){
					jQuery(this).remove();
				}
			});
		}else{
			articleHeader.children().each(function(){
				if (jQuery(this).index() !==0){
					jQuery(this).remove();
				}
			});

		}
		articleHeader.addClass('next-post-header').css('cursor','pointer');
		return articleHeader;
	}
	
	var loadNextPost = function(){
		jQuery('body .next-post-holder').each(function(){
			if (jQuery(this).find('a.next-post-link').length==0){
				return;
			}

			var url = jQuery(this).find('a.next-post-link').attr('href');
			var id = jQuery(this).find('a.next-post-link').attr('id');
			
			var $holder = jQuery(this);
			jQuery.get(url, function(r){
				let d = jQuery('<div>');
				d.html(r);
				var articleHeader = extractHeader(d.find('article#'+id));
				let e = jQuery('<div>');
				e.html(r);
				var article = e.find('article').addClass('next-post-loaded');
				$holder.replaceWith(articleHeader);
				Inkube.players.refresh()
				let nextPost = e.find('.next-post-holder');
				if (nextPost.length>1){
					alert('wtf');
				}
				else if (nextPost.length==1){

					nextPost = nextPost[0].outerHTML;
				}
				else{
					nextPost = null;
				}

				jQuery('body .next-post-header').click(function(){
					jQuery(this).removeClass('next-post-header');
					var prev = jQuery(this).prev();
					if (history.pushState) {
						history.pushState(null, null, url); 
					}

					var t = this.getBoundingClientRect().y;
					var newScrollTop = 0;
					jQuery(this).replaceWith(article);
					prev.remove();

					jQuery(window).scrollTop(newScrollTop);
					if (window.loadAsyncAssets) loadAsyncAssets();
					Inkube.players.refresh()
					if (nextPost){
						jQuery('body .next-post-loaded').after(nextPost);
					}
					jQuery('.next-post-loaded').removeClass('next-post-loaded');
					setTimeout(loadNextPost,1000);
				});

			});
		});
	}
	loadNextPost();
});