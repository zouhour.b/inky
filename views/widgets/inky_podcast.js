
//player
function player() {
var songs = ["/wp-content/themes/inkyfada/assets/audio/audio.mp3","/wp-content/themes/inkyfada/assets/audio/audio.mp3","/wp-content/themes/inkyfada/assets/audio/audio.mp3"];
var poster = ["https://i.picsum.photos/id/0/5616/3744.jpg?hmac=3GAAioiQziMGEtLbfrdbcoenXoWAW-zlyEAMkfEdBzQ","https://picsum.photos/200","https://i.picsum.photos/id/0/5616/3744.jpg?hmac=3GAAioiQziMGEtLbfrdbcoenXoWAW-zlyEAMkfEdBzQ"];
  

 $(".slick-arrow").each( function(){
  if ($(this).hasClass("slick-disabled")) {
  $(this).addClass('d-none')

}
 })       

  
$(".slick-arrow").on('click', function(){

  if ($(this).hasClass("slick-disabled")) {
  $(this).siblings(".slick-arrow").removeClass('d-none')
  $(this).addClass('d-none')
} else
  $(this).removeClass('d-none')

})

        var fillBar = $("#fill");
        
        var song = new Audio();
        var currentSong = 0;    

             //formatting seconds to time
             function formatSecondsAsTime(secs, format) {
                var hr  = Math.floor(secs / 3600);
                var min = Math.floor((secs - (hr * 3600))/60);
                var sec = Math.floor(secs - (hr * 3600) -  (min * 60));

                if (min < 10){ 
                  min = "0" + min; 
                }
                if (sec < 10){ 
                  sec  = "0" + sec;
                }

                return min + ':' + sec;
              }
              //get duration of podcasts
              
        $("i#play").on('click', function(){
             if ((song.src == '') || (($(this).parent().data('mp3Url') != null) && (song.src != $(this).parent().data('mp3Url')))){
             song.src = $(this).parent().data('mp3Url');
             $('.player').css("display", "block");
             } 
        	 if(song.paused){
                song.play();
                  $(this).addClass("fa-pause");
                  $(this).removeClass("fa-play");
                  $('.player > i#play').addClass("fa-pause");
                  $('.player > i#play').removeClass("fa-play");
                 
                
               
            }
            else{              
                song.pause();
                  $(this).addClass("fa-play");
                  $(this).removeClass("fa-pause");
                  $('.player > i#play').addClass("fa-play");
                  $('.player > i#play').removeClass("fa-pause");
                  
            }
            
        });  
        
       
        
        
        fillBar = $("#fill")
        song.addEventListener('timeupdate',function(){ 
            
            var position = song.currentTime / song.duration;
            
            fillBar.css("width", position * 100 +'%');
            currentTimeinSecs = Math.floor(song.currentTime).toString();
            durationinSecs = Math.floor(song.duration).toString();
             $("#current-time").html(formatSecondsAsTime(currentTimeinSecs));
             $("#duration-time").html(formatSecondsAsTime(durationinSecs));
        });
        
            $("i#forward").on('click', function(){
            });
            
            
            $("i#next").on('click', function(){
                 currentSong++;
                  if(currentSong > (songs.length - 1)){
                      currentSong = 0;
                  }
                 $('.bg-image').css( "background", "url("+poster[currentSong]+") no-repeat center" );
                  song.src = songs[currentSong];
                  song.play();
                  $(this).addClass("fa-pause");
                  $(this).removeClass("fa-play");
               
            });  
    
        
            $("i#prev").on('click', function(){
                 currentSong--;
                  if(currentSong < 0){
                      currentSong = songs.length - 1;
                  }
                 $('.bg-image').css( "background", "url("+poster[currentSong]+") no-repeat center" );
                  song.src = songs[currentSong];
                  song.play();
                  $(this).addClass("fa-pause");
                  $(this).removeClass("fa-play");
               
            }); 

            //volume bar
            var e = document.querySelector('.volume-slider-con');
            var eInner = document.querySelector('.volume-slider');
            var drag = false;
            e.addEventListener('mousedown',function(ev){
               drag = true;
               updateBar(ev.clientX);
            });
            document.addEventListener('mousemove',function(ev){
               if(drag){
                  updateBar(ev.clientX);
               }
            });
            document.addEventListener('mouseup',function(ev){
             drag = false;
            });
            var updateBar = function (x, vol) {
               var volume = e;
                    var percentage;
                    //if only volume have specificed
                    //then direct update volume
                    if (vol) {
                        percentage = vol * 100;
                    } else {
                        var position = x - volume.offsetLeft;
                        percentage = 100 * position / volume.clientWidth;
                    }

                    if (percentage > 100) {
                        percentage = 100;
                    }
                    if (percentage < 0) {
                        percentage = 0;
                    }

                    //update volume bar and video volume
                    eInner.style.width = percentage +'%';
                    song.volume = percentage / 100;
            };


              $('.dur-item').each(function(){  
                var s = new Audio();
                let el = $(this)
                  s.src = $(this).data('podcastUrl');
                  s.setAttribute('preload', 'metadata');
                  //console.log($(el)[0])
                  s.onloadedmetadata = function() {
                  //console.log(s.duration)
                    durationinSecs = Math.floor(s.duration).toString();
                    $(el).find('.podcast_time').html(formatSecondsAsTime(durationinSecs));
                    };
              })


  }

      
  


function sliders(selector, dots, arrows, dotsClass, numberOfslides, fade, center){

$.each( $(selector), function(){
 $(this).slick({
  infinite: false,
  speed: 300,
  slidesToShow: numberOfslides,
  slidesToScroll: 1,
  nextArrow: $(this).siblings('.next'),
  prevArrow: $(this).siblings('.prev'),
  arrows: arrows,
  appendDots: dotsClass,
  dots: dots,
  centerMode: center,
  fade: fade,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
}) 
});


}







$( document ).ready(function(){
let container = $('.podcast-content-container')
container.trigger('container-loaded')
if (!container.length){
}
    $('a.ajax-nav').click(function(e){
      e.preventDefault();

      let url = $(this).attr('href');
      $.get(url, function(data){
        let tmp = $('<div>');
        tmp.html(data);
        container.html('');
        tmp.find('.podcast-content-container').children().appendTo(container);
        container.trigger('container-loaded');
      })
     
    })


 });




$(document).on('container-loaded', '.podcast-content-container', function(e){
  //$('.player').css("display", "none")
if (e.delegateTarget.activeElement.id == "series") {
  $('.first-slider').each(function(){
      sliders($(this), false, false, null, 1, true, false)    
    });

} else {
  $('.first-slider').each(function(){
      sliders($(this), true, false, $(this).prev( ".series-items" ), 1, true, false)
    });
   sliders('.series', true, true, null ,4, false, false)
}

 
  player()

})
