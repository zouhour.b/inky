$ = jQuery;
jQuery(document).ready(function(){
	
	var videoplayer = Inkube.players.videoPlayers["kerkennah-webdoc-player"];
	jQuery('#cover-kerkennah').addClass('is-playing');
	setTimeout(function(){
		videoplayer.play();
alert('unmute');
		videoplayer.muted(false);
	},1000);
	videoplayer.on('ended', function(){
		jQuery('#cover-kerkennah').removeClass('is-playing').addClass('player-ended');
	});

	$btnAudio = $('.wave-bars');
	$btnAudio.click(function(){
		let muted = videoplayer.muted();
		videoplayer.muted(!muted);
		$(this).toggleClass('muted');
	});
	$btnSkip = $('.skip-video');
	$btnSkip.click(function(){
		var d = videoplayer.duration();
		videoplayer.currentTime(d-0.1);
		videoplayer.play();
	});
	jQuery('.kerkennah-nav [data-target], .bouncing-button, [data-target="#video-webdoc-kerkennah"]').click(function(){
		var target = jQuery(this).data('target');
		if (!target.length) return;
		if (target ==='#kerkennah-about'){
			jQuery('.kerkennah-nav').hide();
		}
		else{
			jQuery('.kerkennah-nav').show();
		}
		jQuery('.kerkennah-nav [data-target]').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('#cover-kerkennah .item').removeClass('active');
		jQuery('[data-target="'+target+'"]').addClass('active');
		jQuery(target).addClass('active');
	});
});

