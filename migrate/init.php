<?php 
use Inkube\Model\Field;
global $Migration;
$Migration = new Inkube\Migration\MigrationModule();

add_filter('get_post_oldLink', function($v, $id, $obj){
	$p = get_post($id);
	$oldid = get_post_meta($p->ID, 'oldid', true);
	return 'https://inkyfada.com/?p='.$oldid;
},3,3);

add_filter('get_post_form', function($form, $type, $template, $id){
	if (!$id) return $form;
	$form->add(new Field('migration-status', [
		'type'=>'select',
		'label'=>'Etat de la migration',
		'options'=>[
			['label'=>'Validé Pour mobile et Desktop', 'value'=>'valid-mobile-desktop'],
			['label'=>'Validé Pour Desktop', 'value'=>'valid-desktop'],
			['label'=>'A valider', 'value'=>'needs-validation'],
			['label'=>'Contient des erreurs', 'value'=>'not-ready']

		]
	]), 'migration');
	$form->add(new Field('oldLink', ['type'=>'button', 'label'=>'Article Original', 'className'=>'primary', 'size'=>'auto*32px']), 'migration');
	$form->add(new Field('migration-errors',
		[
			'label'=>'Erreurs',
			'className'=>'w-100 my-3',
			'type'=>'collection',
			'display'=>'blocks',
			'addLabel'=>'Signaler un problème',
			'model'=>[
				[
					'type'=> 'select', 
					'name'=>'type', 
					'label'=>'Sévérité', 
					'options'=>[
						['label'=>'critique', 'value'=>'error'],
						['label'=>'normale', 'value'=>'warn'],
					]
				],
				[
					'type'=>'textarea',
					'name'=>'description',
					'label'=>'Description',
					'className'=>'mt-2 mb-0'
				]
			]
		]), 'migration');
	return $form;


}, 4, 4);

add_action('body_start', function(){
	if (is_single() || is_page()){
		$status = get_post_meta(get_the_ID(), 'migration-status', true);
		echo '<style>
		body.migration-status-not-ready .menu-item[href="#migration"]{background: red;}
		body.migration-status-valid-desktop .menu-item[href="#migration"]{background: yellow; color: black;}
		body.migration-status-needs-validation .menu-item[href="#migration"]{background: blue; }
		body.migration-status-valid-mobile-desktop .menu-item[href="#migration"]{background: green;}
		body.migration-status-not-ready .menu-item[href="#migration"]:before{
			font-family: "Font Awesome 5 Free";
			font-weight: 700;
  			content: "\f071 ";
  			margin-right: 5px; 

		}
		</style>

		<script>jQuery("body").addClass("migration-status-'.$status.'"); </script>
		';

	}
});