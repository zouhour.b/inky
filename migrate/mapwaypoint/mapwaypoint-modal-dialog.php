<style>
    #mapwaypoint-dialog {
      height: 550px !important;
    }
    
    .wp-form-key-infos, .wp-form-key-coordinates {
      width: 50%;
      float: left;
    } 
</style>
<!-- The modal / dialog box, hidden somewhere near the footer -->
<div id="mapwaypoint-dialog" class="hidden">
  <div id="mapwaypoint_form">
    <div id="map" style="width: 100%; height: 300px;"></div>
    <hr>
    <?php echo WP_Forms_API::render_form( MapWaypoint::get_form(), $values ); ?>
  </div>
  <div id="mapwaypoint_container_form">
    <p>Before adding waypoints you need to define the map. Please select one of the listed providers.</p>
    <p>Visit <a href="https://leaflet-extras.github.io/leaflet-providers/preview/">https://leaflet-extras.github.io/leaflet-providers/preview/</a> to get the preview of all available providers.</p>
    <?php echo WP_Forms_API::render_form( MapWaypoint::get_map_form(), $values ); ?>
  </div>
  <hr>
  <input type="submit" id="mwp-insert-button" name="insert" class="button button-primary button-large" value="Insert">  
</div>
<script>
  var mwpInitialized = false;
  var firstInsert = true;
  
  function mwpSubmit() {
    var params = [];
    var shortcode = '';
    if(firstInsert){
      params.push('url="' + jQuery('#wp-form-url').val() + '"');
      params.push('ext="' + jQuery('#wp-form-ext').val() + '"');
      shortcode = "[map_waypoints] "
          + "[map_provider " + params.join(' ') + "]" + jQuery('#wp-form-attribution').val() + "[/map_provider]"
          + "<p>Add Map Waypoints here ...</p>"
          + "[/map_waypoints]";
      // Reset form
      jQuery('#wp-form-url').val('');
      jQuery('#wp-form-ext').val('');
      jQuery('#wp-form-attribution').val('');
    }else{
      params.push('title="' + jQuery('#wp-form-infos-title').val() + '"');
      params.push('anchor="' + jQuery('#wp-form-infos-anchor').val() + '"');
      params.push('date="' + jQuery('#wp-form-infos-date').val() + '"');
      params.push('class="' + jQuery('#wp-form-infos-class').val() + '"');
      params.push('latitude="' + jQuery('#wp-form-coordinates-latitude').val() + '"');
      params.push('longitude="' + jQuery('#wp-form-coordinates-longitude').val() + '"');
      params.push('zoom="' + jQuery('#wp-form-coordinates-zoom').val() + '"');
      params.push('marker="' + !(jQuery('#wp-form-coordinates-marker').prop('checked')) + '"');    
      

      shortcode = "[waypoint " + params.join(' ') + "]<p>Type your content here ...</p>[/waypoint]";
      // Reset form
      jQuery('#wp-form-title').val('');
      jQuery('#wp-form-anchor').val('');
      jQuery('#wp-form-date').val('');
    }
    
    // Append shortcode to editor content
    if( ! tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
      jQuery('textarea#content').val(jQuery('textarea#content').val() + shortcode);
    } else {
      tinyMCE.execCommand('mceInsertRawHTML', false, shortcode);
    }
  }
  
  function mwpInit() {
    // Map
    L.Marker.prototype.animateDragging = function () {

      var iconMargin, shadowMargin;

      this.on('dragstart', function () {
        if (!iconMargin) {
          iconMargin = parseInt(L.DomUtil.getStyle(this._icon, 'marginTop'));
          shadowMargin = parseInt(L.DomUtil.getStyle(this._shadow, 'marginLeft'));
        }

        this._icon.style.marginTop = (iconMargin - 15)  + 'px';
        this._shadow.style.marginLeft = (shadowMargin + 8) + 'px';
      });

      return this.on('dragend', function (e) {
        this._icon.style.marginTop = iconMargin + 'px';
        this._shadow.style.marginLeft = shadowMargin + 'px';
        
        // Update coord
        var position = e.target.getLatLng();
        jQuery('#wp-form-coordinates-latitude').val(position.lat);
        jQuery('#wp-form-coordinates-longitude').val(position.lng);
        
      });
    };

    var initPos = [
      jQuery('#wp-form-coordinates-latitude').val(),
      jQuery('#wp-form-coordinates-longitude').val()
    ];
    var initZoom = jQuery('#wp-form-coordinates-zoom').val()

    var map = L.map('map').setView(initPos, initZoom);
    
    map.on('zoomend', function() { 
      jQuery('#wp-form-coordinates-zoom').val(this.getZoom());
    });
    
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; OpenStreetMap contributors'
    }).addTo(map);

    L.marker(initPos, {draggable: true})
      .animateDragging()
      .addTo(map);
            
    mwpInitialized = true;
  }
  
  jQuery(document).ready(function() {
    // Modal dialog
    
    // initalise the dialog
    jQuery('#mapwaypoint-dialog').dialog({
      title: 'Manage Map Waypoints',
      dialogClass: 'wp-dialog',
      autoOpen: false,
      draggable: false,
      width: '800px',
      modal: true,
      resizable: false,
      closeOnEscape: true,
      position: {
        my: "center",
        at: "center",
        of: window
      },
      open: function () {
        firstInsert = jQuery('textarea#content').val().indexOf('[/map_waypoints]') === -1;

        // close dialog by clicking the overlay behind it
        jQuery('.ui-widget-overlay').bind('click', function(){
          jQuery('#mapwaypoint-dialog').dialog('close');
        })
        
        if(firstInsert){
          jQuery('#mapwaypoint_container_form').show();
          jQuery('#mapwaypoint_form').hide();
        }else{
          jQuery('#mapwaypoint_container_form').hide();
          jQuery('#mapwaypoint_form').show();
          // Init map
          if(!mwpInitialized){
            mwpInit();
          }
        }
      },
      create: function () {
        // style fix for WordPress admin
        jQuery('.ui-dialog-titlebar-close').addClass('ui-button');
      },
    });
    jQuery( "#mwp-insert-button" ).on('click', function( event ) {
      mwpSubmit();
      jQuery('#mapwaypoint-dialog').dialog('close');
      event.preventDefault();
    });
    // bind a button or a link to open the dialog
    jQuery('#insert-mwp-button').click(function(e) {
      e.preventDefault();
      jQuery('#mapwaypoint-dialog').dialog('open');
    }); 
  });
</script>