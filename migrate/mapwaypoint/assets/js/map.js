alert('ok');
const base_url = '/wp-content/themes/inkyfada/assets/img/map/';
const getGoogleStyles =()=> {
  return [
  {
    "featureType": "water",
    "stylers": [
    { "color": "#868E91" },
    { "visibility": "on" }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
    { "visibility": "on" },
    { "color": "#CFCCC3" }
    ]
  },
  {
    "featureType": "landscape.natural",
    "stylers": [
    { "visibility": "on" },
    { "color": "#CAC7BE" }
    ]
  },
  {
    "featureType": "administrative.country",
    "stylers": [
    { "visibility": "on" },
    { "color": "#ffffff" }
    ]
  },
  {
    "featureType": "administrative.province",
    "stylers": [
    { "visibility": "off" },
    { "color": "#ffffff" }
    ]
  },
  {
    "featureType": "road.highway",
    "stylers": [
    { "visibility": "on" }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
    { "visibility": "on" },
    { "color": "#BBB8B1" }
    ]
  },
  {
    "featureType": "transit.line",
    "stylers": [
    { "visibility": "off" }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
    { "visibility": "on" }
    ]
  },
  {
    "featureType": "road.arterial",
    "stylers": [
    { "visibility": "on" },
    { "color": "#000" }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels",
    "stylers": [
    { "visibility": "off" }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels",
    "stylers": [
    { "visibility": "off" }
    ]
  },
  {
    "featureType": "landscape",
    "elementType": "labels",
    "stylers": [
    { "visibility": "off" }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels",
    "stylers": [
    { "visibility": "off" }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels",
    "stylers": [
    { "visibility": "off" }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels",
    "stylers": [
    { "visibility": "off" }
    ]
  }
  ]
}
let scrollMap = function(_args){
  var container = this;
  var currentXYZ = null;
  var args = {offset:0, ..._args};
  var map = null;
  var initPos = $(container).attr('data-map-center');
  if (!initPos){
    initPos = '36,10,7';
  }
  const initLat = sanitizeLat(initPos.split(',')[0] );
  const initLng = sanitizeLat(initPos.split(',')[1] );
  const initZoom = sanitizeZoom(initPos.split(',')[2]);
  var markers = [];
  let locked = false;

  function hasChanged(xyz){
    if (!currentXYZ) return true;
    return  (xyz.x!=currentXYZ.x || xyz.y!==currentXYZ.y || xyz.z!=currentXYZ.z);
  }

  function buildMap(){
    map = L.map(container.attr('id'), { 
      
      zoomControl:false, 
      maxZoom: 20 }
      ).setView([initLat, initLng], initZoom);
    var provider_url = $(container).data('mapProvider');
    var provider_attr = $(container).data('attribution');
    alert(provider_attr)
    window.map = map;
    if (provider_url.indexOf('maps.google') !== -1) {
      L.gridLayer.googleMutant({
        type: 'terrain',
        styles: getGoogleStyles()

      }).addTo(map);
    } else {
      L.tileLayer(provider_url, {
        maxZoom: 18,
        useCache: true,
        crossOrigin: true,
        attribution: provider_attr
      }).addTo(map);
    }
    buildMarkers();
    init();
  }

  function sanitizeLat(v){
    if (typeof v==='number') return v;
    if (typeof v==='string'){
      return parseFloat(v.replace(',','.'));
    }
    return null;
  } 
  function sanitizeLng(v){
    return sanitizeLat(v);
  }

  function sanitizeZoom(v){
    if (typeof v  !=='number') v = parseInt(v);
    if (v<1) return 1;
    if (v>20) return 20;
    return v;
  }

  function getCurrentBlock(){
    let y = null;
    let currentBlock = null;
    $('.entry-content [data-map-center]').each(function(index, element){
      const offset = args.offset;
      const bbox = element.getBoundingClientRect();
      if (bbox.y < offset){
        element.setAttribute('data-status', 'past');
      }
      else{
        element.setAttribute('data-status', 'future');
        return;
      }

      if (y==null || (bbox.y<offset && y<bbox.y)){
        currentBlock = element;
        y=bbox.y;
      }
    });
    if (currentBlock){
      currentBlock.setAttribute('data-status', 'active');
    }
    return currentBlock;
  }

  function getMarkers(element){
    let markers = [];
    let _v = element.innerHTML;
    if (!_v) return [];
    try{
      const vals = JSON.parse(_v);
      vals.map(marker=>{
        if (sanitizeLat(marker.lat) && sanitizeLng(marker.lng)){
          markers.push({
            latlng:[
            sanitizeLat(marker.lat),
            sanitizeLng(marker.lng)
            ]
          });
        }
      });
      return markers;
    }
    catch (e){
      alert('failed to load json markers');
      return [];
    }
  }

  function buildMarkers(xyz){

    var iconActive = new L.Icon({
      iconUrl: base_url + 'pin-map-yellow.png',
      iconSize: [32,32],
      iconRetinaUrl: base_url + 'pin-map-yellow.png',
    });

    var iconPast = new L.Icon({
      iconUrl: base_url + 'pin-map-black.png',
      iconSize: [10,10],
      iconRetinaUrl: base_url + 'pin-map-black.png',
    });

    
    const blockMarkers = []

    $('[data-status="past"] [data-markers]').each(function(index, element){
      getMarkers(element).map(function(m){
        blockMarkers.push({
          icon: iconPast,
          latlng: m.latlng
        });
      });

    });
    
    


    $('[data-status="active"] [data-markers]').each(function(index, element){
      getMarkers(element).map(function(m){
        blockMarkers.push({
          icon: iconActive,
          latlng: m.latlng
        });
      });
    });

    blockMarkers.map(function(blockMarker){
      var marker = new L.marker(blockMarker.latlng, {icon: blockMarker.icon});
      markers.push(marker.addTo(map));
    });
  }

  function removeAllMarkers(){
    markers.map(function(marker){
      map.removeLayer(marker);
    });
    markers = []
  }

  function updateMarkers(xyz){
    removeAllMarkers();
    buildMarkers(xyz);
  }

  function updateState(){
    const currentBlock = getCurrentBlock();
    const blockXYZ = getBlockXYZ(currentBlock);
    
    updateMarkers(blockXYZ); 
    if (hasChanged(blockXYZ) && !locked){
      locked = true;
      setTimeout(x=>locked=false, 500);
      setMapXYZ(blockXYZ)

    }

  }


  function init(){
    adjustOverlay();
    $(window).scroll(function(){
      updateState();
    });
    setInterval(updateState,1000);

  }

  function isMobile(){
    return  window.outerWidth < 768;

  }

  function adjustOverlay(){
    if(!isMobile()){
      var overlay = $('.layout-side-overlay');
      var map_width;

      if ($('html').attr('dir') == 'rtl') {
        alert(ajust)
        map_width = window.outerWidth + overlay.outerWidth();
      } else {
        map_width = window.outerWidth + overlay.outerWidth() + overlay.offset().left;
      }

      $(container).css('width', map_width + 'px');

      if (map) {
        map.invalidateSize();
      }
    }
  }

  function setMapXYZ(xyz, panOptions){
    if (!panOptions){
      panOptions = {
        animate: true,
        duration: 2,
        easeLinearity: 0.25
      };

    }
    currentXYZ = xyz;
    let center = L.latLng([xyz.x,xyz.y]);
    let zoom =xyz.z;
    if (true) {
      var point = map.project(center, map._limitZoom(zoom));
      point = point.add({x:-$(window).width()/4, y:0});
      center = map.unproject(point, map._limitZoom(zoom));
    }
    map.flyTo(center, zoom, panOptions)  
  }

  function getBlockXYZ(element){
    if (!element) return {x:initLat, y:initLng, z:initZoom};
    let center = $(element).attr('data-map-center');
    if (!center) {
      return {x:initLat, y:initLng, z:initZoom};
    }

    return {
      x: sanitizeLat(center.split(',')[0]),
      y: sanitizeLng(center.split(',')[1]),
      z: sanitizeZoom(center.split(',')[2])
    }

  }
  buildMap();
}


$(document).ready(function(){
  $.fn.scrollMap = scrollMap();
  $('[data-map-provider]').scrollMap({offset:200});
});
