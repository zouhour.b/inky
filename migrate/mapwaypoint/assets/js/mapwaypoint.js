(function ($) {
    var currentIndex = -1;
    var locations = [];
    var markers = [];
    // var markerLayer = L.markerClusterGroup();
    var map;
    var isMobile = window.outerWidth < 768;
    var base_url = '/wp-content/themes/inkyfada/app/shortcodes/mapwaypoint/assets/css/images/';
    
    var panAnimateOptions = {
        animate: true,
        duration: 3,
        easeLinearity: 0.25
      };

    var panSimpleOptions = {
        animate: false,
        duration: 0,
        easeLinearity: 0
      };
      
    function adjustOverlay(){
        // Resize the map to take overlay width into account
        if(!isMobile){
            var overlay = $('.waypoints');
            var map_width;

            if ($('html').attr('dir') == 'rtl') {
              map_width = window.outerWidth + overlay.outerWidth();
            } else {
              map_width = window.outerWidth + overlay.outerWidth() + overlay.offset().left;
            }

            $('#waypoints_map').css('width', map_width + 'px');

            if (map) {
                map.invalidateSize();
            }
        }
    }
    
    $(document).ready(function() {
        // Adjust map center relativerly to the screen width
        adjustOverlay();
        $(window).on('resize', function () { adjustOverlay() });
        
        // Init Map
        var initPos = [
          $('#waypoints .waypoint:first').data('lat'),
          $('#waypoints .waypoint:first').data('lng'),
        ];
        var initZoom = $('#waypoints .waypoint:first').data('zoom');
        
        map = L.map('waypoints_map', { zoomControl:false, maxZoom: 20 }).setView(initPos, initZoom);
        // map.addLayer(markerLayer);
        
        L.Icon.Default.prototype.options.iconSize = [300, 300];
        L.Icon.Default.prototype.options.shadowUrl = '';
        
        // Set tile provider
        var provider_url = $('#provider_url').val();
        var provider_attr = $('#provider_attr').val();
        
        if (provider_url.indexOf('maps.google') !== -1) {
            L.gridLayer.googleMutant({
                type: 'terrain',
                styles: [
                {
                  "featureType": "water",
                  "stylers": [
                    { "color": "#868E91" },
                    { "visibility": "on" }
                  ]
                },{
                  "featureType": "poi",
                  "stylers": [
                    { "visibility": "on" },
                    { "color": "#CFCCC3" }
                  ]
                },{
                  "featureType": "landscape.natural",
                  "stylers": [
                    { "visibility": "on" },
                    { "color": "#CAC7BE" }
                  ]
                },{
                  "featureType": "administrative.country",
                  "stylers": [
                    { "visibility": "on" },
                    { "color": "#ffffff" }
                  ]
                },{
                  "featureType": "administrative.province",
                  "stylers": [
                    { "visibility": "off" },
                    { "color": "#ffffff" }
                  ]
                },{
                  "featureType": "road.highway",
                  "stylers": [
                    { "visibility": "on" }
                  ]
                },{
                  "featureType": "poi",
                  "stylers": [
                    { "visibility": "on" },
                    { "color": "#BBB8B1" }
                  ]
                },{
                  "featureType": "transit.line",
                  "stylers": [
                    { "visibility": "off" }
                  ]
                },{
                  "featureType": "road",
                  "stylers": [
                    { "visibility": "on" }
                  ]
                },{
                  "featureType": "road.arterial",
                  "stylers": [
                    { "visibility": "on" },
                    { "color": "#000" }
                  ]
                },{
                  "featureType": "road",
                  "elementType": "labels",
                  "stylers": [
                    { "visibility": "off" }
                  ]
                },{
                  "featureType": "poi",
                  "elementType": "labels",
                  "stylers": [
                    { "visibility": "off" }
                  ]
                },{
                  "featureType": "landscape",
                  "elementType": "labels",
                  "stylers": [
                    { "visibility": "off" }
                  ]
                },{
                  "featureType": "transit",
                  "elementType": "labels",
                  "stylers": [
                    { "visibility": "off" }
                  ]
                },{
                  "featureType": "water",
                  "elementType": "labels",
                  "stylers": [
                    { "visibility": "off" }
                  ]
                },{
                  "featureType": "administrative.country",
                  "elementType": "labels",
                  "stylers": [
                    { "visibility": "off" }
                  ]
                }
              ]
            }).addTo(map);
        } else {
            L.tileLayer(provider_url, {
                maxZoom: 18,
                useCache: true,
                crossOrigin: true,
                attribution: provider_attr
              }).addTo(map);
        }
         
        // Init waypoints
        $('<div/>', {'class': 'waypoints-menu'}).insertAfter($("#waypoints_map"));
        $('#waypoints .waypoint').each(function(index, element){
            // Parse DOM for locations
            var location = {
              id: 'waypoint_' + (index+1),
              latlng: [$(element).data('lat'), $(element).data('lng')],
              zoom: $(element).data('zoom'),
              anchor: $(element).data('anchor'),
              marker: $(element).data('marker'),
            };
            locations.push(location);
            
            // Append anchor  as a menu item
            var id = 'waypoint_' + (index+1);
            $(element).attr('id', id);
            var menuItem = $('<div/>', {
                id: 'menu_' + location.id,
                'class': 'waypoints-menu-item'
            });
            $('<a/>', {
                href: '#' + location.id,
                html: $(element).data('anchor')
            }).appendTo(menuItem);
            menuItem.appendTo($(".waypoints-menu"));
            
            // Smooth scroll
            $('#menu_' + location.id + ' a').on('click', function() {
                var page = $(this).attr('href');
                var speed = 100;
                $('html, body').animate( { scrollTop: $(page).offset().top - 50 }, speed);
                return false;
            });
            
            var waypoint = new Waypoint({
                element: element,
                offset: 128,
                handler: function(direction) {
                	console.log('handle', currentIndex, direction);
                  var panOptions = null;
                    // Fix menu to window
                    $('.waypoints-menu').css('position', 'fixed');                    
                    if(direction === 'down') {
                        currentIndex++;
                        var pos = locations[currentIndex].latlng;
                        var zoom = locations[currentIndex].zoom;
                        // Highlight menu item
                        if(locations[currentIndex].anchor){
                            $('.waypoints-menu-item').removeClass('active');
                            $('#menu_'+locations[currentIndex].id).addClass('active');
                        }
                        
                        // Create location and fly to location
                        if(locations[currentIndex].marker) {
                            // Highlight current marker
                            var icon = new L.Icon({
                                iconUrl: base_url + 'marker-icon-red.png',
                                iconSize: [32,32],
                                iconRetinaUrl: base_url + 'marker-icon-active-2x.png',
                            });
                            var marker = new L.marker(pos, {icon: icon});
                            // Unhighlight current marker
                            if (markers.length > 0) {
                                markers[markers.length - 1].setIcon(new L.Icon({
                                    iconUrl: base_url + 'marker-icon.png',
                                iconSize: [32,32],

                                    iconRetinaUrl: base_url + 'marker-icon-2x.png',
                                }));
                            }
                            // Store marker and add to the map
                            markers.push(marker.addTo(map));
                            //markers.push(marker);
                            // markerLayer.addLayer(markers[markers.length - 1]);
                            panOptions = panAnimateOptions;
                        } else {
                        	// Unhighlight current marker
                            if (markers.length > 0) {
                                markers[markers.length - 1].setIcon(new L.Icon({
                                    iconUrl: base_url + 'marker-icon.png',
                                    iconSize:[32,32],
                                    iconRetinaUrl: base_url + 'marker-icon-2x.png',
                                }));
                            }
                            panOptions = panSimpleOptions;
                        }
                        map.flyTo(pos, zoom, panOptions);
                        
                    } else {
                        // Remove marker
                        if(locations[currentIndex].marker) {
                            // Remove current marker from the map
                            map.removeLayer(markers[markers.length - 1]);
                            // markerLayer.removeLayer(markers[markers.length - 1]);
                            var marker =  markers.pop();
                            panOptions = panAnimateOptions;
                        } else {
                        	panOptions = panSimpleOptions;
                        }
                        // Highlight current marker
                        if (markers.length > 0) {
                            markers[markers.length - 1].setIcon(new L.Icon({
                                iconSize: [32,32],

                                iconUrl: base_url + 'marker-icon-active.png',
                                iconRetinaUrl: base_url + 'marker-icon-active-2x.png',
                            }));
                        }
                        currentIndex--;
                        if(currentIndex == -1) {
                            map.flyTo(initPos, initZoom, panOptions);
                            $('.waypoints-menu-item').removeClass('active');
                            // Attach menu to overlay
                            $('.waypoints-menu').css('position', 'absolute');
                        }else{
                            // Move backwards
                            var pos = locations[currentIndex].latlng;
                            var zoom = locations[currentIndex].zoom;
                            // Highlight menu item
                            if(locations[currentIndex].anchor){
                                $('.waypoints-menu-item').removeClass('active');
                                $('#menu_'+locations[currentIndex].id).addClass('active');
                            }
                            if(!locations[currentIndex].marker){
	                            // Unhighlight all markers
	                        	for (var i = markers.length - 1; i >= 0; i--) {
	                        		markers[i].setIcon(new L.Icon({
                                iconSize: [32,32],

	                                    iconUrl: base_url + 'marker-icon.png',
	                                    iconRetinaUrl: base_url + 'marker-icon-2x.png',
	                                }));
	                        	}
	                        }
                            map.flyTo(pos, zoom, panOptions);
                        }
                    }
                }
              })
        });
    });
})(jQuery);

