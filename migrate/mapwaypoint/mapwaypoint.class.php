<?php 
use Inkube\Core\Interfaces\Shortcode;

class MapWaypoint  extends Shortcode{
  public $tag = 'map_waypoints';
  public function getHTML(array $atts, string $content):string{
    return self::map_waypoints_shortcode($atts, $content);
  }
  public function getContainerClass($atts){
    return 'w-100';
  }

  public function getFields():array{
    return [];
  }
  public function getPresets():array{
    return [];
  }
  // Add Shortcodes
  public static function map_waypoints_shortcode( $atts , $content = null ) {
    // Attributes
    $atts = shortcode_atts(
      array(
      ),
      $atts,
      ''
    );
    
    return '<div id="waypoints">'
      
      . '<div id="waypoints_map"></div>'
      . '<div class="waypoints">' . do_shortcode($content) . '</div>'
      . '</div>';
  }
  
  public static function provider_shortcode( $atts , $content = null ) {
    // Attributes
    $atts = shortcode_atts(
      array(
        'url' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        'ext' => 'png',
      ),
      $atts,
      ''
    );
    
    wp_enqueue_script('leafletjs', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/js/leaflet.min.js');
    wp_enqueue_style('leafletcss', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/css/leaflet.css');
    
    wp_enqueue_script('leaflet-markerclusterjs', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/js/leaflet.markercluster.js'); 
    wp_enqueue_style('leaflet-markercluster-css', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/css/MarkerCluster.css');
    wp_enqueue_style('leaflet-markercluster-css-default', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/css/MarkerCluster.Default.css');
    
    wp_enqueue_script('leaflet-markerclusterjs', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/js/leaflet.active-area.js')

    wp_enqueue_script('mapwaypointjs', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/js/map.js', array('inkube-front'));
    wp_enqueue_style('mapwaypointcss', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/css/mapwaypoint.css');
    
    if (strpos($atts['url'], 'maps.google') !== false) {
      wp_enqueue_script('googlemaps', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyBgn5KwbRseKzJnYAQz5iRibnwWfHS2-uo&language=fr&v=3');
      wp_enqueue_script('googlemaps-leaflet', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/js/leaflet-google.js');
    }
    
    if(!$content){
      $content = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
    }
    $atts['url'] = str_replace('{ext}', $atts['ext'], $atts['url']);
    return '<input type="hidden" name="provider_url" id="provider_url" value="'.$atts['url'].'" />'
      . '<input type="hidden" name="provider_attribution" id="provider_attribution" value=\''.$content.'\' />';
  }
  
  public static function waypoint_shortcode( $atts , $content = null ) {
    // Attributes
    $atts = shortcode_atts(
      array(
        'title' => '',
        'anchor' => '',
        'date' => '',
        'class' => '',
        'latitude' => '0.0',
        'longitude' => '0.0',
        'zoom' => '8',
        'marker' => 'true',
      ),
      $atts,
      ''
    );
    
    
    
    $html = '<div class="waypoint '.$atts['class'].'" data-anchor="' . $atts['anchor'] . '" data-lat="' . $atts['latitude'] . '"'
        . ' data-marker="' . $atts['marker'] . '" data-lng="' . $atts['longitude'] . '" data-zoom="' . $atts['zoom'] . '">';
    if($atts['date']){
      $html .= '<span class="waypoint-date">' . $atts['date'] . '</span>';
    }
    if($atts['title']){
      $html .= '<h2 class="waypoint-title">' . $atts['title'] . '</h2>';
    }
    $html .= '<div class="waypoint-content">' . do_shortcode($content) . '</div>';
    $html .= '</div>';
    
    return $html;
  }
  
  // Add Media button
  public static function media_button() {
    require dirname(__FILE__) . '/libs/WP-Forms-API/wp-forms-api.php';
    require dirname(__FILE__) . '/mapwaypoint-modal-dialog.php';
    echo '<button type="button" id="insert-mwp-button" class="button add_mwp" data-editor="content">'
      . '<i class="icon-toolset-map ont-icon-18 ont-color-gray"></i>Map Waypoint'
      . '</button>';
  }
  
  public static function media_button_js_file() {
    // Enqueue these scripts and styles before admin_head
    wp_enqueue_script('leafletjs', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/js/leaflet.min.js');
    wp_enqueue_style('leafletcss', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/css/leaflet.css');
    wp_enqueue_script('leafletjs-search', get_template_directory_uri() . '/app/shortcodes/mapwaypoint/assets/js/leaflet-search.min.js');
    wp_enqueue_style('wp-jquery-ui-dialog' );
  }
  
  public static function get_form(){
    return array(
      '#id' => 'mwp-form',
      'infos' => array(
        'title' => array(
          '#label' => "Title",
          '#type' => 'text',
        ),
        'anchor' => array(
          '#label' => "Anchor",
          '#type' => 'text',
        ),
        'date' => array(
          '#label' => "Date",
          '#type' => 'text',
        ),
        'class' => array(
          '#label' => "CSS Class",
          '#type' => 'text',
        ),
      ),
      'coordinates' => array(
        'latitude' => array(
          '#type' => 'text',
          '#required' => true,
          '#label' => "Latitude",
          '#placeholder' => "0,0",
          '#default' => 36.806495,
          '#size' => 20
        ),
        'longitude' => array(
          '#type' => 'text',
          '#required' => true,
          '#label' => "Longitude",
          '#placeholder' => "0,0",
          '#default' => 10.181532,
          '#size' => 20,
        ),
        'zoom' => array(
          '#type' => 'text',
          '#required' => true,
          '#label' => "Zoom",
          '#default' => 8,
          '#placeholder' => "0 - 18",
          '#size' => 20
        ),
        'marker' => array(
          '#label' => "Do not show a marker",
          '#type' => 'checkbox',
          '#default' => false
        ),
      )
    );
  }
  
  public static function get_map_form(){
    return array(
      '#id' => 'mwp-container-form',

      'url' => array(
        '#label' => "URL",
        '#type' => 'text',
      ),
      'attribution' => array(
        '#label' => "Attribution",
        '#type' => 'text',
      ),
      'ext' => array(
        '#label' => "Tile Extension",
        '#type' => 'text',
      ),
      
    );
  }
}

//add_shortcode( 'map_waypoints', array('MapWaypoint', 'map_waypoints_shortcode'));
add_shortcode( 'waypoint', array('MapWaypoint', 'waypoint_shortcode'));
add_shortcode( 'map_provider', array('MapWaypoint', 'provider_shortcode'));
add_action('media_buttons', array('MapWaypoint', 'media_button'));
add_action('wpz_enqueue_media', array('MapWaypoint', 'media_button_js_file'));

add_action('init', function(){
  global $Inkube;
  $Inkube->getContainer()->get('Inkube\Shortcodes\ShortcodesModule')->register('map_waypoints', new MapWaypoint());
  
});
die();
Inkube\Model\Field::register('map_provider', ['type'=>'select', 'options'=>[
  'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'    =>'Open Street Map Standard',
  'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.pn' =>'Open Street Map black and white',
  'https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}'=>'Fastly',
  'http://maps.googleapis.com/maps/api/js?key=AIzaSyBgn5KwbRseKzJnYAQz5iRibnwWfHS2-uo&amp;language=fr&amp;v=3'=>'Google Maps'
]])