<?php 
use Inkube\Core\Interfaces\Shortcode;

class MapWaypoint{
  public $tag = 'waypoint';
  public function getHTML(array $atts, string $content):string{
    return self::waypoint_shortcode($atts, $content);
  }
  public function getContainerClass($atts){
    return 'w-100';
  }

  public function getFields():array{
    return ['base'=>[
      [
        'name'=>'coords',
        'type'=>'map'
      ]
    ]];
  }

  public function getApiKey(){
    return 'AIzaSyAk9SQLSFSdQfLzZXjF36wVEGhy1r5Msok';
  }
  public function getPresets():array{
    return [];
  }
  // Add Shortcodes
  public static function map_waypoints_shortcode( $atts , $content = null ) {
    // Attributes
    $atts = shortcode_atts(
      array(
      ),
      $atts,
      ''
    );
    
    return '<div id="waypoints">'

    . '<div id="waypoints_map"></div>'
    . '<div class="waypoints">' . do_shortcode($content) . '</div>'
    . '</div>';
  }
  
  public static function map_shortcode( $atts , $content = null ) {
    // Attributes
    $atts = shortcode_atts(
      array(
        'provider' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        'ext' => 'png',
        'center'=>'36,10,7',
        'container_class'=>'pos-fixed top-0 direct-0'
      ),
      $atts,
      ''
    );
    $base = get_template_directory_uri().'/app/shortcodes/map/assets';
    $scripts = [
      ['name'=>'leaflet-core', 'url'=>$base.'/js/leaflet.min.js'],
      ['name'=>'leaflet-pouchedb', 'url'=>$base.'/js/pouchdb.js'],
      ['name'=>'leaflet-cachedb', 'url'=>$base.'/js/leaflet.cachedb.js'],
      ['name'=>'leaflet-markercluster', 'url'=>$base.'/js/leaflet.markercluster.js'],
      ['name'=>'map', 'url'=>$base.'/js/map.js']
    ];

    $styles = [
      ['name'=>'leaflet', 'url'=>$base.'/css/leaflet.css'],
      ['name'=>'leaflet-markers', 'url'=>$base.'/css/MarkerCluster.css'],
      ['name'=>'leaflet-markers-default', 'url'=>$base.'/css/MarkerCluster.Default.css'],
      ['name'=>'map', 'url'=>$base.'/css/map.css']
    ];
    
    
    if (strpos($atts['provider'], 'maps.google') !== false) {
      $scripts[] = ['name'=>'leaflet-google', 'url'=>'http://maps.googleapis.com/maps/api/js?key=AIzaSyBgn5KwbRseKzJnYAQz5iRibnwWfHS2-uo&language=fr&v=3'];
      $scripts[] = ['name'=>'googlemaps-leaflet', 'url'=>$base.'/js/leaflet.google.js'];

    }
    
    if(!$content){
      $content = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
    }
    $atts['provider'] = str_replace('{ext}', $atts['ext'], $atts['provider']);
    $html = sprintf('<div id="map" class="%s" data-map-center="%s" data-map-provider="%s" data-attribution=\'%s\'></div>', $atts['container_class'], $atts['center'], $atts['provider'], $content);
    foreach ($scripts as $script){
      $html .= sprintf('<script id="%s" type="text/javascript" src="%s"></script>', $script['name'], $script['url']);
    }
    foreach ($styles as $style){
      $html.=sprintf('<link rel="stylesheet" href="%s"/>', $style['url']);
    }
    return $html;
  }
  
  public static function waypoint_shortcode( $atts , $content = null ) {
    // Attributes
    $atts = shortcode_atts(
      array(
        'title' => '',
        'anchor' => '',
        'date' => '',
        'class' => '',
        'latitude' => '0.0',
        'longitude' => '0.0',
        'zoom' => '8',
        'marker' => 'true',
      ),
      $atts,
      ''
    );
    
    
    
    $html = '<div class="waypoint '.$atts['class'].'" data-anchor="' . $atts['anchor'] . '" data-lat="' . $atts['latitude'] . '"'
    . ' data-marker="' . $atts['marker'] . '" data-lng="' . $atts['longitude'] . '" data-zoom="' . $atts['zoom'] . '">';
    if($atts['date']){
      $html .= '<span class="waypoint-date">' . $atts['date'] . '</span>';
    }
    if($atts['title']){
      $html .= '<h2 class="waypoint-title">' . $atts['title'] . '</h2>';
    }
    $html .= '<div class="waypoint-content">' . do_shortcode($content) . '</div>';
    $html .= '</div>';
    
    return $html;
  }
  
  
  public static function get_form(){
    return array(
      '#id' => 'mwp-form',
      'infos' => array(
        'title' => array(
          '#label' => "Title",
          '#type' => 'text',
        ),
        'anchor' => array(
          '#label' => "Anchor",
          '#type' => 'text',
        ),
        'date' => array(
          '#label' => "Date",
          '#type' => 'text',
        ),
        'class' => array(
          '#label' => "CSS Class",
          '#type' => 'text',
        ),
      ),
      'coordinates' => array(
        'latitude' => array(
          '#type' => 'text',
          '#required' => true,
          '#label' => "Latitude",
          '#placeholder' => "0,0",
          '#default' => 36.806495,
          '#size' => 20
        ),
        'longitude' => array(
          '#type' => 'text',
          '#required' => true,
          '#label' => "Longitude",
          '#placeholder' => "0,0",
          '#default' => 10.181532,
          '#size' => 20,
        ),
        'zoom' => array(
          '#type' => 'text',
          '#required' => true,
          '#label' => "Zoom",
          '#default' => 8,
          '#placeholder' => "0 - 18",
          '#size' => 20
        ),
        'marker' => array(
          '#label' => "Do not show a marker",
          '#type' => 'checkbox',
          '#default' => false
        ),
      )
    );
  }
  
  public static function get_map_form(){
    return array(
      '#id' => 'mwp-container-form',

      'url' => array(
        '#label' => "URL",
        '#type' => 'text',
      ),
      'attribution' => array(
        '#label' => "Attribution",
        '#type' => 'text',
      ),
      'ext' => array(
        '#label' => "Tile Extension",
        '#type' => 'text',
      ),
      
    );
  }
}

add_shortcode( 'map_waypoints', array('MapWaypoint', 'map_waypoints_shortcode'));
add_shortcode( 'map', array('MapWaypoint', 'map_shortcode'));

Inkube\Model\Field::register('map_provider', ['type'=>'select', 'options'=>[
  [
    'value'=>'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    'label' =>'Open Street Map Standard',
  ],
  [
    'value'=>'https://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
    'label' =>'Open Street Map black and white',
  ],
  [
    'value'=>'https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}',
    'label'=>'Fastly',
  ],

  [
    'value'=>'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    'label'=>'ArcGIS'
  ],
  ['label'=>'Google Maps',
  'value'=>'http://maps.googleapis.com/maps/api/js?key=AIzaSyBgn5KwbRseKzJnYAQz5iRibnwWfHS2-uo&amp;language=fr&amp;v=3',
]
]
]);

Inkube\Model\Field::register('map_center', ['type'=>'map', 'label'=>'Map center']);
