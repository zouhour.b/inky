<nav class="site-header">
  <div id="site-logo" class="site-logo abs-tl">
    <a href="<?php echo home_url() ?>" />
      <img class="dark" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-dark.png" />
      <img class="light" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-light.png" />
    </a>
  </div>
  <ul class="abs-tr" id="main-nav">
    <li class="list-inline-item">
      <?php if (is_rtl()): ?>
        <a class="language-switcher" href="/fr"><span class="icon icon-inkyfada-fr"></span></span></a>
      <?php else: ?>
        <a class="language-switcher" href="/ar"><span class="icon icon-arabe"></span></span></a>
      <?php endif; ?>
    </li>
    <li>
      <?php echo do_shortcode('[import file="@theme/widgets/search.html"]'); ?>
    </li>
    <li >
      <a class="expanded-nav-toggler" data-target="#expanded-nav">
        <span class="icon icon-dots"></span>
      </a>
    </li>
  </ul>
  
  <div id="expanded-nav">
    <a class="expanded-nav-toggler" data-target="#expanded-nav">
      <span class="icon icon-close"></span>
    </a>
    <div class="main-menu-container">
      <?php
      wp_nav_menu(array( 'theme_location' => 'primary', 'menu_class' => 'nav navbar-nav', 'walker' => new wp_bootstrap_navwalker()));
      ?>
    </div>
    <div class="secondary-menu-container text-center">
      <?php //the_social_links() ?>
      <ul class="list-inline social-links">
        <li><a href="https://www.facebook.com/InkyFada/" target="_blank"><i class="icon icon-facebook-2"></i></a></li>
        <li><a href="https://twitter.com/inkyfada" target="_blank"><i class="icon icon-twitter-2"></i></a></li>
        <li><a href="https://www.youtube.com/user/inkyfada" target="_blank"><i class="icon icon-youtube-2"></i></a></li>
        <li><a href="https://www.instagram.com/inkyfada_com/" target="_blank"><i class="icon icon-instagram-2"></i></a></li>
        <li><a href="https://www.pinterest.com/inkyfada/" target="_blank"><i class="icon icon-pinterest-2"></i></a></li>
      </ul>
      <?php wp_nav_menu(array( 'theme_location' => 'secondary', 'menu_class' => 'list-inline-with-separators fs-6 menu-secondary', 'walker' => new wp_bootstrap_navwalker()));

      ?>
    </div>
  </div>
</nav>
