<?php
  global $wp_query;
  $newsletter_order = false;
  $i=0;
	if ( have_posts() ) {
		while ( have_posts() ) { 
			the_post();
      if($newsletter_order === $i && $wp_query->query_vars['paged'] == 0){
        ?>
          <section class="col-12 p-0 mt-2">
            <?php echo do_shortcode( '[inky_mailchimp]' ); ?>
          </section>
        <?php
      }
      $card_args['index'] = $i;
			$card_options = apply_filters('card_options', array(), get_the_ID(), $card_args);
			extract($card_options);
      $template = (isset($template))?$template:'card-by-default';
			include 'template-parts/'.$template.'.php';
      $i++;
		}
	}
