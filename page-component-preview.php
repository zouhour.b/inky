<?php 
if (!current_user_can('edit_posts')){
	die();
}
show_admin_bar(false);
get_header();
$class=isset($_GET['class'])?$_GET['class']:'col-12';
$id = isset($_GET['cid'])?$_GET['cid']:0;
$sh = isset($_GET['shortcode'])?$_GET['shortcode']:'';
?>
<div class="entry-content">
	<?php 
	if ($id){
		echo do_shortcode('[components id="'.$id.'" class="'.$class.'"]'); 
	}
	else if($sh){
		$sh = stripslashes(urldecode($sh));
		echo do_shortcode($sh); 

	}
	?>

</div>
<?php wp_footer(); ?>


