<?php 

add_filter('post_gallery', 'gallery_migration_shortcode', 3, 99); 		


function gallery_migration_shortcode($str, $atts, $caption){
	$caption = $atts['caption']??'';
	$args = [];
	$args['ids'] = $atts['ids']??'';
	$args['container_class'] = $atts['galleryclass']??'mb-5 w-100';
	$args['layout']='carousel-fade';

	global $Inkube;
	$handler = $Inkube->getContainer()->get('Inkube\Shortcodes\ShortcodesModule')->getHandler('images');
	return $handler->render($args, $caption);
}