<?php
add_action('body_start', function(){
	ini_set('max_execution_time',1000);
	new InkyfadaDBMigration();
	
});
add_action('template_redirect', function(){
	if (isset($_GET['edit']) && is_single()){
		global $post;
		$id = $post->ID;
		wp_redirect( get_edit_post_link( $id , false),302 );
	}
});
class InkyfadaDBMigration{
	

	public function syncTermsMeta(){
		$terms = get_terms(['taxonomy'=>['theme', 'genre'], 'lang'=>'fr']);
		foreach ($terms as $t){
			if ($tr = pll_get_term($t->term_id, 'ar')){
				$cover = get_term_meta($t->term_id, '_thumbnail_id', true);
				$color = get_term_meta($t->term_id, 'color', true);
				update_term_meta($tr, 'color', $color);
				update_term_meta($tr, '_thumbnail_id', $cover);
				var_dump($t->name);
			}
		}
		die();
	}

	public function checkContributors(){
		$posts = get_posts(['numberposts'=>-1, 'lang'=>'ar,fr']);
		foreach ($posts as $p){
			$c = get_post_meta($p->ID, '_contributors', true);
			$_c = [];
			if (!$c){
				echo sprintf('<h1><a href="%s">%s</a></h1>', get_post_permalink($p->ID), $p->post_title);
				continue;
			}
			foreach ($c as $r){
				$_r['user_id'] = (int)$r['user_id'];
				$_r['role'] = $r['role'];
				$_c[] = $_r;
			}
			update_post_meta($p->ID, '_contributors', $_c);
		}
		die();
	}

	public function updateUserMetas(){
		$us  = get_users();
		$metas = [];
		foreach ($us as $u){
			$id = $u->ID;
			$rs = get_user_meta($id);
			foreach ($rs as $k=>$r){
				$metas[$k] = $metas[$k]??0;
				$metas[$k]++;
			}
			foreach ([
				'description'=>'description_fr',
				'description'=>'biography_fr', 
				'user_twitter'=>'twitter', 
				'user_bio_fr'=>'biography_fr', 
				'user_name_ar'=>'name_ar', 
				'user_bio_ar'=>'biography_ar', 
				'wp_user_avatar'=>'photo',
				'user_mail'=>'contact_email'
			] as $k=>$nk){
				$m = get_user_meta($id, $k, true);

				if ($m) update_user_meta( $id, $nk, $m );
			}
			update_user_meta($id, 'name_fr', $u->data->display_name);

		}
		die();
	}

	public function updateStringTranslations(){ 
		global $Inkube;
		$tr = $Inkube->getContainer()->get(Inkube\Translations\TranslationsModule::class);
		$tr->saveTranslation('ar', [

			'display more content'=>'عرض المزيد من المحتوى',
			'written by'=>'بقلم',
			'minutes'=>'دقيقة', 
			'month-01'=>'جانفي',
			'month-02'=>'فيفري',
			'month-03'=>'مارس',
			'month-04'=>'أفريل',
			'month-05'=>'ماي',
			'month-06'=>'جوان',
			'month-07'=>'جويلية',
			'month-08'=>'أوت',
			'month-09'=>'سبتمبر',
			'month-10'=>'أكتوبر',
			'month-11'=>'نوفمبر',
			'month-12'=>'ديسمبر'

		]);
		$tr->saveTranslation('fr', [
			'display more content'=>'Afficher plus de contenu',
			'written by'=>'Par',
			'month-01'=>'Janvier',
			'month-02'=>'Février',
			'month-03'=>'Mars',
			'month-04'=>'Avril',
			'month-05'=>'Mai',
			'month-06'=>'Juin',
			'month-07'=>'Juillet',
			'month-08'=>'Août',
			'month-09'=>'Septembre',
			'month-10'=>'Octobre',
			'month-11'=>'Novembre',
			'month-12'=>'Décembre',
		]); 

		$tr->saveTranslation('en', [
			'display more content'=>'Display more content',
			'written by'=>'Written By',
			'month-01'=>'January',
			'month-02'=>'February',
			'month-03'=>'March',
			'month-04'=>'April',
			'month-05'=>'May',
			'month-06'=>'June',
			'month-07'=>'July',
			'month-08'=>'August',
			'month-09'=>'September',
			'month-10'=>'October',
			'month-11'=>'November',
			'month-12'=>'December',
		]); 
		die();

	}

	public function syncReadingTime(){
		$i = 0;
		$fails = [];
		$posts = get_posts(['lang'=>'ar,fr', 'numberposts'=>-1]);
		foreach ($posts as $p){
			$t = get_post_meta($p->ID, 'inky_storFy_time', true);
			if ($t){
				update_post_meta($p->ID, 'reading-duration', $t);
			}
			else{
				$id = get_post_meta($p->ID,'oldid', true );
				if (!$id){
					$i++;
					$fails[]=sprintf('<div><a href="?p=%s">%s</a> (no id)</div>', $p->ID, $p->post_title);
					continue;
				}
				$v = 0;
				$prod = $this->getDB();
				foreach ($prod->query("SELECT meta_key, meta_value from wp_postmeta where meta_key='inky_story_time' and post_id=".$id) as $row){
					$v = $row['meta_value'];
				}
				if ($v){
					update_post_meta($p->ID, 'reading-duration', $v);
				}
				else{
					$fails[]=sprintf('<div><a href="?p=%s">%s</a> (no value)</div>', $p->ID, $p->post_title);

					$i++;
				}
			}
		}
		echo '<h4>'.$i.' fails</h4>'.implode('', $fails);
		die();
	}

	public function copyUsers(){

		die();
	}



	public function __construct(){
		$action = $_GET['migrate_db']??'';
		switch ($action){
			case 'copyUsers':
			$this->copyUsers();
			break;
			case 'checkContributors':
			$this->checkContributors();
			break;
			case 'updateUserMetas':
			$this->updateUserMetas();
			break;
			case 'updateStringTranslations';
			$this->updateStringTranslations();
			break;
			case 'syncReadingTime':
			$this->syncReadingTime();
			break;
			case 'getUsersInfos':
			$table = $this->getUsersInfos();
			$this->renderInfos($table);
			break;
			case 'getAttachmentsInfos':
			$table = $this->getAttachmentsInfos();
			$this->renderInfos($table);
			die();
			break;
			case "syncTermTranslationsMeta":
			$this->syncTermsMeta();
			break;
			case 'fixZeroImpunityAr':
			$id = 21027;
			$others = [21868, 21767, 21860];
			$options = get_post_meta($id, 'view_options', true);
			$footer = get_post_meta($id, 'wpcf-footer', true);
			$style = get_post_meta($id, 'wpcf-styles', true);
			foreach ($others as $p){
				update_post_meta($p, 'wpcf-styles', $style);
				update_post_meta($p, 'view_options', $options); 
				update_post_meta($p, 'wpcf-footer', $footer);
			}
			die();

			break;
			case 'oldContent';
			$this->showOldContent();
			die();
			break;
			case "content":
			$this->showContent();
			break;
			case 'copyAttachments':
			$this->copy_attachments();
			break;

			case 'copyPosts':
			$table = $this->copy_posts();
			$this->renderInfos($table);
			break;

			case 'syncPost':
			if (is_single()){

				$this->copyPostTaxonomies(get_the_ID());
				$this->copyPostMeta(get_the_ID());
			}


			break;
			case 'syncLang':
			$ar = $fr =0;
			foreach (get_posts(['numberposts'=>-1, 'lang'=>'ar,fr']) as $p){
				if (self::is_arabic($p->post_title)){
					pll_set_post_language($p->ID, 'ar');
					$ar++;
				}
				else {
					pll_set_post_language($p->ID, 'fr');
					$fr++;
				}

			}
			echo sprintf('<h4>%s fr | %s ar</h4>', $fr, $ar);
			break;
		}

	}

	public function showContent(){
		$post_id = get_the_ID();
		$c = get_post($post_id);
		$c = $c->post_content;
		echo '<textarea class="w-100 h-12 text-left" dir="ltr">'.$c.'</textarea>';
		die();
	}

	public function showOldContent(){
		$post_id = get_the_ID();
		$id = get_post_meta($post_id,'oldid', true );
		$prod = $this->getDB();
		$q = "SELECT post_content from wp_posts where ID=".$id;
		try{

			foreach ($prod->query($q) as $row){
				echo '<textarea class="w-100 h-12 text-left" dir="ltr">'.$row['post_content'].'</textarea>';
			}
		}
		catch (PDOException $e) {
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}
		die();
	}

	public function copyPostTaxonomies($post_id){
		$id = get_post_meta($post_id,'oldid', true );
		$prod = $this->getDB();
		$terms = [];
		$q = "SELECT object_id, taxonomy, name from wp_term_relationships left join wp_term_taxonomy on wp_term_taxonomy.term_id=wp_term_relationships.term_taxonomy_id left join wp_terms on wp_terms.term_id = wp_term_taxonomy.term_taxonomy_id   where object_id=".$id;
		foreach ($prod->query($q) as $row){
			$taxonomy = $row['taxonomy'];
			if ($taxonomy=='inky_tax_jkind'){
				$taxonomy = 'genre';
			}
			if ($taxonomy!='inky_tax_lang'){
				$terms[$taxonomy]=$terms[$taxonomy]??[];
				$terms[$taxonomy][]=$row['name'];
			}
			else {
				pll_set_post_language($post_id, strtolower($row['name']));
			}

		}
		$info = [];
		foreach ($terms as $tax=>$list){
			wp_set_object_terms( $post_id, $list, $tax, true );
			$info[]=['taxonomy'=>$tax, 'terms'=>implode(', ',$list), 'status'=>'ok'];
		}
		echo '<div class="container bg-gray-dark my-5"><h1>Taxonomies</h1>';
		$this->renderInfos($info);
		echo '</div>';
	}

	public function copyPostMeta($post_id){
		$id = get_post_meta($post_id,'oldid', true );
		$prod = $this->getDB();

		foreach ($prod->query("SELECT meta_key, meta_value from wp_postmeta where post_id=".$id) as $row){
			$k = $row['meta_key'];
			$v = $row['meta_value'];
			$infos[] = ['key'=>$k, "value"=>maybe_unserialize( $v )];
			if ($k=='_thumbnail_id'){

				$v = $this->get_new_post_thumbnail($id);

			}
			if (isset($meta[$k])){
				$meta[$k] = !is_array($meta[$k])?[$meta[$k]]:$meta[$k];
				$meta[$k][]=maybe_unserialize(get_post_meta($id, $k, true),1);
			}
			else {
				if ($v){ 
					$meta[$k][]=maybe_unserialize(get_post_meta($id, $k, true),1);
				}
			}
		}
		echo '<div class="container bg-gray-dark my-5"><h1>Meta</h1>';
		$this->renderInfos($infos);
		echo '</div>';
		$authors = $infos = [];
		$c = $meta['inky_authors']??[];
		if (!is_array($c)) $c = [$c];
		foreach ($c as $uid){
			$infos[] = ['user_id'=>(int)$uid, 'role'=>'author', 'status'=>'ok'];
			$authors[]=['user_id'=>(int)$uid, 'role'=>'author'];
		}
		$d = $meta['inky_contributor_name']??[];
		foreach ($d as $i=>$uid){
			$role = $meta['inky_role_contributor'][$i]??'';
			$authors[]=['user_id'=>(int)$uid, 'role'=>$role];
			$infos[]=['user_id'=>(int)$uid, 'role'=>$role, 'status'=>'ok'];

		}
		echo '<div class="container bg-gray-dark my-5"><h1>Contributors</h1>';
		$this->renderInfos($infos);
		echo '</div>';
		$note = $meta['inky_author_note']??'';
		$readingTime = $meta['inky_story_time']??'';
		if ($readingTime){
			update_post_meta($post_id, 'reading-duration', $readingTime);

		}
		update_post_meta($post_id, 'wpcf-author-note', $note);
		update_post_meta($post_id, '_contributors', $authors);
		foreach ($meta as $k=>$v){
			echo '<pre>"'.$k.'": '.print_r($v,1).'</pre>';
			update_post_meta($post_id, $k, $v);
			
		}
		var_dump(get_post_meta($post_id, '_aioseop_opengraph_settings', true));

		die();

	}
	public function copy_post($post){
		global $count;
		$count++;
		$info = [];
		$info['index']=$count;
		$r = get_page_by_title( $post['post_title'], OBJECT, 'post' );
		if (!$r){
			$doublecheck = get_posts(['name'=>$post['post_name']]);
			if ($doublecheck){
				$r = $doublecheck[0];
			}
		}
		if (!$r){
			$aid = $this->get_new_post_thumbnail($post['ID']);
			$_post = $post;
			unset($_post['ID']);
			$newid = wp_insert_post($_post);
			update_post_meta($newid, '_thumbnail_id', $aid);
			$info['id']=[
				'old'=>$post['ID'],
				'new'=>$newid
			];
			$info['title']=[
				'new'=>$post['post_title']
			];
			$info['status']='warning';

		}
		else {
			update_post_meta($r->ID, 'oldid', $post['ID']);
			$info['id']=[
				'old'=>$post['ID'],
				'new'=>$r->ID,
			];
			$info['title']=[
				'old'=>$post['post_title'],
				'new'=>$r->post_title
			];
			$info['status']='copied';
		}
		return $info;
	}

	public function get_old_post_attachment($id){
		$prod = $this->getDB();
		foreach ($prod->query("SELECT meta_value from wp_postmeta where meta_key='_thumbnail_id' and post_id=".$id) as $data){
			return $data['meta_value'];
		}

	}

	public function get_new_post_thumbnail($id){
		$aid = $this->get_old_post_attachment($id);
		if (!$aid) return '';
		$prod = $this->getDB();
		foreach ($prod->query("SELECT guid from wp_posts where ID=".$aid) as $data){
			return get_attachment_id_by_url($data['guid']);
		}
	}

	public function copy_posts(){
		global $count;
		$table =[];
		$prod = $this->getDB();
		foreach ($prod->query("SELECT * from wp_posts where post_type='post' and post_status='publish'") as $data){
			$post = [];
			foreach ($data as $k=>$v){
				if (!is_int($k) ){
					$post[$k]=$v;
				}
			}
			$table[] = $this->copy_post($post);
		}
		return $table;
	}

	public function copy_attachments(){
		$prod = $this->getDB();

		$table = $this->getAttachmentsInfos();
		foreach ($table as $t){
			foreach ($prod->query("SELECT * from wp_posts where ID=".$t['id']) as $data){
				$post = [];
				foreach ($data as $k=>$v){
					if (!is_int($k) ){
						$post[$k]=$v;
					}
				}
				$this->copy_attachment($post);
			}

		}
	}

	public function copy_attachment($post){
		$_url = explode('uploads/', $post['guid']);
		$relative_path = $_url[1]??'';

		$filename = ABSPATH.'wp-content/uploads/'.$relative_path;

		if (!file_exists($filename)){
			list($year, $month, $file) = explode('/', $relative_path);
			if (!is_dir(WP_CONTENT_DIR.'/uploads/'.$year)){
				mkdir(WP_CONTENT_DIR.'/uploads/'.$year);
			}
			if (!is_dir(WP_CONTENT_DIR.'/uploads/'.$year.'/'.$month)){
				mkdir(WP_CONTENT_DIR.'/uploads/'.$year.'/'.$month);
			}
			copy($post['guid'], $filename);
		}
		$media = $post;
		unset($media['ID']);
		if (get_attachment_id_by_url(WP_CONTENT_URL.'/uploads/'.$relative_path)){
			return;
		}
		$attach_id = wp_insert_attachment( $media, $filename );
		require_once  ABSPATH . 'wp-admin/includes/image.php' ;
		require_once  ABSPATH . 'wp-admin/includes/media.php' ;


		update_post_meta($attach_id, 'oldid', $post['ID']);
		$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		return $attach_id;
	}



	public function getAttachmentsInfos(){
		$prod = $this->getDB();
		add_filter('_get_attachments_infos', function ($infos, $row){
			$id = (int)$row['ID'];
			$post = get_post($id);
			$infos['id'] =  $id;
			$infos['name']=[
				'old'=>$row['guid'],
				'new'=>$post?$post->guid:'###',
			];

			$infos['status']= $post ? (($infos['name']['old']===$infos['name']['new'])?'copied':'warning'):'not-copied';
			if (get_attachment_id_by_url($row['guid']) ){
				$infos['status'] = 'copied';
			}
			return $infos;

		},2,2);
		$table = [];
		$i = 1;

		foreach ($prod->query('SELECT * from wp_posts where post_type="attachment"') as $index=>$u){
			$row = ['index'=>$i, 'status'=>''];
			$infos = apply_filters('_get_attachments_infos', $row, $u);
			if ($infos['status']!='copied'){
				$i++;
			}
			$table[] = $infos;
		}
		return $table;

	}



	public function getUsersInfos(){
		$prod = $this->getDB();

		global $wpdb;


		add_filter('_get_user_infos', function ($infos, $row){
			$id = (int)$row['ID'];
			$olddisplayname = $row['display_name'];

			$u = get_user_by( 'id', $id );
			$infos['id'] =  $id;
			$infos['name']=[
				'old'=>$row['display_name'],
				'new'=>$u?$u->display_name:'###',
			];
			$infos['status']= $u ? (($infos['name']['old']===$infos['name']['new'])?'copied':'warning'):'not-copied';
			return $infos;
		},2,2);
		$table = [];

		foreach ($prod->query('SELECT * from wp_users') as $index=>$u){
			$row = ['index'=>$index];
			$table[] = apply_filters('_get_user_infos', $row, $u);
		}
		return $table;

	}

	public function getDB(){
		$options = array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		);
		$env  = explode('.',get_site_url());
		$env = $env[1]==='localhost'?'local':'preprod';
		$user = ($env==='preprod')?'production':'root';
		$pass = ($env==='preprod')?'ksCZAbs4XV':'DlzPx37c';
		$db = ($env==='preprod')?'production':'inkyprod';
		$proddb = new PDO('mysql:host=localhost;dbname='.$db, $user, $pass, $options);
		return $proddb;
	}

	public function isUserMigrated($id){
		$u = get_user_by( 'id', (int)$id);
		return $u instanceof WP_User;
	}

	public function renderInfos($infos){
		require_once dirname(__DIR__).'/app/Table.php';
		$table = new TableRenderer();
		echo $table->render($infos, function($info){
			$status = $info['status']??'';
			switch ($status){
				case 'not-copied': return ' bg-danger color-white';
				case 'copied': return 'bg-success color-white';
				case 'warning': return 'bg-warning color-gray-900';
				default: return $status;
			}
			return '';
		}, ['head_class'=>'fw-700 uppercase h5']);
	}

	public static function uniord($u) {
    // i just copied this function fron the php.net comments, but it should work fine!
		$k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
		$k1 = ord(substr($k, 0, 1));
		$k2 = ord(substr($k, 1, 1));
		return $k2 * 256 + $k1;
	}

	static function is_arabic($str) {
		if(mb_detect_encoding($str) !== 'UTF-8') {
			$str = mb_convert_encoding($str,mb_detect_encoding($str),'UTF-8');
		}

    /*
    $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
    $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
    */
    preg_match_all('/.|\n/u', $str, $matches);
    $chars = $matches[0];
    $arabic_count = 0;
    $latin_count = 0;
    $total_count = 0;
    foreach($chars as $char) {
        //$pos = ord($char); we cant use that, its not binary safe 
    	$pos = self::uniord($char);
    	
    	if($pos >= 1536 && $pos <= 1791) {
    		$arabic_count++;
    	} else if($pos > 123 && $pos < 123) {
    		$latin_count++;
    	}
    	$total_count++;
    }
    if(($arabic_count/$total_count) > 0.6) {
        // 60% arabic chars, its probably arabic
    	return true;
    }
    return false;
}

} 

