<?php 

add_filter('wp_audio_shortcode_override', 'audio_migration_shortcode',10,4);

function audio_migration_shortcode($str, $atts, $content){
	global $Inkube;
	$handler = $Inkube->getContainer()->get('Inkube\Shortcodes\ShortcodesModule')->getHandler('audioplayer');
	$manager = $Inkube->getContainer()->get('Inkube\Media\MediaManager');
	$src = $atts['mp3']??$atts['src']??'';

	$id = $manager->get_attachment_id_by_url($src);
	if ($id) {
		unset($atts['src']);
	
		unset($atts['mp3']);
	}
	$atts['container_class'] = $atts['class']??'col-md-6 mx-auto';
	unset($atts['class']);

	$atts['id'] = $id;
	return $handler->render($atts, $content);
}
