<?php 
add_filter('img_caption_shortcode', array('Caption', 'shortcode'), 10,3);	
Class Caption {
	public function __construct(){
	}

	static function shortcode($str="", $attr="", $content=""){
		$c = preg_replace('/(<[^>]+) srcset=".*?"/i', '$1', $content);
		$c = preg_replace('/(<[^>]+) sizes=".*?"/i', '$1', $c);
		preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $c, $matches);
		$url = $matches[1]??'';
		preg_match('/< *img[^>]*title *= *["\']?([^"\']*)/i', $c, $matches);
		$title = @$matches[1];
		$atts = shortcode_atts( array(
			'id'	  => '',
			'align'	  => 'alignnone',
			'width'	  => '',
			'caption' => '',
			'class'   => '',
			'figcaptionclass'=>'col-12'
		), $attr, 'caption' );
		$id = get_attachment_id_by_url($url);
		if ($id){
			$image = new \Inkube\Shortcodes\Image();
			$args = [
				'id'=>$id, 
				'container_class'=>$atts['class'],
				'caption_class'=>$atts['figcaptionclass']
			];
			if (
				strpos($atts['class'], 'cover')>-1 
				|| strpos($atts['class'], 'fullscreen')>-1 
			){
				$args['background']=1;
			}
			if (strpos($atts['class'], 'parallax')>-1){
				$args['background']= 1;
				$args['parallax']= 'parallax';
			}
			$args['caption_class'] = str_replace('card-caption', 'pos-absolute', $args['caption_class']);
			return $image->render($args, $atts['caption']);

		}
		$shcode = '[caption ';
		foreach ($atts as $k => $v){
			if ($k !="caption")
				$shcode .= $k.'="'.$v.'" ';
		}
		$shcode  .= ']'.$c.$atts['caption'].'[/caption]';
		$atts['width'] = (int) $atts['width'];
		
		if ( ! empty( $atts['id'] ) )
			$atts['id'] = 'id="' . esc_attr( sanitize_html_class( $atts['id'] ) ) . '" ';

		$class = trim( 'wp-caption ' . $atts['align'] . ' ' . $atts['class'] );
		//die();
		$html5 = current_theme_supports( 'html5', 'caption' );
	// HTML5 captions never added the extra 10px to the image width
		$width = $html5 ? $atts['width'] : ( 10 + $atts['width'] );

		$caption_width = apply_filters( 'img_caption_shortcode_width', $width, $atts, $content );

		if ( $html5 ) {
			$classes = explode(' ',$class);
			if (in_array('fullscreen', $classes) || 
				in_array('fullwidth', $classes) || 
				in_array('parallax', $classes) || 
				in_array('cover',$classes)
			){
				$html = '<div  data-shortcode-tag="caption" data-shortcode="' .base64_encode($shcode).'" '. $atts['id']  .'class="' . esc_attr( $class ) . '"><div class="background-image" style="background-image:url('.$url.')"></div><figcaption class="wp-caption-text '.$atts['figcaptionclass'].'">' . $atts['caption'] . '</figcaption></div>';
		} elseif (in_array('skrollr-fade-full', $classes)) {
			$html = '<div  data-shortcode-tag="caption" data-shortcode="' .base64_encode($shcode).'" '. $atts['id']  .'class="' . esc_attr( $class ) . '"><div class="background-image" data-bottom="filter:grayscale(1);-webkit-filter: grayscale(100%);" data-center-bottom="filter:grayscale(0);-webkit-filter: grayscale(0%);" style="background-image:url('.$url.');"><h2 class="wp-caption-title">' . $title . '</h2></div><figcaption class="wp-caption-text '.$atts['figcaptionclass'].'">' . $atts['caption'] . '</figcaption></div>';
		} elseif (in_array('skrollr-fade', $classes)) {
			$html = '<div  data-shortcode-tag="caption" data-shortcode="' .base64_encode($shcode).'" '. $atts['id']. 'class="' . esc_attr( $class ) . '" data-bottom="filter:grayscale(1);-webkit-filter: grayscale(100%);" data-center-bottom="filter:grayscale(0);-webkit-filter: grayscale(0%);">'
			. do_shortcode( $content ) . '<figcaption class="wp-caption-text '.$atts['figcaptionclass'].'">' . $atts['caption'] . '</figcaption></div>';

		} else {
			$html = '<div  data-shortcode-tag="caption" data-shortcode="' .base64_encode($shcode).'" '. $atts['id']. 'class="' . esc_attr( $class ) . '">'
			. do_shortcode( $content ) . '<figcaption class="wp-caption-text '.$atts['figcaptionclass'].'">' . $atts['caption'] . '</figcaption></div>';
		}
	}

	return $html;

}
}

Class InkubeCaption{
	public function __construct(){
		add_action('wp_ajax_get_attachment_id', array($this, 'get_image_id'));       
	}

	public function get_image_id(){
		if (!isset($_POST['url'])){
			wp_send_json_error('empty url');
			die();
		}
		$url = $_POST['url'];
		$id = get_attachment_id_by_url($url);
		if ($id){
			$attachment = wp_get_attachment_image_src( $id, 'full');
			wp_send_json_success(array(
				'id'=>$id, 
				'url'=>$attachment[0],
				'width'=>$attachment[1], 
				'height'=>$attachment[2])
		);
		}
		else {
			wp_send_json_error('id not found');

		}
		die();
	}
}

new InkubeCaption();