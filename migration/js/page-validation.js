(function(){
	var setActiveTab = (el)=>{
		document.querySelectorAll('#validation-menu .toggler').forEach((tab)=>{
			tab.classList.remove('color-red');
		});
		el.classList.add('color-red');
		document.querySelectorAll('.validation-tab').forEach((tab)=>{
			tab.style.display="none";
		});
		var selector = el.getAttribute('href');
		var target = document.querySelector(selector);
		target.style.display="block";


	}
	let lastTab = localStorage.getItem('current-validation-tab');
	if (lastTab){
		var el = document.querySelector('[href="'+lastTab+'"]');
		setActiveTab(el);
	}
	document.querySelector('#validation-search').focus();
	document.querySelectorAll('.toggler').forEach((el)=>{
		el.addEventListener('click', (e)=>{
			localStorage.setItem('current-validation-tab', e.target.getAttribute('href'));
			e.preventDefault();
			setActiveTab(e.target);

		});
	});
})();