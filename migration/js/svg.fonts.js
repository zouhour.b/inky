jQuery.get('/wp-json/inkube/v1/fonts/', function(r){
	init(r);
});

var  init = (fonts)=>{
	let missingFonts = [];
	let hidesuccess = false;
	let hide_success = ()=>{
		hidesuccess = !hidesuccess;
		if (hidesuccess){
		jQuery('.bg-success[data-svg-url]').hide();
		}
		else{
		jQuery('.bg-success[data-svg-url]').show();
		}
	}
	jQuery('.hide-success').click(hide_success);
	
	setInterval(function(){
		let total = jQuery('[data-svg-url]').length;
		let errors = jQuery('.bg-danger[data-svg-url]').length;
		let success = jQuery('.bg-success[data-svg-url]').length;
		let warn = jQuery('.bg-warning[data-svg-url]').length;
		let fatal = jQuery('.bg-black[data-svg-url]').length;
		let missing = missingFonts.join(",\n");
		
		jQuery('#svg-log').html('total: '+total+'<br/>invalid XML:'+fatal+'<br/>font not installed:'+errors+
			'<br/>font not loaded:'+warn+'<br/>ok:'+success+'<br/>Missing:</br/><pre class="fs-14px">'+missing+'</pre>');
	}, 1000);

	let fixSVGFont = (fontList, svg)=>{
		let list = fontList.map(file=>fonts[file]);
		list = list.filter((f,i)=>{return f && list.findIndex(x=>x==f)===i});
		
		let css = list.map(fonturl=>{
			let url = '/'+fonturl.split('/').filter((p,i)=>i>2).join('/');
			return '@import url('+url+');'+"\n";
		}).join('');
		let style = svg.querySelector('style.fonts');
		if (!style){
			style=jQuery('<style>')
			style.addClass('fonts');
			jQuery(svg).prepend(style);
		}
		style = svg.querySelector('style.fonts');
		style.innerHTML = css;
		return svg.outerHTML;
	}

	let setStatus = (el, code)=>{
		let cls="";
		switch (code){
			case 'error':
			cls = 'bg-danger color-white w-50 lh-125 fs-14px p-2';
			break;
			case 'warning':
			cls = 'bg-warning color-black w-50 lh-125 fs-14px p-2';
			break;
			case 'info':
			cls = 'bg-info color-white w-50 lh-125 fs-14px p-2';
			break;
			case 'interrupted':
			cls = 'bg-black color-white w-50 lh-125 fs-14px p-2';
			break;
			default:
			cls = 'bg-success color-white w-50 lh-125 fs-14px p-2'
			break;
		}
		el.attr('class', cls);
	}
	let isFontLoaded = (doc, font)=>{
		let html = doc.querySelector('svg').innerHTML;
		let fontUrl = fonts[font];
		if (!fontUrl){
			return false;
		}
		let url = '/'+fontUrl.split('/').filter((p,i)=>i>2).join('/');
		if (html.indexOf(url)>1){
			return true;
		}
		return false;
	}
	
	jQuery('[data-svg-url]').each(function(){
		let $this = jQuery(this);
		let url = $this.attr('data-svg-url');
		let preview = $this.parent().find('.preview');
		let $fonts = $this.parent().find('div.font-list');
		setStatus($this, 'info');
		jQuery.ajax({
			url: url, 
			type: 'get',
			error: function(XMLHttpRequest, textStatus, errorThrown){
				$this.click(function(e){
					jQuery('.preview').html('').css('border', 'none');
					e.preventDefault();

					let d = ('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText
						+ '<p>'+errorThrown+'</p>');
					preview.html(d);
				});
				setStatus($this, 'interrupted');

			},
			success:  function(r){
				setStatus($this, 'success');	
				let $d = jQuery('div');
				
				let fontList = [];
				r.querySelectorAll('[font-family]').forEach(function(ff){
					let fontfamily = ff.getAttribute('font-family');
					fontfamily=fontfamily.replace('"', "").replace('"', "").replace("'", '').replace("'", '');
					if (!fontList.find(e=>e==fontfamily)){
						fontList.push(fontfamily);
					}
				});
				r.querySelectorAll('style, [style]').forEach(function(s){
					let html  = s.innerHTML;
					let regex = /font-family:([^;]+);/g
					let  m=regex.exec(html);
					let i=0;
					if (!m) return;
					while (m && i<100){
						i++;
						console.log(m[1]);
						let fontfamily = m[1];

						fontfamily=fontfamily.replace('"', "").replace('"', "").replace("'", '').replace("'", '');
						fontfamily = fontfamily.split(' ').filter(e=>e).join(' ');
						if (!fontList.find(e=>e==fontfamily)){
							fontList.push(fontfamily);
						}
						m=regex.exec(html)
					}

				});
				if (fontList.length){
					$fonts.html('');
				}
				else{
					$fonts.html("Aucune font n'est détectée")
				}
				let canbefixed = false;
				fontList.map(function(e){
					let status = 'fa fa-check-square';
					let message  ='this font is loaded properly';
					if (!fonts[e]){
						setStatus($this, 'error');
						status ='fa fa-exclamation-circle';
						if (!missingFonts.find(i=>i==e) ){
							missingFonts.push(e);
						}
						message = 'this font is not installed on inkube';
						
					}
					else if (!isFontLoaded(r, e)){
						setStatus($this, 'warning');
						status ='fa fa-exclamation-triangle';
						canbefixed = true;

						message = 'this font is not loaded in the SVG ['+e+']';
					}

					$fonts.append('<a title="'+message+'" class="d-block"><span class="'+status+'"></span> '+e+'</a>');

				});
				if (canbefixed){
					$fonts.append('<a class="d-block alignright m-2 bg-gray-900 py-1 px-3 color-white  fix-fonts">Fix</a><div class="clear-both" style="clear:both"></div>');
				}
				$fonts.find('.fix-fonts').click(function(e){
					e.preventDefault();
					let svg = fixSVGFont(fontList, r.querySelector('svg'));
					jQuery.post('/wp-json/inkube/v1/files', {
						'dir':$this.attr('data-svg-dir'),
						'name':$this.attr('data-svg-file'),
						'file':svg
					}, function(r){
						console.log(r);
					});
				});


				$this.click(function(e){
					jQuery('.preview').html('').css('border', 'none');
					preview.css('border', '1px solid #333');
					preview.load(url);
				});

			}
		});


	});
}