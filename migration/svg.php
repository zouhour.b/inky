<?php 

add_filter('shortcode_html', function($html, $shortcode, $atts, $content){
	if ($shortcode->tag !='components'){
		return $html;
	}
	$id = $atts['id']??null;
	if (!$id) return $html;
	$type = wp_get_object_terms( $id, 'component-type' );
	$is_svg = false;
	foreach ($type as $t){
		if ($t->slug =='svg') $is_svg=true;
	}
	if (!$is_svg){
		return $html;
	}
	$mobile_file = get_post_meta($id, 'wpcf-filename-mobile', true);
	$file = get_post_meta($id, 'wpcf-filename', true);
	return do_shortcode(sprintf('[svg sm="%s" md="%s" container_class="%s"]%s[/svg]', $mobile_file, $file, $atts['container_class']??'w-100 mb-5', $content ));


}, 4, 4);

add_filter('shortcode_html', function($html, $sh, $atts, $content){
	if ($sh->tag !=='svg'){
		return $html;
	}
	global $Inkube;
	$manager = $Inkube->getContainer()->get('Inkube\Media\MediaManager');
	global $post;
	$pathprefix = WP_CONTENT_DIR.'/uploads/components/svg/';
	if (isset($atts['sm'])){
		$atts['sm'] = insert_svg($pathprefix.$atts['sm'], $post);
		
	}
	if (isset($atts['md'])){
		$atts['md'] = insert_svg($pathprefix.$atts['md'], $post);
		
	}
	$inf = new \Inkube\Shortcodes\Infographic();
	return $inf->render($atts, $content);

}, 4,4);

function insert_svg($file, $post){
	$filename = basename($file);
	$title = str_replace('.svg', '', $filename );
	$title = str_replace('-', ' ', $title);
	$date = explode(' ',$post->post_date)[0];
	$month = explode('-', $date)[1];
	$year = explode('-', $date)[0];
	$target = sprintf(WP_CONTENT_DIR.'/uploads/%s/%s/%s', $year, $month, $filename);
		$url = str_replace(WP_CONTENT_DIR,WP_CONTENT_URL, $target);
	
	if (file_exists($target)){
		global $Inkube;
		$manager = $Inkube->getContainer()->get('Inkube\Media\MediaManager');
		$id = $manager->get_attachment_id_by_url($url);
		if ($id) return $id;
	}
	$attachment = array(
		'post_mime_type' => 'image/svg',
		'post_title' => $title,
		'guid'=>$url,
		'post_parent'=>$post->ID,
		'post_date'=>$post->post_date,
		'post_status' => 'inherit',
	);

	copy ($file, $target);
	$image_id = wp_insert_attachment($attachment, $target, $post->ID);
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	$image_data = wp_generate_attachment_metadata($image_id, $target);
	wp_update_attachment_metadata($image_id, $image_data);
	return $image_id;
}