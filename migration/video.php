<?php 
add_filter('wp_video_shortcode_override', 'video_migration_shortcode',10,4);

function video_migration_shortcode($str, $atts, $content){
	global $Inkube;
	$handler = $Inkube->getContainer()->get('Inkube\Shortcodes\ShortcodesModule')->getHandler('videoplayer');
	$manager = $Inkube->getContainer()->get('Inkube\Media\MediaManager');
	$src = $atts['src']??'';
	$param = $atts['urlparams']??'';
	$atts['src'] = $src.$param;
	$atts['container_class']= str_replace('video-js', '', $atts['class']??'');
	$atts['container_class']=str_replace('fullwidth', 'w-100', $atts['container_class']);
	$atts['caption_class'] = $atts['figcaptionclass']??'';
	unset($atts['width']);
	unset($atts['height']);
	unset($atts['urlparams']);
	unset($atts['figcaptionclass']);
	unset($atts['class']);
	return $handler->render($atts, $content);
}
