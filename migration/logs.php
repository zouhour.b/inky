<?php 
add_shortcode('validation_logs', function($atts, $content){
	ob_start();
	$data = [];
	$posts =get_posts(['numberposts'=>-1, 'lang'=>'ar,fr']);
	$total = count($posts);
	foreach ($posts as $p){
		$status = get_post_meta($p->ID, 'migration-status', true);
		if (!$status){
			update_post_meta($p->ID, 'migration-status', 'not-ready');
		}
		$status = get_post_meta($p->ID, 'migration-status', true);

			$data[$status] = isset($data[$status])?$data[$status]+1:1;
		$logs = get_post_meta($p->ID, 'migration-errors', true);
		if ($logs){
			?>
			<div class="row fs-16px no-gutters">
				<div class="col-5 border p-2">
					<a href="<?php echo get_post_permalink($p->ID); ?>" target="_blank">
						<?php echo $p->post_title ?>
					</a>
				</div>
				<div class="col-7 border">
					<?php foreach ($logs as $log): ?>
						<?php $log['type'] = $log['type']??''; ?>
						<?php $rowclass = $log['type']=='error'?'bg-danger color-white':'bg-gray-200'; ?>
						<div class="row no-gutters <?= $rowclass ?>">
							<div class="col-2 p-2">
								<?= $log['type']??'-'; ?>
							</div>
							<div class="col-10 p-2">
								<?= $log['description']??''; ?>
							</div>
						</div>
					<?php endforeach; ?>


				</div>
			</div>
			<?php 

		}
	}
	$status = '<h4>Progress</h4><div class="d-flex mb-5  border h6">';
	$classes = [
		'not-ready'=>['name'=>'en cours', 'class'=>'bg-danger color-white'],
		'needs-validation'=>['name'=>'en attente', 'class'=>'bg-warning'],
		'valid-desktop'=>['name'=>'Validé pour Desktop', 'class'=>'bg-success color-white'],
		'valid-mobile-dektop-'=>['name'=>'Validé Desktop et mobile', 'class'=>'bg-success color-white'],
		
		'undefined'=>['name'=>'undefined', 'class'=>'gray-100']
	
	];
	foreach ($data as $k=>$v){
		$w  = $v*100/$total;
		$style = '';
		if ($w < 10){
			$style = 'pos-absolute bottom-12 d-block direct-0 color-gray-800" style="width:100px; margin-left:-50px';
		}
		$status.=sprintf('<div style="width:%1$s;" class="%2$s pos-relative">
			<a class="d-block %6$s" title="%3$s">%4$s <small class="%5$s">%3$s</small></a></div>', 
			($w).'%', 
			"py-2 ".($classes[$k]['class']??$classes['undefined']['class']), 
			$classes[$k]['name']??$k, 
			$v, 
			$style,
			$w>10?'mx-2':'text-center d-block'
			 
			

		);
	}
	$status .='</div>';

	$rows = ob_get_clean();
	$head = '<h4>Tickets</h4><div class="bg-gray-900 color-white py-2 row no-gutters">
	<div class="col-5 px-2">Article</div>
	<div class="col-7 d-flex">
	<div class="col-2">Priorité</div>
	<div class="col-10">Description</div>
	</div></div>';
	return '<div style="min-height:100vh">'.$status.$head.$rows.'</div>';//sprintf('%s',$rows);

});