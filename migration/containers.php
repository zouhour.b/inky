<?php

add_filter('container_class', function($str, $tag, $atts){
	if (!$str && $tag==='components'){
		return $atts['class']??'';
	}
	return $str;
},3,3);

add_filter('before_encode_atts', function($atts, $tag){
	if (!$atts) return $atts;
	if ($tag == 'ref') return $atts;
	if (!isset($atts['container_class']) && !isset($atts['class'])){
		$atts['container_class'] = 'p';
		return $atts;
	}
	if (!isset($atts['container_class']) && isset($atts['class'])){
		$atts['container_class']=$atts['class'];
		unset($atts['class']);
	}

	return $atts;
},2,2);