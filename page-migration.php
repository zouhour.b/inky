<?php if (isset($_GET['reset'])){
/*
	$posts = get_posts(array('numberposts'=>-1));

	foreach ($posts as $p){
		wp_set_object_terms( $p->ID, array('Not Ready'), 'migration-status', false );
		echo 'updating '.$p->ID.'  migration status...<br/>';
		if (!is_arabic($p->post_title)){
			pll_set_post_language($p->ID, 'fr');
		}

	}*/


	include get_template_directory().'/inc/migration.class.php';


	$migrator = new InkyfadaMigration();
	
	$query = $wpdb->get_results("SELECT * FROM `wp_terms` as t left join wp_term_taxonomy as tt on tt.term_id=t.term_id where tt.taxonomy='genre'");


	foreach ($query as $term) {
		$migrator->migrateTerm($term);
	}
	die();
}
function migrate_post_authors(){
	$userposts = array();
	$posts= get_posts(array('numberposts'=>-1));

	$errors = array('no_author'=>array(), 'contributor_not_found'=>array(), 'no_contributor');
	foreach ($posts as $p){
		$author = $p->post_author;
		$_contribs = get_post_meta($p->ID, 'inky_contributor_name');
		$_contribs_role = get_post_meta($p->ID, 'inky_role_contributor');
		$_authors = get_post_meta($p->ID, 'inky_authors');
		if (!$_authors){
			$errors['no_author'][] = $p->ID;
		}
		$_contributors = array();
		foreach ($_authors as $auth){
			$user_meta = get_userdata( $auth );
			if (!$user_meta){
				echo '<div class="alert alert-danger  dismissable"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$p->post_title.' '.$auth.' not found</div>';
				$errors['contributor_not_found'][] = $p->ID;
				wp_set_object_terms( $p->ID, array('Author error'), "migration-status", true );
			}
			else{
				$name = $user_meta->first_name .' '.$user_meta->last_name;
				$_contributors[] = array(
					'user_id'=>$auth,
					'name'=>$name,
					'role'=>'author'
					);
				if (isset($userposts[$auth])){
					$userposts[$auth][] = $p->ID;
				}
				else {
					$userposts[$auth] = array($p->ID);
				}
			}
		}
		foreach ($_contribs as $i=>$_contrib){
			if 	(isset($_contribs_role[$i])){
				$user_meta = get_userdata( $_contrib );
				if (!$user_meta){
					echo '<div class="alert alert-danger  dismissable"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$p->post_title.' '.$_contrib.' not found</div>';
					$errors['contributor_not_found'][] = $p->ID;
					wp_set_object_terms( $p->ID, array('Author error'), "migration-status", true );
				}
				else{
					$name = $user_meta->first_name .' '.$user_meta->last_name;
					$_contributors[] = array(
						'user_id'=>$_contrib,
						'name'=>$name,
						'role'=>$_contribs_role[$i]
						);
				}
			}
			
		}
		if (!$_contributors){
			echo '<div class="alert alert-danger  dismissable"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$p->post_title.' No Contributors found</div>';
			wp_set_object_terms( $p->ID, array('Author error'), "migration-status", true );
			$errors['no_contributors'][] = $p->ID;

		}
		else update_post_meta($p->ID, '_contributors', $_contributors);
		
	}
	foreach ($userposts as $uid=>$pids){
		update_user_meta( $uid, 'userposts', $pids );
	}

	return $errors;
}


global $post;

$posts = get_posts(array('numberposts'=>-1, 'offset'=>20, 'migration-status'=>'not-ready')); 
$readyposts = get_posts(array('numberposts'=>-1, 'offset'=>20, 'migration-status'=>'ready')); 
$validposts = get_posts(array('numberposts'=>-1, 'offset'=>20, 'migration-status'=>'valid')); 




get_header(); 
if (isset($_GET['authors'])) var_dump(migrate_post_authors()); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main row" role="main">
		<div class="col-md-4 " style="margin-top: 100px">
			<h2>Not Ready <span class="badge"><?php echo count($posts) ?></span></h2>
			<ul>
				<?php foreach ($posts as $post):setup_postdata($post); ?>

					<li><a  target="_blank" class="" href="<?php the_permalink(  ); ?>"><?php the_title() ?></a></li>
				<?php endforeach; ?> 
			</ul>
		</div>

		<div class="col-md-4" style="margin-top: 100px">
			<h2 class="pt-0">Ready <span class="badge"><?php echo count($readyposts) ?></span></h2>
			<ul>
				<?php foreach ($readyposts as $post):setup_postdata($post); ?>

					<li><a class="" href="<?php the_permalink(  ); ?>"><?php the_title() ?></a></li>
				<?php endforeach; ?> 
			</ul>
		</div>
		<div class="col-md-4" style="margin-top: 100px">
			<h2 class="pt-0">Valid <span class="badge"><?php echo count($validposts) ?></span></h2>
			<ul>
				<?php foreach ($validposts as $post):setup_postdata($post); ?>

					<li><a class="" href="<?php the_permalink(  ); ?>"><?php the_title() ?></a></li>
				<?php endforeach; ?> 
			</ul>
		</div>

		<div class="col-md-4" style="margin-top: 100px">
			<h2 class="pt-0">Revision </h2>
			<ul>
				<?php $revs= get_posts(array("post_type"=>"revision","numberposts"=>-1,"post_author"=>2,"post_status"=>"inherit"));?>
				<?php foreach ($revs as $post):setup_postdata($post); ?>

					<li><?php echo $post->post_date;?> <a class="" href="<?php the_permalink($post->post_parent  ); ?>"><?php the_title() ?></a></li>
				<?php endforeach; ?> 
			</ul>
		</div>
	</main>
</div>
<?php get_footer() ?>