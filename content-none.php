<section class="no-results not-found">
	<header class="page-header">
		<h2 class="page-title"><?php _e( 'Rien n\'a été trouvé', 'inkyfada' ); ?></h2>
	</header><!-- .page-header -->

	<div class="page-content text-center">

		<p><?php _e( 'Désolé, mais rien ne correspond à vos termes de recherche. Veuillez réessayer avec d\'autres mots-clés.', 'inkyfada' ); ?></p>

	</div><!-- .page-content -->
</section><!-- .no-results -->
