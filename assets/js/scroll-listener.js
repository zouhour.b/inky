ScrollListener = function(args){
	this.args = args;
	this.init();
}

ScrollListener.prototype = {
	init: function(){
		if (this.args.sel){
			this.$sel = jQuery(this.args.sel);
		}
		else if (this.args.id) {
			this.$sel = jQuery("#gallery-"+this.args.id+" .gallery");
		}
		else {
			alert('Scroll Listser: Enable to select container \n Please call me with an id or a selector !!!');
			return;
		}
		var SL = this;
		setTimeout(function(){SL.loadPositions()}, 1000);
		this.$slides = jQuery('.item', this.$sel).css('height', '700vh');
		this.loadPositions();
		this.initAnimations();
		this.currentSlides = [];
		
		jQuery(window).resize(function(){
			SL.loadPositions();
		});
		jQuery(window).scroll(function(d){
			SL.scrollTop = jQuery(window).scrollTop();
			slide = SL.getCurrentSlides();
			if (!slide.length){
				SL.currentSlide = null;
				SL.$slides.removeClass('active');
				return;
				
			}

			if (slides != SL.currentSlides){
				SL.$sel.trigger('changedSlide', {prev: SL.currentSlides, current: slides});
				SL.currentSlides = slides;
				SL.$slides.removeClass('active');
			}
			if (!slides || slides.length == 0) return;
			for (var i=0; i<slides.length;i++){
				slide = slides[i];
				slide.$sel.addClass('active');
				var slideIndex = 1+slide.$sel.index();
				var animations = SL.getAnimations(slideIndex);
				SL.executeAnimations(animations, slide);
			}


		});
		this.$sel.on('bienchangedSlide', function(e,d){
			if (d.prev){
				var animations = SL.getAnimations(d.prev.$sel.index()+1);
				var p = (d.prev.$sel.index() > d.current.$sel.index())?0:100;
				SL.executeAnimations(animations, d.prev, p);

				console.log('changed');
			}
		})
		
	},

	loadPositions: function(){
		var positions = [];
		this.windowHeight = jQuery(window).height();
		this.$slides.each(function(){
			positions[positions.length] = 
			{	top:jQuery(this).offset().top,
				bottom:jQuery(this).offset().top+jQuery(this).height(),
				$sel:jQuery(this)
			};
		});
		return this.positions = positions;

	},


	getAnimations: function(slideIndex){
		var animations = null;
		this.args.animations.forEach(function(a){
			if (a.slide == slideIndex){
				animations =  a.animations;
			}
		});
		return animations;
	},

	initAnimations: function(){
		var SL =this;
		this.args.animations.forEach(function(a){
			$slide = jQuery(SL.args.sel+" .item:nth-child("+a.slide+")");
			a.animations.forEach(function(d){
				var args = {
					$sel : $slide.find(d.sel),
					progress: 0,
					param: d.param
				}
				if (args.$sel.length < 1) return;
				if (typeof SL.fn[d.fn] ==='function' ){
					SL.fn[d.fn](args);
				}
			});
		});
	},

	getCurrentSlides: function(){
		var s = this.scrollTop;
		var wh = this.windowHeight;
		slides = [];
		for(var i=0; i<this.positions.length; i++){
			if(s >= this.positions[i].top - wh && s < this.positions[i].bottom){
				var t = this.positions[i].top-wh;
				var b = this.positions[i].bottom;
				this.positions[i].progress = Math.floor(100*(s-t)/(b-t));
				slides[slides.length] = this.positions[i];

			}
		}
		return slides;
	},

	executeAnimations: function(animations, currentSlide, _progress){
		if (!animations) return;
			console.log(currentSlide.progress);

		var SL  =this;
		animations.forEach(function(d){
			var progress  = Math.floor(100*(currentSlide.progress-d.start)/(d.end - d.start));
			if (progress < 0) progress = 0;
			if (progress>100) progress = 100;
			var args = {
				$sel : currentSlide.$sel.find(d.sel),
				progress: _progress?_progress:progress,
				param: d.param,
				scrollTop: SL.scrollTop,
				currentSlide: currentSlide,
				windowHeight: SL.windowHeight
			}
			if (args.$sel.length < 1) console.log(d.sel);
			var fns = d.fn.split(' ');
			fns.forEach(function(f){
				if (typeof SL.fn[f] ==='function' ){
					SL.fn[f](args);
				}
				else console.log('function '+d.fn+"not found");
			});
			
		})
	},

	
	fn: {
		fadeIn: function(o){
			var p = o.progress;
			o.param = o.param?o.param:500;
			o.$sel.css('opacity', p/100 );
		},
		fadeInOut: function(o){
			var d = o.param?o.param:500;
			var p = o.progress;
			if (p>1 && p<100){
				o.$sel.fadeIn(d);
			}
			else {
				o.$sel.fadeOut(d);
			}
		},
		fadeOut: function(o){
			var p = o.progress;
			o.$sel.css('opacity', 1 - p/100 );
		},
		enterLeft: function(o){
			var p = o.progress;
			o.param = o.param?o.param:500;
			o.$sel.css('transform', 'translateX('+((p-100)*o.param/100)+'px)');
		},
		enterRight: function(o){
			var p = o.progress;
			o.param = o.param?o.param:500;
			o.$sel.css('transform', 'translateX('+((100-p)*o.param/100)+'px)');
		},
		exitLeft: function(o){
			var p = o.progress;
			o.param = o.param?o.param:500;
			o.$sel.css('transform', 'translateX(-'+(p*o.param/100)+'px)');	
		},
		exitRight: function(o){
			var p = o.progress;
			o.param = o.param?o.param:500;
			o.$sel.css('transform', 'translateX('+(p*o.param/100)+'px)');	
		},
		zoomIn: function(o){
			var p = o.progress;
			var zmax = o.param?o.param:1.2;
			o.$sel.css('transform', 'scale('+(1+ p*(zmax-1)/100)+')' );
		},
		stick: function(o){
			if (!o.currentSlide){
				o.$sel.css('position', 'absolute').css('top', 0);
				return;
			}

			CL = o;
			var t = o.currentSlide.top;
			var b = o.currentSlide.bottom;
			var h = o.$sel.height();
			var s = o.scrollTop;
			var wh = o.windowHeight;
			//center vertically for mobile.
			var eltop  = Math.max(0, s-t+(wh-h)/2);
			if (s < t){

				o.$sel.css('position', 'absolute').css('top', eltop);
			}

			var margin = Math.max((wh-h)/2,0)
			if (s >= t){
				if (b > s+h+margin){
					o.$sel.css('position', 'fixed').css('top', 0*(s-t+margin));
				}
				else {
					o.$sel.css('position', 'absolute').css('top',b-t-h).css('bottom', 'auto'); 

				}

			}
			
		}
	}

}