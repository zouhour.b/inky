jQuery('body').attr('id', 'skrollr-body');

var skrollrInit = function(){
    if(jQuery(window).width() > 768) {
      jQuery('.card').each(function (index, post) {
        if(index%3 == 0) {
          jQuery('.card:eq('+index+')')
                  .removeClass('card-full')
                  .addClass('style-regular card-left');
          jQuery('.card:eq('+index+') .card-caption')
                  .removeClass('bottom col-md-8 offset-md-2')
                  .addClass('col-md-6 offset-md-5 vcenter')
                  .attr('data-bottom-top', 'transform: translateY(5%)')
                  .attr('data-top-bottom', 'transform: translateY(-100%)');
        } else if(index%3 == 1) {
          jQuery('.card:eq('+index+')')
                  .removeClass('card-full')
                  .addClass('style-regular card-right');
          jQuery('.card:eq('+index+') .card-caption')
                  .removeClass('bottom col-md-8 offset-md-2')
                  .addClass('col-md-6 offset-md-1 vcenter')
                  .attr('data-bottom-top', 'transform: translateY(5%)')
                  .attr('data-top-bottom', 'transform: translateY(-100%)');
        } else {
          jQuery('.card:eq('+index+') .card-caption')
                  .attr('data-bottom-top', 'transform: translateY(80px)')
                  .attr('data-top-center', 'transform: translateY(0px)');
        }
      });
      var s = skrollr.init({
        forceHeight : false,
      });
      s.refresh();
      
    }else{
      // jQuery('#primary').attr('id', '');
      jQuery('.card').each(function (index, post) {
        if(index%3 == 0) {
          jQuery('.card:eq('+index+') .card-caption')
                  .addClass('col-10 vcenter');
        } else if(index%3 == 1) {
          jQuery('.card:eq('+index+') .card-caption')
                  .addClass('col-10 offset-2 vcenter');
        }else {
          jQuery('.card:eq('+index+') .card-caption')
                  .addClass('col-10 offset-1 vcenter');
        }
      });
      jQuery('.card .card-caption').removeClass('vcenter');
      jQuery('.card .card-caption')
                  .attr('data-bottom-top', 'transform: translateY(10%)')
                  .attr('data-top-center', 'transform: translateY(-10%)');

      
    }
}

jQuery( document ).ready(skrollrInit);
jQuery( document ).on( "infiniteScrollLoaded", skrollrInit);