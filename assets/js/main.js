jQuery(document).ready(function(){
        // Toggle ajax search
        
        // Back To top
        jQuery('<a>',{
        	'id': 'go-to-top',
        	'class': 'icon-back-to-top'
        }).appendTo('body');
        jQuery('#go-to-top').each(function(){
        	jQuery(this).click(function(){ 
        		jQuery('html,body').animate({ scrollTop: 0 }, 'slow');
        		return false; 
        	});
        });
        jQuery(window).scroll(function() {
        	if (jQuery(this).scrollTop() >= jQuery(window).height()) {
        		jQuery('#go-to-top').fadeIn();
        	} else {
        		jQuery('#go-to-top').fadeOut();
        	}
        });
        // Toggle Newsletter
        jQuery('#menu-item-22510').click(function(){
        //	jQuery('body').toggleClass('newsletter-toggled');
        //	jQuery('#expanded-nav > .expanded-nav-toggler').trigger('click');
    });
        jQuery('.newsletter-form>.icon-close').click(function(){
        	jQuery('body').removeClass('newsletter-toggled');
        });
        //jQuery('.newsletter-container').append(jQuery('.mc4wp-response'));
	/*if(jQuery('.mc4wp-success').length>0){
		jQuery('#email').val("Abonnement pris en compte. Merci!");
	}else{
		jQuery('#email').val("Entrez votre adresse email");
	}*/

	
});

//SR
jQuery(document).ready(function(){
	jQuery('[data-animation-sr]').each(function(){
		jQuery(this).attr('data-sr', jQuery(this).attr('data-animation-sr'));
	});
	window.sr = new scrollReveal({reset: false, mobile:   true});
});






jQuery(document).ready(function(){
	new MakeModal();
});

MakeModal = function(args){
	this.args = args;
	this.selector = '.make-modal';
	this.init();
}

MakeModal.prototype = {
	init: function(){
		this.loadItems();
	},
	loadItems: function(){
		var M =this;
		jQuery(this.selector).each(function(){
			M.initItem(jQuery(this));
		})
	},
	initItem: function($item){
		var id= $item.attr('id');
		var content = $item.html();
		var size = $item.attr('data-size');
		size = size?" modal-"+size:'';
		var output = '\
		<div class="modal fade" id="'+id+'-modal">\
		<div class="modal-dialog'+size+'" role="document">\
		<div class="modal-content">\
		<div class="modal-header">\
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">\
		<span aria-hidden="true">&times;</span>\
		</button>\
		</div>\
		<div class="modal-body">'+content+'</div>\
		</div>\
		</div>\
		</div>';
		$item.replaceWith(output);
		var togglers = jQuery('.modal-toggle-'+id);
		if (togglers.length == 0) {
			alert( 'no toggler for '+id);
			return;
		}
		togglers.attr('href', '#'+id+'-modal').attr('data-toggle', 'modal');
		
	}
}






menuHandler = function(args){
	this.args = args;
	this.init();
}

menuHandler.prototype = {
	init:function(){
		var MH = this;
		this.$ = jQuery(this.args.sel);
		this.menuToggler = jQuery('.expanded-nav-toggler');
		this.searchToggler = jQuery('.search-toggler');
		this.expandedNav = jQuery(this.menuToggler.attr('data-target'));
		if (!this.$.length) alert('no menu ' + this.args.sel);

		this.menuToggler.click(function(){
			if (jQuery(this).hasClass('active')){
				MH.closeNav();
			}
			else {
				MH.openNav();
			}
		});
	},
	openNav: function(){
		this.expandedNav.addClass('active');
		this.menuToggler.addClass('active');
		jQuery('body').addClass('main-menu-expanded');
	},
	closeNav: function(){
		this.expandedNav.removeClass('active');
		this.menuToggler.removeClass('active');
		jQuery('body').removeClass('main-menu-expanded');


	},
	append:function(data, index){
		var cls = this.$.children('li:not(.active)').attr('class');
		this.$.append('<li class="'+cls+'">'+data+'</li>')

	},
	destroy: function() {
		this.$.find('li').remove();
	}
}
jQuery(document).ready(function(){
	if (jQuery('#main-nav').length){
		window.mainMenu = new menuHandler({sel:"#main-nav"});
	}
});




jQuery(document).ready(function(){
	jQuery('.screen-height').height($(window).height());
	jQuery(window).resize(function(){
		jQuery('.screen-height').height($(window).height()).css('overflow', 'hidden');
	});
	jQuery(window).scroll(function(){
		jQuery('.screen-height').height($(window).height()).css('overflow', 'hidden');
	});

	jQuery('body').on('click', 'a[href^="#"]',function(e){
		if ($(this).attr('role')) return;
		var body = $("html, body");
		if (jQuery(this).data('slide')) return;
		var target = jQuery(jQuery(this).attr('href'));
		if (!target.length){
			return;
		}
		e.preventDefault();
		var top = target.position().top;
		body.stop().animate({scrollTop:top}, 500, 'swing', function() { 

		});
	});
});

function updateStickyTop(){
	jQuery('.js-sticky-top').each(function(){
		let p = $(this).parent().css('overflow', 'hidden');
		let bbox = p[0].getBoundingClientRect();
		if (bbox.top > 0){
			$(this).css({position: 'initial', top:0, bottom:0});
		}
		else if (bbox.top<=0 && bbox.top+bbox.height> $(window).height()){
			$(this).css({position: 'fixed', top:'0px', bottom:'auto'});

		}
		else{
			$(this).css({position: 'absolute', top:'auto', bottom:0});

		}

	});
}

jQuery(window).on('toucstart scroll touchmove', updateStickyTop)

$(window).scroll(function(){
	let top = $(window).scrollTop();
	if (top<$(window).height()){
		$('.effect-cover-old .image').css('top', (-top/2)+'px');
	}
});
$(document).ready(function(){
	$('.effect-cover-old .image').css({
		position: 'fixed',
		top:0,
		
		left:0,
		transition: 'all 2ms linear'
	});
	$('.effect-cover-old').css({
		overflow:'hidden',
		position: 'absolute',
		clip: 'rect(0, auto, auto, 0)'
	}).parent().height($(window).height());
});