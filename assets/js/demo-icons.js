function renderIcons(s){
	let container = document.querySelector('.demo-icons');
	container.innerHTML = '';
	let icons = ["icon-play",
	"icon-pause",
	"icon-download",
	"icon-volume-on",
	"icon-volume-off",
	"icon-subs-off",
	"icon-subs-on",
	"icon-rewind",
	"icon-ff",
	"icon-full-screen",
	"icon-settings-off",
	"icon-settings-on",
	"icon-close",
	"icon-slide-gallery",
	"icon-post-menu",
	"icon-dots",
	"icon-link",
	"icon-share",
	"icon-search",
	"icon-pdf",
	"icon-link-video",
	"icon-menu",
	"icon-ok",
	"icon-play-video",
	"icon-photo",
	"icon-cc",
	"icon-facebook",
	"icon-twitter",
	"icon-insta",
	"icon-pinterest",
	"icon-snapchat",
	"icon-mail",
	"icon-youtube",
	"icon-soundcloud",
	"icon-rss",
	"icon-facebook-2",
	"icon-twitter-2",
	"icon-instagram-2",
	"icon-pinterest-2",
	"icon-snapchat-2",
	"icon-mail-2",
	"icon-youtube-2",
	"icon-soundcloud-2",
	"icon-rss-2",
	"icon-facebook-3",
	"icon-twitter-3",
	"icon-instagram-3",
	"icon-pinterest-3",
	"icon-snapchat-3",
	"icon-mail-3",
	"icon-youtube-3",
	"icon-soundcloud-3",
	"icon-rss-3",
	"icon-information",
	"icon-inkube",
	"icon-inkyfada-fr",
	"icon-inkyfada-ar",
	"icon-facebook-4",
	"icon-twitter-4",
	"icon-law-text",
	"icon-headphones",
	"icon-stouchi",
	"icon-dot",
	"icon-arabe",
	"icon-rss-full",
	"icon-download-full",
	"icon-menu-hamburger",
	"icon-chapitre",
	"icon-rewind-chapitre",
	"icon-ff-chapitre",
	"icon-francais",
	"icon-newsletter"].map(icon=>{
		let div = document.createElement('div');
		div.className='text-center pos-relative iconholder';
		div.style.height = '200px';
		div.style.width = '200px';
		let html = '<div class="pos-absolute fs-50px vcenter direct-0 w-100"><span class="'+icon+'"></span></div>';
		html +='<div class="pos-absolute bottom-0 fs-16px direct-0 w-100">'+icon+'</div>';
		div.innerHTML = html;
		document.querySelector('.demo-icons').appendChild(div)

	});
}

renderIcons();