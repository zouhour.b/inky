(function(){
	Inkube = typeof Inkube!=="undefined"? Inkube:{};
	jQuery(document).ready(function(){
		Inkube.Players = new InkubePlayers();
	});

	var InkubePlayers = function(args){
		this.config = {
			videoSelector:'.inkube-player:not(.loaded) video, [data-shortcode-tag="videoplayer"] video',
			audioSelector:'.inkube-player:not(.loaded) .podcast-player[data-source]',
		};
		this.audioPlayers = {};
		this.videoPlayers = {};
		this.initEventListeners();
	};
	InkubePlayers.prototype={
		initEventListeners: function(){
			var IP = this;
			this.refresh();
			jQuery(window).scroll(function(){
				IP.handleAutoplay();
			});
		},
		get: function(id){
			if (!id) return videojs.players;
			if (id && videojs.players[id]!=="undefined" ) return videojs.players[id];
			return null;
		},
		refresh: function(){
			var playerID;
			var IP = this;
			jQuery(this.config.videoSelector).each(function(){
				IP.loadVideo(jQuery(this));
			});
			jQuery(this.config.audioSelector).each(function(){
				IP.loadAudio(jQuery(this));
			});
			for (playerID in videojs.players){
				if (jQuery('#'+playerID).length==0){
					try {
						videojs.players[playerID].dispose();
					} catch(e){

					}
					delete videojs.players[playerID];
				}
			}
			for (playerID in this.audioPlayers){
				if (jQuery('#'+playerID).length==0){
					delete this.audioPlayers.players[playerID];
				}	
			}
		},
		handleAutoplay: function(){
			var IP = this;

			jQuery('.inkube-player.autoplay > [id^="video-player"]').each(function(){
				var id = jQuery(this).attr('id');

				if (!videojs.players[id]) return;
				if (videojs.players[id].ended()) return;
				if (jQuery(videojs.players[id].el()).hasClass('user-interacted') 
					&& videojs.players[id].paused()) return;

					if (IP.isOnScreen(jQuery(this)) ){
						videojs.players[id].play();	
					}
					else {
						videojs.players[id].pause();		
					}
				});
		},
		loadAudio: function($el){
			var IP = this;

			var waveConfig = {
				barWidth: 1,
				cursorWidth: 1,
				height: 55,
				responsive: true,
				hideScrollbar: true,
				normalize: true,
				backend: 'MediaElement',
				progressColor: 'white',
				cursorColor: 'rgba(0,0,0,0)',
				waveColor: 'rgba(255,255,255,.6)'
			}
			if ($el.find('.pjs-poster').length==0){
				waveConfig.waveColor='#999';
				waveConfig.progressColor='#333';

			}

			var file = jQuery($el).data('source');
			var playerID = $el.attr('id');
			jQuery($el).parent().addClass('loaded');	
			var config = {
				sel: '#'+playerID,
				wavesurfer: waveConfig,
				file: file,
			}
			var player = new InkubeAudioPlayer(config);
			player.player.on('play', function(){
				IP.pauseAllPlayers({except:playerID});
			});
			this.audioPlayers[playerID] = player;
		},
		loadVideo: function($el){
			var IP = this;
			var options = jQuery($el).data('playerSetup');
			options = options || {}
			options.controlBar= {
				volumePanel: {inline: false}

			};

			var id = jQuery($el).addClass('loaded').attr('id');

			if (!id){
				alert('no id for selector');
				return;
			}
			var mediaContainer = jQuery($el).parent();
			var mediaCaption = mediaContainer.find('figcaption');
			var player = videojs(id, options);


			player.ready(function(){
				IP.handleAutoplay();
				if (mediaContainer.hasClass('vjs-has-caption') && mediaCaption.hasClass('card-caption')){
					var mediaElement = mediaContainer.find('.video-js');
					mediaElement.addClass('vjs-masked-when-paused');
					mediaCaption.prepend('<div class="vjs-caption-head h2"><a class="icon-play vjs-caption-play"></a></div>');
					mediaCaption.find('.icon-play').click(function(){
						player.play();
					});

				}
			});
			if (mediaCaption.hasClass('card-caption')){

				player.one('loadedmetadata', function(){
					var durationStr = IP.getDurationStr(player);

					if (durationStr){
						mediaCaption.append('<div class="vjs-caption-footer">'+durationStr+'</div>')
					}
				});
			}
			player.on('ended', function(){
					//mediaCaption.find('.icon-play').removeClass('icon-play').addClass('icon-replay');
				});

			player.on('play', function(){
				IP.pauseAllPlayers({except:id});
			});
			jQuery($el).click(function(){
				player.addClass('user-interacted');
			});
			this.videoPlayers[id] = player;
			return player;

		},

		pauseAllPlayers(args){
			for (var playerID in videojs.players){
				if (args.except !== playerID){
					videojs.players[playerID].pause();
				}
			}
			for (var playerID in this.audioPlayers){
				if (args.except !== playerID){
					this.audioPlayers[playerID].pause();

				}
			}

		},

		getDurationStr(player){
			if (!player) return; 
			var dur = Math.floor(player.duration());
			p = player;
			var m = Math.floor(dur / 60);
			var s = (dur - m*60);
			var str = '';
			if (m) str +='<span class="vjs-duration-m">'+m+'</span>';
			if (s) str +='<span class="vjs-duration-s">'+s+'</span>';
			return str;

		},

		isOnScreen: function(element, deb) {
			var elementOffsetTop = element.offset().top;
			var elementHeight = element.parent().height();
			var elementWidth = element.width();


			var screenScrollTop = jQuery(window).scrollTop();
			var screenHeight = jQuery(window).height();
			var screenWidth = jQuery(window).width();
			var rect={};
			rect.y1 = Math.max(screenScrollTop, elementOffsetTop);
			rect.y2 = Math.min(elementOffsetTop+elementHeight, screenScrollTop+screenHeight);
			if (deb) 
				console.log(screenScrollTop, elementOffsetTop, elementHeight);
			if (elementOffsetTop>screenHeight + screenScrollTop) {
				if (deb) alert('eltop not yet visible');
				return false;
			}
			if (rect.y2 < rect.y1) {
				if (deb) alert('humm');
				return false;
			}
			var visiblePourcentage =  (rect.y2-rect.y1)/(elementHeight);
			if (deb) alert(visiblePourcentage);
			return (visiblePourcentage > .25);
		}






	}
})(jQuery)