$.fn.pageSlides = function(args){
	let height = $(window).height()/2;
	const container = $(this);
	const wrapper = $(this).children('.slides-inner');
	let currentSlide = null;
	let slides = [];
	let wh = $(window).height();


	let getCurrentSlide = function(){
		const top = $(window).scrollTop();
		const n = Math.floor(top/wh);
		return n; 	
	}

	let updateWrapper = function(){
		let bbox  = container[0].getBoundingClientRect();
		if (bbox.top >0){
			wrapper.css({position:'initial', top:'auto', bottom:'auto', left:'auto', right: 'auto'});
		}
		else if (bbox.top+bbox.height< wh){
			wrapper.css({position:'absolute', top:'auto', bottom:0, left:0, right: 0});

		}
		else {
			wrapper.css({position:'fixed',  top:0, bottom:'auto', left:0, right: 0})	
		}
	}

	let updateSlide  = function(e){
		updateWrapper();
		let cs = getCurrentSlide();
		if (cs!==currentSlide && args.onSlideChange){
			args.onSlideChange(currentSlide, cs);
			e && e.preventDefault()
			currentSlide  = cs;
			slides.map((x,i)=>{
				if (i<currentSlide) {
					x.removeClass('active').removeClass('future').addClass('past')
				}
				else if (i===currentSlide){
					x.removeClass('past').removeClass('future').addClass('active')
				}
				else if (i>currentSlide){
					x.removeClass('past').removeClass('active').addClass('future')
				}
			});
		}
		



	}

	let resize = function(){
		wh = $(window).height();
		height = $(window).height()/2;
		wrapper.children('.slide').each(function(){
			slides.push($(this));
			height += $(this).height();
		});
		container.addClass('pos-relative').height(height);
		updateSlide();
	}


	let init = function(){
		$(window).scroll(updateSlide);
		$(window).resize(resize);
		$(window).resize(updateSlide);
		container.on('click', '[data-slide-to]', function(e){
			e.preventDefault();
			const target = $(this).data('slideTo');


			$(window).scrollTop((target)*wh+40);
		});
		resize();




	}
	init();

}