<?php
require_once 'controllers/PodcastFormat.php';
require_once 'controllers/ArchiveView.php';
require_once 'shortcodes/post-navigation.php';
require_once 'shortcodes/map/Map.php';
require_once 'components/media-slideshow.php';
require_once 'components/LineGraph.php';
require_once 'components/GoogleGraph.php';
require_once 'components/map-tunisia.php';
require_once 'components/TerroristAttack.php';
require_once 'components/SVGScrollNavigation.php';
require_once 'components/PictogramComponent.php';
//require_once 'shortcodes/newsletter/newsletter.php';
require_once 'shortcodes/languageSwitcher.php';
require_once 'shortcodes/Menu.php';


require_once 'Model.php';
require_once 'setup.php';
require_once 'migration.php';
require_once 'tests.php';

require_once dirname(__DIR__).'/migration/caption.php';
require_once dirname(__DIR__).'/migration/audio.php';
require_once dirname(__DIR__).'/migration/video.php';
require_once dirname(__DIR__).'/migration/gallery.php';
require_once dirname(__DIR__).'/migration/svg.php';
require_once dirname(__DIR__).'/migration/migrate.php';

require_once dirname(__DIR__).'/migration/containers.php';
require_once dirname(__DIR__).'/migration/logs.php';
require_once dirname(__DIR__).'/migrate/init.php';


$styleConverter = new \Inkube\Core\RtlStyleConverter('main', 'main.rtl');


add_action('body_start', function(){
	if (is_author()){
		\Inkube\Core\CronJob::syncUsersArchive();
	}




});

add_filter('get_user_photo', function($v){
	if (!$v) return "30610";
	return $v;
}, 1,1);
add_filter('inkube.wordify', function($v){
	if (!$v) return $v;
	$v = trim($v);
	$x = explode(' ', $v);
	$v =  '<span>'.implode('</span> <span>', $x).'</span>';

	return $v;
});
add_filter('get_post_metaInfo', function($v, $id, $data){
	$p = $data->post;
	if (has_term('photoreportage', 'formats', $p->ID) || has_term(2079, 'formats', $p->ID)){
		return $data->get('media-infos');
	}

	if (has_term('audio', 'formats', $p->ID) || has_term(2079, 'formats', $p->ID)){
		return apply_filters('inkube.unit', $data->get('media-infos'), ['minute']);
	}
	if (has_term('stouchi', 'genre', $p->ID) || has_term(1653, 'genre', $p->ID)){
		return '';
	}
	if ($p->post_type === 'webdoc') return '';
	return apply_filters('inkube.unit', $data->get('reading-duration'), ['minute']);
}, 3, 3);
add_filter('get_term_url', function($v, $id, $data){
	if ($data->term->slug=="webdoc"){
		return '/fr/webdocs';
	}
	return $v;
}, 3, 3);
