<?php

class InkyfadaMigration{
	

	public function getSyncInfo($number=30, $page=0){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$url = 'https://inkyfada.com/wp-json/wp/v2/posts?per_page='.$number.'&offset='.($number*$page);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		curl_close($ch);
		function make_link($url, $text){
			return sprintf('<a href="%s" target="_blank">%s</a>', $url, $text);
		}



		$posts = json_decode($result);
		if (!$posts) {
			var_dump($result);
			die();

		}
		$infos=[];
		foreach ($posts as $index=>$p){
			$info = [
				'index'=>$index+1+($number*$page),
				'id'=>[
					'old'=>make_link($p->guid->rendered, $p->id),
				],
				'date'=>$p->date,
				'title'=>['old'=>make_link($p->guid->rendered, $p->title->rendered)],
				'status'=>'not-copied',
			];
			
			$localpost = get_page_by_title( $p->title->rendered, OBJECT, 'post' );
			if (!$localpost){
				$_p = get_posts(['lang'=>'','meta_query'=>[['key'=>'oldid','value'=>$p->id]]]);
				if (count($_p)==1){
					$localpost = $_p[0];
				}
			}

			if ($localpost){
				$info['id']['new']=make_link(get_post_permalink($localpost->ID), $localpost->ID);
				$info['title']['new'] = make_link(get_post_permalink($localpost->ID),$localpost->post_title);
				$status = get_post_meta($localpost->ID, 'migration-status',true);
				if (!$status) {
					update_post_meta($localpost->ID, 'migration-status', 'not-ready');
					$status = get_post_meta($localpost->ID, 'migration-status',true);
					
				}
				$info['status'] = $status;
			}



			
			$infos[] = $info;
		}
		return $infos;
	}
	
	public function __construct(){
		add_filter('inkube_client_data', function($data){
			if (!is_single()) {
				return $data;
			}
			$p = get_post();
			$oldid = get_post_meta($p->ID, 'oldid', true);
			if ($oldid){
				$data['oldLink'] = 'https://inkyfada.com/?p='.$oldid;
			}
			return $data;
		});
		add_action('init', function(){
			global $Inkube;
			$api = $Inkube->getContainer()->get('Inkube\Client\RestApi');
			$api->addRoute(
				'GET', 
				'/migration/(?P<id>[\d]+)', 
				[$this, 'getMigrationStatus'],
				['id'=>[
					'validate_callback' => function($id, $request, $key) {
						return is_numeric($id) && get_post($id);
					}
				]] 
			);
			$api->addRoute(
				'POST', 
				'/migration/(?P<id>[\d]+)', 
				[$this, 'setMigrationStatus'],
				['id'=>[
					'validate_callback' => function($id, $request, $key) {
						return is_numeric($id) && get_post($id);
					}
				]] 
			);

		});
	}

	public function getMigrationStatus($data){
		$id = $data['id'];
		$errors = get_post_meta($id, 'migration-errors', true);
		$status = get_post_meta($id, 'migration-status', true);
		return [
			'migration'=>[
				[
					'type'=>'select',
					'name'=>'status',
					'value'=>$status,
					'options'=>[
						['label'=>'Validé Pour mobile et Desktop', 'value'=>'valid-mobile-desktop'],
						['label'=>'Validé Pour Desktop', 'value'=>'valid-desktop'],
						['label'=>'A valider', 'value'=>'needs-validation'],
						['label'=>'Contient des erreurs', 'value'=>'not-ready']

					]
				],
				[
					'name'=>'errors',
					'value'=>$errors,
					'label'=>'Errors',
					'type'=>'collection',
					'model'=>[
						[
							'type'=> 'select', 
							'name'=>'type', 
							'label'=>'Sévérité', 
							'options'=>[
								['label'=>'critique', 'value'=>'error'],
								['label'=>'normale', 'value'=>'warn'],
							]
						],
						[
							'type'=>'textarea',
							'name'=>'description',
							'label'=>'Description'
						]
					]
				]
			]
		];
	}

	public function setMigrationStatus($data){
		$id = $data['id'];
		$status = $data->get_params();
		$status=wp_parse_args($status, ['status'=>'', 'errors'=>false]);
		update_post_meta($id, 'migration-errors', $status['errors']);
		update_post_meta($id, 'migration-status', $status['status']);
		return $status;
	}
}

$migration = new InkyfadaMigration();
$callSyncInfo = $_GET['syncinfo']??null;
if ($callSyncInfo!==null){
	add_action('body_start', function() use ($migration){
		$page = $_GET['pagination']??0;
		$infos = $migration->getSyncInfo(50, $page);
		require_once __DIR__.'/Table.php';
		$table = new TableRenderer();
		echo $table->render($infos, function($info){
			switch ($info['status']){
				case 'not-copied': return 'bg-gray-900 color-white';
				case 'not-ready': return 'bg-danger color-white';
				case 'needs-validation': return 'bg-warning color-gray-900';
				case 'valid-desktop': return 'bg-info color-white';
				case 'valid-mobile-desktop': return 'bg-success color-white';
			}
			return '';
		});
		echo '<div class="mx-3">'.$table->renderPagination(home_url().'?syncinfo', 5).'</div>';
		get_footer();
		die();
	});
}