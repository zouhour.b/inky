<?php
namespace Inkube\Components;
use Inkube\Model\Field; 

class PictogramComponent extends Component{
	static $defaultView = '@theme/component/pictogram/default';
	
	public static function getForm($data) {
        $form = parent::getForm($data);
        

		$form->add(new Field('view_options', ['type'=>'viewOptions', 'filter'=>'pictogram']), 'view');
        
        $form->add(new Field('default_value'), 'data');
        $form->add(new Field('unit'), 'data');
        $form->add(new Field('pattern_scale'), 'data');

        

        $form->add(new Field('data', [
        
            'type'=> 'code', 'mode'=> 'json' ]
        
        ), 'data');

        $form->add(new Field('defs', [
        
            'type'=> 'code', 'mode'=> 'html' ]
        
        ), 'pattern');
        $form->add(new Field('caption', [
        
            'type'=> 'textarea', 'disableRichText'=> true ]
        
        ), 'caption');

		
		return $form;
	}

	
}
add_action('inkube_loaded', function(){

    ComponentRepository::register('pictogram', PictogramComponent::class);
    
});
