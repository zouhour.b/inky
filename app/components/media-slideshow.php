<?php
namespace Inkube\Components;
use Inkube\Model\Field; 



class MediaSlideshow extends Component{
	public static function getForm($data) {
		$form = parent::getForm($data);
		$form->add(new Field('view_options', ['type'=>'viewOptions', 'filter'=>'slideshow']), 'view');
		$form->add(new Field('backgroundColor'), 'view');
		$form->add(new Field('data', ['type'=>'collection','display'=>'blocks', 'model'=>[
			['name'=>'id', 'label'=>'id'],
			['name'=>'type', 'label'=>'type' ],
			['name'=>'src', 'label'=>'source'],
			['name'=>'src_mobile', 'label'=>'source mobile'],
			['name'=>'caption', 'label'=>'Caption', 'type'=>'textarea', 'disableRichText'=>true]



		]]), 'data');
		return $form;
	}

	public function get($key){
		if ($key=='data' ){
			$d = get_post_meta($this->id, 'data', true);
			if (!$d) return $d;
			if (!is_array($d)) $d = json_decode($d, true);
			return $d;
		}
		return parent::get($key);
	}
}


add_action('inkube_loaded', function(){
	ComponentRepository::register('media-slideshow', MediaSlideshow::class);
});


