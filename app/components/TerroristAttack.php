<?php
namespace Inkube\Components;
use Inkube\Model\Field; 



class TerroristAttack extends Component{
	static $defaultView = '@theme/component/terrorist-attack';
	public static function getForm($data) {
		$form = parent::getForm($data);
		$form->add(new Field('event_start_date', ['type'=>'text', 'label'=>"Date de l''attaque"]), 'event');

		$form->add(new Field('location'), 'event');
		$form->add(new Field('map_center', ['type'=>'map']), 'event');
		$form->add(new Field('markers', [
			'type'=>'collection',
			'model'=>self::getMarkersModel(),
			'display'=>'table',
			'label'=>'Markers',
			'addLabel'=>' Add Marker'
		]), 'event');
		$form->add(
			new Field('data', [
				'className'=>'w-100',
				'type'=>'collection',
				'className'=>'markers',
				'disableDelete'=>true,
				'disableAdd'=>true,
				'model'=>self::getDataModel(),
				'value'=>self::getData(0)
			]
		), 'data');
		return $form;
		
	}

	public static function getMarkersModel(){
		return [
			['name'=>'lat', 'label'=>'Lattitude'],
			['name'=>'lng', 'label'=>'Longitude'],
		];
	}



	public static function getDataModel(){
		return [
			['name'=>'label', 'type'=>'label','label'=>'Categorie'],
			['name'=>'dead', 'label'=>'Morts'],
			['name'=>'wounded', 'label'=>'Blessés'],
			['name'=>'arrested', 'label'=>'Arrestations']
		];
	}


	public function get($attr){
		if ($attr ==='data'){
			return $this->getData($this->id);
		}
		if ($attr === 'event_start_date'){
			$date = get_post_meta($this->id, 'event_start_date', true);
			if (!$date){
				return $this->get('date');
			}
			return $date;
		}
		return parent::get($attr);
	}

	public static function getKeys(){
		return [
			["key"=> "civil", "label"=>"Civils"	], 
			["key"=> "gardeNationale", "label"=>"Garde Nationale"	],
			["key"=> "police", "label"=>"Police"],
			["key"=> "army", "label"=>"Armée"]
		];

	}

	public static function getRowValue($data, $key){
		foreach ($data as $row){
			$k = $row['key']??0;
			if ($k==$key) return $row['value']??0;
		}
		return 0;
	}


	public static function getData($id){
		$data = [];
		$_data = get_post_meta($id, 'data', true);
		if (!is_array($_data)){
			$_data = json_decode($_data, true);
			if (!$_data) $_data=[];
		}

		foreach (self::getKeys() as $index=>$row){
			$row = wp_parse_args($_data[$index]??[], [
				'label'=>$row['label'],
				'dead'=>0,
				'wounded'=>0,
				'arrested'=>0
			]);
			$data[] = $row;

		}
		return $data;
	}
}
add_action('inkube_loaded', function(){
	ComponentRepository::register('terrorist-attack', TerroristAttack::class);
});