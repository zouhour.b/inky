<?php
namespace Inkube\Components;
use Inkube\Model\Field; 



class MapTunisia extends Component{
	static $defaultView = '@theme/component/map-tunisia';
	
	public static function getForm($data) {
		$form = parent::getForm($data);
		$form->add(new Field('view_options', ['type'=>'viewOptions', 'filter'=>'map-tunisia']), 'view');
		$form->add(new Field('unit'), 'data');

		$form->add(
			new Field('data', [
				'className'=>'w-100',
				'type'=>'collection',
				'disableDelete'=>true,
				'disableAdd'=>true,
				'model'=>[
					['name'=>'label', 'type'=>'label','label'=>''],
					['name'=>'value', 'label'=>'valeur']
				],
			]
		), 'data');
		$form->add(new Field('data-json', ['type'=>'code']), 'data');

		$form->add(new Field('colors', [
			'className'=>'w-100',
			'type'=>'collection',
			'model'=>[
				['name'=>'min', 'label'=>'min'],
				['name'=>'max', 'label'=>'max'],
				['name'=>'color', 'label'=>'color'],

			],
			'addLabel'=>'Add color condition'
		]), 'colors');
		$form->add(new Field('minColor',['label'=>'Color min']), 'colors');
		$form->add(new Field('maxColor',['label'=>'Color max']), 'colors');
		$form->add(new Field('caption', ['type'=>'textarea', 'disableRichText'=>true]), 'caption');
		
		return $form;
	}

	
	public function set($key, $value){
		if ($key=='data-json' && $value){
			try{
				$r  = json_decode($value, true);
				if ($r){
					$this->set('data', $r);
				}
			}
			catch (\Exception $e){
				return;
			}
			return;
		}
		parent::set($key, $value);

	}

	public function get($attr){
		if ($attr ==='data'){
			return $this->getData($this->id);
		}
		if ($attr==='colors'){
			$colors = get_post_meta($this->id, 'colors', true);
			return $colors?$colors:[];
		}
		return parent::get($attr);
	}

	public function getKeyLabel($k){
		$r = '';
		foreach ($this->getKeys() as $k){
			if ($k['key']==$k) return $k['label'];
		}
	}

	public function getKeys(){
		return [	
			["key"=> "TN.AR", "label"=>"Ariana"	], 
			["key"=> "TN.BJ", "label"=>"Béja"	], 
			["key"=> "TN.BA", "label"=>"Ben Arous"	], 
			["key"=> "TN.BZ", "label"=>"Bizerte"	], 
			["key"=> "TN.GF", "label"=>"Gafsa"	], 
			["key"=> "TN.GB", "label"=>"Gabès"	], 
			["key"=> "TN.JE", "label"=>"Jendouba"	], 
			["key"=> "TN.KR", "label"=>"Kairouan"	], 
			["key"=> "TN.KS", "label"=>"Kasserine"	], 
			["key"=> "TN.KB", "label"=>"Kébili"	], 
			["key"=> "TN.KF", "label"=>"Le Kef"	], 
			["key"=> "TN.MH", "label"=>"Mahdia"	], 
			["key"=> "TN.MN", "label"=>"Mannouba"	], 
			["key"=> "TN.ME", "label"=>"Medenine"	], 
			["key"=> "TN.MS", "label"=>"Monastir"	], 
			["key"=> "TN.NB", "label"=>"Nabeul"	], 
			["key"=> "TN.SF", "label"=>"Sfax"	], 
			["key"=> "TN.SZ", "label"=>"Sidi Bouzid"], 
			["key"=> "TN.SL", "label"=>"Siliana"	], 
			["key"=> "TN.SS", "label"=>"Sousse"	], 
			["key"=> "TN.TA", "label"=>"Tataouine"	],
			["key"=> "TN.TO", "label"=>"Tozeur"	], 
			["key"=> "TN.TU", "label"=>"Tunis"], 
			["key"=> "TN.ZA", "label"=>"Zaghouan"	] 
		];

	}

	public function getRowValue($data, $key){
		foreach ($data as $row){
			$k = $row['key']??'';
			if ($k==$key) return $row['value']??'';
		}
		return null;
	}


	public function getData($id){
		$data = [];

		$_data = get_post_meta($id, 'data', true);
		if (!is_array($_data)){
			$_data = json_decode($_data, true);
			if (!$_data) $_data=[];
		}

		foreach ($this->getKeys() as $row){
			$row['value'] = $this->getRowValue($_data, $row['key']);
			$data[] = $row;

		}
		return $data;
	}
}
add_action('inkube_loaded', function(){

	ComponentRepository::register('map-tunisia', MapTunisia::class);
});
