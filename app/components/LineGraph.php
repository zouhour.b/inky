<?php
namespace Inkube\Components;
use Inkube\Model\Field; 



class LineGraph extends Component{
	public $icon = 'fa fa-chart-line';
	static $defaultView = '@theme/component/graph/line';
	public static function getForm($data) {
		$form = parent::getForm($data);
		$form->add(new Field('unit'), 'data');
		$form->add(new Field('data', ['rows'=>20,'type'=>'textarea', 'label'=>"CSV", 'disableRichText'=>true]), 'data');
		$form->add(new Field('lines', ['type'=>'collection', 'label'=>"graphes", 'model'=>[
			['name'=>'color','label'=>'Color'],
			['name'=>'thickness','type'=>'number', 'label'=>'thickness'],
			['name'=>'dotColor', 'label'=>'dotColor']
		]]), 'graphs');
		$form->add(new Field('yaxismin'), 'Axis');
		$form->add(new Field('yaxismax'), 'Axis');
		$form->add(new Field('chartCaption', ['type'=>'textarea', 'disableRichText'=>true]), 'legends');
		$form->add(new Field('view_options', ['type'=>'viewOptions','filter'=>'component/graph']),'view');
		return $form;
		
	}

	public static function convertStringToData($str){
		$v = explode("\n", $str);
		$arr = [];
		$labels = [];
		for ($row = 0; $row< count($v);$row++){
			$dataRow = [];
			$r = explode(',', $v[$row] );
			for ($col=0; $col < count($r); $col++){
				$dataRow[] = $r[$col];
			}
			$arr[] = $dataRow;
		}
		return $arr;
	}

	public function get($key){
		if ($key==='graphData'){
			$v = get_post_meta($this->id, 'data', true);
			return self::convertStringToData($v);
		}
		if ($key==='graphColors'){
			$v = get_post_meta($this->id, 'colors', true);
			$v =  explode("\n", $v);
			return $v;
		}
		return parent::get($key);

	}

	
}
add_action('inkube_loaded', function(){
	ComponentRepository::register('graph-line', LineGraph::class);
});