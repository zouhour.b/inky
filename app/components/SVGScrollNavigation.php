<?php
namespace Inkube\Components;
use Inkube\Model\Field; 



class SVGScrollNavigator extends Component{
	static $defaultView = '@theme/component/svg-scroll-nav/svg-scroll-nav';
	public static function getForm($data) {
		$animationsModel = [
			['name'=>'sel', 'label'=>'Selector'],
			['name'=>'fn', 'label'=>'fn'],
			['name'=>'start', 'label'=>'from'],
			['name'=>'end', 'label'=>'to'],
			['name'=>'param', 'label'=>'param']
		];
		$stepsModel = [
			['name'=>'x', 'label'=>'x'],
			['name'=>'y', 'label'=>'y'],
			['name'=>'z', 'label'=>'zoom'],
			['name'=>'p', 'label'=>'% scroll']
		];
		$form = parent::getForm($data);
		$form->add(new Field('svg-md', ['type'=>'image', 'library'=>['type'=>['image/svg', 'image/svg+xml']]] ), 'desktop');
		$form->add(new Field('svg-sm', ['type'=>'image', 'library'=>['type'=>['image/svg', 'image/svg+xml'] ] ] ), 'mobile');

		$form->add(new Field('height-md'), 'desktop');
		$form->add(new Field('height-sm'), 'mobile');
		$form->add(new Field('steps-md', ['label'=>'Steps','type'=>'collection', 'model'=>$stepsModel, 'addLabel'=>'Add Step']), 'desktop');
		$form->add(new Field('steps-sm', ['label'=>'Steps','type'=>'collection', 'model'=>$stepsModel, 'addLabel'=>'Add Step']), 'mobile');
		$form->add(new Field('animations-md', ['label'=>'Animations', 'type'=>'collection', 'model'=>$animationsModel, 'addLabel'=>'Add Animation']), 'desktop');
		$form->add(new Field('animations-sm', ['label'=>'Animations', 'type'=>'collection', 'model'=>$animationsModel, 'addLabel'=>'Add Animation']), 'mobile');

		$form->add(new Field('animations-sm-json-file', ['label'=>'SM', 'type'=>'text', 'library'=>['type'=>'application/json']]), 'json');
		$form->add(new Field('animations-md-json-file', ['label'=>'MD', 'type'=>'text', 'library'=>['type'=>'application/json']]), 'json');




		return $form;
		
	}

	public function get($attr){
		if (in_array($attr, ['animations-md', 'animations-sm', 'steps-md', 'steps-sm'])){
			$r = get_post_meta($this->id, $attr.'-json-file', true);
			if ($r && file_exists(ABSPATH.$r)){
				$r = json_decode(file_get_contents(ABSPATH.$r));
			}
			else{
				$r = get_post_meta($this->id, $attr, true);
			}
			

			if (!$r) return [];
			return $r;
		}
		return parent::get($attr);
	}

}
add_action('inkube_loaded', function(){
	ComponentRepository::register('svg-scroll-navigator', SVGScrollNavigator::class);
});