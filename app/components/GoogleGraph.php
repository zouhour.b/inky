<?php
namespace Inkube\Components;
use Inkube\Model\Field; 



class GoogleGraph extends Component{
	public $icon = 'fa fa-chart-line';
	static $defaultView = '@theme/component/graph/bars';
	public static function getForm($data) {
		$form = parent::getForm($data);
		$form->add(new Field('unit'), 'data');
		$form->add(new Field('data_source', ['label'=>'google spreadsheet link ']), 'graphs');
		$form->add(new Field('series', ['type'=>'collection', 'label'=>"Series", 'model'=>[
			['name'=>'color','label'=>'Color'],
			['name'=>'thickness','type'=>'number', 'label'=>'thickness'],
			['name'=>'unit', 'label'=>'unit']
		]]), 'graphs');
		$form->add(new Field('yaxismin'), 'Axis');
		$form->add(new Field('yaxismax'), 'Axis');
		$form->add(new Field('chartCaption', ['type'=>'textarea', 'disableRichText'=>true]), 'legends');
		$form->add(new Field('view_options', ['type'=>'viewOptions','filter'=>'component/graph']),'view');
		return $form;
		
	}

	public static function convertStringToData($str){
		$v = explode("\n", $str);
		$arr = [];
		$labels = [];
		for ($row = 0; $row< count($v);$row++){
			$dataRow = [];
			$r = explode(',', $v[$row] );
			for ($col=0; $col < count($r); $col++){
				$dataRow[] = $r[$col];
			}
			$arr[] = $dataRow;
		}
		return $arr;
	}

	public function get($key){
		if ($key==='series'){
			$v = parent::get($key);
			if (!$v) return [];
			return $v;
		}

		if ($key==='graphColors'){
			$v = get_post_meta($this->id, 'colors', true);
			$v =  explode("\n", $v);
			return $v;
		}
		return parent::get($key);

	}

	
}
add_action('inkube_loaded', function(){
	ComponentRepository::register('google-graph', GoogleGraph::class);
});