<?php 
use Inkube\Core\Interfaces\Shortcode;
use Inkube\Menu\Walker\BootstrapWalker;


class Menu extends Shortcode{
	public $isPrivate = true;
	public $tag = 'Menu'; 
	public function getHTML(array $atts, string $content):string 
	{
		$defaults = array( 
			'theme_location' => 'primary', 
			'menu_class' => 'nav navbar-nav', 
			'walker' => new BootstrapWalker(),
			'fallback_cb'=>null
		);
		unset($atts['container_class']);
		$args = wp_parse_args($atts, $defaults);
		ob_start();
		wp_nav_menu($args);
		return ob_get_clean();


	}
	public function getFields():array{
		return [];
	}
	
	public function getPresets():array{
		return [];
	}
}

add_action('init', function(){
	global $Inkube;
	$Inkube->getContainer()->get('Inkube\Shortcodes\ShortcodesModule')->register('Menu', new Menu());
});
