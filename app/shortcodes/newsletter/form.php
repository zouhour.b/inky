<?php 

global $epm_options, $current_user;
wp_get_current_user();
$epm_default_email_value = null;
if(is_user_logged_in()) {
	$epm_default_email_value = $current_user->user_email;
}
?>
<div class="newsletter <?php echo (isset($class))?$class:''; ?>">
  <div class="newsletter-banner h3">
    <a data-toggle-active="#newsletter-form" class="newsletter-toggler" href="javascript:void(0);">
    <span class="uppercase btn-text"><?= do_shortcode( '[tr]newsletter_btn_text_1[/tr]'); ?></span> 
    <span class="icon icon-newsletter"></span> 
    <span class="uppercase btn-text"><?= do_shortcode( '[tr]newsletter_btn_text_2[/tr]');?></span> 
    </a>
  </div>
  <div class="newsletter-form" id="newsletter-form">
    <a href="javascript:void(0);" data-toggle-active="#newsletter-form" class="icon icon-close"></a>
    <div class="newsletter-container">
      <h1 class="uppercase"><?php _e('Recevez le meilleur de inkyfada','easy-peasy-mailchimp');?></h1>
      <h1 class="uppercase"><?php _e('Abonnez vous à notre newsletter','easy-peasy-mailchimp');?></h1>
      <form class="epm-sign-up-form" name="epm-sign-up-form" action="#" method="post">

        <?php if(epm_get_option('display_name_fields')) : ?>

        <div class="epm-form-field">
          <label for="epm-first-name"><?php _e('First Name','easy-peasy-mailchimp');?></label>
          <input type="text" placeholder="<?php _e('First Name','easy-peasy-mailchimp');?>" name="epm-first-name" tabindex="7" class="name first-name" id="epm-first-name"/>
        </div>

        <div class="epm-form-field">
          <label for="epm-last-name"><?php _e('Last Name','easy-peasy-mailchimp');?></label>
          <input type="text" placeholder="<?php _e('Last Name','easy-peasy-mailchimp');?>" name="epm-last-name" tabindex="7" class="name last-name" id="epm-last-name"/>
        </div>

        <?php endif; ?>

        <div class="epm-form-field input-group col-md-10 offset-md-1">
            <input type="email" placeholder="<?php _e('Email Address','easy-peasy-mailchimp');?>" name="epm-email" tabindex="8" class="email" id="epm-email" value="<?php echo $epm_default_email_value; ?>" onfocus="this.placeholder=''"/>
          <button type="submit" class="inky-submit-chimp" value=""><span class="icon icon-newsletter"></span></button>
        </div>

        <input type="hidden" name="epm_submit" id="epm_submit" value="true" />
        <input type="hidden" name="epm_list_id" id="epm_list_id" value="<?php echo $list;?>" />

        <!--<input type="submit" name="epm-submit-chimp" value="<?php _e('Sign Up Now','easy-peasy-mailchimp');?>" data-wait-text="<?php _e('Please wait...','easy-peasy-mailchimp');?>" tabindex="10" class="button btn epm-sign-up-button epm-submit-chimp"/> -->

      </form>
      <!--<div class="">
        <input type="email" id="email" name="EMAIL" placeholder="Entrez votre adresse email" required />
        <button type="submit" value=""><span class="icon icon-newsletter"></span></button>
      </div>  -->
      </div>
  </div>
</div>
<script>
	jQuery('.inky-submit-chimp').click(function() {

		//get form values
		var epm_form = jQuery(this);
		var epm_list_id = jQuery(epm_form).parent().find('#epm_list_id').val();
		var epm_firstname = jQuery(epm_form).parent().find('#epm-first-name').val();
		var epm_lastname = jQuery(epm_form).parent().find('#epm-last-name').val();
		var epm_email = jQuery(epm_form).parent().find('#epm-email').val();

		//change submit button text
		var submit_wait_text = jQuery(this).data('wait-text');
		var submit_orig_text = jQuery(this).val();
		jQuery(this).val(submit_wait_text);

		jQuery.ajax({
			type: 'POST',
			context: this,
			url: "<?php echo admin_url('admin-ajax.php');?>",
			data: {
				action: 'epm_mailchimp_submit_to_list',
				epm_list_id: epm_list_id,
				epm_firstname: epm_firstname,
				epm_lastname: epm_lastname,
				epm_email: epm_email
			},
			success: function(data, textStatus, XMLHttpRequest){
				var epm_ajax_response = jQuery(data);
				jQuery(epm_form).parent().parent().find('.epm-message').remove(); // remove existing messages on re-submission
				jQuery(epm_form).parent().parent().append(epm_ajax_response);
				jQuery(epm_form).val(submit_orig_text); // restore submit button text
        //jQuery(epm_form).parent().find('#epm-email').hide();
				<?php do_action('epm_jquery_ajax_success_event');?>
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert('Something Went Wrong!');
			}
		});
		return false;

	});
</script>