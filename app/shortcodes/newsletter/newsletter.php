<?php

use Inkube\Core\Interfaces\Shortcode;


class Newsletter extends Shortcode{
	public $tag = 'newsletter'; 
	public function getHTML(array $atts, string $content):string 
	{
		extract(shortcode_atts(array(
			'list' => '',
			'class' => '',
		), $atts));
		ob_start();
		get_epm_template( 'app/shortcodes/newsletter/form.php', array(
			'list' => $list,
			'class' => $class,
		));
		return ob_get_clean();
	}
	public function getFields():array{
		return [];
	}
	public function getPresets():array{
		return [];
	}
}



add_action('init', function(){
	global $Inkube;
	$Inkube->getContainer()->get('Inkube\Shortcodes\ShortcodesModule')->register('newsletter', new Newsletter());

});
