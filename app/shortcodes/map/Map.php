<?php 
use Inkube\Core\Interfaces\Shortcode;

class Map extends Shortcode{
  public $tag = 'map';
  public $label = 'Map';
  public $group = 'Media';
  public function getContainerClass($atts){
    return 'w-100';
  }

  public function getFields():array{
    return ['base'=>[
      [
        'name'=>'coords',
        'type'=>'map'
      ]
    ]];
  }

  public function getApiKey(){
    return 'AIzaSyAk9SQLSFSdQfLzZXjF36wVEGhy1r5Msok';
  }

  public function getPresets():array{
    return [];
  }
  public function getHTML(array $atts, string $content):string{
    $atts = shortcode_atts(
      array(
        'provider' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        'ext' => 'png',
        'center'=>'36,10,7',
        'container_class'=>'pos-fixed top-0 direct-0'
      ),
      $atts,
      ''
    );
    $base = get_template_directory_uri().'/app/shortcodes/map/assets';
    $scripts = [
      ['name'=>'leaflet-core', 'url'=>$base.'/js/leaflet.min.js'],
      ['name'=>'leaflet-pouchedb', 'url'=>$base.'/js/pouchdb.js'],
      ['name'=>'leaflet-cachedb', 'url'=>$base.'/js/leaflet.cachedb.js'],
      ['name'=>'leaflet-markercluster', 'url'=>$base.'/js/leaflet.markercluster.js'],
      ['name'=>'map', 'url'=>$base.'/js/map.js']
    ];

    $styles = [
      ['name'=>'leaflet', 'url'=>$base.'/css/leaflet.css'],
      ['name'=>'leaflet-markers', 'url'=>$base.'/css/MarkerCluster.css'],
      ['name'=>'leaflet-markers-default', 'url'=>$base.'/css/MarkerCluster.Default.css'],
      ['name'=>'map', 'url'=>$base.'/css/map.css']
    ];
    
    
    if (strpos($atts['provider'], 'maps.google') !== false) {
      $scripts[] = ['name'=>'leaflet-google', 'url'=>'//maps.googleapis.com/maps/api/js?key=AIzaSyBgn5KwbRseKzJnYAQz5iRibnwWfHS2-uo&language=fr&v=3'];
      $scripts[] = ['name'=>'googlemaps-leaflet', 'url'=>$base.'/js/leaflet.google.js'];

    }
    
    if(!$content){
      $content = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
    }
    $atts['provider'] = str_replace('{ext}', $atts['ext'], $atts['provider']);
    $html = sprintf('<div id="map" class="%s" data-map-center="%s" data-map-provider="%s" data-attribution=\'%s\'></div>', $atts['container_class'], $atts['center'], $atts['provider'], $content);
    foreach ($scripts as $script){
      $html .= sprintf('<script id="%s" type="text/javascript" src="%s"></script>', $script['name'], $script['url']);
    }
    foreach ($styles as $style){
      $html.=sprintf('<link rel="stylesheet" href="%s"/>', $style['url']);
    }
    return $html;
  }
}



add_action('init', function(){
  global $Inkube;
  $Inkube->getShortcodes()->register('map', new Map());
});


Inkube\Model\Field::register('map_provider', ['type'=>'select', 'options'=>[
  [
    'value'=>'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    'label' =>'Open Street Map Standard',
  ],
  [
    'value'=>'https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
    'label' =>'Open Street Map black and white',
  ],
  [
    'value'=>'https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}',
    'label'=>'Fastly',
  ],
  [
    'value'=>'https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}',
    'label'=>'Toner'
  ],

  [
    'value'=>'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    'label'=>'ArcGIS'
  ],
  ['label'=>'Google Maps',
  'value'=>'//maps.googleapis.com/maps/api/js?key=AIzaSyBgn5KwbRseKzJnYAQz5iRibnwWfHS2-uo&amp;language=fr&amp;v=3',
]
]
]);

Inkube\Model\Field::register('map_center', ['type'=>'map', 'label'=>'Map center']);
