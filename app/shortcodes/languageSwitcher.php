<?php 
use Inkube\Core\Interfaces\Shortcode;


class LanguageSwitcher extends Shortcode{
	public $isPrivate=true;

	public $tag = 'languageSwitcher'; 
	public function render($atts, $content){
		$lang = pll_current_language();
		$color = $atts['color']??"#FFF";
		if (!$color) $color = "#FFF";
		$html = [];
		foreach (['ar', 'fr', 'en'] as $l){
			if ($l == $lang) continue;
			$html[] = '<a class="language-switcher" href="/'.$l.'">
				<svg viewBox="0 0 200 185" style="height:1.1rem; width:auto;"><use fill="'.$color.'" xlink:href="/wp-content/themes/inkyfada/assets/langues.svg#'.$l.'"></use></svg>
				</a>';
			
		}
		$html = implode('<span style="font-size:1.3rem" class="mx-2">|</span>', $html);

      
		$html = sprintf('<span %s contenteditable="false">%s</span>', 
			$this->encode($this->tag, $atts, $content),
			$html 
		);
		return $html;
	}
	public function getHTML(array $atts, string $content):string 
	{
		return "";
	}
	public function getFields():array{
		return [];
	}
	public function getPresets():array{
		return [];
	}
}

add_action('init', function(){
	global $Inkube;
	$Inkube->getContainer()->get('Inkube\Shortcodes\ShortcodesModule')->register('languageSwitcher', new LanguageSwitcher());

});
