<?php 
add_shortcode( 'post-navigation', array('PostNavigation', 'shortcode') );

Class PostNavigation {

	public static function shortcode($atts){
		$atts = wp_parse_args($atts, array('prev'=>'', 'next'=>"", 'layout'=>'default'));
		$prev = get_post($atts['prev']);
		$next = get_post($atts['next']);
		ob_start();
		$shortcode = '[post-navigation prev="'.$atts['prev'].'" next="'.$atts['next'].'"]';

		switch ($atts['layout']){
			default:
			?>
			<nav data-shortcode-tag="post-navigation" data-shortcode="<?php echo base64_encode($shortcode) ?>" class="w-100">
				<div class="pos-relative">
				<div class="post-navigation post-navigation-prev">
					<a class="post-navigation-arrow fa fa-chevron-left"></a>
					<div class="post-navigation-block">
						<a href="<?php echo get_post_permalink($prev->ID); ?>" target="_blank">
							<?php echo get_the_post_thumbnail($prev->ID, "medium"); ?>
						</a>
						<h6 class="fs-16px">
							<a href="<?php echo get_post_permalink($prev->ID); ?>" target="_blank"><?php echo $prev->post_title ?>
							</a>
						</h6>

					</div>
				</div>
				<div class="post-navigation post-navigation-next">
					<a class="post-navigation-arrow fa fa-chevron-right"></a>
					<div class="post-navigation-block">
						<a href="<?php echo get_post_permalink($next->ID); ?>" target="_blank">
							<?php echo get_the_post_thumbnail($next->ID, "medium"); ?>
						</a>
						<h6>
							<a href="<?php echo get_post_permalink($next->ID); ?>" target="_blank">
								<?php echo $next->post_title ?>
							</a>
						</h6>


					</div>
				</div>
			</div>
			</nav>

			<?php 
			break;
		} 
		return  ob_get_clean();
	}
}