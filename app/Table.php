<?php 

class TableRenderer{
	public function __construct(){
	}
	public function render($array, $callback=null, $options=[]){
		if (!$array) return '';
		$head = array_keys($array[0]);
		$thead = '<thead class="sticky-top '.($options['head_class']??'bg-white').'">'.$this->renderRow($head, 'th').'</thead>';
		$tbody = '';

		foreach ($array as $row){
			$class = is_callable($callback)?$callback($row):'';
			$tbody .= $this->renderRow(array_values($row), 'td', $class);
		}
		$tbody = '<tbody>'.$tbody.'</tbody>';
		return  sprintf('<table class="table table-stripped">%s%s</table>', $thead, $tbody);
	}

	public function renderRow($row, $container="td", $class="") {
		$html = '';
		foreach ($row as $col){
			$html .= '<'.$container.'>'.$this->format($col).'</'.$container.'>';
		}
		return '<tr class="'.$class.'">'.$html.'</tr>';
	}

	public function format($r){
		if (is_string($r) || is_int($r)){
			return $r;
		}
		if (is_bool($r)){
			return (int)$r;
		}
		if (is_object($r)){
			return print_r($r, true);
		}
		if (is_array($r)){
			$html = '';
			foreach ($r as $k=>$v){
				$html .='<div class="d-flex"><span class="d-inline-block h6" style="">'.$k.'</span> <span>'.$this->format($v).'</span></div>';
			}
			return $html;
		}
		return json_encode($r);

	}
	public function renderPagination($url,  $max, $param="pagination"){
		$html = '<nav aria-label="Page navigation example">
 			<ul class="pagination">';

		for ($i=0; $i<$max; $i++){
				$current = $_GET[$param]??0;
				$class = ($current==$i)?'active':'';
				$html .=sprintf('<li class="page-item %1$s"><a class="page-link" href="%2$s&%3$s=%4$s">%5$s</a></li>', $class, $url, $param, $i, $i+1);
		}
		$html .=' </ul></nav>';
		return $html;
	}

}