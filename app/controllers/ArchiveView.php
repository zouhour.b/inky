<?php
use Inkube\Collections\Grid;
use Inkube\Model\Post;
class ArchiveCollection extends Grid{
	public function renderItems(array $dataset):string
	{
		$html = '';
		$tm = $this->renderer;
		foreach ($dataset as $index=>$data){
			if ($data instanceof Post){
				if (has_term('stouchi', 'genre', $data->id) || has_term('سطوشي', 'genre', $data->id)){
					$viewOptions = [
						'view'=>'@theme/post/card/archive-stouchi',
						'item_class'=>'col-md-8 mx-auto mb-5',

					];
				}
				else if (($data->get('type')==='webdoc' || get_post_meta($data->id, '_is_featured', true)) && $index>0){
					$viewOptions = [
						'view'=>'@theme/post/card/cover',
						'item_class'=>'w-100 mb-5 pb-5',
						'caption_class'=>'pos-absolute bottom-1 direct-0 offset-md-2 col-md-6',
						"title_class"=>"fw-700"
					];
				}
				else {
					$viewOptions = [
						'view'=>'@theme/post/card/archive-regular',
						'item_class'=>'col-md-8 mx-auto mb-5'
					];
				}
				$template = $tm->getTemplate($viewOptions['view']);
				$viewOptions['data'] = $data;
				$html .= sprintf('<div class="%1$s">%2$s</div>',$viewOptions['item_class'], $template->render($viewOptions));
			}
			else{
				$html .= '<div class="alert alert-danger">This collection supports only posts</div>';
			}

		}
		return $html;
	}

}

add_action('init', function(){

    global $Inkube;

    $templateManager = $Inkube->getRenderer();
    $Inkube->getContainer()->get('Inkube\Collections\CollectionsModule')->register('inkyfada-archive', new ArchiveCollection([
        'container_class'=>'w-100',
        'grid_class'=>'row mx-0'
    ], $templateManager));
});