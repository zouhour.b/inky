<?php 

use Inkube\Model\Field;

class PodcastFormat {
	public static function init(){
		add_filter('get_post_form', [self::class, 'form'], 4, 4);

	}


	public static function form($form, $type, $template=null, $id=null){
		if ($id && has_term( 'audio', 'post-type', $id )){
			$form->add(new Field('subtitleFile', [
				'type'=>'file',
				'label'=>'Subtitles',
				'library'=>['type'=>'text']

			]), 'podcast');
			$form->add(new Field('waveFile', [
				'type'=>'file',
				'label'=>'Wave',
				'library'=>['type'=>'application/json']
			]), 'podcast');
		}
		return $form;

	}
}

PodcastFormat::init();