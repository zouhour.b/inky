<?php 


class InkyfadaSetup{

	public function __construct(){
		$this->setupWebdocs();
		$this->setupBlog();
		$this->setupStouchi();
		$this->setupMaps();
		add_action('body_start',[$this, 'setupZeroImpunity']);
	}

	public function setupZeroImpunity(){
		ob_start();
		?>
		<div class="row bg-gray-dark fs-7 fw-200 py-4">
			<div class="col-md-3 offset-md-2">
				<img src="https://inkyfada.com/wp-content/uploads/2017/01/ZEROIMPUNITY_Logo_Texture_Action_stroke_Web.png" style="float:right;padding-bottom: 25px;">
			</div>
			<div class="col-md-5 outro-note-text">Cette enquête est tirée du projet international <a href="https://zeroimpunity.com/" target="_blank">ZERO IMPUNITY</a>, une opération transmédia qui documente et dénonce l’impunité qui protège les auteurs de violences sexuelles en conflit armé.  Le projet a été créé par Nicolas Blies, Stéphane Hueber-Blies et Marion Guth (a_BAHN) qui se définissent comme des « documentaristes activistes ».<br><br>Un consortium de médias internationaux publie les six grandes enquêtes exclusives de ZERO IMPUNITY, qui décryptent les mécaniques de l’impunité au sein d'institutions publiques, d'organisations internationales et même d'armées. ZERO IMPUNITY est aussi un travail d’investigation qui se prolonge par une véritable action citoyenne et qui a l’ambition de réimaginer la notion d’activisme en ligne en mobilisant le public grâce à des témoignages forts et des enquêtes accablantes.
			</div>
			<?php $footer = ob_get_clean();
			$posts = get_posts(['tax_query'=>[
				['field'=>'slug', 'taxonomy'=>'theme',  'terms'=>['zero-impunity-viol-violences-sexuelles-guerre']]
			], 'numberposts'=>-1]
		);
			foreach ($posts as $p){
				$view_options = [
					'view'=>'@theme/post/single/default',
					'article_class'=>'container-fluid px-0 zero-impunity-theme',
					'cover_mask'=>'linear-gradient(rgba(150, 150, 150,0.5), rgba(255,255,255,1))',
					'cover_class'=>'w-100 h-12',
					'cover_caption_class'=>'col-md-6 vcenter offset-md-3 color-gray-900',
					'subfooter_class'=>'d-none',
					'title_class'=>'fs-44px color-red mb-3',
					'cover_caption_footer_class'=>'d-none',
					'content_class'=>'layout-default fs-20px fw-300 w-100',
				];
				update_post_meta($p->ID, 'view_options', $view_options);
				update_post_meta($p->ID,  'wpcf-footer', $footer);
				update_post_meta($p->ID, 'wpcf-styles', '<style>.zero-impunity-theme h1, .zero-impunity-theme h2, .zero-impunity-theme .reference-number, .entry-content > header:first-letter{color: #dc582a;}
					.zero-impunity-theme .reference-number{font-weight:bold;font-size: 18px;}.zero-impunity-theme .reference-content{text-align:left !important; margin-right: 5px}</style>');
			}
		}

		public function setupMaps(){
			wp_update_post(['ID'=>10401, 'post_type'=>'post']);
		}

		public function setupBlog(){
			update_option('default_post_post_template', ['view'=>'@theme/post/single/default']);
			update_option('default_post_page_template', ['view'=>'@theme/page/single/default']);
			update_option('default_term_genre_template',['view'=>'@theme/term/single/default']);
			update_option('default_term_folder_template',['view'=>'@theme/term/single/theme']);
			update_option('default_term_webdoc_template',['view'=>'@theme/term/single/webdoc']);


			update_option('_taxonomies', [
				['slug'=>'genre'],
				['slug'=>'folder'],
				['slug'=>'theme'],
				['slug'=>'webdoc'],
				['slug'=>'post-type']
			]);
			$frontpage_id = get_option( 'page_on_front' );
			update_post_meta($frontpage_id, 'view_options', ['view'=>'@theme/page/single/home']);
			$archive_id = 19287;
			update_post_meta($archive_id, 'view_options', ['view'=>'@theme/page/single/archive']);

		}

		public function initMigration(){
			foreach (get_posts(['numberposts'=>-1]) as $p){
				update_post_meta($p->ID, 'migration-errors', []);
				update_post_meta($p->ID, 'migration-status', 'not-ready');

			}
		}

		public function setupStouchi(){
			add_action('wp_head', function(){
				$posts = get_posts(['genre'=>'stouchi', 'numberposts'=>-1]);
				foreach ($posts as $p){
					update_post_meta($p->ID, 'view_options', ['view'=>'@theme/post/single/stouchi']);
				}

			});
		}

		public function setupWebdocs(){
			$page = get_page_by_title( 'Webdocs');
			if (!$page){
				wp_insert_post( ['post_status'=>'publish', 'post_type'=>'page', 'post_title'=>'Webdocs'] );
			}
			$page = get_page_by_title( 'Webdocs');
			update_post_meta($page->ID, 'view_options', ['view'=>'@theme/page/single/webdocs']);
			add_action('init', function(){
				$voyage = wp_insert_term( 'Le voyage', 'webdoc', array( '' ) );
				if (is_wp_error($voyage)){
					$voyageid = $voyage->error_data['term_exists'];
				}
				else $voyageid = $voyage->term_id;
				update_term_meta($voyageid, 'view_options', ['view'=>'@theme/term/single/le-voyage']);
				$elkamour = wp_insert_term( 'El Kamour', 'webdoc', array( '' ) );
				if (is_wp_error($voyage)){
					$elkamourid = $elkamour->error_data['term_exists'];
				} else {
					$elkamourid = $elkamour->term_id;
				}
				update_term_meta($elkamourid, 'view_options', ['view'=>'@theme/term/single/el-kamour']);


				$kerkennah =get_posts(['tax_query'=>[['taxonomy'=>'webdoc', 'field'=>'slug', 'terms'=>['kerkennah']]]]);
				foreach ($kerkennah as $p){
					$v = get_post_meta($p->ID, 'wpcf-cover-video-source', true);
					update_post_meta($p->ID, 'featuredVideo',$v);
					update_post_meta($p->ID, 'view_options', ['view'=>'@theme/term/single/kerkennah/post']);
				}
			});
		}
	}




	if (isset($_GET['setup'])){
		new InkyfadaSetup();
	}
	add_action('wp_head', function(){
		if (isset($_GET['debug_meta'])){
			if (is_single()){
				$meta = get_post_meta(get_the_ID());
			}
			else if (is_tax()){
				$meta = get_term_meta(get_queried_object_id());
			}
			else if (is_author()){
				$meta = get_user_meta(get_queried_object_id() );
			}
			echo '<div class="h-3"></div>';
			foreach ($meta as $k=>$m){
				$v = get_post_meta(get_the_ID(), $k, true);
				if (!is_string($v)){
					var_dump($v);
					$v = json_encode($v);
				}

				echo sprintf('<div class="px-5 py-2"><h6>%s</h6><textarea style="width:1000px; height: 200px;">%s</textarea></div>', $k,$v);
			}
			die();
		}
	});