<?php


add_filter('load_more_btn_text', function(){
	return do_shortcode('[tr]display more content[/tr]');
});


add_filter('relatedPosts_default_view', function(){
	return '@theme/component/related-posts-classic';
});

add_filter('relatedTerms_default_view', function(){
	return '@theme/component/related-terms-classic';
});
add_filter('component_law-text_default_view', function(){
	return '@theme/component/law-text';
});


add_filter('share_defaults', function($d){
	$d['facebook_icon']='icon-facebook-2';
	$d['twitter_icon']='icon-twitter-2';
	$d['email_icon']='icon-mail-2';

	$d['icon_class']= 'list-inline-item rounded-circle border mx-1';
	return $d;
});

Inkube\Model\Field::register('wpcf-author-note', ['type'=>'textarea', 'label'=>"Note d'auteur"]);
Inkube\Model\Field::register('wpcf-footer', ['type'=>'code', 'label'=>"Footer", 'mode'=>'html', 'tab'=>'scripts']);

Inkube\Model\Field::register('podcast', ['type'=>'audio', 'label'=>"Podcast"]);
Inkube\Model\Field::register('waveFile', ['type'=>'file', 'library'=>['type'=>'application/json'], 'label'=>"Fichier Wave"]);
Inkube\Model\Field::register('waveFile', ['type'=>'file', 'library'=>['type'=>'application/json'], 'label'=>"Fichier Wave"]);
Inkube\Model\Field::register('subtitles', ['type'=>'file', 'library'=>['type'=>'text'], 'label'=>"Soustitres"]);

Inkube\Model\Field::register('audioChapters', [
	'type'=>'collection', 
	'label'=>'Chapitres',
	'model'=>[
		['name'=>'time', 'label'=>'time'],
		['name'=>'title', 'label'=>'title'],
		['name'=>'backgroundImage', 'label'=>'Background image', 'type'=>'image'],


	],
	'tab'=>'format',
	'display'=>'blocks'
]);