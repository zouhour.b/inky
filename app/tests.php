<?php 

Class AppTests{
	public function __construct(){
		$this->tests = [];
		add_shortcode('apptests', [$this, "renderForm"]);
		add_action('load_tests', function(){

			foreach (scandir(dirname(__DIR__).'/test') as $file){
				if (strpos($file, ".php")!==false){
					require_once dirname(__DIR__).'/test/'.$file;
				}
			}
		});
	}
	public function register($name, $label, $handler){
		$this->tests[$name]=['label'=>$label, 'handler'=>$handler];
	}

	public function renderForm(){
		ob_start();
		
		?>
		<div class="sticky-top pt-5 mb-5 bg-white">
			<form method="POST" class="form-inline bg-gray-300 p-2 mb-2">
				Run test 
				<select name="testname" class="form-control">
					<?php 
					foreach ($this->tests as $name=>$data){
						echo sprintf('<option value="%s">%s</option>', $name, $data['label']);
					}
					?>
				</select>
				<input type="submit" value="run" class="btn btn-primary form-control"/>
			</form>
		</div>
		<?php 
		echo $this->runTest();
		return ob_get_clean();
	}

	public function runTest(){
		$test = $_POST['testname']??'';
		if (!$test || !isset($this->tests[$test])){
			return;
		}
		ob_start();
		$handler = $this->tests[$test]['handler'];
		$handler();
		return ob_get_clean();
	}





}
global $InkubeTests;
$InkubeTests = new AppTests();
do_action('load_tests');